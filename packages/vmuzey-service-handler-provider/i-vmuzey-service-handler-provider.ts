export interface IVmuzeyServiceHandlerProvider {
  execute(request: any): Promise<any>;
}
