import { OpenAPIV3 } from 'openapi-types';
import { OpenapiRequestValidationSchema, OpenapiResponseSchema } from '../vmuzey-openapi-schema-validator';
import { IVmuzeyOpenapiSchemasContainer } from './i-vmuzey-openapi-schemas-container';

export class VmuzeyOpenapiSchemasContainer implements IVmuzeyOpenapiSchemasContainer {
  constructor(private requestSchema: OpenapiRequestValidationSchema, private responseSchema: OpenapiResponseSchema) {}

  private description: string = '';
  private tag: string = '';

  addDescription(description: string): void {
    this.description = description;
  }

  addTag(tag: string): void {
    this.tag = tag;
  }

  getRequestSchema(): OpenapiRequestValidationSchema {
    return this.requestSchema;
  }

  getResponseSchema(): OpenapiResponseSchema {
    return this.responseSchema;
  }

  getDescription(): string {
    return this.description;
  }

  getTags(): string[] {
    return [this.tag];
  }

  getControllerMetadata(): OpenAPIV3.OperationObject {
    return {
      ...this.getRequestSchema(),
      responses: this.getResponseSchema(),
      description: this.getDescription(),
      tags: this.getTags(),
    };
  }
}
