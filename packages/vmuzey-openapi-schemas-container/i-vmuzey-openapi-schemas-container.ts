import { OpenAPIV3 } from 'openapi-types';
import { OpenapiRequestValidationSchema, OpenapiResponseSchema } from '../vmuzey-openapi-schema-validator';

export interface IVmuzeyOpenapiSchemasContainer {
  getRequestSchema(): OpenapiRequestValidationSchema;
  getResponseSchema(): OpenapiResponseSchema;

  addDescription(description: string): void;
  addTag(tag: string): void;

  getDescription(): string;
  getTags(): string[];

  getControllerMetadata(): OpenAPIV3.OperationObject;
}
