export interface IVmuzeyMongodbDataModelConfig {
  collections: IVmuzeyMongodbDataModelCollection[];
}

export interface IVmuzeyMongodbDataModelCollection {
  dbName: string;
  collectionName: string;
  indexes?: IVmuzeyMongodbDataModelIndex[];
  fixtures?: any;
}

export interface IVmuzeyMongodbDataModelIndex {
  field: string;
  unique?: boolean;
}
