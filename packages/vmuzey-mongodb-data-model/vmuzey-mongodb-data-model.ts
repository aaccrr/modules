import { Collection, Db } from 'mongodb';
import { IVmuzeyMongodbProvider } from '../vmuzey-mongodb-provider';
import { IVmuzeyMongodbDataModel } from './i-vmuzey-mongodb-data-model';
import { IVmuzeyMongodbDataModelCollection, IVmuzeyMongodbDataModelConfig, IVmuzeyMongodbDataModelIndex } from './interfaces';

export class VmuzeyMongodbDataModel implements IVmuzeyMongodbDataModel {
  constructor(private MongodbConnector: IVmuzeyMongodbProvider) {}

  private models = {};

  async init(config: IVmuzeyMongodbDataModelConfig): Promise<void> {
    this.models = await this.createModels(config.collections);
  }

  getModel(collectionName: string): Collection {
    return this.models[collectionName];
  }

  protected createDbConnections(models: IVmuzeyMongodbDataModelCollection[]): any {
    return models
      .map((model) => model.dbName)
      .reduce((dbAcc, dbName) => {
        if (!dbAcc[dbName]) dbAcc[dbName] = this.MongodbConnector.connection.db(dbName);

        return dbAcc;
      }, {});
  }

  private async createModels(models: IVmuzeyMongodbDataModelCollection[]): Promise<any> {
    const dbConnections = this.createDbConnections(models);
    const collections = await Promise.all(
      models.map(async (model) => ({
        name: `${model.dbName}.${model.collectionName}`,
        instance: await this.createModel(dbConnections[model.dbName], model),
      }))
    );

    return collections.reduce((collectionsAcc, collection) => {
      collectionsAcc[collection.name] = collection.instance;
      return collectionsAcc;
    }, {});
  }

  private async createModel(db: Db, model: IVmuzeyMongodbDataModelCollection): Promise<Collection> {
    const isModelExisting = await this.checkModelExistence(db, model.collectionName);
    if (isModelExisting) return db.collection(model.collectionName);

    const collection = await db.createCollection(model.collectionName);

    if (Array.isArray(model.indexes)) {
      await this.createIndexes(collection, model.indexes);
    }

    if (Array.isArray(model.fixtures)) {
      await collection.deleteMany({});
      await collection.insertMany(model.fixtures);
    }

    return collection;
  }

  private async createIndexes(collection: Collection, indexes: IVmuzeyMongodbDataModelIndex[]): Promise<void> {
    for (const index of indexes) {
      const options = { unique: index.unique || false };
      await collection.createIndex({ [index.field]: 1 }, options);
    }
  }

  private async checkModelExistence(db: Db, collectionName: string): Promise<boolean> {
    const existingModels = await db.listCollections().toArray();
    return !!existingModels.find((model) => model.name === collectionName);
  }
}
