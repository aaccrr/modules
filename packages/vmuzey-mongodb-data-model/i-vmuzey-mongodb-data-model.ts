import { Collection } from 'mongodb';
import { IVmuzeyMongodbDataModelConfig } from './interfaces';

export interface IVmuzeyMongodbDataModel {
  init(config: IVmuzeyMongodbDataModelConfig): Promise<void>;
  getModel(collectionName: string): Collection;
}
