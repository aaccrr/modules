export interface IMailer {
  verify(): Promise<void>;
  send(payload: any): Promise<any>;
}
