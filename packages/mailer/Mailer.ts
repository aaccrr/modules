import { createTransport, Transporter } from 'nodemailer';
import { IMailer } from './IMailer';

interface IMailerCredentials {
  mainTransport: IMailerTransportConfig;
}

interface IMailerTransportConfig {
  host: string;
  auth: {
    user: string;
    pass: string;
  };
  port?: number;
}

export class Mailer implements IMailer {
  constructor(credentials: IMailerCredentials) {
    this.transport = this.createTransport(credentials.mainTransport);
  }

  private transport: Transporter;

  verify(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.transport.verify((err, success) => {
        if (err) reject(err);
        resolve();
      });
    });
  }

  async send(payload: any): Promise<any> {
    try {
      const sendMailResult = await this.transport.sendMail(payload);
      return sendMailResult;
    } catch (error) {
      console.log('sending error ', error);

      throw error;
    }
  }

  private createTransport(transportConfig: IMailerTransportConfig): Transporter {
    if (!transportConfig.port) {
      transportConfig.port = 465;
    }

    return createTransport({
      ...transportConfig,
      secure: false,
      tls: {
        ciphers: 'SSLv3',
        rejectUnauthorized: false,
      },
    });
  }
}
