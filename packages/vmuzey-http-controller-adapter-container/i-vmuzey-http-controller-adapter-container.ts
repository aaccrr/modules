import { RequestPayload } from '../globals';
import { IAccountingAppModuleFacade } from '../vmuzey-app-module-facade-provider';
import { IHttpControllerCallPayload } from '../vmuzey-http-controller-provider';

export interface IVmuzeyHttpControllerAdapterContainer<T> {
  runAdapter(ctx: RequestPayload): void;
  getBodyWithCurrentUser(AccountingModuleFacade: IAccountingAppModuleFacade): Promise<IHttpControllerCallPayload<T>>;
}
