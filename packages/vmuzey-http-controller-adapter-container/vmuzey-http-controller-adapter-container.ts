import { getRequestMeta } from '../common';
import { ICurrentUserInfoView, RequestPayload } from '../globals';
import { IAccountingAppModuleFacade } from '../vmuzey-app-module-facade-provider';
import { IVmuzeyCurrentUser, VmuzeyCurrentUserFactory } from '../vmuzey-current-user';
import { IHttpControllerCallPayload } from '../vmuzey-http-controller-provider';
import { IVmuzeyHttpControllerAdapterContainer } from './i-vmuzey-http-controller-adapter-container';

export type VmuzeyHttpAdapter<T> = (ctx: RequestPayload) => T;

export class VmuzeyHttpControllerAdapterContainer<T> implements IVmuzeyHttpControllerAdapterContainer<T> {
  constructor(adapter?: VmuzeyHttpAdapter<T>) {
    if (typeof adapter === 'function') {
      this.adapter = adapter;
    } else {
      this.adapter = this.defaultAdapter;
    }
  }

  private adapter: VmuzeyHttpAdapter<T>;
  private body: Partial<IHttpControllerCallPayload<T>>;

  private defaultAdapter = (ctx: RequestPayload): T => {
    return {} as T;
  };

  runAdapter(ctx: RequestPayload): void {
    this.body = {
      body: this.adapter(ctx),
      meta: getRequestMeta(ctx),
    };
  }

  async getBodyWithCurrentUser(AccountingModuleFacade: IAccountingAppModuleFacade): Promise<IHttpControllerCallPayload<T>> {
    const currentUser = await new VmuzeyCurrentUserFactory(AccountingModuleFacade).create(this.body.meta.user);

    return {
      ...this.body,
      currentUser,
    } as IHttpControllerCallPayload<T>;
  }
}
