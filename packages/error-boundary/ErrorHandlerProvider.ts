import { Responder } from '../common';
import { ILogger } from '../Logger';

export class ErrorHandlerProvider {
  private logger: ILogger;

  middleware($logger?: ILogger) {
    this.logger = $logger;

    return async (ctx: any, next) => {
      try {
        await next();
      } catch (e) {
        console.log('ERROR ', e);
        const method = `${ctx.method} ${ctx.url}`;

        if (e.type === 'BUSINESS_ERROR') {
          await this.logError(method, e);
          return Responder.reply(ctx, e.httpCode, e.format());
        }

        if (e.type === 'VALIDATION_ERROR') {
          await this.logError(method, e);
          return Responder.reply(ctx, e.httpCode, e.format());
        }

        await this.logError(method, e);

        const code = this.normalizeErrorCode(e.code);

        if (e.isJson) {
          const message = this.handleJsonError(e);
          return Responder.reply(ctx, code, message);
        }

        if (code === 500) {
          const message = this.handleInternalError();
          return Responder.reply(ctx, code, message);
        }

        Responder.reply(ctx, code, e.message);
      }
    };
  }

  normalizeErrorCode(code: number | string): number {
    if (!code) return 500;
    if (typeof code === 'string') return 500;
    if ((typeof code === 'number' && code < 100) || code > 526) return 500;
    return code;
  }

  handleInternalError() {
    return {
      message: 'INTERNAL_ERROR',
    };
  }

  handleJsonError(e) {
    try {
      const parsedMessage = JSON.parse(e.message);

      return parsedMessage;
    } catch (err) {
      return e.message;
    }
  }

  private async logError(url: string, e) {
    const formatted = typeof e.format === 'function' ? e.format() : e.message;
    if (this.logger) {
      await this.logger.error({ handlerType: 'http', method: url, stacktrace: e.stack || '', error: formatted });
    } else {
      console.log(JSON.stringify({ type: 'ERROR', timestamp: new Date(), requestType: 'http', url, stacktrace: e.stack || '', error: formatted }));
    }
  }
}
