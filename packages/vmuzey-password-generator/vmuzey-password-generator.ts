import * as cryptoRandomString from 'crypto-random-string';
import { IVmuzeyPasswordHelper, VmuzeyPasswordHelper } from '../vmuzey-password-helper';

export class VmuzeyPasswordGenerator {
  static generate(length: number = 6): IVmuzeyPasswordHelper {
    const password = cryptoRandomString({ length });

    return new VmuzeyPasswordHelper(password);
  }
}
