import { IMongoDBCredentials } from '../connectors';
import { IVmuzeyMongodbProvider } from './i-vmuzey-mongodb-provider';
import { VmuzeyMongodbProvider } from './vmuzey-mongodb-provider';
import { VmuzeyMongodbProviderStub } from './vmuzey-mongodb-provider-stub';

export class VmuzeyMongodbProviderFactory {
  static create(env: string, mongodbConfig: IMongoDBCredentials): IVmuzeyMongodbProvider {
    if (env === 'test' || env === 'openapi') {
      return new VmuzeyMongodbProviderStub(mongodbConfig);
    }

    return new VmuzeyMongodbProvider(mongodbConfig);
  }
}
