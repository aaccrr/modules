import { MongoClient } from 'mongodb';

export interface IVmuzeyMongodbProvider {
  connection: MongoClient;
  init(): Promise<void>;
}
