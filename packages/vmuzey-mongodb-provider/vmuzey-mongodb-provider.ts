import { MongoClient } from 'mongodb';
import { IMongoDBCredentials, IMongoDBOptions } from '../connectors';
import { IVmuzeyMongodbProvider } from './i-vmuzey-mongodb-provider';

const MONGODB_CONNECT_RETRY_COUNT = 5;

export class VmuzeyMongodbProvider implements IVmuzeyMongodbProvider {
  constructor(credentials: IMongoDBCredentials) {
    this.connectionUrl = this.createConnectionUrl(credentials);
  }

  public connection: MongoClient;
  private connectionUrl: string;
  private connectionOptions: IMongoDBOptions = { useNewUrlParser: true, useUnifiedTopology: true };
  private retriesCount: number = 0;

  async init(): Promise<void> {
    this.connection = await this.createConnection(this.connectionUrl, this.connectionOptions);
  }

  protected async createConnection(url: string, options: IMongoDBOptions): Promise<MongoClient> {
    if (this.retriesCount > MONGODB_CONNECT_RETRY_COUNT) throw new Error('DATABASE_NOT_CONNECTED');
    try {
      return MongoClient.connect(url, options);
    } catch (e) {
      await new Promise((resolve) => setTimeout(resolve, 5000));
      this.retriesCount++;

      return this.createConnection(url, options);
    }
  }

  private createConnectionUrl(credentials: IMongoDBCredentials): string {
    const { user, password, host } = credentials;
    return `mongodb://${user}:${password}@${host}?authSource=admin`;
  }
}
