export interface ISchemaValidatorFactory {
  create(config?: ISchemaValidatorConfig): ISchemaValidator;
}

export interface ISchemaValidator {
  validate(schema: any, payload: any): void;
}

export interface ISchemaValidatorConfig {
  allErrors?: boolean;
  coerceTypes?: boolean;
  useDefaults?: boolean;
}
