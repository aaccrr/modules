import * as Ajv from 'ajv';
import * as localize from 'ajv-i18n';
import { stripHtml } from 'string-strip-html';
import { Errors, ValidationError } from '../errors';
import { ISchemaValidator, ISchemaValidatorConfig } from './Interfaces';
import { CheckHostname } from '../common';
import { EMAIL_REGEXP, PHONE_REGEXP, DATE_REGEXP, TIME_REGEXP } from '../globals';

export class SchemaValidator implements ISchemaValidator {
  private static defaultConfig = {
    allErrors: true,
    coerceTypes: true,
    useDefaults: true,
  };
  private validator: Ajv.Ajv;

  constructor(config: ISchemaValidatorConfig = SchemaValidator.defaultConfig) {
    this.validator = new Ajv(config);

    this.validator.addFormat('hostname', CheckHostname);
    this.validator.addFormat('email', EMAIL_REGEXP);
    this.validator.addFormat('phone', PHONE_REGEXP);
    this.validator.addFormat('date', DATE_REGEXP);
    this.validator.addFormat('time', TIME_REGEXP);
    this.validator.addFormat('digits', '^\\d+$');
    this.validator.addKeyword('vm-sanitize-string', {
      modifying: true,
      compile(schema) {
        return (data, currentDataPath, parentDataObject, propertyName) => {
          if (typeof data === 'string') parentDataObject[propertyName] = stripHtml(data).result;

          return true;
        };
      },
    });
  }

  validate(schema: object, payload: any): void {
    const valid = this.validator.validate(schema, payload);
    localize.ru(this.validator.errors);
    if (!valid) {
      const errors = this.validator.errors.map((error) => {
        if (error.keyword === 'additionalProperties') {
          return {
            message: error.message,
            field: (error.params as any).additionalProperty,
          };
        }

        return {
          message: error.message,
          field: this.normalizeErrorField(error.dataPath),
        };
      });

      throw new ValidationError('ru', errors, 400, false);
    }
  }

  private normalizeErrorField(rawFieldName: string): string {
    const prefixes = ['.insertion', '.selection', '.information', '.pagination', '.sort'];
    const normalizedFieldName = prefixes.reduce((name, prefix) => name.replace(prefix, ''), rawFieldName).replace('.', '');
    return normalizedFieldName;
  }
}
