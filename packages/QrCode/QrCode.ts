import * as fs from 'fs';
import * as path from 'path';
import * as qr from 'qrcode';
import { IQrCode } from './IQrCode';

export class QrCode implements IQrCode {
  async save(itemIds: string[]): Promise<void> {
    for (const id of itemIds) {
      const path = this.getFilePath(id);
      await this.createDirectoryIfNotExists(this.getTargetDirectoryPath(path));
      const writeStream = fs.createWriteStream(path);
      await qr.toFileStream(writeStream, id, { margin: 0 });
    }
  }

  private getFilePath(id: string): string {
    const FILES_STORE = '/var/docker/files';
    const QR_CODES_DIRECTORY = '/qr';
    const EXT = '.png';
    const FILE_NAME = id;
    const FILE_PATH = FILES_STORE + QR_CODES_DIRECTORY + `/${FILE_NAME}` + EXT;
    return FILE_PATH;
  }

  private async createDirectoryIfNotExists(targetDirPath: string): Promise<void> {
    const isTargetDirectoryExists = fs.existsSync(targetDirPath);
    if (!isTargetDirectoryExists) {
      await fs.promises.mkdir(targetDirPath, { recursive: true });
    }
  }

  private getTargetDirectoryPath(targetFilePath: string): string {
    return path.dirname(targetFilePath);
  }
}
