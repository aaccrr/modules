import { RequestPayload } from '../globals';

export class VmuzeyHttpAuthMiddleware {
  middleware(): any {
    return async (ctx: RequestPayload, next) => {
      const auth = ctx.request.query._auth || ctx.headers['x-authentication'];

      ctx.state._auth = this.parse_auth(auth);

      if (typeof ctx.headers['x-authentication'] === 'string') {
        ctx.state._auth.v = 2;
      }

      if (ctx.request.query._auth) {
        delete ctx.request.query._auth;
      }

      await next();
    };
  }

  private parse_auth(auth: string): any {
    if (typeof auth === 'string') {
      return auth.split(',').reduce((acc, prop: any) => {
        let [key, value] = prop.split(':');
        if (key === 'm') value = value === 'all' ? value : value.split(';');
        acc[key] = value;
        return acc;
      }, {} as any);
    }

    return {};
  }
}
