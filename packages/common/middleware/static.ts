import * as koaStatic from 'koa-static';

export function StaticMiddleware(path: string) {
  return () => koaStatic(path);
}
