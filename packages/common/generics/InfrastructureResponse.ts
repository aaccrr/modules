export interface IInfrastructureResponse<T> {
  success: boolean;
  data?: T;
}
