import { IOperation } from '../../../interfaces';
import { RequestPayload } from '../../../globals';

export interface IPointer {
  handleHttp(ctx: RequestPayload): Promise<void>;
  handleRpc(req: any, cb: any): Promise<void>;
}

export interface IPointerOps {
  admin?: IOperation<any, any>;
  office?: IOperation<any, any>;
  client?: IOperation<any, any>;
  entrycontrol?: IOperation<any, any>;
  bank?: IOperation<any, any>;
  guest?: IOperation<any, any>;
}
