import { getUserGroup } from '../GetRequestMeta';
import { UserRole, RequestPayload } from '../../../globals';
import { IPointerOps, IPointer } from './Interfaces';
import { IOperation } from '../../../interfaces';
import { Errors } from '../../../errors';

export class Pointer implements IPointer {
  private ops: IPointerOps;

  constructor(ops: IPointerOps) {
    this.ops = ops;
  }

  private getOp(role: UserRole): IOperation<any, any> {
    const group = getUserGroup(role);
    const op = this.ops[group.toLowerCase()];

    if (!op) Errors.AccessError('OPERATION_REQUIRE_PERMISSION');

    return op;
  }

  async handleHttp(ctx: RequestPayload): Promise<void> {
    const UserRole = ctx.headers['x-auth-role'];
    const op = this.getOp(UserRole);

    await op.handleHttp(ctx);
  }

  async handleRpc(req: any, cb): Promise<void> {
    const op = this.getOp(req.meta.user.role);

    await op.handleRpc(req, cb);
  }
}
