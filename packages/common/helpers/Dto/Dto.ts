import { SchemaValidator } from '../../../schema-validator';

export class Dto {
  private validator = new SchemaValidator({ coerceTypes: false, allErrors: true });

  constructor(private data: any, private schema: any, private mapper?: any) {
    if (typeof this.mapper === 'function') this.map();
    this.validate();
  }

  private map() {
    this.data = this.mapper(this.data);
  }

  private validate() {
    this.validator.validate(this.schema, this.data);
  }

  public getData() {
    return this.data;
  }
}
