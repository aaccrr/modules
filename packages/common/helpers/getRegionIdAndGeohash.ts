import { regionCenterGeo, regionIdByGeo } from '../../data';
import { normalizeRegionId } from './normalizeRegionId';

const pairedRegionId = {
  '306,310': 310,
  '110,118': 118,
};

const pairedGeohash = {
  310: '306,310',
  118: '110,118',
};

export interface IGetRegionIdAndGeohashResponse {
  geo: string;
  regionId: number[];
}

export function getRegionIdAndGeohash(regionId: string, userGeohash: string): IGetRegionIdAndGeohashResponse {
  if (regionId) return processRegionId(regionId);
  if (userGeohash) return processGeohash(userGeohash);

  return defaultValue();
}

export function processRegionId(regionId: string): IGetRegionIdAndGeohashResponse {
  const normalizedRegionId = normalizeRegionId(regionId).join(',');
  if (pairedRegionId[normalizedRegionId]) regionId = pairedRegionId[normalizedRegionId];
  const isRegionIdValid = regionId && regionCenterGeo[regionId] !== undefined;
  if (isRegionIdValid) {
    return {
      regionId: normalizeRegionId(regionId),
      geo: regionCenterGeo[regionId],
    };
  }
}

export function processGeohash(geohash: string): IGetRegionIdAndGeohashResponse {
  let regionId: string = regionIdByGeo[geohash] || '310';
  if (pairedGeohash[regionId]) regionId = pairedGeohash[regionId];
  return {
    regionId: normalizeRegionId(regionId),
    geo: geohash,
  };
}

export function defaultValue() {
  return {
    regionId: normalizeRegionId('306,310'),
    geo: regionCenterGeo[310],
  };
}
