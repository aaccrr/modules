import { Errors } from '../../errors';

export function SelectOperation(method: string) {
  return async (ctx: any, next) => {
    const Operation = this.methods[method];

    if (!Operation) Errors.AccessError('Метод не существует.');

    const Command = await Operation.mapper(ctx);

    await Operation.validator(Command);
    const data = await Operation.handler.run(Command);

    ctx.status = 200;
    ctx.body = { success: true, data };
  };
}
