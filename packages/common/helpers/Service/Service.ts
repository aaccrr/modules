import { Dto } from '../Dto';
import { RequestPayload } from '../../../globals';
import { Responder } from '../Responder';
import { IService, IServiceOptions } from './Interfaces';

const DEFAULT_OPTIONS: IServiceOptions = {
  responderOptions: {
    format: 'JSON',
    withSuccess: true,
  },
};

export abstract class Service implements IService {
  async run(dto: any): Promise<any> {}

  async handleHttp(ctx: RequestPayload, schema: any, mapper, options?: IServiceOptions): Promise<void> {
    options = options ? { ...DEFAULT_OPTIONS, ...options } : DEFAULT_OPTIONS;
    const payload = new Dto(ctx, schema, mapper);
    const data = await this.run(payload.getData());

    Responder.reply(ctx, 200, data, options.responderOptions);
  }

  async handleRpc(ctx: any, schema): Promise<void> {
    try {
      const payload = new Dto(ctx.req, schema);
      const data = await this.run(payload.getData());
      ctx.cb(null, data);
    } catch (e) {
      ctx.cb({ code: e.code || 500, message: e.message });
    }
  }
}
