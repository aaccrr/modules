import { RequestPayload } from '../../../globals';
import { IResponderOptions } from '../Responder';

export interface IService {
  run(dto: any): Promise<any>;
  handleHttp(ctx: RequestPayload, schema: any, mapper?, options?: IServiceOptions): Promise<void>;
  handleRpc(ctx: any, schema): Promise<void>;
}

export interface IServiceOptions {
  returningFormat?: 'JSON' | 'XML';
  responderOptions?: IResponderOptions;
}
