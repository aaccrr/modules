import { IRequestMeta, UserGroup, IRequestUser, UserRole, RequestPayload } from '../../globals';

export function extractUserInfoFromHeader(header: string): IRequestUser {
  return header.split(',').reduce((acc, prop: any) => {
    let [key, value] = prop.split(':');
    if (key === 'm') value = value === 'all' ? value : value.split(';');
    acc[key] = value;
    return acc;
  }, {} as any);
}

export function getRequestMeta(ctx: RequestPayload): IRequestMeta {
  const requestMeta: IRequestMeta = {
    clientIp: ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'],
    requestId: ctx.headers['req-id'],
    user: ctx.state._auth,
  };

  return requestMeta;
}

export function getUserGroup(userRole: UserRole) {
  const ADMIN_GROUP = {
    group: UserGroup.ADMIN,
    roles: [UserRole.BOOKER, UserRole.MANAGER, UserRole.PRIMARY_MANAGER],
  };

  const OFFICE_GROUP = {
    group: UserGroup.OFFICE,
    roles: [UserRole.CONTENT_MANAGER, UserRole.MODERATOR, UserRole.OFFICE_ADMIN],
  };

  const ENTRY_CONTROL_GROUP = {
    group: UserGroup.ENTRY_CONTROL,
    roles: [UserRole.CONTROLLER, UserRole.GUIDE],
  };

  const CLIENT_GROUP = {
    group: UserGroup.CLIENT,
    roles: [UserRole.GUEST, UserRole.CLIENT],
  };

  const GUEST_GROUP = {
    group: UserGroup.GUEST,
    roles: [UserRole.GUEST],
  };

  const { group } = [ADMIN_GROUP, OFFICE_GROUP, ENTRY_CONTROL_GROUP, CLIENT_GROUP, GUEST_GROUP].find((g) => g.roles.includes(userRole));

  return group;
}
