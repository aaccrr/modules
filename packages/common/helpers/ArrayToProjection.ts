import { IProjection } from '../../interfaces';
import { Errors } from '../../errors';

const def: IProjection = { _id: 0 };

export const arrayToProjection = (array: string | string[] = [], isProjection?: boolean): IProjection => {
  if (typeof array === 'string') {
    try {
      array = JSON.parse(array);
    } catch (e) {
      const message = isProjection ? `"fields" query param should be processed with JSON.stringify` : 'Parse Error';
      Errors.BadRequestError(message);
    }
  }

  return (array as string[]).reduce((acc, item, idx, arr) => {
    acc[item] = 1;
    return acc;
  }, def);
};
