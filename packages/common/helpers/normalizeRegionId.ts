export function normalizeRegionId(regionId: string | number | number[] = ''): number[] {
  if (Array.isArray(regionId)) return regionId;
  if (typeof regionId === 'number') return [regionId];
  return regionId
    .split(',')
    .map(p => parseInt(p.trim()))
    .sort((a, b) => a - b);
}
