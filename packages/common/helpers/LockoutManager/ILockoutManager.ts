import { IRedisConnector } from '../../../connectors';

export interface ILockoutManager {
  updateLockoutList($store: IRedisConnector, ip: string): Promise<void>;
  isLocked($store: IRedisConnector, ip: string): Promise<void>;
}
