import { IRedisConnector } from '../../../connectors';
import { ILockoutManager } from './ILockoutManager';
import { Errors } from '../../../errors';

export class LockoutManager implements ILockoutManager {
  private MAX_COUNT = 5;
  private prefix = 'lockout';

  async updateLockoutList($store: IRedisConnector, ip: string): Promise<void> {
    const key = this.getKey(ip);
    console.log('UPDATE_LOCKOUT_LIST KEY ', key);
    const counter = await $store.db.get(key);
    console.log('RAW COUNTER ', counter);

    if (counter !== null) {
      const counterNum = parseInt(counter);

      if (counterNum > this.MAX_COUNT) {
        console.log('COUNTER GREATER THAN MAX ', counterNum);
        await $store.db.set(key, this.MAX_COUNT.toString(), ['EX', 1200]);
        console.log('COUNTER SET MAX VALUE');
      }

      if (counterNum < this.MAX_COUNT) {
        console.log('COUNTER LESS THAN NAX ', counterNum);
        const newCounterNum = counterNum + 1;

        if (newCounterNum === this.MAX_COUNT) {
          console.log('NEW COUNTER EQUALS MAX ', newCounterNum);
          await $store.db.set(key, this.MAX_COUNT.toString(), ['EX', 1200]);
          console.log('COUNTER SET MAX VALUE');
        } else {
          console.log('NEW COUNTER LESS THAN MAX ', newCounterNum);
          const ttl = await $store.db.ttl(key);
          console.log('COUNTER TTL ', ttl);
          await $store.db.set(key, newCounterNum.toString(), ['EX', ttl]);
          console.log('COUNTER SET NEW COUNTER VALUE');
        }
      }
    }

    if (counter === null) {
      console.log('COUNTER IS NOT A NUMBER ', counter);
      await $store.db.set(key, '1', ['EX', 3600]);
      console.log('COUNTER SET INITIAL VALUE ', '1');
    }
  }

  async isLocked($store: IRedisConnector, ip: string): Promise<void> {
    const key = this.getKey(ip);
    console.log('IS_LOCAKED KEY ', key);
    const counter = await $store.db.get(key);
    console.log('IS_LOCKED RAW COUNTER ', counter);
    const counterNum = parseInt(counter);
    if (typeof counterNum === 'number' && counterNum >= 5) Errors.TooManyRequests('TOO_MANY_REQUESTS');
  }

  private getKey(ip: string): string {
    return `${this.prefix}:${ip}`;
  }
}
