import * as url from "url";

export function CheckHostname(data) {
  const HostRegExp = new RegExp("^([\\w\\-]+?\\.?)+?\\.[\\w\\-]+?$");

  if (data.search(/^(http|https):\/\//) === -1) {
    data = "http://" + data;
  }

  const { hostname } = url.parse(data);

  if (
    !hostname ||
    hostname.length < 4 ||
    hostname.length > 255 ||
    !HostRegExp.test(hostname)
  ) {
    return false;
  }
  return true;
}
