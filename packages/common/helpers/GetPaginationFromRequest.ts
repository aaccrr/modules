import { IPagination } from '../../globals';
import { MIN_PAGINATION_OFFSET, DEFAUL_PAGINATION_LIMIT } from '../../globals';

export function getPaginationFromRequest(requestQuery: any, defaultPagination: Partial<IPagination> = {}): IPagination {
  if (typeof defaultPagination.limit !== 'number') {
    defaultPagination.limit = DEFAUL_PAGINATION_LIMIT;
  }

  if (typeof defaultPagination.offset !== 'number') {
    defaultPagination.offset = MIN_PAGINATION_OFFSET;
  }

  return {
    limit: parseInt(requestQuery.limit) || defaultPagination.limit,
    offset: parseInt(requestQuery.offset) || defaultPagination.offset,
  };
}
