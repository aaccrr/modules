import { RequestPayload } from '../../../globals';

export interface IResponder {
  reply(ctx: RequestPayload, status: number, body: any, options?: IResponderOptions): void;
}

export interface IResponderOptions {
  format?: 'JSON' | 'XML' | 'TEXT';
  withSuccess?: boolean;
}
