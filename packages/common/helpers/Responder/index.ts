import { Responder } from './Responder';
import { Converter } from '../../../converter';

const Module = new Responder(Converter);

export { Module as Responder };
export * from './Interfaces';
