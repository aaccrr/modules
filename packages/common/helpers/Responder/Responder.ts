import { IResponderOptions, IResponder } from './Interfaces';
import { RequestPayload } from '../../../globals';
import { IConverter } from '../../../converter';

const DEFAULT_REPLY_OPTIONS: IResponderOptions = {
  format: 'JSON',
  withSuccess: true,
};

export class Responder implements IResponder {
  constructor(private Converter: IConverter) {}

  reply(ctx: RequestPayload, status: number, body: any, options: IResponderOptions = DEFAULT_REPLY_OPTIONS): void {
    if (options.format === 'JSON') this.replyJSON(ctx, status, body, options);
    if (options.format === 'XML') this.replyXML(ctx, status, body);
    if (options.format === 'TEXT') this.replyTEXT(ctx, status, body);
  }

  private replyJSON(ctx: RequestPayload, status: number, body: any, options: IResponderOptions): void {
    ctx.status = status;
    ctx.body = this.getResponseBody(status, body, options);
  }

  private replyXML(ctx: RequestPayload, status: number, body: any): void {
    ctx.status = status;
    ctx.body = this.Converter.jsonToXml(body);
  }

  private replyTEXT(ctx: RequestPayload, status: number, body: any): void {
    ctx.status = status;
    ctx.body = body;
  }

  private getResponseBody(status: number, body: any, options: IResponderOptions) {
    return options.withSuccess ? { success: status >= 200 && status < 300, data: body } : body;
  }
}
