import { RequestPayload } from '../../globals';

export function HandleHttpRequest(operation: any): any {
  return async (ctx: RequestPayload, next): Promise<void> => {
    const data = await operation(ctx);

    ctx.status = 200;
    ctx.body = {
      success: true,
      data,
    };
  };
}
