export function HandleRpcRequest(operation: any): any {
  return async (args, callback) => {
    try {
      console.log('CALLED WITH ', args);
      const data = await operation(args);
      console.log('RETURNS ', data);
      callback(null, data);
    } catch (e) {
      callback({ code: e.code || 500, message: e.message });
    }
  };
}
