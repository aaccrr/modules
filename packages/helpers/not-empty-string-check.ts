export const notEmptyStringCheck = (str: string) => typeof str === 'string' && str.length > 0;
