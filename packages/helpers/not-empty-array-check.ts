export const notEmptyArrayCheck = (arr: any[]) => Array.isArray(arr) && arr.length > 0;
