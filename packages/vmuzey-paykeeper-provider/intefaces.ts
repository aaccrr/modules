export interface IVmuzeyPaykeeperConfig {
  username: string;
  password: string;
  host: string;
  secret: string;
  accessTokenValidityPeriodInSeconds: number;
}
