import { BusinessError } from '../errors';
import { IVmuzeyPaykeeperConfig } from './intefaces';
import { IVmuzeyRedisProvider } from '../vmuzey-redis-provider';

import { IVmuzeyPaykeeperFetcher, VmuzeyPaykeeperFetcher } from './vmuzey-paykeeper-fetcher';
import { IVmuzeyHttpFetcherProvider } from '../vmuzey-http-fetcher-provider';
import { IVmuzeyPaykeeperAuthorizer, VmuzeyPaykeeperAuthorizer } from './vmuzey-paykeeper-authorizer';
import { IVmuzeyPaykeeperCrypto } from './vmuzey-paykeeper-crypto/i-vmuzey-paykeeper-crypto';
import { VmuzeyPaykeeperCrypto } from './vmuzey-paykeeper-crypto';
import { IAcceptedRefundByPaymentSystemDTO, ICreatedInvoiceDTO, IRefundStatusDTO } from '../globals';
import { IVmuzeyPaykeeperProvider } from './i-vmuzey-paykeeper-provider';

export class VmuzeyPaykeeperProvider implements IVmuzeyPaykeeperProvider {
  constructor(private config: IVmuzeyPaykeeperConfig, redis: IVmuzeyRedisProvider, fetcher: IVmuzeyHttpFetcherProvider) {
    this.paykeeperFetcher = new VmuzeyPaykeeperFetcher(this.config, fetcher);
    this.paykeeperAuthorizer = new VmuzeyPaykeeperAuthorizer(this.config, redis, fetcher);
    this.crypto = new VmuzeyPaykeeperCrypto(this.config.secret);
  }

  public crypto: IVmuzeyPaykeeperCrypto;
  private paykeeperFetcher: IVmuzeyPaykeeperFetcher;
  private paykeeperAuthorizer: IVmuzeyPaykeeperAuthorizer;

  async sendInvoice(invoice: any): Promise<ICreatedInvoiceDTO> {
    try {
      const token = await this.paykeeperAuthorizer.getToken();
      const createdInvoice = await this.paykeeperFetcher.sendInvoice(invoice, token);
      return createdInvoice;
    } catch (e) {
      console.log('ERROR_MESSAGE ', e.message);
      if (e.message === 'Токен безопасности не верен.') {
        await this.paykeeperAuthorizer.forceReauthorize();
        return this.sendInvoice(invoice);
      }

      throw new BusinessError('ru', 'INTERNAL_ERROR');
    }
  }

  async sendRefund(refund: any): Promise<IAcceptedRefundByPaymentSystemDTO> {
    try {
      const token = await this.paykeeperAuthorizer.getToken();
      const acceptedRefund = await this.paykeeperFetcher.sendRefund(refund, token);
      return acceptedRefund;
    } catch (e) {
      if (e.message === 'Токен безопасности не верен.') {
        await this.paykeeperAuthorizer.forceReauthorize();
        return this.sendRefund(refund);
      }

      throw new BusinessError('ru', 'INTERNAL_ERROR');
    }
  }

  async checkRefundStatus(paymentId: string): Promise<IRefundStatusDTO> {
    return this.paykeeperFetcher.checkRefundStatus(paymentId);
  }
}
