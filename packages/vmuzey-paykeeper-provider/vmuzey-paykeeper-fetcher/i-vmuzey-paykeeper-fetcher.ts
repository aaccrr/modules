import { IAcceptedRefundByPaymentSystemDTO, ICreatedInvoiceDTO, IRefundStatusDTO } from '../../globals';

export interface IVmuzeyPaykeeperFetcher {
  sendInvoice(invoice: any, token: string): Promise<ICreatedInvoiceDTO>;
  sendRefund(refund: any, token: string): Promise<IAcceptedRefundByPaymentSystemDTO>;
  checkRefundStatus(paymentId: string): Promise<IRefundStatusDTO>;
}
