import { IAcceptedRefundByPaymentSystemDTO, ICreatedInvoiceDTO, IRefundStatusDTO } from '../../globals';
import { IVmuzeyHttpFetcherProvider } from '../../vmuzey-http-fetcher-provider';
import { IVmuzeyPaykeeperConfig } from '../intefaces';
import { IVmuzeyPaykeeperFetcher } from './i-vmuzey-paykeeper-fetcher';

export class VmuzeyPaykeeperFetcher implements IVmuzeyPaykeeperFetcher {
  constructor(private config: IVmuzeyPaykeeperConfig, private fetcher: IVmuzeyHttpFetcherProvider) {}

  async sendInvoice(invoice: any, token: string): Promise<ICreatedInvoiceDTO> {
    const url = `${this.config.host}/change/invoice/preview`;

    const sendInvoiceResponse = await this.fetcher.request<any>({
      method: 'post',
      url,
      auth: {
        type: 'basic',
        credentials: {
          username: this.config.username,
          password: this.config.password,
        },
      },
      type: 'URL_ENCODED',
      body: { ...invoice.bill(), token },
    });

    if (sendInvoiceResponse.data.result === 'fail') {
      throw new Error(sendInvoiceResponse.data.msg);
    }

    return {
      invoiceUrl: sendInvoiceResponse.data.invoice_url,
      invoiceId: sendInvoiceResponse.data.invoice_id,
    };
  }

  async sendRefund(refund: any, token: string): Promise<IAcceptedRefundByPaymentSystemDTO> {
    const url = `${this.config.host}/change/payment/reverse`;

    const paykeeperRefundResponse = await this.fetcher.request<any>({
      method: 'post',
      url,
      auth: {
        type: 'basic',
        credentials: {
          username: this.config.username,
          password: this.config.password,
        },
      },
      type: 'URL_ENCODED',
      body: { ...refund.getPaykeeper(), token },
    });

    if (paykeeperRefundResponse.data.result === 'fail') {
      throw new Error(paykeeperRefundResponse.data.msg);
    }

    return {
      isRefundAccepted: true,
    };
  }

  async checkRefundStatus(paymentId: string): Promise<IRefundStatusDTO> {
    const url = `${this.config.host}/info/refunds/bypaymentid/?id=${paymentId}`;

    // от пэйкипера в ответе приходит список возвратов, офорленных на платеж (если их нет, то придет undefined, а не пустой массив)
    const listPaymentRefunds = await this.fetcher.request<any>({
      method: 'get',
      url,
      auth: {
        type: 'basic',
        credentials: {
          username: this.config.username,
          password: this.config.password,
        },
      },
    });

    if (!Array.isArray(listPaymentRefunds.data)) return { isProcessed: false };

    const refund = listPaymentRefunds.data[0];
    return { isProcessed: refund.status === 'done' };
  }
}
