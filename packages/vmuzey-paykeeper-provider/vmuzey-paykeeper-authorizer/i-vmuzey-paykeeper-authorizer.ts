export interface IVmuzeyPaykeeperAuthorizer {
  getToken(): Promise<string>;
  authorize(): Promise<void>;
  forceReauthorize(): Promise<void>;
}
