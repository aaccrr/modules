import { BusinessError } from '../../errors';
import { IVmuzeyRedisProvider } from '../../vmuzey-redis-provider';
import { IVmuzeyPaykeeperAuthorizer } from './i-vmuzey-paykeeper-authorizer';
import { IVmuzeyPaykeeperConfig } from '../intefaces';
import { IVmuzeyHttpFetcherProvider } from '../../vmuzey-http-fetcher-provider';

export class VmuzeyPaykeeperAuthorizer implements IVmuzeyPaykeeperAuthorizer {
  constructor(private config: IVmuzeyPaykeeperConfig, private store: IVmuzeyRedisProvider, private fetcher: IVmuzeyHttpFetcherProvider) {}

  private token: string = '';

  async getToken(): Promise<string> {
    if (!this.token) {
      await this.authorize();
    }

    return this.token;
  }

  async authorize(): Promise<void> {
    const hasCachedToken = await this.hasCachedToken();
    if (hasCachedToken) return;

    const hasNewToken = await this.setNewToken();
    if (!hasNewToken) {
      throw new BusinessError('ru', 'UNABLE_TO_AUTHORIZE_IN_PAYMENT_SYSTEM');
    }
  }

  async forceReauthorize(): Promise<void> {
    const hasNewToken = await this.setNewToken();
    if (!hasNewToken) {
      throw new BusinessError('ru', 'UNABLE_TO_AUTHORIZE_IN_PAYMENT_SYSTEM');
    }
  }

  private async hasCachedToken(): Promise<boolean> {
    const token = await this.store.methods.get('paykeeper');
    if (!token) return false;

    this.token = token;
    return true;
  }

  private async setNewToken(): Promise<boolean> {
    const url = `${this.config.host}/info/settings/token`;

    const newTokenResponse = await this.fetcher.request<any>({
      method: 'get',
      url,
      auth: {
        type: 'basic',
        credentials: {
          username: this.config.username,
          password: this.config.password,
        },
      },
    });

    if (newTokenResponse.data.token) {
      await this.cacheToken(newTokenResponse.data.token);
      this.token = newTokenResponse.data.token;
      return true;
    }

    return false;
  }

  private async cacheToken(token: string): Promise<void> {
    const PAYKEEPER_TOKEN_ACTIVE_IN_SECONDS = 82800;
    await this.store.methods.set('paykeeper', token, PAYKEEPER_TOKEN_ACTIVE_IN_SECONDS);
  }
}
