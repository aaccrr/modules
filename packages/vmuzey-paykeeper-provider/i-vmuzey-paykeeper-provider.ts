import { IAcceptedRefundByPaymentSystemDTO, ICreatedInvoiceDTO, IRefundStatusDTO } from '../globals';
import { IVmuzeyPaykeeperCrypto } from './vmuzey-paykeeper-crypto';

export interface IVmuzeyPaykeeperProvider {
  crypto: IVmuzeyPaykeeperCrypto;

  sendInvoice(invoice: any): Promise<ICreatedInvoiceDTO>;
  sendRefund(refund: any): Promise<IAcceptedRefundByPaymentSystemDTO>;
  checkRefundStatus(paymentId: string): Promise<IRefundStatusDTO>;
}
