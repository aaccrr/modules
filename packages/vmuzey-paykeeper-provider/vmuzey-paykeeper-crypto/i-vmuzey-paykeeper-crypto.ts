import { IBaseOrderModel } from '../../globals';

export interface IVmuzeyPaykeeperCrypto {
  signPaymentResponse(paymentId: string): string;
  validatePaymentSign(paymentSign: string, paymentPayload): boolean;
  signPurchaseNotificationForPaymentSystem(purchaseNotification: IBaseOrderModel): string;
}
