import * as crypto from 'crypto';
import { IBaseOrderModel } from '../../globals';
import { IVmuzeyPaykeeperCrypto } from './i-vmuzey-paykeeper-crypto';

export class VmuzeyPaykeeperCrypto implements IVmuzeyPaykeeperCrypto {
  constructor(private paykeeperSecret: string) {}

  signPaymentResponse(paymentId: string): string {
    const signPayload = `${paymentId}${this.paykeeperSecret}`;
    return this.hashMD5(signPayload);
  }

  validatePaymentSign(paymentSign: string, paymentPayload): boolean {
    const { paymentId, amount, customer, orderId } = paymentPayload;
    const signPayload = `${paymentId}${amount}${customer}${orderId}${this.paykeeperSecret}`;

    const sign = this.hashMD5(signPayload);
    console.log('SIGN ', sign);

    return sign === paymentSign;
  }

  signPurchaseNotificationForPaymentSystem(purchaseNotification: IBaseOrderModel): string {
    const { amount, customer, id } = purchaseNotification;
    const clientId = `${customer.lastName} ${customer.firstName}`;

    const signPayload = `${amount}${clientId}${id}${''}${customer.email}${customer.phone}${this.paykeeperSecret}`;
    return this.hashMD5(signPayload);
  }

  private hashMD5(hashPayload: string): string {
    return crypto.createHash('md5').update(hashPayload).digest('hex');
  }
}
