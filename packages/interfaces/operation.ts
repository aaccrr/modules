import { RequestPayload } from '../globals';
import { IHandler } from './handler';

export interface IOperation<T, U> {
  returningFormat: 'JSON' | 'XML';
  handler: IHandler<T, U>;
  mapper(Request: RequestPayload): Promise<T>;
  validator(Request: T): Promise<void>;
  run(Request: any): Promise<U>;
  authorizer: (ctx: any) => Promise<any>;
  handleHttp(Request: any): Promise<void>;
  handleRpc(Request: T, callback): Promise<void>;
}

export interface IOperationMapper<T> {
  (ctx: RequestPayload, next?): Promise<T>;
}
export interface IOperationValidator<T> {
  (request: T): Promise<void>;
}

export interface IOperationAccess {
  auth: boolean;
  roles: string[];
}
