export interface IContract<T> {
  success: boolean;
  data: T;
}
