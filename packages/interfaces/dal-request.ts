import { IProjection } from './projection';
import { IPagination } from '../globals';

export interface IDalRequest {
  selection?: any;
  projection?: IProjection;
  insertion?: any;
  pagination?: IPagination;
  information?: any;
}
