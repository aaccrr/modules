import { RouteMiddleware } from '../http-router';
import { IVmuzeyHttpMiddleware } from '../vmuzey-http-server-provider';
import { VmuzeyHandlerTransportEnum } from '../vmuzey-logger-provider';
import { IVmuzeyObservabilityProvider, ObservabilityContextSourceTypeEnum, VmuzeyObservabilityContext } from '../vmuzey-observability';

export class VmuzeyHttpTraceMiddleware implements IVmuzeyHttpMiddleware {
  constructor(private ObservabilityProvider: IVmuzeyObservabilityProvider) {}

  middleware(): RouteMiddleware {
    return async (ctx, next) => {
      const span = this.ObservabilityProvider.tracer.startRootSpan(
        new VmuzeyObservabilityContext({
          sourceType: ObservabilityContextSourceTypeEnum.HTTP_REQUEST,
          sourceName: `${ctx.method} ${ctx.url.split('?')[0]}`,
          handlerTransportType: VmuzeyHandlerTransportEnum.HTTP,
        })
      );

      ctx.state.trace = span.extractContext();

      try {
        await next();

        span.finish();
      } catch (e) {
        span.finish();
        throw e;
      }
    };
  }
}
