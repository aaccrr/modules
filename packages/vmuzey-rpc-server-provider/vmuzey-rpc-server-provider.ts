import { Server } from 'jayson';
import { IVmuzeyInternalHandlerProvider } from '../vmuzey-internal-handler-provider';
import { VmuzeyHandlerTransportEnum } from '../vmuzey-logger-provider';
import { IVmuzeyObservabilityProvider, ObservabilityContextSourceTypeEnum, VmuzeyObservabilityContext } from '../vmuzey-observability';
import { ITransportMessage } from '../vmuzey-transport-layer';
import { IVmuzeyRpcServerProviderConfig } from './interfaces';

export abstract class VmuzeyRpcServerProvider {
  constructor(private config: IVmuzeyRpcServerProviderConfig, private ObservabilityProvider: IVmuzeyObservabilityProvider) {}

  private methods: any = {};
  private rpcServer: Server;

  abstract initControllers(): void;
  abstract initRoutes(): void;

  async init(): Promise<void> {
    this.initControllers();
    this.initRoutes();

    this.proxifyMethods();
    this.rpcServer = new Server(this.methods);
    this.rpcServer.http().listen(this.config.port || 50051);
  }

  addMethod(methodName: string, handler: IVmuzeyInternalHandlerProvider): void {
    this.methods[methodName] = handler.getRpcHandler();
  }

  private proxifyMethods(): void {
    this.methods = new Proxy(this.methods, {
      get: (obj, key: string) => async (message: ITransportMessage, cb) => {
        const payload = message.payload || (message as any);

        const tracingSpan = this.ObservabilityProvider.tracer.startChildSpan(
          new VmuzeyObservabilityContext({
            sourceType: ObservabilityContextSourceTypeEnum.RPC_REQUEST,
            sourceName: key,
            payload: payload.body,
          }),
          payload.trace
        );

        payload.trace = tracingSpan.extractContext();

        try {
          const response = await obj[key](payload);

          tracingSpan.withResponse(response);

          cb(null, response);
        } catch (e) {
          tracingSpan.withError(e);

          if (e.type === 'BUSINESS_ERROR' || e.type === 'VALIDATION_ERROR') {
            cb({ code: e.httpCode, message: JSON.stringify(e.formatWithLocale()) });
          } else {
            cb({ code: e.code || 500, message: e.message });
          }
        }
      },
    });
  }
}
