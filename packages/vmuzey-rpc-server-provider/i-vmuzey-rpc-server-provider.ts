import { IVmuzeyInternalHandlerProvider } from '../vmuzey-internal-handler-provider';

export interface IVmuzeyRpcServerProvider {
  init(): Promise<void>;
  addMethod(methodName: string, handler: IVmuzeyInternalHandlerProvider): void;
}
