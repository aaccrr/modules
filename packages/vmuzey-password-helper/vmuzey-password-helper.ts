import { hash, compare } from 'bcryptjs';
import { IVmuzeyPasswordHelper } from './i-vmuzey-password-helper';

const BCRYPT_SALT_ROUNDS = 10;

export class VmuzeyPasswordHelper implements IVmuzeyPasswordHelper {
  constructor(private password: string) {}

  async getHash(): Promise<string> {
    return hash(this.password, BCRYPT_SALT_ROUNDS);
  }

  async compare(compareWith: string): Promise<boolean> {
    return compare(this.password, compareWith);
  }

  getRawPassword(): string {
    return this.password;
  }
}
