export interface IVmuzeyPasswordHelper {
  getHash(): Promise<string>;
  compare(compareWith: string): Promise<boolean>;
  getRawPassword(): string;
}
