export interface IVmuzeyOrangedataConfig {
  apiUrl: string;
  cert: Buffer;
  key: Buffer;
  passphrase: string;
  ca: Buffer;
  privateKey: Buffer;
}
