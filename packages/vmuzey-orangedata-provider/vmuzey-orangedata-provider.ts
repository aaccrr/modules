import * as orange from 'node-orangedata';
import { BusinessError } from '../errors';
import { IVmuzeyOrangedataProvider } from './i-vmuzey-orangedata-provider';
import { IVmuzeyOrangedataConfig } from './interfaces';

export class VmuzeyOrangedataProvider implements IVmuzeyOrangedataProvider {
  constructor(private config: IVmuzeyOrangedataConfig) {
    this.orangeAgent = new orange.OrangeData(this.config);
  }

  private orangeAgent: orange.OrangeData;

  async sendReceipt(receipt: any): Promise<any> {
    try {
      const printedReceipt = await this.printReceipt(this.prepareReceipt(receipt));
      return printedReceipt;
      // invoice.setOrderReceiptId(printedReceipt.id);
    } catch (e) {
      console.log('SEND_RECEIPT_ERROR ', e);

      throw new BusinessError('ru', 'INTERNAL_ERROR');
    }
  }

  private async checkReceiptStatus(orderId: string) {
    const status = await this.orangeAgent.getOrderStatus('7725713770', orderId);
    if (status) return status;

    return new Promise((resolve, reject) => {
      setTimeout(() => this.checkReceiptStatus(orderId).then(resolve, reject), 300);
    });
  }

  private prepareReceipt(receiptPayload: any): any {
    const receipt = new orange.Order(receiptPayload);

    for (const position of receiptPayload.positions) {
      receipt.addPosition(position);
    }

    receipt.addPayment({ type: 1, amount: receiptPayload.payment });
    receipt.addAgent({
      agentType: 127,
      paymentTransferOperatorPhoneNumbers: ['+79998887766'],
      paymentAgentOperation: 'Операция агента',
      paymentAgentPhoneNumbers: ['+79998887766'],
      paymentOperatorPhoneNumbers: ['+79998887766'],
      paymentOperatorName: 'Наименование оператора перевода',
      paymentOperatorAddress: 'Адрес оператора перевода',
      paymentOperatorINN: '3123011520',
      supplierPhoneNumbers: ['+79998887766'],
    });

    return receipt;
  }

  private async printReceipt(receipt: any): Promise<any> {
    await this.orangeAgent.sendOrder(receipt);
    return this.checkReceiptStatus(receipt.id);
  }
}
