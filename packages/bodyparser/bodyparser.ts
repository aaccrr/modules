import * as bodyParser from "koa-body";

export function Bodyparser() {
  return bodyParser({ multipart: true });
}
