import { IVmuzeyBrokerConsumerProvider } from '../vmuzey-broker-consumer-provider';
import { IVmuzeyDependencyContainer } from '../vmuzey-dependency-container';
import { IVmuzeyHttpServerProvider } from '../vmuzey-http-server-provider';
import { IVmuzeyRpcServerProvider } from '../vmuzey-rpc-server-provider';

export interface IVmuzeyAppModuleConfig {
  name: string;
  dependencyContainer: IVmuzeyDependencyContainer;
  httpServer?: IVmuzeyHttpServerProvider;
  rpcServer?: IVmuzeyRpcServerProvider;
  messageBrokerServer?: IVmuzeyBrokerConsumerProvider;
}
