import { IVmuzeyDependencyContainer } from '../vmuzey-dependency-container';
import { IVmuzeyAppModuleConfig } from './interfaces';
import { IVmuzeyHttpServerProvider } from '../vmuzey-http-server-provider';
import { IVmuzeyRpcServerProvider } from '../vmuzey-rpc-server-provider';
import { IVmuzeyBrokerConsumerProvider } from '../vmuzey-broker-consumer-provider';
import { EventEmitter } from 'events';

export class VmuzeyAppModule extends EventEmitter {
  constructor(private appModuleConfig: IVmuzeyAppModuleConfig) {
    super();

    this.name = appModuleConfig.name;

    this.dependencyContainer = this.appModuleConfig.dependencyContainer;
    this.httpServer = this.appModuleConfig.httpServer;
    this.rpcServer = this.appModuleConfig.rpcServer;
    this.messageBrokerServer = this.appModuleConfig.messageBrokerServer;
  }

  private name: string = '';

  private dependencyContainer: IVmuzeyDependencyContainer;
  private httpServer: IVmuzeyHttpServerProvider;
  private rpcServer: IVmuzeyRpcServerProvider;
  private messageBrokerServer: IVmuzeyBrokerConsumerProvider;

  private get isSingletoneMode(): boolean {
    return process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'openapi';
  }

  public async init(): Promise<void> {
    if (this.isSingletoneMode) {
      await this.initAppInSingletoneMode();
    } else {
      await this.initAppInServiceMode();
      this.handleExeptions();
      this.emitInitializedEvent();

      if (typeof this.appModuleConfig.httpServer !== 'undefined') {
        this.httpServer.enableAppInitStatus();
      }
    }

    console.log(`${this.name} READY`);
  }

  private async initAppInServiceMode(): Promise<void> {
    await this.dependencyContainer.init();

    if (typeof this.appModuleConfig.httpServer !== 'undefined') {
      this.httpServer.setupMiddlewares();
      this.httpServer.setupRoutes();
      this.httpServer.startServer();
    }

    if (typeof this.appModuleConfig.rpcServer !== 'undefined') {
      await this.rpcServer.init();
    }

    if (typeof this.appModuleConfig.messageBrokerServer !== 'undefined') {
      await this.messageBrokerServer.init();
      await this.messageBrokerServer.start();
    }
  }

  private async initAppInSingletoneMode(): Promise<void> {
    try {
      console.log('starting in sigletone mode');
      await this.dependencyContainer.init();
      console.log('dependency container with mocks inited');
      this.httpServer.setupMiddlewares();
      this.httpServer.setupRoutes();
      console.log('start generating spec');
      await this.httpServer.saveOpenapiSpecification(process.env.OPENAPI_SPECIFICATION_PATH);
      console.log(`openapi spec saved to ${process.cwd()}${process.env.OPENAPI_SPECIFICATION_PATH}`);
      process.exit(0);
    } catch (e) {
      console.log('error ', e);
      process.exit(0);
    }
  }

  private handleExeptions(): void {
    ['unhandledRejection', 'uncaughtException'].map((type: any) => {
      process.on(type, (e) => {
        console.log('UNHANDLED_REJECTION_1 ', e);
        process.exit(1);
      });
    });

    ['SIGTERM', 'SIGINT'].map((type: any) => {
      process.once(type, (e) => {
        console.log('UNHANDLED_REJECTION_2 ', e);
        process.kill(process.pid, type);
      });
    });
  }

  private emitInitializedEvent(): void {
    this.emit('INITIALIZED', this.dependencyContainer);
  }
}
