import * as koaStatic from 'koa-static';

export class VmuzeyHttpStaticMiddleware {
  constructor(private path: string) {}

  middleware(): any {
    return koaStatic(this.path);
  }
}
