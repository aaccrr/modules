export interface IVmuzeyLogView {
  level: VmuzeyLogLevelEnum;
  timestamp: Date;
  context: IVmuzeyLoggerProviderLogContext;
}

export enum VmuzeyLogLevelEnum {
  INFO = 'INFO',
  WARNING = 'WARNING',
  ERROR = 'ERROR',
  ALERT = 'ALERT',
}

export interface IVmuzeyLoggerProviderLogContext {
  handlerTransportType: VmuzeyHandlerTransportEnum;

  requestId?: string;
  topic?: string;
  rpcMethod?: string;
  httpUrl?: string;
  httpMethod?: string;

  payload?: any;
  error?: any;
  response?: any;
}

export enum VmuzeyHandlerTransportEnum {
  HTTP = 'HTTP',
  RPC = 'RPC',
  BROKER = 'BROKER',
}

export interface IVmuzeyLoggerErrorContext {
  handlerTransportType: VmuzeyHandlerTransportEnum;
  payload: any;
  timestamp?: Date;
  requestId?: string;
  topic?: string;
  rpcMethod?: string;
  httpUrl?: string;
  httpMethod?: string;
  stacktrace?: string;
}

export interface IVmuzeyLoggerTransport {
  sendInfoLog(context: IVmuzeyLogView): Promise<void>;
  sendError(error: Error, context: IVmuzeyLoggerErrorContext): Promise<void>;
}
