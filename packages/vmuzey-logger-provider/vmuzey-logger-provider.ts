import { IVmuzeyLoggerProvider } from './i-vmuzey-logger-provider';
import { IVmuzeyLoggerProviderLogContext, IVmuzeyLoggerTransport, VmuzeyLogLevelEnum, IVmuzeyLoggerErrorContext } from './interfaces';

export class VmuzeyLoggerProvider implements IVmuzeyLoggerProvider {
  private transport: IVmuzeyLoggerTransport;

  setTransport(transport: IVmuzeyLoggerTransport): void {
    this.transport = transport;
  }

  async info(logContext: IVmuzeyLoggerProviderLogContext): Promise<void> {
    await this.transport.sendInfoLog({ level: VmuzeyLogLevelEnum.INFO, timestamp: this.createTimestamp(), context: logContext });
  }

  async warning(logContext: IVmuzeyLoggerProviderLogContext): Promise<void> {
    await this.transport.sendInfoLog({ level: VmuzeyLogLevelEnum.WARNING, timestamp: this.createTimestamp(), context: logContext });
  }

  async error(error: Error, context: IVmuzeyLoggerErrorContext): Promise<void> {
    await this.transport.sendError(error, context);
  }

  async alert(logContext: IVmuzeyLoggerProviderLogContext): Promise<void> {
    await this.transport.sendInfoLog({ level: VmuzeyLogLevelEnum.ALERT, timestamp: this.createTimestamp(), context: logContext });
  }

  private createTimestamp(): Date {
    return new Date();
  }
}
