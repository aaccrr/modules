import { IVmuzeyLoggerProviderLogContext, IVmuzeyLoggerTransport, IVmuzeyLoggerErrorContext } from './interfaces';

export interface IVmuzeyLoggerProvider {
  setTransport(transport: IVmuzeyLoggerTransport): void;

  info(logContext: IVmuzeyLoggerProviderLogContext): Promise<void>;
  warning(logContext: IVmuzeyLoggerProviderLogContext): Promise<void>;
  error(error: Error, context: IVmuzeyLoggerErrorContext): Promise<void>;
  alert(logContext: IVmuzeyLoggerProviderLogContext): Promise<void>;
}
