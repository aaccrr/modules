import { VmuzeyLoggerConsoleTransport } from '../vmuzey-logger-console-transport';
import { VmuzeyLoggerSentryTransport } from '../vmuzey-logger-sentry-transport';
import { IVmuzeyLoggerProvider } from './i-vmuzey-logger-provider';
import { VmuzeyLoggerProvider } from './vmuzey-logger-provider';

export class VmuzeyLoggerProviderFactory {
  static create(env: string): IVmuzeyLoggerProvider {
    const logger = new VmuzeyLoggerProvider();

    if (env === 'dev' || env === 'production') {
      const loggerTransport = new VmuzeyLoggerSentryTransport({
        host: process.env.SENTRY_API_KEY as string,
        environment: env,
      });

      logger.setTransport(loggerTransport);

      return logger;
    }

    logger.setTransport(new VmuzeyLoggerConsoleTransport());
    return logger;
  }
}
