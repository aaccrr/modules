import { OpenAPIV3 } from 'openapi-types';

export const getRequestBodyOpenapiSchema = (schema: OpenAPIV3.SchemaObject): OpenAPIV3.RequestBodyObject => {
  return {
    content: {
      'application/json': {
        schema,
      },
    },
  };
};
