import { OpenAPIV3 } from 'openapi-types';

export const getSuccessfulResponseOpenapiSchema = (successfulResponseSchema: OpenAPIV3.SchemaObject): OpenAPIV3.ResponseObject => {
  return {
    description: 'ответ в случае успешной обработки запроса',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            success: {
              type: 'boolean',
              example: true,
            },
            data: successfulResponseSchema,
          },
        },
      },
    },
  };
};
