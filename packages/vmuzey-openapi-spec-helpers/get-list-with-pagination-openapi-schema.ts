import { OpenAPIV3 } from 'openapi-types';

export const getListWithPaginationOpenapiSchema = (listItemSchema: OpenAPIV3.SchemaObject): OpenAPIV3.SchemaObject => {
  return {
    type: 'object',
    properties: {
      total: {
        type: 'number',
      },
      offset: {
        type: 'number',
      },
      items: {
        type: 'array',
        items: listItemSchema,
      },
    },
    additionalProperties: false,
  };
};
