import { OpenAPIV3 } from 'openapi-types';
import { IBusinessErrorView } from '../errors/Interfaces';

export const getBusinessErrorOpenapiSchema = (errorView: IBusinessErrorView): OpenAPIV3.SchemaObject => {
  return {
    type: 'object',
    properties: {
      type: {
        type: 'string',
        example: errorView.type,
      },
      message: {
        type: 'string',
        example: errorView.message,
      },
      code: {
        type: 'string',
        example: errorView.code,
      },
    },
    additionalProperties: false,
  };
};
