import { OpenAPIV3 } from 'openapi-types';
import { getListBusinessErrors } from './get-list-business-errors';

export const getBusinessErrorResponseOpenapiSchema = (httpErrorCode: number, ...businessErrors: string[]): OpenAPIV3.ResponseObject => {
  const listErrors = getListBusinessErrors(businessErrors);

  const data = listErrors.length > 1 ? { oneOf: listErrors } : listErrors[0];

  return {
    description: getResponseDescriptionByHttpCode(httpErrorCode),
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            success: {
              type: 'boolean',
              example: false,
            },
            data,
          },
        },
      },
    },
  };
};

const getResponseDescriptionByHttpCode = (httpCode: number): string => {
  const errorMessages = {
    400: 'некорректный запрос',
    401: 'не авторизован',
    403: 'в доступе отказано',
    404: 'не найдено',
  };

  return errorMessages[httpCode];
};
