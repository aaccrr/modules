import { OpenAPIV3 } from 'openapi-types';

export const ValidationErrorOpenapiSchema: OpenAPIV3.SchemaObject = {
  type: 'object',
  properties: {
    type: {
      type: 'string',
      example: 'VALIDATION_ERROR',
    },
    errors: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          message: {
            type: 'string',
          },
          field: {
            type: 'string',
          },
        },
        additionalProperties: false,
      },
    },
  },
  additionalProperties: false,
};
