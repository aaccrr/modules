import { OpenAPIV3 } from 'openapi-types';
import { getBusinessErrorByCode } from '../errors';
import { getBusinessErrorOpenapiSchema } from './get-business-error-openapi-schema';

export const getListBusinessErrors = (businessErrors: string[]): OpenAPIV3.SchemaObject[] => {
  return businessErrors.map((businessErrorCode) => {
    return getBusinessErrorOpenapiSchema(getBusinessErrorByCode(businessErrorCode));
  });
};
