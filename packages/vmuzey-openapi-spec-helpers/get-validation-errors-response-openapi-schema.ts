import { OpenAPIV3 } from 'openapi-types';
import { getListBusinessErrors } from './get-list-business-errors';
import { ValidationErrorOpenapiSchema } from './validation-error-openapi-schema';

export const getValidationErrorsResponseOpenapiSchema = (businessErrors: string[] = []): OpenAPIV3.ResponseObject => {
  const additionalBusinessErrors = getListBusinessErrors(businessErrors);
  const data =
    additionalBusinessErrors.length > 1
      ? {
          oneOf: [ValidationErrorOpenapiSchema, ...additionalBusinessErrors],
        }
      : ValidationErrorOpenapiSchema;

  return {
    description: 'возможные ошибки валидации',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            success: {
              type: 'boolean',
              example: false,
            },
            data,
          },
        },
      },
    },
  };
};
