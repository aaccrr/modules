import { Readable } from 'stream';
import { IVmuzeyFileStorageProvider } from './i-vmuzey-file-storage-provider';
import { FileSavingTypes, IVmuzeyFileStorageBatchItem, IVmuzeyFileStorageDriver, IVmuzeyFileStorageDriverSaveParams } from './interfaces';

export class VmuzeyFileStorageProvider implements IVmuzeyFileStorageProvider {
  constructor(private driver: IVmuzeyFileStorageDriver) {}

  async init(): Promise<void> {
    await this.driver.init();
  }

  async save(fileStream: FileSavingTypes, params: IVmuzeyFileStorageDriverSaveParams): Promise<string> {
    const uploadedFilePath = await this.driver.save(fileStream, params);
    return uploadedFilePath;
  }

  async saveBatch(batch: IVmuzeyFileStorageBatchItem[]): Promise<string[]> {
    const upladedFilesPaths = await this.driver.saveBatch(batch);
    return upladedFilesPaths;
  }

  async get(fileName: string): Promise<Readable> {
    const file = await this.driver.get(fileName);
    return file;
  }
}
