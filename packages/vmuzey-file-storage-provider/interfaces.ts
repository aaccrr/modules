import { Readable } from 'stream';

export enum VmuzeyFileStorageDriverEnum {
  LOCAL = 'LOCAL',
  S3 = 'S3',
}

export interface IVmuzeyFileStorageDriverSaveParams {
  path: string;
  isPublic?: boolean;
  contentType?: string;
  contentEncoding?: string;
}

export interface IVmuzeyFileStorageDriver {
  init(): Promise<void>;
  save(fileStream: FileSavingTypes, params: IVmuzeyFileStorageDriverSaveParams): Promise<string>;
  saveBatch(batch: IVmuzeyFileStorageBatchItem[]): Promise<string[]>;
  get(fileName: string): Promise<Readable>;
}

export interface IVmuzeyFileStorageBatchItem {
  file: FileSavingTypes;
  params: IVmuzeyFileStorageDriverSaveParams;
}

export type FileSavingTypes = Readable | Buffer | string;
