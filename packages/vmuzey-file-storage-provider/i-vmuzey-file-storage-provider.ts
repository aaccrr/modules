import { Readable } from 'stream';
import { IVmuzeyFileStorageBatchItem, IVmuzeyFileStorageDriverSaveParams } from './interfaces';

export interface IVmuzeyFileStorageProvider {
  init(): Promise<void>;
  save(fileStream: Readable | Buffer, params: IVmuzeyFileStorageDriverSaveParams): Promise<string>;
  saveBatch(batch: IVmuzeyFileStorageBatchItem[]): Promise<string[]>;
  get(fileName: string): Promise<Readable>;
}
