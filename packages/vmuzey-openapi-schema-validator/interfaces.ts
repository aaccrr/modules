import { OpenAPIV3 } from 'openapi-types';

export type OpenapiRequestValidationSchema = OpenAPIV3.OperationObject;
export type OpenapiResponseSchema = OpenAPIV3.ResponsesObject;
