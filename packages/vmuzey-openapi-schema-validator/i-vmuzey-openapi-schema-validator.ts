export interface IVmuzeyOpenapiSchemaValidator {
  validate(validationPayload: any): void;
}
