import * as KoaRouter from "koa-router";
import { IHttpRouter, RouterOptions, RouteMiddleware } from "./Interfaces";

export class HttpRouter implements IHttpRouter {
  private router: KoaRouter;
  private prefix: string = "";

  constructor(options: RouterOptions = {}) {
    this.router = new KoaRouter();
    if (options.prefix) {
      this.prefix = options.prefix;
    }
  }

  public route(
    method: string,
    path: string,
    ...handlers: RouteMiddleware[]
  ): HttpRouter {
    const url = this.prefix + path;
    this.router[method](url, ...handlers);
    return this as any;
  }

  public get(path: string, ...handlers: RouteMiddleware[]): HttpRouter {
    return this.route("get", path, ...handlers);
  }

  public post(path: string, ...handlers: RouteMiddleware[]): HttpRouter {
    return this.route("post", path, ...handlers);
  }

  public put(path: string, ...handlers: RouteMiddleware[]): HttpRouter {
    return this.route("put", path, ...handlers);
  }

  public delete(path: string, ...handlers: RouteMiddleware[]): HttpRouter {
    return this.route("delete", path, ...handlers);
  }

  public patch(path: string, ...handlers: RouteMiddleware[]): HttpRouter {
    return this.route("patch", path, ...handlers);
  }

  public getRoutes() {
    return this.router.routes();
  }

  public use(router: HttpRouter): HttpRouter {
    this.router.use(router.getRoutes());
    return this;
  }

  allowedMethods() {
    return this.router.allowedMethods();
  }
}
