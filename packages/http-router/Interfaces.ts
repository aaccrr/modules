import { HttpRouter } from "./http-router";
// import { RequestPayload } from 'Application/Types/Shared';

export interface IHttpRouter {
  route(
    method: string,
    path: string,
    ...handlers: RouteMiddleware[]
  ): HttpRouter;
  get(path: string, ...handlers: RouteMiddleware[]): HttpRouter;
  post(path: string, ...handlers: RouteMiddleware[]): HttpRouter;
  put(path: string, ...handlers: RouteMiddleware[]): HttpRouter;
  delete(path: string, ...handlers: RouteMiddleware[]): HttpRouter;
  patch(path: string, ...handlers: RouteMiddleware[]): HttpRouter;
  use(router: HttpRouter): HttpRouter;
  getRoutes();
  allowedMethods();
}

export type RouteMiddleware = (ctx: any, next: any) => void;

export interface RouterOptions {
  prefix?: string;
}
