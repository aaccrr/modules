import { OpenAPIV3 } from 'openapi-types';
import { Responder } from '../common';
import { RequestPayload } from '../globals';
import { RouteMiddleware } from '../http-router';
import { IHttpControllerOptions, IHttpHandlerProperties, IVmuzeyHttpControllerProvider } from '../vmuzey-http-controller-provider';
import { VmuzeyHandlerTransportEnum } from '../vmuzey-logger-provider';
import {
  IVmuzeyObservabilityProvider,
  IVmuzeyTracingSpan,
  ObservabilityContextSourceTypeEnum,
  VmuzeyObservabilityContext,
} from '../vmuzey-observability';
import { IVmuzeyServiceHandlerProvider } from '../vmuzey-service-handler-provider';
import { IVmuzeyHttpHandlerProvider } from './i-vmuzey-http-handler-provider';

const DEFAULT_HANDLER_OPTIONS: IHttpControllerOptions = {
  responderOptions: {
    format: 'JSON',
    withSuccess: true,
  },
};

export class VmuzeyHttpHandlerProvider implements IVmuzeyHttpHandlerProvider {
  constructor(
    private httpController: IVmuzeyHttpControllerProvider,
    private serviceHandler: IVmuzeyServiceHandlerProvider,
    private observabilityProvider?: IVmuzeyObservabilityProvider,
    requestHandlerOptions?: IHttpControllerOptions
  ) {
    this.httpController.initializeRequestValidator();
    this.setHandlerOptions(requestHandlerOptions);
  }

  private handlerOptions: IHttpControllerOptions;
  private tracingSpan: IVmuzeyTracingSpan;
  private properties: Partial<IHttpHandlerProperties> = {};

  getInputRequestMiddleware(): RouteMiddleware[] {
    return this.httpController.getInputRequestMiddleware();
  }

  getOpenapiMetadata(): OpenAPIV3.OperationObject {
    return this.httpController.getOpenapiMetadata();
  }

  getRequestHandler(): RouteMiddleware {
    return async (request: RequestPayload) => {
      if (request.state.trace) {
        this.tracingSpan = this.observabilityProvider.tracer.startChildSpan(
          new VmuzeyObservabilityContext({
            sourceType: ObservabilityContextSourceTypeEnum.HTTP_CONTROLLER,
            sourceName: `${request.method} ${request.url.split('?')[0]}`,
            handlerTransportType: VmuzeyHandlerTransportEnum.HTTP,
          }),
          request.state.trace
        );
      } else {
        this.tracingSpan = this.observabilityProvider.tracer.startRootSpan(
          new VmuzeyObservabilityContext({
            sourceType: ObservabilityContextSourceTypeEnum.HTTP_CONTROLLER,
            sourceName: `${request.method} ${request.url.split('?')[0]}`,
            handlerTransportType: VmuzeyHandlerTransportEnum.HTTP,
          })
        );
      }

      try {
        const transformedRequestPayload = await this.httpController.handleInputRequest(request);
        transformedRequestPayload.handler = this.properties as IHttpHandlerProperties;

        this.tracingSpan.withPayload(transformedRequestPayload);
        transformedRequestPayload.trace = this.tracingSpan.extractContext();

        const response = await this.serviceHandler.execute(transformedRequestPayload);

        this.tracingSpan.withResponse(response);

        Responder.reply(request, 200, response, this.handlerOptions.responderOptions);
      } catch (e) {
        this.tracingSpan.withError(e);

        throw e;
      }
    };
  }

  setHandlerProperties(handlerProperties: IHttpHandlerProperties): void {
    this.properties = handlerProperties;
  }

  private setHandlerOptions(options?: IHttpControllerOptions): void {
    this.handlerOptions = options ? { ...DEFAULT_HANDLER_OPTIONS, ...options } : DEFAULT_HANDLER_OPTIONS;
  }
}
