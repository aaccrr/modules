import { OpenAPIV3 } from 'openapi-types';
import { RouteMiddleware } from '../http-router';
import { IHttpHandlerProperties } from '../vmuzey-http-controller-provider';

export interface IVmuzeyHttpHandlerProvider {
  getInputRequestMiddleware(): RouteMiddleware[];
  getRequestHandler(): RouteMiddleware;
  getOpenapiMetadata(): OpenAPIV3.OperationObject;

  setHandlerProperties(handlerProperties: IHttpHandlerProperties): void;
}
