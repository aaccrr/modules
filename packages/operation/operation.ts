import { IOperationMapper, IHandler, IOperationValidator, IOperationAccess } from '../interfaces';
import { Responder } from '../common';

export class Operation<T, U> {
  public returningFormat: 'JSON' | 'XML' = 'JSON';
  public mapper: IOperationMapper<T>;
  public validator: IOperationValidator<T>;
  public handler: IHandler<T, U>;
  public access: IOperationAccess;
  public authorizer: (ctx) => Promise<void>;

  public async run(payload: any): Promise<U> {
    let Request = payload;
    if (this.mapper && !payload.isInternal) Request = await this.mapper(payload);
    if (this.validator && !payload.isInternal) await this.validator(Request);

    delete payload.isInternal;

    return await this.handler.run(Request);
  }

  public async handleHttp(ctx) {
    const Request = this.mapper ? await this.mapper(ctx) : ({} as T);
    if (this.validator) await this.validator(Request);
    const data = await this.handler.run(Request);

    Responder.reply(ctx, 200, data, { format: this.returningFormat });
  }

  public async handleRpc(Request: T, callback) {
    try {
      if (this.validator) await this.validator(Request);
      const data = await this.handler.run(Request);
      callback(null, data);
    } catch (e) {
      console.log('error (operation): ', e);
      callback({ code: e.code || 500, message: e.message });
    }
  }
}
