import { Client, HttpClient } from 'jayson/promise';
import { BusinessError, ValidationError } from '../errors';
import { IJsonRpcProvider, IJsonRpcCallOptions } from './IJsonRpcProvider';

export class JsonRpcProvider implements IJsonRpcProvider {
  private services = {};

  constructor(services: string[]) {
    this.services = services.reduce((servicesAcc, service) => {
      const host = `http://${service}:50051`;
      servicesAcc[service] = Client.http(host as any);
      return servicesAcc;
    }, {});
  }

  async call(options: IJsonRpcCallOptions): Promise<any> {
    const service = this.pickService(options.service);
    const response = await this.callService(service, options.method, options.payload);
    return response;
  }

  private pickService(serviceName: string): any {
    const service = this.services[serviceName];
    if (!service) new BusinessError('ru', 'INTERNAL_ERROR');
    return service;
  }

  private async callService(service: HttpClient, method: string, payload: any): Promise<any> {
    try {
      const res = await service.request(method, payload);
      if (res.result) {
        return res.result;
      }

      if (res.error) {
        throw res.error;
      }
    } catch (e) {
      const message = this.parseErrorMessage(e);

      if (message.type === 'BUSINESS_ERROR') {
        const error = new BusinessError(message.locale, message.code);
        error.stack = message.stacktrace;
        throw error;
      }

      if (message.type === 'VALIDATION_ERROR' && message.isPublic) {
        const error = new ValidationError(message.locale, message.errors);
        error.stack = message.stacktrace;
        throw error;
      }

      console.log(
        JSON.stringify({ type: 'ERROR', timestamp: new Date(), responseType: 'rpc', method: '', stacktrace: message.stack, error: message })
      );

      throw new BusinessError('ru', 'INTERNAL_ERROR');
    }
  }

  private parseErrorMessage(error) {
    try {
      const message = JSON.parse(error.message);
      return message;
    } catch (e) {
      return error;
    }
  }
}
