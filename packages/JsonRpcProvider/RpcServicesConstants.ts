export const ACCOUNT_RPC_SERVICE = 'accounting';
export const ORGANIZATION_RPC_SERVICE = 'organization';
export const MUSEUM_RPC_SERVICE = 'museum';
export const NOMENCLATURE_RPC_SERVICE = 'nomenclature';
