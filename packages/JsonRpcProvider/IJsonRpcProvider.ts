export interface IJsonRpcProvider {
  call(options: IJsonRpcCallOptions): Promise<any>;
}

export interface IJsonRpcCallOptions {
  service: string;
  method: string;
  payload: any;
}
