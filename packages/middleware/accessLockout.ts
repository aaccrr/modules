import { IRedisConnector } from '../connectors';
import { LockoutManager } from '../common';

export function AccessLockoutMiddleware($store: IRedisConnector) {
  return () => {
    return async (ctx, next) => {
      const ip = ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'];
      await LockoutManager.isLocked($store, ip);
      await next();
    };
  };
}
