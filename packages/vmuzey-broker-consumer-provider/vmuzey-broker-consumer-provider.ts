import { Consumer, Kafka } from 'kafkajs';
import { IVmuzeyInternalHandlerProvider } from '../vmuzey-internal-handler-provider';
import { IVmuzeyObservabilityProvider, ObservabilityContextSourceTypeEnum, VmuzeyObservabilityContext } from '../vmuzey-observability';
import { ITransportMessagePayload } from '../vmuzey-transport-layer';
import { IVmuzeyBrokerConsumerProviderConfig } from './interfaces';

const EmptyLogger = (logLevel) => {
  return ({ namespace, level, label, log }) => {};
};

export abstract class VmuzeyBrokerConsumerProvider {
  constructor(private config: IVmuzeyBrokerConsumerProviderConfig, protected ObservabilityProvider: IVmuzeyObservabilityProvider) {}

  private handlers: any = {};
  private broker: Kafka;
  private consumer: Consumer;

  abstract initControllers(): void;
  abstract initRoutes(): void;

  async init(): Promise<void> {
    try {
      this.initControllers();
      this.initRoutes();

      this.broker = new Kafka({
        clientId: this.config.serviceName,
        brokers: [this.config.host],
        logCreator: EmptyLogger,
      });

      this.consumer = this.broker.consumer({ groupId: this.config.serviceName });

      await this.consumer.connect();

      const subscribedTopics = this.getSubscribedTopics();

      for (const subscribedTopic of subscribedTopics) {
        await this.consumer.subscribe({ topic: subscribedTopic });
      }
    } catch (e) {
      console.log('CONSUMER_CONNECT_ERROR ', e);
      process.exit(1);
    }
  }

  async start(): Promise<void> {
    await this.consumer.run({ eachMessage: this.onMessage() });
  }

  addHandler(topic: string, handler: IVmuzeyInternalHandlerProvider): void {
    this.handlers[topic] = handler.getBrokerHandler();
  }

  private getSubscribedTopics(): string[] {
    return Object.keys(this.handlers);
  }

  private onMessage(): any {
    return async ({ topic, partition, message }) => {
      const transportPayload = JSON.parse(message.value.toString()) as ITransportMessagePayload;

      const handler = this.handlers[topic];

      if (typeof handler === 'function') {
        const tracingSpan = this.ObservabilityProvider.tracer.startSpanWithReference(
          new VmuzeyObservabilityContext({
            sourceType: ObservabilityContextSourceTypeEnum.BROKER_MESSAGE,
            sourceName: topic,
            payload: transportPayload.body,
          }),
          transportPayload.trace
        );

        transportPayload.trace = tracingSpan.extractContext();

        try {
          const response = await handler({
            body: transportPayload.body,
            trace: transportPayload.trace,
          });

          tracingSpan.withResponse(response || {});
        } catch (error) {
          console.log('broker error ', error);
          tracingSpan.withError(error);
        }
      }
    };
  }
}
