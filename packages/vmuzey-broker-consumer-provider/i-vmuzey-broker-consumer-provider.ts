import { IVmuzeyInternalHandlerProvider } from '../vmuzey-internal-handler-provider';

export interface IVmuzeyBrokerConsumerProvider {
  init(): Promise<void>;
  start(): Promise<void>;
  addHandler(topic: string, handler: IVmuzeyInternalHandlerProvider): void;
}
