export interface IVmuzeyBrokerConsumerProviderConfig {
  serviceName: string;
  host: string;
}
