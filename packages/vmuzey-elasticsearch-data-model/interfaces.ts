export interface IVmuzeyElasticsearchDataModelConfig {
  indices: IVmuzeyElasticsearchIndexConfig[];
}

export interface IVmuzeyElasticsearchIndexConfig {
  name: string;
  mappings: object;
  settings?: object;
}
