import { Client } from '@elastic/elasticsearch';
import { IVmuzeyElasticsearchDataModelConfig } from './interfaces';

export interface IVmuzeyElasticsearchDataModel {
  connection: Client;
  init(config: IVmuzeyElasticsearchDataModelConfig): Promise<void>;
}
