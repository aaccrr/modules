import { Client } from '@elastic/elasticsearch';
import { IVmuzeyElasticsearchProvider } from '../vmuzey-elasticsearch-provider';
import { IVmuzeyElasticsearchDataModel } from './i-vmuzey-elasticsearch-data-model';
import { IVmuzeyElasticsearchIndexConfig, IVmuzeyElasticsearchDataModelConfig } from './interfaces';

const wait = () => new Promise((resolve) => setTimeout(resolve, 5000));

export class VmuzeyElasticsearchDataModel implements IVmuzeyElasticsearchDataModel {
  constructor(private ElasticsearchProvider: IVmuzeyElasticsearchProvider) {
    this.connection = this.ElasticsearchProvider.connection;
  }

  public connection: Client;

  async init(config: IVmuzeyElasticsearchDataModelConfig): Promise<void> {
    await this.establishElasticsearchConnection();

    for (const index of config.indices) {
      await this.createIndexIfNotExists(index);
    }
  }

  private async createIndexIfNotExists(index: IVmuzeyElasticsearchIndexConfig): Promise<void> {
    const isExists = await this.ElasticsearchProvider.connection.indices.exists({ index: index.name });

    if (!isExists.body) {
      const indexConfiguration: any = {
        mappings: { properties: index.mappings },
      };

      if (index.settings) {
        indexConfiguration.settings = index.settings;
      }

      await this.ElasticsearchProvider.connection.indices.create({
        index: index.name,
        body: indexConfiguration,
      });
    }
  }

  private async establishElasticsearchConnection(): Promise<void> {
    try {
      await this.ElasticsearchProvider.connection.indices.exists({ index: 'test' });
    } catch (e) {
      await wait();
      await this.establishElasticsearchConnection();
    }
  }
}
