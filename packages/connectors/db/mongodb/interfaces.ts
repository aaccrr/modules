import { Db, Collection } from 'mongodb';

export interface IMongodbConnector {
  init(config: IMongoDBConfig): Promise<void>;
  models: IMongoDBModelsSet;
}

export interface IMongoDBConfig {
  credentials: IMongoDBCredentials;
  models: IMongoDBModel[];
}

export interface IMongoDBOptions {
  useNewUrlParser?: boolean;
  useUnifiedTopology?: boolean;
}

export interface IMongoDBModelsSet {
  [modelName: string]: Collection;
}

export interface IMongoDBCredentials {
  host: string;
  user: string;
  password: string;
}

export interface IMongoDBModel {
  dbName: string;
  collectionName: string;
  indexes?: IMongoDBIndex[];
  fixtures?: any;
}

export interface IMongoDBIndex {
  field: string;
  options?: {
    unique?: boolean;
  };
}

export interface IMongoDBConnectionsSet {
  [dbName: string]: Db;
}
