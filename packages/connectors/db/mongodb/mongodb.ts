import { MongoClient, Db, Collection } from 'mongodb';
import {
  IMongodbConnector,
  IMongoDBConfig,
  IMongoDBModel,
  IMongoDBOptions,
  IMongoDBCredentials,
  IMongoDBConnectionsSet,
  IMongoDBModelsSet,
  IMongoDBIndex,
} from './interfaces';

const CONNECT_RETRY_COUNT = 5;

export class MongodbConnector implements IMongodbConnector {
  public models: IMongoDBModelsSet = {};

  async init(config: IMongoDBConfig): Promise<void> {
    const connection = await this.createConnection(this.createConnectionUrl(config.credentials), { useNewUrlParser: true, useUnifiedTopology: true });
    this.models = await this.createModels(connection, config.models);
  }

  protected async createConnection(url: string, options: IMongoDBOptions = {}, retryCount: number = 0): Promise<MongoClient> {
    if (retryCount > CONNECT_RETRY_COUNT) throw new Error('DATABASE_NOT_CONNECTED');
    try {
      console.log(`${retryCount} CONNECT TO MONGO: "${url}"`);
      return MongoClient.connect(url, options);
    } catch (e) {
      await new Promise((resolve) => setTimeout(resolve, 5000));
      return this.createConnection(url, options, retryCount++);
    }
  }

  protected createDbConnections(client: MongoClient, models: IMongoDBModel[]): IMongoDBConnectionsSet {
    return models
      .map((model) => model.dbName)
      .reduce((dbAcc, dbName) => {
        if (!dbAcc[dbName]) dbAcc[dbName] = client.db(dbName);

        return dbAcc;
      }, {});
  }

  private async createModels(client: MongoClient, models: IMongoDBModel[]): Promise<any> {
    const dbConnections = this.createDbConnections(client, models);
    const collections = await Promise.all(
      models.map(async (model) => ({
        name: `${model.dbName}.${model.collectionName}`,
        instance: await this.createModel(dbConnections[model.dbName], model),
      }))
    );

    return collections.reduce((collectionsAcc, collection) => {
      collectionsAcc[collection.name] = collection.instance;
      return collectionsAcc;
    }, {});
  }

  private async createModel(db: Db, model: IMongoDBModel): Promise<Collection> {
    const existingModel = await db.collection(model.collectionName);
    if (existingModel) return existingModel;

    const collection = await db.createCollection(model.collectionName);

    if (Array.isArray(model.indexes)) {
      await this.createIndexes(collection, model.indexes);
    }

    if (Array.isArray(model.fixtures)) {
      await collection.deleteMany({});
      await collection.insertMany(model.fixtures);
    }

    return collection;
  }

  private async createIndexes(collection: Collection, indexes: IMongoDBIndex[]): Promise<void> {
    for (const index of indexes) {
      const options = index.options || {};
      await collection.createIndex({ [index.field]: 1 }, options);
    }
  }

  private createConnectionUrl(credentials: IMongoDBCredentials): string {
    const { user, password, host } = credentials;
    return `mongodb://${user}:${password}@${host}?authSource=admin`;
  }
}
