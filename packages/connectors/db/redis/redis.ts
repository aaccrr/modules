import { createHandyClient, IHandyRedis } from 'handy-redis';
import { StoreUpdateField, IRedisDBConfig } from './interfaces';
import { IRedisConnector } from './interfaces';

export class RedisConnector implements IRedisConnector {
  public db: IHandyRedis;

  constructor(config: IRedisDBConfig) {
    this.db = createHandyClient({
      host: config.credentials.host,
      port: config.credentials.port,
      password: config.credentials.password,
    });
  }

  public methods = {
    set: async (key: string, value: any, expiration: number = null): Promise<void> => {
      const isObject = typeof value === 'object';
      if (isObject) value = JSON.stringify(value);
      if (expiration) {
        await this.db.set(key, value, ['EX', expiration]);
      } else {
        await this.db.set(key, value);
      }
    },
    get: async (key: string): Promise<any> => {
      const res = await this.db.get(key);

      try {
        return JSON.parse(res);
      } catch (e) {
        return res;
      }
    },
    keys: async (key): Promise<any> => {
      return this.db.keys(key);
    },
    findKeyFullByCompositeKey: async (key: string): Promise<string> => {
      const keys = await this.methods.findAllKeys(key);
      return keys[0];
    },
    getCompositeKey: async (key: string): Promise<any> => {
      const keys = await this.methods.keys(key);

      if (!keys.length) return null;
      if (keys.length === 1) {
        return this.methods.get(keys[0]);
      }

      const promises = keys.map((key) => this.methods.get(key));
      return Promise.all(promises);
    },
    deleteCompositeKey: async (key: string): Promise<void> => {
      const keys = await this.methods.keys(key);
      await this.methods.delete(keys[0]);
    },
    delete: async (...keys): Promise<void> => {
      await this.db.del(...keys);
    },
    exists: async (key: string): Promise<boolean> => {
      const value = await this.db.get(key);
      if (value) return true;
      return false;
    },
    update: async (key: string, field: StoreUpdateField): Promise<void> => {
      const value = await this.methods.get(key);
      switch (field.action) {
        case 'add':
          value[field.name].push(field.value);
          break;
        case 'delete':
          value[field.name] = value[field.name].filter((v) => v !== field.value);
          break;
        case 'set':
          value[field.name] = field.value;
          break;
      }
      await this.methods.set(key, value);
    },
    findAllKeys: async (key: string): Promise<string[]> => {
      return this.db.keys(key);
    },
    findOne: async (key: string): Promise<any> => {
      const keys = await this.db.keys(key);
      if (keys && keys.length) {
        return this.db.get(keys[0]);
      } else {
        return null;
      }
    },
    findAll: async (key: string): Promise<any[]> => {
      const keys = await this.db.keys(key);
      const promises = keys.map((key) => this.db.get(key));
      return Promise.all(promises);
    },
  };
}
