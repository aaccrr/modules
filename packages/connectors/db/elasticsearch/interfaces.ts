import { Client } from '@elastic/elasticsearch';

export interface IElasticsearchConnector {
  db: Client;
}

export interface IElasticsearchDBConfig {
  credentials: IElasticsearchDbCredentials;
}

export interface IElasticsearchDbCredentials {
  host: string;
  port: number;
  user: string;
  password: string;
}
