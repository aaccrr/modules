import { Client } from '@elastic/elasticsearch';
import { IElasticsearchConnector, IElasticsearchDBConfig } from './interfaces';

export class ElasticsearchConnector implements IElasticsearchConnector {
  public db: Client;

  constructor(config: IElasticsearchDBConfig) {
    const node = `http://${config.credentials.host}:${config.credentials.port}`;
    this.db = new Client({
      node,
      auth: {
        username: config.credentials.user,
        password: config.credentials.password,
      },
    });
  }
}
