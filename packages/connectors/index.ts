import { ConnectorFactory } from './Factory';

const ConnectorFactoryInstance = new ConnectorFactory();

export { ConnectorFactoryInstance as ConnectorFactory };
export * from './db';
export * from './interfaces';
