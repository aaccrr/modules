import { Collection } from 'mongodb';
import { IMongoDBConfig, IRedisDBConfig, IElasticsearchDBConfig } from '../db';

export type dbTypes = 'mongodb' | 'postgres' | 'elasticsearch' | 'redis';

export interface IConnectorFactory {
  create<T>(dbType: dbTypes, config: IDbConfig): Promise<T>;
}

export type IDbConfig = IMongoDBConfig | IRedisDBConfig | IElasticsearchDBConfig;

export interface IDbConfigCollection {
  name: string;
  indexes?: IDbConfigCollectionIndex[];
  fixtures?: any;
}

export interface IDbConfigCollectionIndex {
  name: string;
}

export interface IDbConfigCredentials {
  host: string;
  port: number;
  user?: string;
  password: string;
}

export interface IConnectorModels {
  [key: string]: Collection;
}

export interface IDalConfig {
  mongodb?: IMongoDBConfig;
  redis?: IRedisDBConfig;
  elasticsearch?: IElasticsearchDBConfig;
}
