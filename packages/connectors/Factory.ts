import { IConnectorFactory, dbTypes, IDbConfig } from './interfaces';
import { MongodbConnector, RedisConnector, ElasticsearchConnector, IElasticsearchDBConfig, IRedisDBConfig, IMongoDBConfig } from './db';
import { Errors } from '../errors';

export class ConnectorFactory implements IConnectorFactory {
  async create<T>(dbType: dbTypes, config: IDbConfig): Promise<T> {
    switch (dbType) {
      case 'mongodb': {
        const connector: any = new MongodbConnector();
        await connector.init(config as IMongoDBConfig);
        return connector;
      }
      case 'redis': {
        const connector: any = new RedisConnector(config as IRedisDBConfig);
        return connector;
      }
      case 'elasticsearch': {
        const connector: any = new ElasticsearchConnector(config as IElasticsearchDBConfig);
        return connector;
      }
      default: {
        Errors.InternalError('UNKNOWN DB TYPE');
      }
    }
  }
}
