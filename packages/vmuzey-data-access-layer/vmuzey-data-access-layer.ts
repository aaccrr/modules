export abstract class VmuzeyDataAccessLayer {
  abstract init(): Promise<void>;

  getRepository<T>(repositoryName: string): T {
    return this[repositoryName];
  }
}
