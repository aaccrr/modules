import { FilterQuery, FindOneOptions, UpdateQuery } from 'mongodb';
import { IVmuzeyElasticsearchDataModelConfig } from '../vmuzey-elasticsearch-data-model';
import { IVmuzeyMongodbDataModelConfig } from '../vmuzey-mongodb-data-model';

export interface IVmuzeyDataLayerConfig {
  mongodb?: IVmuzeyMongodbDataModelConfig;
  elasticsearch?: IVmuzeyElasticsearchDataModelConfig;
}

export interface IVmuzeyDataAccessLayerMongoQuery<T> {
  getSelection(): FilterQuery<T>;
  getOptions?(): FindOneOptions<T>;
}

export interface IVmuzeyDataAccessLayerMongoCommand<T> {
  getSelection?(): FilterQuery<T>;
  getInsertion?(): Partial<T>;
  getUpdation?(): UpdateQuery<T>;
}

export interface IVmuzeyDataAccessLayerElasticQuery<T> {
  getPayload(): T;
}
