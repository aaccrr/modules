import { IVmuzeyDependencyContainer } from '../vmuzey-dependency-container';
import { IVmuzeyDataLayerConfig } from './interfaces';

export interface IVmuzeyDataAccessLayer {
  init(): Promise<void>;
  getRepository<T>(repositoryName: string): T;
}
