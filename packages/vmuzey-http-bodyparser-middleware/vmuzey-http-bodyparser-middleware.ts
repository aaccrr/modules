import * as bodyParser from 'koa-body';

export class VmuzeyHttpBodyparserMiddleware {
  middleware(): any {
    return bodyParser({ multipart: true });
  }
}
