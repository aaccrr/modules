export function Benchmark() {
  return async (ctx: any, next) => {
    console.time(`BENCKMARKS: ${ctx.request.url} responded`);
    await next();
    console.timeEnd(`BENCKMARKS: ${ctx.request.url} responded`);
  };
}
