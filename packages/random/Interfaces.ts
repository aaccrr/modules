import * as cryptoRandomString from 'crypto-random-string';

export interface IRandom {
  generate(options?: IRandomGenerateConfig): string;
  randomHash(): Promise<string>;
  getHashOf(payload: any): Promise<string>;
}

export type IRandomGenerateConfig = cryptoRandomString.Options;
