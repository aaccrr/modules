import { promisify } from 'util';
import { createHash, randomBytes } from 'crypto';
import * as cryptoRandomString from 'crypto-random-string';
import { IRandomGenerateConfig, IRandom } from './Interfaces';

const promisedRandomBytes = promisify(randomBytes);

const DEFAULT_GENERATE_OPTIONS = {
  length: 10,
};

export class Random implements IRandom {
  generate(options: IRandomGenerateConfig = DEFAULT_GENERATE_OPTIONS): string {
    return cryptoRandomString(options);
  }

  async randomHash(): Promise<string> {
    const random = await promisedRandomBytes(256);
    return createHash('sha256')
      .update(random)
      .digest('hex');
  }

  async getHashOf(payload: any): Promise<string> {
    if (typeof payload === 'object' && payload !== null) {
      payload = JSON.stringify(payload);
    }
    return createHash('sha256')
      .update(payload)
      .digest('hex');
  }
}
