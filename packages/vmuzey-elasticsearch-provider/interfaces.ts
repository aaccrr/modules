export interface IVmuzeyElasticsearchProviderConfig {
  host: string;
  user: string;
  password: string;
}
