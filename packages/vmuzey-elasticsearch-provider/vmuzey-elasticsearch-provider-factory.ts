import { IVmuzeyElasticsearchProvider } from './i-vmuzey-elasticsearch-provider';
import { IVmuzeyElasticsearchProviderConfig } from './interfaces';
import { VmuzeyElasticsearchProvider } from './vmuzey-elasticsearch-provider';
import { VmuzeyElasticsearchProviderStub } from './vmuzey-elasticsearch-provider-stub';

export class VmuzeyElasticsearchProviderFactory {
  static create(env: string, elasticsearchConfig: IVmuzeyElasticsearchProviderConfig): IVmuzeyElasticsearchProvider {
    if (env === 'test' || env === 'openapi') {
      return new VmuzeyElasticsearchProviderStub(elasticsearchConfig);
    }

    return new VmuzeyElasticsearchProvider(elasticsearchConfig);
  }
}
