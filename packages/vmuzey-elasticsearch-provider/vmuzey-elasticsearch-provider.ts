import { Client } from '@elastic/elasticsearch';
import { IVmuzeyElasticsearchProvider } from './i-vmuzey-elasticsearch-provider';
import { IVmuzeyElasticsearchProviderConfig } from './interfaces';

export class VmuzeyElasticsearchProvider implements IVmuzeyElasticsearchProvider {
  public connection: Client;

  constructor(config: IVmuzeyElasticsearchProviderConfig) {
    this.connection = new Client({
      node: config.host,
      auth: {
        username: config.user,
        password: config.password,
      },
    });
  }

  init(): void {}
}
