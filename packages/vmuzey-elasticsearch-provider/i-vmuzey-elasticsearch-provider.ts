import { Client } from '@elastic/elasticsearch';

export interface IVmuzeyElasticsearchProvider {
  connection: Client;

  init(): void;
}
