import { Client } from '@elastic/elasticsearch';
import { IVmuzeyElasticsearchProvider } from './i-vmuzey-elasticsearch-provider';
import { IVmuzeyElasticsearchProviderConfig } from './interfaces';

export class VmuzeyElasticsearchProviderStub implements IVmuzeyElasticsearchProvider {
  public connection: any = {
    indices: {
      async exists(index: any): Promise<any> {
        return true;
      },
      async create(index): Promise<any> {
        return true;
      },
    },
  };

  constructor(config: IVmuzeyElasticsearchProviderConfig) {}

  init(): void {}
}
