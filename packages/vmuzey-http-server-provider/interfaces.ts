import { RouteMiddleware } from '../http-router';
import { IHttpControllerOptions } from '../vmuzey-http-controller-provider';
import { IpBlockerMethodsEnum } from '../vmuzey-ip-blocker';

export interface IVmuzeyHttpServerProviderConfig {
  port?: number;
  middlewares?: IVmuzeyHttpMiddleware[];
}

export interface IHttpServerRouteOptions {
  ipBlocker?: IpBlockerMethodsEnum;
  controller?: IHttpControllerOptions;
}

export interface IVmuzeyHttpMiddleware {
  middleware(): RouteMiddleware;
}
