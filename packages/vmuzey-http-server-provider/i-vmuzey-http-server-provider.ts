import { IVmuzeyHttpHandlerProvider } from '../vmuzey-http-handler-provider';

export interface IVmuzeyHttpServerProvider {
  description: string;
  initControllers(): void;
  initRoutes(): void;

  setupMiddlewares(): void;
  setupRoutes(): void;
  startServer(): void;

  route(method: string, path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider;
  get(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider;
  post(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider;
  put(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider;
  delete(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider;
  patch(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider;
  use(router: IVmuzeyHttpServerProvider): void;
  getRoutes(): any;
  saveOpenapiSpecification(specificationPath: string): Promise<void>;
  enableAppInitStatus(): void;
}
