import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';
import { RouteMiddleware } from '../http-router';
import { VmuzeyFileStorageLocalDriver } from '../vmuzey-file-storage-local-driver';
import { VmuzeyFileStorageProvider } from '../vmuzey-file-storage-provider';
import { IVmuzeyHttpControllerProvider } from '../vmuzey-http-controller-provider';
import { IVmuzeyHttpHandlerProvider } from '../vmuzey-http-handler-provider';
import { VmuzeyOpenapiSpecificationGenerator } from '../vmuzey-openapi-specification-generator';
import { IVmuzeyHttpServerProvider } from './i-vmuzey-http-server-provider';
import { IVmuzeyHttpServerProviderConfig } from './interfaces';

export abstract class VmuzeyHttpServerProvider implements IVmuzeyHttpServerProvider {
  constructor(private config: IVmuzeyHttpServerProviderConfig) {}

  private httpServer = new Koa();
  private router = new KoaRouter();
  private openapiSpecificationGenerator = new VmuzeyOpenapiSpecificationGenerator();
  private isAppFullyInited = false;

  abstract description: string;
  abstract initControllers(): void;
  abstract initRoutes(): void;

  setupMiddlewares(): void {
    if (Array.isArray(this.config.middlewares)) {
      this.config.middlewares.forEach((middleware) => this.httpServer.use(middleware.middleware()));
    }
  }

  setupRoutes(): void {
    this.initControllers();
    this.initRoutes();
    this.addHealthcheckRoute();

    this.httpServer.use(this.getRoutes());
  }

  startServer(): void {
    this.httpServer.listen(this.config.port || 80);
  }

  public route(method: string, path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider {
    requestHandler.setHandlerProperties({
      method,
      path,
    });

    const requestHandlersMiddleware: RouteMiddleware[] = [...requestHandler.getInputRequestMiddleware(), requestHandler.getRequestHandler()];

    this.openapiSpecificationGenerator.addPath(path, method, requestHandler.getOpenapiMetadata());

    this.router[method](path, ...requestHandlersMiddleware);

    return this;
  }

  public get(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider {
    return this.route('get', path, requestHandler);
  }

  public post(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider {
    return this.route('post', path, requestHandler);
  }

  public put(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider {
    return this.route('put', path, requestHandler);
  }

  public delete(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider {
    return this.route('delete', path, requestHandler);
  }

  public patch(path: string, requestHandler: IVmuzeyHttpHandlerProvider): IVmuzeyHttpServerProvider {
    return this.route('patch', path, requestHandler);
  }

  public getRoutes() {
    return this.router.routes();
  }

  public use(router: IVmuzeyHttpServerProvider): void {
    router.setupRoutes();
    this.router.use(router.getRoutes());
  }

  public async saveOpenapiSpecification(specificationPath: string): Promise<void> {
    this.openapiSpecificationGenerator.addTitle(this.description);
    this.openapiSpecificationGenerator.addVersion('1.0.0');

    const fileStorage = new VmuzeyFileStorageProvider(new VmuzeyFileStorageLocalDriver());
    await fileStorage.save(JSON.stringify(this.openapiSpecificationGenerator.getSpecification()), { path: specificationPath });
  }

  public enableAppInitStatus(): void {
    this.isAppFullyInited = true;
  }

  private addHealthcheckRoute(): void {
    this.router.get('/healthcheck', async (ctx) => {
      if (this.isAppFullyInited) {
        ctx.status = 200;
      } else {
        ctx.status = 502;
      }
    });
  }
}
