import { OpenAPIV3 } from 'openapi-types';
import { IVmuzeyOpenapiSpecificationGenerator } from './i-vmuzey-openapi-specification-generator';

export class VmuzeyOpenapiSpecificationGenerator implements IVmuzeyOpenapiSpecificationGenerator {
  private openApiSpecification: Partial<OpenAPIV3.Document> = {
    openapi: '3.0.0',
    info: {
      title: '',
      version: '',
    },
    paths: {},
  };

  addTitle(title: string): void {
    this.openApiSpecification.info.title = title;
  }

  addVersion(version: string): void {
    this.openApiSpecification.info.version = version;
  }

  addPath(path: string, method: string, spec: OpenAPIV3.PathItemObject): void {
    if (typeof this.openApiSpecification.paths[path] === 'undefined') {
      this.openApiSpecification.paths[path] = {};
    }

    this.openApiSpecification.paths[path][method] = spec;
  }

  getSpecification(): OpenAPIV3.Document {
    return this.openApiSpecification as OpenAPIV3.Document;
  }
}
