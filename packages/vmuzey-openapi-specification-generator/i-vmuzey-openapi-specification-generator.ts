import { OpenAPIV3 } from 'openapi-types';

export interface IVmuzeyOpenapiSpecificationGenerator {
  addPath(path: string, method: string, spec: OpenAPIV3.PathItemObject): void;
  addTitle(title: string): void;
  addVersion(version: string): void;
  getSpecification(): OpenAPIV3.Document;
}
