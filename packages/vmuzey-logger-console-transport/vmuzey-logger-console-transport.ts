import { IVmuzeyLoggerErrorContext, IVmuzeyLoggerTransport, IVmuzeyLogView } from '../vmuzey-logger-provider';

export class VmuzeyLoggerConsoleTransport implements IVmuzeyLoggerTransport {
  async sendInfoLog(log: IVmuzeyLogView): Promise<void> {
    console.log(JSON.stringify(log));
  }

  async sendError(error: Error, context: IVmuzeyLoggerErrorContext): Promise<void> {
    if (context.payload) {
      if (context.payload.headers) context.payload.headers = JSON.stringify(context.payload.headers);
      if (context.payload.query) context.payload.query = JSON.stringify(context.payload.query);
      if (context.payload.params) context.payload.params = JSON.stringify(context.payload.params);
      if (context.payload.body) context.payload.body = JSON.stringify(context.payload.body);
    }

    context.stacktrace = error.stack;

    console.log(JSON.stringify(context));
  }
}
