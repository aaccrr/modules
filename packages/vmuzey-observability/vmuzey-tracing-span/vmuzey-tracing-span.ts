import { FORMAT_TEXT_MAP, Span } from 'opentracing';
import { IVmuzeyTracingSpan } from './i-vmuzey-tracing-span';

export class VmuzeyTracingSpan implements IVmuzeyTracingSpan {
  constructor(private span: Span) {}

  withPayload(payload: any): void {
    this.addTagsFromPayload(payload);

    this.span.log({
      'log.payload': payload,
    });
  }

  withError(error: any): void {
    this.span.setTag('error', true);

    const parsedError = this.parseError(error);

    this.span.log({
      'log.error': parsedError,
    });

    this.finish();
  }

  withResponse(response: any): void {
    this.span.setTag('error', false);

    this.span.log({
      'log.response': response,
    });

    this.finish();
  }

  extractContext(): any {
    const carrier = {};
    this.span.tracer().inject(this.span.context(), FORMAT_TEXT_MAP, carrier);
    return carrier;
  }

  finish(): void {
    this.span.finish();
  }

  addTagsFromPayload(payload?: any): void {
    if (!payload) {
      payload = {};
    }

    const tags = {
      ...this.getEnvironmentTags(),
      ...this.getEntityTagsFromObject(payload),
      ...this.getEntityTagsFromObject(payload.body),
      ...this.getUserIdTagFromPayload(payload),
    };

    this.span.addTags(tags);
  }

  private getEntityTagsFromObject(obj: any): any {
    if (!obj) {
      return {};
    }

    return Object.keys(obj).reduce((tagsList, payloadKey) => {
      if (payloadKey.endsWith('Id') || payloadKey === 'id') {
        tagsList[`entity.${payloadKey}`] = obj[payloadKey];
      }

      return tagsList;
    }, {});
  }

  private getUserIdTagFromPayload(payload: any): any {
    if (typeof payload.meta?.user?.id === 'string' && payload.meta?.user?.id?.length > 0) {
      return {
        'user.id': payload.meta?.user?.id,
      };
    }

    return {};
  }

  private parseError(err: any = {}): any {
    if (!err) {
      return {};
    }

    if (!err.type) {
      return {
        message: err.message,
        stacktrace: err.stack,
      };
    }

    return err;
  }

  private getEnvironmentTags(): any {
    return {
      env: process.env.NODE_ENV,
    };
  }
}
