import { IVmuzeyObservabilityContext } from '../vmuzey-observability-context';
import { IVmuzeyTracingSpan } from '../vmuzey-tracing-span';

export interface IVmuzeyTracerProvider {
  startRootSpan(observabilityContext: IVmuzeyObservabilityContext): IVmuzeyTracingSpan;
  startSpanWithReference(observabilityContext: IVmuzeyObservabilityContext, referenceSpanContext: any): IVmuzeyTracingSpan;
  startChildSpan(observabilityContext: IVmuzeyObservabilityContext, parentSpanContext: any): IVmuzeyTracingSpan;
}
