import { Tracer, FORMAT_TEXT_MAP, SpanOptions, followsFrom } from 'opentracing';
import { IVmuzeyObservabilityContext } from '../vmuzey-observability-context';
import { IVmuzeyTracingSpan, VmuzeyTracingSpan } from '../vmuzey-tracing-span';
import { IVmuzeyTracerProvider } from './i-vmuzey-tracer-provider';
import { IVmuzeyTracerProviderConfig, IVmuzeyTracerProviderConfigReporter } from './interfaces';

import { initTracer, ReporterConfig } from 'jaeger-client';
import { ITraceModel } from '../../globals';

export class VmuzeyTracerProvider implements IVmuzeyTracerProvider {
  constructor(config: IVmuzeyTracerProviderConfig) {
    this._tracer = initTracer(
      {
        serviceName: config.serviceName,
        reporter: {
          logSpans: true,
          ...this.getReporterConnectionConfig(config.reporter),
        },
        sampler: {
          type: 'const',
          param: 1,
        },
      },
      {}
    );
  }

  private _tracer: Tracer;

  startRootSpan(observabilityContext: IVmuzeyObservabilityContext): IVmuzeyTracingSpan {
    return this._createSpan(observabilityContext);
  }

  startSpanWithReference(observabilityContext: IVmuzeyObservabilityContext, trace: ITraceModel): IVmuzeyTracingSpan {
    const referenceSpan = this._tracer.extract(FORMAT_TEXT_MAP, trace);
    return this._createSpan(observabilityContext, { references: [followsFrom(referenceSpan)] });
  }

  startChildSpan(observabilityContext: IVmuzeyObservabilityContext, trace: ITraceModel): IVmuzeyTracingSpan {
    const parentSpan = this._tracer.extract(FORMAT_TEXT_MAP, trace);
    return this._createSpan(observabilityContext, { childOf: parentSpan });
  }

  private _createSpan(observabilityContext: IVmuzeyObservabilityContext, spanOptions?: SpanOptions): IVmuzeyTracingSpan {
    const spanName = observabilityContext.getSpanName();
    const span = new VmuzeyTracingSpan(this._tracer.startSpan(spanName, spanOptions));

    if (observabilityContext.hasStartPayload) {
      span.withPayload(observabilityContext.getSpanStartLogPayload());
    }

    return span;
  }

  private getReporterConnectionConfig(reporterConfig: IVmuzeyTracerProviderConfigReporter): Partial<ReporterConfig> {
    if (reporterConfig.agent) {
      const [agentHost, agentPort] = reporterConfig.agent.host.split(':');

      return {
        agentHost,
        agentPort: parseInt(agentPort),
      };
    }

    return {
      collectorEndpoint: reporterConfig.collector.host,
    };
  }
}
