export interface IVmuzeyTracerProviderConfig {
  serviceName: string;
  reporter: IVmuzeyTracerProviderConfigReporter;
}

export interface IVmuzeyTracerProviderConfigReporter {
  agent?: {
    host: string;
  };
  collector?: {
    host: string;
  };
}
