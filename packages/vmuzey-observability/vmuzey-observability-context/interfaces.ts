import { VmuzeyHandlerTransportEnum } from '../../vmuzey-logger-provider';

export interface IObservabilityContextModel {
  sourceType?: ObservabilityContextSourceTypeEnum;
  sourceName?: string;

  handlerTransportType?: VmuzeyHandlerTransportEnum;

  requestId?: string;
  topic?: string;
  rpcMethod?: string;
  httpUrl?: string;
  httpMethod?: string;

  payload?: any;
  error?: any;
  response?: any;
}

export enum ObservabilityContextSourceTypeEnum {
  HTTP_REQUEST = 'HTTP_REQUEST',
  SCHEDULED_JOB = 'SCHEDULED_JOB',
  RPC_REQUEST = 'RPC_REQUEST',
  BROKER_MESSAGE = 'BROKER_MESSAGE',
  HTTP_CONTROLLER = 'HTTP_CONTROLLER',
}
