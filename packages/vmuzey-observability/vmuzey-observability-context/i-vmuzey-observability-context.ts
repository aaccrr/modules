export interface IVmuzeyObservabilityContext {
  hasStartPayload: boolean;

  getSpanName(): string;
  getSpanStartLogPayload(): any;
  getSpanFinishLogPayload(): any;
}
