import { VmuzeyHandlerTransportEnum } from '../../vmuzey-logger-provider';
import { IVmuzeyObservabilityContext } from './i-vmuzey-observability-context';
import { IObservabilityContextModel, ObservabilityContextSourceTypeEnum } from './interfaces';

export class VmuzeyObservabilityContext implements IVmuzeyObservabilityContext {
  constructor(private observabilityContextModel: IObservabilityContextModel) {}

  get hasStartPayload(): boolean {
    return typeof this.observabilityContextModel.payload !== 'undefined';
  }

  getSpanName(): string {
    return `${this.observabilityContextModel.sourceType} "${this.observabilityContextModel.sourceName}"`;
  }

  getSpanStartLogPayload(): any {
    return this.observabilityContextModel.payload;
  }

  getSpanFinishLogPayload(): any {
    return this.observabilityContextModel.error || this.observabilityContextModel.response || {};
  }
}
