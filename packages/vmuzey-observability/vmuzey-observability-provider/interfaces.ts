import { IVmuzeyTracerProviderConfig } from '../vmuzey-tracer-provider';

export interface IVmuzeyObservabilityProviderConfig {
  tracer: IVmuzeyTracerProviderConfig;
}
