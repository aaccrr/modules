import { IVmuzeyTracerProvider, VmuzeyTracerProvider } from '../vmuzey-tracer-provider';
import { IVmuzeyObservabilityProvider } from './i-vmuzey-observability-provider';
import { IVmuzeyObservabilityProviderConfig } from './interfaces';

export class VmuzeyObservabilityProvider implements IVmuzeyObservabilityProvider {
  constructor(config: IVmuzeyObservabilityProviderConfig) {
    this.tracer = new VmuzeyTracerProvider(config.tracer);
  }

  tracer: IVmuzeyTracerProvider;

  async init(): Promise<void> {}
}
