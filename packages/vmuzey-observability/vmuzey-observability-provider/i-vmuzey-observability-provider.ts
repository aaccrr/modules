import { IVmuzeyTracerProvider } from '../vmuzey-tracer-provider';

export interface IVmuzeyObservabilityProvider {
  tracer: IVmuzeyTracerProvider;

  init(): Promise<void>;
}
