export interface IVmuzeyHelperHasher {
  hashSHA256(): Promise<string>;
}
