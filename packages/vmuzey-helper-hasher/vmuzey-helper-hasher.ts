import { createHash } from 'crypto';
import { IVmuzeyHelperHasher } from './i-vmuzey-helper-hasher';

export class VmuzeyHelperHasher implements IVmuzeyHelperHasher {
  constructor(private hashPayload: object) {
    this.stringifiedHashPayload = JSON.stringify(this.hashPayload);
  }

  private stringifiedHashPayload: string;

  async hashSHA256(): Promise<string> {
    return createHash('sha256').update(this.stringifiedHashPayload).digest('hex');
  }
}
