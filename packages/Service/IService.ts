// import { RequestPayload } from '../../../interfaces';
// import { IResponderOptions } from '../Responder';

export interface IServiceNew {
  run(dto: any): Promise<any>;
  handleHttp(): any;
  handleRpc(): any;
  handleBrokerMessage(): any;
}

export interface IServiceNewOptions {
  returningFormat?: 'JSON' | 'XML';
  responderOptions?: any;
  useAdapter?: boolean;
  useValidator?: boolean;
}
