import { RequestPayload, SchemaValidator, Responder } from '../';
import { ValidationError } from '../errors';
import { IServiceNew, IServiceNewOptions } from './IService';

const DEFAULT_OPTIONS: IServiceNewOptions = {
  responderOptions: {
    format: 'JSON',
    withSuccess: true,
  },
};

export abstract class ServiceNew implements IServiceNew {
  protected adapter = (ctx: RequestPayload) => ({});
  protected validationSchema = { type: 'object' };
  private validator = new SchemaValidator({ coerceTypes: false, allErrors: true });

  async run(dto: any): Promise<any> {}

  handleHttp(options?: IServiceNewOptions): any {
    return async (ctx) => {
      options = options ? { ...DEFAULT_OPTIONS, ...options } : DEFAULT_OPTIONS;

      let payload = ctx;

      if (typeof options.useAdapter === 'undefined' || options.useAdapter === true) {
        payload = this.adapter(ctx);
      }

      this.validateRequest(payload, options);

      const data = await this.run(payload);

      Responder.reply(ctx, 200, data, options.responderOptions);
    };
  }

  handleRpc(): any {
    return async (req) => {
      this.validator.validate(this.validationSchema, req);
      const data = await this.run(req);
      return data;
    };
  }

  handleBrokerMessage(): any {
    return async (message) => {
      await this.run(message.body);
    };
  }

  private validateRequest(payload, options): void {
    try {
      if (typeof options.useValidator === 'undefined' || options.useValidator === true) {
        this.validator.validate(this.validationSchema, payload);
      }
    } catch (e) {
      throw new ValidationError(e.locale, e.errors, e.httpCode, e.isPublic);
    }
  }
}
