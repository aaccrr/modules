import { IVmuzeyAppModuleConfig, VmuzeyAppModule } from '../vmuzey-app-module';

export class VmuzeyAppFactory {
  static create(appConfig: IVmuzeyAppModuleConfig): VmuzeyAppModule {
    return new VmuzeyAppModule(appConfig);
  }
}
