import { IGetWidgetAddressQuery, IGetWidgetAddressResponse } from '../../globals/interfaces/widget/AppModuleFacade/GetWidgetAddress';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IWidgetAppModuleFacade {
  getWidgetAddress(transportMessage: ITransportMessagePayload<IGetWidgetAddressQuery>): Promise<IGetWidgetAddressResponse>;
}
