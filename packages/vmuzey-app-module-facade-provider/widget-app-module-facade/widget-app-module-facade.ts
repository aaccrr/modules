import { IGetWidgetAddressQuery, IGetWidgetAddressResponse } from '../../globals/interfaces/widget/AppModuleFacade/GetWidgetAddress';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IWidgetAppModuleFacade } from './i-widget-app-module-facade';

export class WidgetAppModuleFacade implements IWidgetAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  getWidgetAddress(transportMessage: ITransportMessagePayload<IGetWidgetAddressQuery>): Promise<IGetWidgetAddressResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'widget',
      method: 'GET_WIDGET_ADDRESS',
      payload: transportMessage,
    });
  }
}
