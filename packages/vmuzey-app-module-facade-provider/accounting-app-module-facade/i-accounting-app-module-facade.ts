import {
  IAssignOrganizationToPrimaryManagerCommand,
  IAttachAvatarToAdminAccountCommand,
  IAttachAvatarToEmployeeAccountCommand,
  ICheckPrimaryManagerPermissionsToRegisterOrganizationQuery,
  ICheckPrimaryManagerPermissionsToRegisterOrganizationResponse,
  ICollectCustomerInformationQuery,
  ICollectCustomerInformationResponse,
  ICreateBookerAccountCommand,
  IDeleteUnconfirmedOrganizationAdminAccountCommand,
  IGetAdminAccountForAttachAvatarQuery,
  IGetAdminAccountForAttachAvatarResponse,
  IGetCurrentUserInfoQuery,
  IGetCurrentUserInfoResponse,
  IGetEmployeeAccountForAttachAvatarQuery,
  IGetEmployeeAccountForAttachAvatarResponse,
  IPurgeEmployeesOfMuseumCommand,
  IValidateSessionQuery,
  IValidateSessionResponse,
  IViewPrimaryManagerAccountInOrganizationQuery,
  IViewPrimaryManagerAccountInOrganizationResponse,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IAccountingAppModuleFacade {
  assignOrganizationToPrimaryManager(messagePayload: ITransportMessagePayload<IAssignOrganizationToPrimaryManagerCommand>): Promise<void>;

  checkPrimaryManagerPermissionsToRegisterOrganization(
    messagePayload: ITransportMessagePayload<ICheckPrimaryManagerPermissionsToRegisterOrganizationQuery>
  ): Promise<ICheckPrimaryManagerPermissionsToRegisterOrganizationResponse>;

  viewPrimaryManagerAccountInOrganization(
    transportMessage: ITransportMessagePayload<IViewPrimaryManagerAccountInOrganizationQuery>
  ): Promise<IViewPrimaryManagerAccountInOrganizationResponse>;

  createBookerAccount(messagePayload: ITransportMessagePayload<ICreateBookerAccountCommand>): Promise<void>;

  deleteUnconfirmedOrganizationAdminAccount(
    messagePayload: ITransportMessagePayload<IDeleteUnconfirmedOrganizationAdminAccountCommand>
  ): Promise<void>;

  getCurrentUserInfo(messagePayload: ITransportMessagePayload<IGetCurrentUserInfoQuery>): Promise<IGetCurrentUserInfoResponse>;

  validateSession(messagePayload: ITransportMessagePayload<IValidateSessionQuery>): Promise<IValidateSessionResponse>;

  collectCustomerInformation(
    messagePayload: ITransportMessagePayload<ICollectCustomerInformationQuery>
  ): Promise<ICollectCustomerInformationResponse>;

  purgeEmployeesOfMuseum(transportMessage: ITransportMessagePayload<IPurgeEmployeesOfMuseumCommand>): Promise<void>;

  getAdminAccountForAttachAvatar(
    transportMessage: ITransportMessagePayload<IGetAdminAccountForAttachAvatarQuery>
  ): Promise<IGetAdminAccountForAttachAvatarResponse>;

  attachAvatarToAdminAccount(transportMessage: ITransportMessagePayload<IAttachAvatarToAdminAccountCommand>): Promise<void>;

  getEmployeeAccountForAttachAvatar(
    transportMessage: ITransportMessagePayload<IGetEmployeeAccountForAttachAvatarQuery>
  ): Promise<IGetEmployeeAccountForAttachAvatarResponse>;

  attachAvatarToEmployeeAccount(transportMessage: ITransportMessagePayload<IAttachAvatarToEmployeeAccountCommand>): Promise<void>;
}
