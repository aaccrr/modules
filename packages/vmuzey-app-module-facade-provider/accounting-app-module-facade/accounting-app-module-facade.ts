import {
  IAssignOrganizationToPrimaryManagerCommand,
  IAttachAvatarToAdminAccountCommand,
  IAttachAvatarToEmployeeAccountCommand,
  ICheckPrimaryManagerPermissionsToRegisterOrganizationQuery,
  ICheckPrimaryManagerPermissionsToRegisterOrganizationResponse,
  ICollectCustomerInformationQuery,
  ICollectCustomerInformationResponse,
  ICreateBookerAccountCommand,
  IDeleteUnconfirmedOrganizationAdminAccountCommand,
  IGetAdminAccountForAttachAvatarQuery,
  IGetAdminAccountForAttachAvatarResponse,
  IGetCurrentUserInfoQuery,
  IGetCurrentUserInfoResponse,
  IGetEmployeeAccountForAttachAvatarQuery,
  IGetEmployeeAccountForAttachAvatarResponse,
  IPurgeEmployeesOfMuseumCommand,
  IValidateSessionQuery,
  IValidateSessionResponse,
  IViewPrimaryManagerAccountInOrganizationQuery,
  IViewPrimaryManagerAccountInOrganizationResponse,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IAccountingAppModuleFacade } from './i-accounting-app-module-facade';

export class AccountingAppModuleFacade implements IAccountingAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  assignOrganizationToPrimaryManager(messagePayload: ITransportMessagePayload<IAssignOrganizationToPrimaryManagerCommand>): Promise<void> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'ASSIGN_ORGANIZATION_TO_PRIMARY_MANAGER',
      payload: messagePayload,
    });
  }

  checkPrimaryManagerPermissionsToRegisterOrganization(
    messagePayload: ITransportMessagePayload<ICheckPrimaryManagerPermissionsToRegisterOrganizationQuery>
  ): Promise<ICheckPrimaryManagerPermissionsToRegisterOrganizationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'CHECK_PRIMARY_MANAGER_PERMISSIONS_TO_REGISTER_ORGANIZATION',
      payload: messagePayload,
    });
  }

  viewPrimaryManagerAccountInOrganization(
    transportMessage: ITransportMessagePayload<IViewPrimaryManagerAccountInOrganizationQuery>
  ): Promise<IViewPrimaryManagerAccountInOrganizationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'CONTROLLER_VIEW_PRIMARY_MANAGER_ACCOUNT',
      payload: transportMessage,
    });
  }

  createBookerAccount(messagePayload: ITransportMessagePayload<ICreateBookerAccountCommand>): Promise<void> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'CREATE_BOOKER_ACCOUNT',
      payload: messagePayload,
    });
  }

  async deleteUnconfirmedOrganizationAdminAccount(
    messagePayload: ITransportMessagePayload<IDeleteUnconfirmedOrganizationAdminAccountCommand>
  ): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'DELETE_ADMIN_ACCOUNT_IF_NOT_CONFIRMED',
      payload: messagePayload,
    });
  }

  getCurrentUserInfo(messagePayload: ITransportMessagePayload<IGetCurrentUserInfoQuery>): Promise<IGetCurrentUserInfoResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'GET_CURRENT_USER_INFO',
      payload: messagePayload,
    });
  }

  validateSession(messagePayload: ITransportMessagePayload<IValidateSessionQuery>): Promise<IValidateSessionResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'GetSession',
      payload: messagePayload,
    });
  }

  async collectCustomerInformation(
    messagePayload: ITransportMessagePayload<ICollectCustomerInformationQuery>
  ): Promise<ICollectCustomerInformationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'COLLECT_CUSTOMER_INFORMATION',
      payload: messagePayload,
    });
  }

  async purgeEmployeesOfMuseum(transportMessage: ITransportMessagePayload<IPurgeEmployeesOfMuseumCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'MUSEUM_DELETED_BY_ADMIN',
      message: transportMessage,
    });
  }

  getAdminAccountForAttachAvatar(
    transportMessage: ITransportMessagePayload<IGetAdminAccountForAttachAvatarQuery>
  ): Promise<IGetAdminAccountForAttachAvatarResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'VIEW_ADMIN_ACCOUNT_BEFORE_ATTACH_AVATAR',
      payload: transportMessage,
    });
  }

  async attachAvatarToAdminAccount(transportMessage: ITransportMessagePayload<IAttachAvatarToAdminAccountCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'UploadAvatarToAdminAccount',
      payload: transportMessage,
    });
  }

  getEmployeeAccountForAttachAvatar(
    transportMessage: ITransportMessagePayload<IGetEmployeeAccountForAttachAvatarQuery>
  ): Promise<IGetEmployeeAccountForAttachAvatarResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'ViewControl',
      payload: transportMessage,
    });
  }

  async attachAvatarToEmployeeAccount(transportMessage: ITransportMessagePayload<IAttachAvatarToEmployeeAccountCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'accounting',
      method: 'UploadAvatarToEmployeeAccount',
      payload: transportMessage,
    });
  }
}
