import { IReuploadImagesForMuseumRequest } from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { IUploadAppModuleFacade } from './i-upload-app-module-facade';

export class UploadAppModuleFacade implements IUploadAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async reuploadImagesForMuseum(request: IReuploadImagesForMuseumRequest): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'upload',
      method: 'REUPLOAD_IMAGES_FOR_MUSEUM',
      payload: request,
    });
  }
}
