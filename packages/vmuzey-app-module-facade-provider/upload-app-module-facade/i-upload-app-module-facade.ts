import { IReuploadImagesForMuseumRequest } from '../../globals';

export interface IUploadAppModuleFacade {
  reuploadImagesForMuseum(request: IReuploadImagesForMuseumRequest): Promise<void>;
}
