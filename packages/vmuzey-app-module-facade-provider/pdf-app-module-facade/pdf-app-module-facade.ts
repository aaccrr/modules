import {
  IPrintDetailedSalesReportResponse,
  IPrintMuseumCardCommand,
  IPrintControlSpinesCommand,
  IPrintControlSpinesResponse,
  IPrintMonthlyReportsCommand,
  IPrintPurchaseCommand,
  IPrintDetailedSalesReportCommand,
  IPrintDetailedSalesReportAndSendOnEmailCommand,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IPdfAppModuleFacade } from './i-pdf-app-module-facade';

export class PdfAppModuleFacade implements IPdfAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  printControlSpines(transportMessage: ITransportMessagePayload<IPrintControlSpinesCommand>): Promise<IPrintControlSpinesResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'pdf',
      method: 'PRINT_CONTROL_SPINES_REPORT',
      payload: transportMessage,
    });
  }

  printDetailedSalesReport(transportMessage: ITransportMessagePayload<IPrintDetailedSalesReportCommand>): Promise<IPrintDetailedSalesReportResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'pdf',
      method: 'PRINT_DETAILED_SALES_REPORT',
      payload: transportMessage,
    });
  }

  async printDetailedSalesReportAndSendOnEmail(
    transportMessage: ITransportMessagePayload<IPrintDetailedSalesReportAndSendOnEmailCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PRINT_DETAILED_SALES_REPORT_AND_SEND_ON_EMAIL',
      message: transportMessage,
    });
  }

  async printMonthlyReports(transportMessage: ITransportMessagePayload<IPrintMonthlyReportsCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PRINT_MONTHLY_REPORTS_FOR_ORGANIZATION',
      message: transportMessage,
    });
  }

  async printMuseumCard(transportMessage: ITransportMessagePayload<IPrintMuseumCardCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PRINT_MUSEUM_CARD',
      message: transportMessage,
    });
  }

  async printPurchase(transportMessage: ITransportMessagePayload<IPrintPurchaseCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'pdf',
      method: 'PRINT_PURCHASE',
      payload: transportMessage,
    });
  }
}
