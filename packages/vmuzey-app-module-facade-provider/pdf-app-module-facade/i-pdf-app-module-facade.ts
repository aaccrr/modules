import {
  IPrintDetailedSalesReportResponse,
  IPrintMuseumCardCommand,
  IPrintControlSpinesCommand,
  IPrintControlSpinesResponse,
  IPrintMonthlyReportsCommand,
  IPrintPurchaseCommand,
  IPrintDetailedSalesReportCommand,
  IPrintDetailedSalesReportAndSendOnEmailCommand,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IPdfAppModuleFacade {
  printControlSpines(transportMessage: ITransportMessagePayload<IPrintControlSpinesCommand>): Promise<IPrintControlSpinesResponse>;
  printDetailedSalesReport(transportMessage: ITransportMessagePayload<IPrintDetailedSalesReportCommand>): Promise<IPrintDetailedSalesReportResponse>;
  printDetailedSalesReportAndSendOnEmail(transportMessage: ITransportMessagePayload<IPrintDetailedSalesReportAndSendOnEmailCommand>): Promise<void>;
  printMonthlyReports(transportMessage: ITransportMessagePayload<IPrintMonthlyReportsCommand>): Promise<void>;
  printMuseumCard(transportMessage: ITransportMessagePayload<IPrintMuseumCardCommand>): Promise<void>;
  printPurchase(transportMessage: ITransportMessagePayload<IPrintPurchaseCommand>): Promise<void>;
}
