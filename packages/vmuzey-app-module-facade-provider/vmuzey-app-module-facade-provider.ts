import { IVmuzeyBrokerProducerProvider } from '../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../vmuzey-rpc-client-provider';
import { AccountingAppModuleFacade, IAccountingAppModuleFacade } from './accounting-app-module-facade';
import { BillingAppModuleFacade, IBillingAppModuleFacade } from './billing-app-module-facade';
import { GeographyAppModuleFacade, IGeographyAppModuleFacade } from './geography-app-module-facade';
import { IVmuzeyAppModuleFacadeProvider } from './i-vmuzey-app-module-facade-provider';
import { IMailerAppModuleFacade, MailerAppModuleFacade } from './mailer-app-module-facade';
import { IMuseumAppModuleFacade, MuseumAppModuleFacade } from './museum-app-module-facade';
import { INomenclatureAppModuleFacade, NomenclatureAppModuleFacade } from './nomenclature-app-module-facade';
import { IOrderingAppModuleFacade, OrderingAppModuleFacade } from './ordering-app-module-facade';
import { IOrganizationAppModuleFacade, OrganizationAppModuleFacade } from './organization-app-module-facade';
import { IPdfAppModuleFacade, PdfAppModuleFacade } from './pdf-app-module-facade';
import { ISchedulerAppModuleFacade, SchedulerAppModuleFacade } from './scheduler-app-module-facade';
import { ISeoOptimizationModuleFacade, SeoOptimizationModuleFacade } from './seo-optimization-module-facade';
import { IReportingAppModuleFacade, ReportingAppModuleFacade } from './reporting-app-module-facade';

export class VmuzeyAppModuleFacadeProvider implements IVmuzeyAppModuleFacadeProvider {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  getAccountingModuleFacade(): IAccountingAppModuleFacade {
    return new AccountingAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getBillingModuleFacade(): IBillingAppModuleFacade {
    return new BillingAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getGeographyModuleFacade(): IGeographyAppModuleFacade {
    return new GeographyAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getMailerModuleFacade(): IMailerAppModuleFacade {
    return new MailerAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getMuseumModuleFacade(): IMuseumAppModuleFacade {
    return new MuseumAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getNomenclatureModuleFacade(): INomenclatureAppModuleFacade {
    return new NomenclatureAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getOrderingModuleFacade(): IOrderingAppModuleFacade {
    return new OrderingAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getOrganizationModuleFacade(): IOrganizationAppModuleFacade {
    return new OrganizationAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getPdfModuleFacade(): IPdfAppModuleFacade {
    return new PdfAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getSchedulerModuleFacade(): ISchedulerAppModuleFacade {
    return new SchedulerAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getSeoOptimizationModuleFacade(): ISeoOptimizationModuleFacade {
    return new SeoOptimizationModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }

  getReportingModuleFacade(): IReportingAppModuleFacade {
    return new ReportingAppModuleFacade(this.VmuzeyRpcProvider, this.VmuzeyBrokerProducerProvider);
  }
}
