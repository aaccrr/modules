import {
  IFetchOrganizationSummaryQuery,
  IFetchOrganizationSummaryResponse,
  IFetchOrganizationCardInfoQuery,
  IFetchOrganizationCardInfoResponse,
  IFetchOrganizationNameQuery,
  IFetchOrganizationNameResponse,
  IFetchServiceProviderForPurchaseQuery,
  IFetchServiceProviderForPurchaseResponse,
  IFetchOrganizationDataForReportQuery,
  IFetchOrganizationDataForReportResponse,
  IFetchServiceProviderForWidgetQuery,
  IFetchServiceProviderForWidgetResponse,
  IAttachEmployeeCommand,
  IAttachMuseumCommand,
  IAttachDocumentsCommand,
  IFetchOrganizationDocumentsQuery,
  IFetchOrganizationDocumentsResponse,
  IFetchOrganizationRefundSettingsQuery,
  IFetchOrganizationRefundSettingsResponse,
  IFetchOrganizationsDataForAccountingReportResponse,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IOrganizationAppModuleFacade {
  attachDocuments(transportMessage: ITransportMessagePayload<IAttachDocumentsCommand>): Promise<void>;
  attachEmployee(transportMessage: ITransportMessagePayload<IAttachEmployeeCommand>): Promise<void>;
  attachMuseum(transportMessage: ITransportMessagePayload<IAttachMuseumCommand>): Promise<void>;
  fetchOrganizationCardInfo(messagePayload: ITransportMessagePayload<IFetchOrganizationCardInfoQuery>): Promise<IFetchOrganizationCardInfoResponse>;
  fetchOrganizationDataForReport(
    messagePayload: ITransportMessagePayload<IFetchOrganizationDataForReportQuery>
  ): Promise<IFetchOrganizationDataForReportResponse>;
  fetchOrganizationDocuments(
    transportMessage: ITransportMessagePayload<IFetchOrganizationDocumentsQuery>
  ): Promise<IFetchOrganizationDocumentsResponse>;
  fetchOrganizationName(messagePayload: ITransportMessagePayload<IFetchOrganizationNameQuery>): Promise<IFetchOrganizationNameResponse>;
  fetchOrganizationRefundSettings(
    transportMessage: ITransportMessagePayload<IFetchOrganizationRefundSettingsQuery>
  ): Promise<IFetchOrganizationRefundSettingsResponse>;
  fetchOrganizationsDataForAccountingReport(
    messagePayload: ITransportMessagePayload<{}>
  ): Promise<IFetchOrganizationsDataForAccountingReportResponse>;
  fetchOrganizationSummary(messagePayload: ITransportMessagePayload<IFetchOrganizationSummaryQuery>): Promise<IFetchOrganizationSummaryResponse>;
  fetchServiceProviderForPurchase(
    transportMessage: ITransportMessagePayload<IFetchServiceProviderForPurchaseQuery>
  ): Promise<IFetchServiceProviderForPurchaseResponse>;
  fetchServiceProviderForWidget(
    messagePayload: ITransportMessagePayload<IFetchServiceProviderForWidgetQuery>
  ): Promise<IFetchServiceProviderForWidgetResponse>;
  startMonthlyReportsMailing(messagePayload: ITransportMessagePayload<{}>): Promise<void>;
}
