import {
  IAttachDocumentsCommand,
  IAttachEmployeeCommand,
  IAttachMuseumCommand,
  IFetchOrganizationCardInfoQuery,
  IFetchOrganizationCardInfoResponse,
  IFetchOrganizationDataForReportQuery,
  IFetchOrganizationDataForReportResponse,
  IFetchOrganizationDocumentsQuery,
  IFetchOrganizationDocumentsResponse,
  IFetchOrganizationNameQuery,
  IFetchOrganizationNameResponse,
  IFetchOrganizationRefundSettingsQuery,
  IFetchOrganizationRefundSettingsResponse,
  IFetchOrganizationsDataForAccountingReportResponse,
  IFetchOrganizationSummaryQuery,
  IFetchOrganizationSummaryResponse,
  IFetchServiceProviderForPurchaseQuery,
  IFetchServiceProviderForPurchaseResponse,
  IFetchServiceProviderForWidgetQuery,
  IFetchServiceProviderForWidgetResponse,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IOrganizationAppModuleFacade } from './i-organization-app-module-facade';

export class OrganizationAppModuleFacade implements IOrganizationAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async attachDocuments(transportMessage: ITransportMessagePayload<IAttachDocumentsCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'UpdateAdmin',
      payload: transportMessage,
    });
  }

  async attachEmployee(transportMessage: ITransportMessagePayload<IAttachEmployeeCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'EMPLOYEE_CREATED',
      message: transportMessage,
    });
  }

  async attachMuseum(transportMessage: ITransportMessagePayload<IAttachMuseumCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ATTACH_CREATED_MUSEUM_TO_ORGANIZATION',
      message: transportMessage,
    });
  }

  fetchOrganizationCardInfo(messagePayload: ITransportMessagePayload<IFetchOrganizationCardInfoQuery>): Promise<IFetchOrganizationCardInfoResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'ADMIN_VIEW_CARD_INFO',
      payload: messagePayload,
    });
  }

  fetchOrganizationDataForReport(
    messagePayload: ITransportMessagePayload<IFetchOrganizationDataForReportQuery>
  ): Promise<IFetchOrganizationDataForReportResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'FETCH_ORGANIZATION_DATA_FOR_REPORT',
      payload: messagePayload,
    });
  }

  fetchOrganizationDocuments(
    transportMessage: ITransportMessagePayload<IFetchOrganizationDocumentsQuery>
  ): Promise<IFetchOrganizationDocumentsResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'ViewAdmin',
      payload: transportMessage,
    });
  }

  fetchOrganizationName(messagePayload: ITransportMessagePayload<IFetchOrganizationNameQuery>): Promise<IFetchOrganizationNameResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'ADMIN_VIEW_ORGANIZATION_NAME',
      payload: messagePayload,
    });
  }

  fetchOrganizationRefundSettings(
    transportMessage: ITransportMessagePayload<IFetchOrganizationRefundSettingsQuery>
  ): Promise<IFetchOrganizationRefundSettingsResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'VIEW_REFUND_SETTINGS',
      payload: transportMessage,
    });
  }

  async fetchOrganizationsDataForAccountingReport(
    messagePayload: ITransportMessagePayload<{}>
  ): Promise<IFetchOrganizationsDataForAccountingReportResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'FETCH_ORGANIZATIONS_FOR_ACCOUNTING_REPORT',
      payload: messagePayload,
    });
  }

  fetchOrganizationSummary(messagePayload: ITransportMessagePayload<IFetchOrganizationSummaryQuery>): Promise<IFetchOrganizationSummaryResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'ADMIN_VIEW_ORGANIZATION_SUMMARY',
      payload: messagePayload,
    });
  }

  fetchServiceProviderForPurchase(
    transportMessage: ITransportMessagePayload<IFetchServiceProviderForPurchaseQuery>
  ): Promise<IFetchServiceProviderForPurchaseResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'FETCH_BEFORE_PURCHASE',
      payload: transportMessage,
    });
  }

  fetchServiceProviderForWidget(
    messagePayload: ITransportMessagePayload<IFetchServiceProviderForWidgetQuery>
  ): Promise<IFetchServiceProviderForWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'GET_SERVICE_PROVIDER_INFO_FOR_WIDGET',
      payload: messagePayload,
    });
  }

  async startMonthlyReportsMailing(messagePayload: ITransportMessagePayload<{}>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'organization',
      method: 'CREATE_MONTHLY_REPORTS',
      payload: messagePayload,
    });
  }
}
