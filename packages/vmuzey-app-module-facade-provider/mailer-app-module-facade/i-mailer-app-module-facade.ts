import {
  INotifyCeoAboutNewOrganizationCommand,
  INotifyClientAboutRegistrationAfterFirstPurchaseCommand,
  ISendLicenseAgreementToOrganizationPrimaryManagerCommand,
  ISendOrganizationAccountRegistrationNotificationCommand,
  ISendOrganizationMonthlyReportsOnEmailCommand,
  ISendAdminRegistrationConfirmMailCommand,
  ISendClientRegistrationConfirmMailCommand,
  ISendResetPasswordCodeCommand,
  ISendWidgetInstallationGuideCommand,
  ISendChangeEmailConfirmMailCommand,
  ISendRefundRejectNotificationCommand,
  ISendMuseumCardCommand,
  ISendSalesReportCommand,
  ISendPrintedPurchaseToCustomerCommand,
  ISendRefundApprovedNotificationToCustomerCommand,
  ISendRefundCreatedNotificationToCustomerCommand,
  ISendPurchaseCancellationNotificationToCustomerCommand,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IMailerAppModuleFacade {
  notifyCeoAboutNewOrganization(transportMessage: ITransportMessagePayload<INotifyCeoAboutNewOrganizationCommand>): Promise<void>;
  notifyClientAboutRegistrationAfterFirstPurchase(
    transportMessage: ITransportMessagePayload<INotifyClientAboutRegistrationAfterFirstPurchaseCommand>
  ): Promise<void>;
  sendAdminRegistrationConfirmMail(transportMessage: ITransportMessagePayload<ISendAdminRegistrationConfirmMailCommand>): Promise<void>;
  sendChangeEmailConfirmMail(transportMessage: ITransportMessagePayload<ISendChangeEmailConfirmMailCommand>): Promise<void>;
  sendClientRegistrationConfirmMail(transportMessage: ITransportMessagePayload<ISendClientRegistrationConfirmMailCommand>): Promise<void>;
  sendLicenseAgreementToOrganizationPrimaryManager(
    transportMessage: ITransportMessagePayload<ISendLicenseAgreementToOrganizationPrimaryManagerCommand>
  ): Promise<void>;
  sendMuseumCard(transportMessage: ITransportMessagePayload<ISendMuseumCardCommand>): Promise<void>;
  sendOrganizationAccountRegistrationNotification(
    transportMessage: ITransportMessagePayload<ISendOrganizationAccountRegistrationNotificationCommand>
  ): Promise<void>;
  sendOrganizationMonthlyReportsOnEmail(transportMessage: ITransportMessagePayload<ISendOrganizationMonthlyReportsOnEmailCommand>): Promise<void>;
  sendPrintedPurchaseToCustomer(transportMessage: ITransportMessagePayload<ISendPrintedPurchaseToCustomerCommand>): Promise<void>;
  sendPurchaseCancellationNotificationToCustomer(
    transportMessage: ITransportMessagePayload<ISendPurchaseCancellationNotificationToCustomerCommand>
  ): Promise<void>;
  sendRefundApprovedNotificationToCustomer(
    transportMessage: ITransportMessagePayload<ISendRefundApprovedNotificationToCustomerCommand>
  ): Promise<void>;
  sendRefundCreatedNotificationToCustomer(transportMessage: ITransportMessagePayload<ISendRefundCreatedNotificationToCustomerCommand>): Promise<void>;
  sendRefundRejectNotification(transportMessage: ITransportMessagePayload<ISendRefundRejectNotificationCommand>): Promise<void>;
  sendResetPasswordCode(transportMessage: ITransportMessagePayload<ISendResetPasswordCodeCommand>): Promise<void>;
  sendSalesReport(transportMessage: ITransportMessagePayload<ISendSalesReportCommand>): Promise<void>;
  sendWidgetInstallationGuide(transportMessage: ITransportMessagePayload<ISendWidgetInstallationGuideCommand>): Promise<void>;
}
