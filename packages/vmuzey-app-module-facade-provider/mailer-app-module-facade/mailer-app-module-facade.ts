import {
  INotifyCeoAboutNewOrganizationCommand,
  INotifyClientAboutRegistrationAfterFirstPurchaseCommand,
  ISendAdminRegistrationConfirmMailCommand,
  ISendChangeEmailConfirmMailCommand,
  ISendClientRegistrationConfirmMailCommand,
  ISendLicenseAgreementToOrganizationPrimaryManagerCommand,
  ISendMuseumCardCommand,
  ISendOrganizationAccountRegistrationNotificationCommand,
  ISendOrganizationMonthlyReportsOnEmailCommand,
  ISendPrintedPurchaseToCustomerCommand,
  ISendPurchaseCancellationNotificationToCustomerCommand,
  ISendRefundApprovedNotificationToCustomerCommand,
  ISendRefundCreatedNotificationToCustomerCommand,
  ISendRefundRejectNotificationCommand,
  ISendResetPasswordCodeCommand,
  ISendSalesReportCommand,
  ISendWidgetInstallationGuideCommand,
} from '../../globals/interfaces/mailer/AppModuleFacade';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IMailerAppModuleFacade } from './i-mailer-app-module-facade';

export class MailerAppModuleFacade implements IMailerAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async notifyCeoAboutNewOrganization(transportMessage: ITransportMessagePayload<INotifyCeoAboutNewOrganizationCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'NOTIFY_CEO_ABOUT_NEW_ORGANIZATION',
      message: transportMessage,
    });
  }

  async notifyClientAboutRegistrationAfterFirstPurchase(
    transportMessage: ITransportMessagePayload<INotifyClientAboutRegistrationAfterFirstPurchaseCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'NOTIFY_CLIENT_ABOUT_REGISTRATION_AFTER_FIRST_PURCHASE',
      message: transportMessage,
    });
  }

  async sendAdminRegistrationConfirmMail(transportMessage: ITransportMessagePayload<ISendAdminRegistrationConfirmMailCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_ADMIN_REGISTRATION_CONFIRM_MAIL',
      message: transportMessage,
    });
  }

  async sendChangeEmailConfirmMail(transportMessage: ITransportMessagePayload<ISendChangeEmailConfirmMailCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_CHANGE_EMAIL_CONFIRM_MAIL',
      message: transportMessage,
    });
  }

  async sendClientRegistrationConfirmMail(transportMessage: ITransportMessagePayload<ISendClientRegistrationConfirmMailCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_CLIENT_REGISTRATION_CONFIRM_MAIL',
      message: transportMessage,
    });
  }

  async sendLicenseAgreementToOrganizationPrimaryManager(
    transportMessage: ITransportMessagePayload<ISendLicenseAgreementToOrganizationPrimaryManagerCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_LICENSE_AGREEMENT_TO_ORGANIZATION_PRIMARY_MANAGER',
      message: transportMessage,
    });
  }

  async sendMuseumCard(transportMessage: ITransportMessagePayload<ISendMuseumCardCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_MUSEUM_CARD',
      message: transportMessage,
    });
  }

  async sendOrganizationAccountRegistrationNotification(
    transportMessage: ITransportMessagePayload<ISendOrganizationAccountRegistrationNotificationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_ORGANIZATION_ACCOUNT_REGISTRATION_NOTIFICATION',
      message: transportMessage,
    });
  }

  async sendOrganizationMonthlyReportsOnEmail(
    transportMessage: ITransportMessagePayload<ISendOrganizationMonthlyReportsOnEmailCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_ORGANIZATION_MONTHLY_REPORTS_ON_EMAIL',
      message: transportMessage,
    });
  }

  async sendPrintedPurchaseToCustomer(transportMessage: ITransportMessagePayload<ISendPrintedPurchaseToCustomerCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'mailer',
      method: 'SEND_PRINTED_PURCHASE_TO_CUSTOMER',
      payload: transportMessage,
    });
  }

  async sendPurchaseCancellationNotificationToCustomer(
    transportMessage: ITransportMessagePayload<ISendPurchaseCancellationNotificationToCustomerCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_PURCHASE_CANCELLATION_NOTIFICATION_TO_CUSTOMER',
      message: transportMessage,
    });
  }

  async sendRefundApprovedNotificationToCustomer(
    transportMessage: ITransportMessagePayload<ISendRefundApprovedNotificationToCustomerCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_REFUND_APPROVED_NOTIFICATION_TO_CUSTOMER',
      message: transportMessage,
    });
  }

  async sendRefundCreatedNotificationToCustomer(
    transportMessage: ITransportMessagePayload<ISendRefundCreatedNotificationToCustomerCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_REFUND_CREATED_NOTIFICATION_TO_CUSTOMER',
      message: transportMessage,
    });
  }

  async sendRefundRejectNotification(transportMessage: ITransportMessagePayload<ISendRefundRejectNotificationCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_REFUND_REJECT_NOTIFICATION',
      message: transportMessage,
    });
  }

  async sendResetPasswordCode(transportMessage: ITransportMessagePayload<ISendResetPasswordCodeCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_RESET_PASSWORD_CODE',
      message: transportMessage,
    });
  }

  async sendSalesReport(transportMessage: ITransportMessagePayload<ISendSalesReportCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_SALES_REPORT',
      message: transportMessage,
    });
  }

  async sendWidgetInstallationGuide(transportMessage: ITransportMessagePayload<ISendWidgetInstallationGuideCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SEND_WIDGET_INSTALLATION_GUIDE',
      message: transportMessage,
    });
  }
}
