import {
  IConfirmFiscalizationCommand,
  IConfirmFiscalizationResponse,
  IConfirmRefundPaymentCommand,
  IConfirmRefundPaymentResponse,
  IFiscalizePurchasePaymentCommand,
  IFiscalizeRefundCommand,
  IRegisterRefundInPaymentSystemCommand,
  IRegisterRefundInPaymentSystemResponse,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IBillingAppModuleFacade } from './i-billing-app-module-facade';

export class BillingAppModuleFacade implements IBillingAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  confirmFiscalization(transportMessage: ITransportMessagePayload<IConfirmFiscalizationCommand>): Promise<IConfirmFiscalizationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'billing',
      method: 'CONFIRM_FISCALIZATION',
      payload: transportMessage,
    });
  }

  confirmRefundPayment(transportMessage: ITransportMessagePayload<IConfirmRefundPaymentCommand>): Promise<IConfirmRefundPaymentResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'billing',
      method: 'CONFIRM_REFUND_PAYMENT',
      payload: transportMessage,
    });
  }

  fiscalizePurchasePayment(transportMessage: ITransportMessagePayload<IFiscalizePurchasePaymentCommand>): Promise<void> {
    return this.VmuzeyRpcProvider.call({
      service: 'billing',
      method: 'FISCALIZE_PURCHASE_PAYMENT',
      payload: transportMessage,
    });
  }

  async fiscalizeRefund(transportMessage: ITransportMessagePayload<IFiscalizeRefundCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'FISCALIZE_REFUND',
      message: transportMessage,
    });
  }

  registerRefundInPaymentSystem(
    transportMessage: ITransportMessagePayload<IRegisterRefundInPaymentSystemCommand>
  ): Promise<IRegisterRefundInPaymentSystemResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'billing',
      method: 'REGISTER_REFUND_IN_PAYMENT_SYSTEM',
      payload: transportMessage,
    });
  }
}
