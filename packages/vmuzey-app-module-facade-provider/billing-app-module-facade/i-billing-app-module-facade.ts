import {
  IConfirmRefundPaymentCommand,
  IConfirmFiscalizationCommand,
  IConfirmFiscalizationResponse,
  IConfirmRefundPaymentResponse,
  IFiscalizePurchasePaymentCommand,
  IFiscalizeRefundCommand,
  IRegisterRefundInPaymentSystemCommand,
  IRegisterRefundInPaymentSystemResponse,
} from '../../globals/interfaces/billing/AppModuleFacade';

import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IBillingAppModuleFacade {
  confirmFiscalization(transportMessage: ITransportMessagePayload<IConfirmFiscalizationCommand>): Promise<IConfirmFiscalizationResponse>;
  confirmRefundPayment(transportMessage: ITransportMessagePayload<IConfirmRefundPaymentCommand>): Promise<IConfirmRefundPaymentResponse>;
  fiscalizePurchasePayment(transportMessage: ITransportMessagePayload<IFiscalizePurchasePaymentCommand>): Promise<void>;
  fiscalizeRefund(transportMessage: ITransportMessagePayload<IFiscalizeRefundCommand>): Promise<void>;
  registerRefundInPaymentSystem(
    transportMessage: ITransportMessagePayload<IRegisterRefundInPaymentSystemCommand>
  ): Promise<IRegisterRefundInPaymentSystemResponse>;
}
