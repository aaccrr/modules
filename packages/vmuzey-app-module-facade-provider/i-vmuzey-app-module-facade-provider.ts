import { IAccountingAppModuleFacade } from './accounting-app-module-facade';
import { IBillingAppModuleFacade } from './billing-app-module-facade';
import { IGeographyAppModuleFacade } from './geography-app-module-facade';
import { IMailerAppModuleFacade } from './mailer-app-module-facade';
import { IMuseumAppModuleFacade } from './museum-app-module-facade';
import { INomenclatureAppModuleFacade } from './nomenclature-app-module-facade';
import { IOrderingAppModuleFacade } from './ordering-app-module-facade';
import { IOrganizationAppModuleFacade } from './organization-app-module-facade';
import { IPdfAppModuleFacade } from './pdf-app-module-facade';
import { IReportingAppModuleFacade } from './reporting-app-module-facade';
import { ISchedulerAppModuleFacade } from './scheduler-app-module-facade';
import { ISeoOptimizationModuleFacade } from './seo-optimization-module-facade';

export interface IVmuzeyAppModuleFacadeProvider {
  getAccountingModuleFacade(): IAccountingAppModuleFacade;
  getBillingModuleFacade(): IBillingAppModuleFacade;
  getGeographyModuleFacade(): IGeographyAppModuleFacade;
  getMailerModuleFacade(): IMailerAppModuleFacade;
  getMuseumModuleFacade(): IMuseumAppModuleFacade;
  getNomenclatureModuleFacade(): INomenclatureAppModuleFacade;
  getOrderingModuleFacade(): IOrderingAppModuleFacade;
  getOrganizationModuleFacade(): IOrganizationAppModuleFacade;
  getPdfModuleFacade(): IPdfAppModuleFacade;
  getSchedulerModuleFacade(): ISchedulerAppModuleFacade;
  getSeoOptimizationModuleFacade(): ISeoOptimizationModuleFacade;
  getReportingModuleFacade(): IReportingAppModuleFacade;
}
