import {
  IScheduleAutorunTicketSalesCommand,
  IScheduleFinishSeasonTicketSalesCommand,
  IScheduleRefundPurchaseDueToServiceCancellationCommand,
  IUnscheduleAutorunTicketSalesCommand,
  IRescheduleTicketSalesFinishCommand,
  IScheduleRegisterRefundInPaymentSystemCommand,
  IUnscheduleEventProgressTrackingCommand,
  IScheduleCreateMonthlyReportsForOrganizationCommand,
  IScheduleDeleteUnconfirmedAdminAccountCommand,
  IScheduleEventProgressTrackingCommand,
  ISchedulePrintPurchaseCommand,
  IScheduleFiscalizePurchasePaymentCommand,
  IScheduleConfirmPurchasePaymentFiscalizationCommand,
  IUnscheduleReservePurchaseCommand,
  IScheduleConfirmRefundFiscalizationCommand,
  IScheduleSendPrintedPurchaseToCustomerCommand,
  IScheduleReservePurchaseCommand,
  IScheduleStartEventSalesCommand,
  IScheduleClosePurchaseDueToExpirationCommand,
  IScheduleClosePurchaseDueToUnpayCommand,
  IScheduleConfirmRefundPaymentCommand,
  IUnscheduleStartEventSalesCommand,
  IUnscheduleClosePurchaseDueToUnpayCommand,
  IUnscheduleFinishSeasonTicketSalesCommand,
  IUnscheduleDeleteUnconfirmedAdminAccountCommand,
  IUnscheduleClosePurchaseDueToExpirationCommand,
  IRescheduleReservePurchaseCommand,
  IRescheduleFinishSeasonTicketSalesCommand,
  IUnscheduleFinishTicketSalesCommand,
} from '../../globals/interfaces/scheduler';

import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface ISchedulerAppModuleFacade {
  rescheduleReservePurchase(transportMessage: ITransportMessagePayload<IRescheduleReservePurchaseCommand>): Promise<void>;
  rescheduleFinishSeasonTicketSales(transportMessage: ITransportMessagePayload<IRescheduleFinishSeasonTicketSalesCommand>): Promise<void>;
  rescheduleTicketSalesFinish(transportMessage: ITransportMessagePayload<IRescheduleTicketSalesFinishCommand>): Promise<void>;
  scheduleAutorunTicketSales(transportMessage: ITransportMessagePayload<IScheduleAutorunTicketSalesCommand>): Promise<void>;
  scheduleClosePurchaseDueToUnpay(transportMessage: ITransportMessagePayload<IScheduleClosePurchaseDueToUnpayCommand>): Promise<void>;
  scheduleClosePurchaseDueToExpiration(transportMessage: ITransportMessagePayload<IScheduleClosePurchaseDueToExpirationCommand>): Promise<void>;
  scheduleConfirmPurchasePaymentFiscalization(
    transportMessage: ITransportMessagePayload<IScheduleConfirmPurchasePaymentFiscalizationCommand>
  ): Promise<void>;
  scheduleConfirmRefundFiscalization(transportMessage: ITransportMessagePayload<IScheduleConfirmRefundFiscalizationCommand>): Promise<void>;
  scheduleConfirmRefundPayment(transportMessage: ITransportMessagePayload<IScheduleConfirmRefundPaymentCommand>): Promise<void>;
  scheduleCreateMonthlyReportsForOrganization(
    transportMessage: ITransportMessagePayload<IScheduleCreateMonthlyReportsForOrganizationCommand>
  ): Promise<void>;
  scheduleDeleteUnconfirmedAdminAccount(transportMessage: ITransportMessagePayload<IScheduleDeleteUnconfirmedAdminAccountCommand>): Promise<void>;
  scheduleEventProgressTracking(transportMessage: ITransportMessagePayload<IScheduleEventProgressTrackingCommand>): Promise<void>;
  scheduleFiscalizePurchasePayment(transportMessage: ITransportMessagePayload<IScheduleFiscalizePurchasePaymentCommand>): Promise<void>;
  scheduleFinishSeasonTicketSales(transportMessage: ITransportMessagePayload<IScheduleFinishSeasonTicketSalesCommand>): Promise<void>;
  schedulePrintPurchase(transportMessage: ITransportMessagePayload<ISchedulePrintPurchaseCommand>): Promise<void>;
  scheduleRefundPurchaseDueToServiceCancellation(
    transportMessage: ITransportMessagePayload<IScheduleRefundPurchaseDueToServiceCancellationCommand>
  ): Promise<void>;
  scheduleRegisterRefundInPaymentSystem(transportMessage: ITransportMessagePayload<IScheduleRegisterRefundInPaymentSystemCommand>): Promise<void>;
  scheduleReservePurchase(transportMessage: ITransportMessagePayload<IScheduleReservePurchaseCommand>): Promise<void>;
  scheduleSendPrintedPurchaseToCustomer(transportMessage: ITransportMessagePayload<IScheduleSendPrintedPurchaseToCustomerCommand>): Promise<void>;
  unscheduleAutorunTicketSales(transportMessage: ITransportMessagePayload<IUnscheduleAutorunTicketSalesCommand>): Promise<void>;
  unscheduleEventProgressTracking(transportMessage: ITransportMessagePayload<IUnscheduleEventProgressTrackingCommand>): Promise<void>;
  scheduleStartEventSales(transportMessage: ITransportMessagePayload<IScheduleStartEventSalesCommand>): Promise<void>;
  unscheduleReservePurchase(transportMessage: ITransportMessagePayload<IUnscheduleReservePurchaseCommand>): Promise<void>;
  unscheduleStartEventSales(transportMessage: ITransportMessagePayload<IUnscheduleStartEventSalesCommand>): Promise<void>;
  unscheduleClosePurchaseDueToUnpay(transportMessage: ITransportMessagePayload<IUnscheduleClosePurchaseDueToUnpayCommand>): Promise<void>;
  unscheduleDeleteUnconfirmedAdminAccount(transportMessage: ITransportMessagePayload<IUnscheduleDeleteUnconfirmedAdminAccountCommand>): Promise<void>;
  unscheduleFinishSeasonTicketSales(transportMessage: ITransportMessagePayload<IUnscheduleFinishSeasonTicketSalesCommand>): Promise<void>;
  unscheduleClosePurchaseDueToExpiration(transportMessage: ITransportMessagePayload<IUnscheduleClosePurchaseDueToExpirationCommand>): Promise<void>;
  unscheduleFinishTicketSales(transportMessage: ITransportMessagePayload<IUnscheduleFinishTicketSalesCommand>): Promise<void>;
}
