import {
  IRescheduleFinishSeasonTicketSalesCommand,
  IRescheduleReservePurchaseCommand,
  IRescheduleTicketSalesFinishCommand,
  IScheduleAutorunTicketSalesCommand,
  IScheduleClosePurchaseDueToExpirationCommand,
  IScheduleClosePurchaseDueToUnpayCommand,
  IScheduleConfirmPurchasePaymentFiscalizationCommand,
  IScheduleConfirmRefundFiscalizationCommand,
  IScheduleConfirmRefundPaymentCommand,
  IScheduleCreateMonthlyReportsForOrganizationCommand,
  IScheduleDeleteUnconfirmedAdminAccountCommand,
  IScheduleEventProgressTrackingCommand,
  IScheduleFinishSeasonTicketSalesCommand,
  IScheduleFiscalizePurchasePaymentCommand,
  ISchedulePrintPurchaseCommand,
  IScheduleRefundPurchaseDueToServiceCancellationCommand,
  IScheduleRegisterRefundInPaymentSystemCommand,
  IScheduleReservePurchaseCommand,
  IScheduleSendPrintedPurchaseToCustomerCommand,
  IScheduleStartEventSalesCommand,
  IUnscheduleAutorunTicketSalesCommand,
  IUnscheduleClosePurchaseDueToExpirationCommand,
  IUnscheduleClosePurchaseDueToUnpayCommand,
  IUnscheduleDeleteUnconfirmedAdminAccountCommand,
  IUnscheduleEventProgressTrackingCommand,
  IUnscheduleFinishSeasonTicketSalesCommand,
  IUnscheduleFinishTicketSalesCommand,
  IUnscheduleReservePurchaseCommand,
  IUnscheduleStartEventSalesCommand,
} from '../../globals/interfaces/scheduler';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { ISchedulerAppModuleFacade } from './i-scheduler-app-module-facade';

export class SchedulerAppModuleFacade implements ISchedulerAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async rescheduleReservePurchase(transportMessage: ITransportMessagePayload<IRescheduleReservePurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CHANGE_PURCHASE_RESERVATION_PERIOD',
      message: transportMessage,
    });
  }

  async rescheduleFinishSeasonTicketSales(transportMessage: ITransportMessagePayload<IRescheduleFinishSeasonTicketSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CHANGE_SEASON_TICKET_SALES_FINISH_TRACKING_DATE',
      message: transportMessage,
    });
  }

  async rescheduleTicketSalesFinish(transportMessage: ITransportMessagePayload<IRescheduleTicketSalesFinishCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REFRESH_TICKET_SALES_SCHEDULE_FINISH_TRACKING',
      message: transportMessage,
    });
  }

  async scheduleAutorunTicketSales(transportMessage: ITransportMessagePayload<IScheduleAutorunTicketSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_AUTORUN_TICKET_SALES',
      message: transportMessage,
    });
  }

  async scheduleClosePurchaseDueToExpiration(
    transportMessage: ITransportMessagePayload<IScheduleClosePurchaseDueToExpirationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'START_PURCHASE_EXPIRATION_TRACKING',
      message: transportMessage,
    });
  }

  async scheduleClosePurchaseDueToUnpay(transportMessage: ITransportMessagePayload<IScheduleClosePurchaseDueToUnpayCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'START_PURCHASE_PAYMENT_TRACKING',
      message: transportMessage,
    });
  }

  async scheduleConfirmPurchasePaymentFiscalization(
    transportMessage: ITransportMessagePayload<IScheduleConfirmPurchasePaymentFiscalizationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_PURCHASE_PAYMENT_FISCALIZATION_CONFIRM',
      message: transportMessage,
    });
  }

  async scheduleConfirmRefundFiscalization(transportMessage: ITransportMessagePayload<IScheduleConfirmRefundFiscalizationCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_REFUND_FISCALIZATION_CONFIRM',
      message: transportMessage,
    });
  }

  async scheduleConfirmRefundPayment(transportMessage: ITransportMessagePayload<IScheduleConfirmRefundPaymentCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_CONFIRM_REFUND_PAYMENT',
      message: transportMessage,
    });
  }

  async scheduleCreateMonthlyReportsForOrganization(
    transportMessage: ITransportMessagePayload<IScheduleCreateMonthlyReportsForOrganizationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_CREATE_MONTHLY_REPORTS_FOR_ORGANIZATION',
      message: transportMessage,
    });
  }

  async scheduleDeleteUnconfirmedAdminAccount(
    transportMessage: ITransportMessagePayload<IScheduleDeleteUnconfirmedAdminAccountCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ADMIN_ACCOUNT_CREATED',
      message: transportMessage,
    });
  }

  async scheduleEventProgressTracking(transportMessage: ITransportMessagePayload<IScheduleEventProgressTrackingCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_EVENT_PROGRESS_TRACKING',
      message: transportMessage,
    });
  }

  async scheduleFiscalizePurchasePayment(transportMessage: ITransportMessagePayload<IScheduleFiscalizePurchasePaymentCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_PURCHASE_PAYMENT_FISCALIZATION',
      message: transportMessage,
    });
  }

  async scheduleFinishSeasonTicketSales(transportMessage: ITransportMessagePayload<IScheduleFinishSeasonTicketSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'TRACK_SEASON_TICKET_SALES_FINISH',
      message: transportMessage,
    });
  }

  async schedulePrintPurchase(transportMessage: ITransportMessagePayload<ISchedulePrintPurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_PRINT_PURCHASE',
      message: transportMessage,
    });
  }

  async scheduleRegisterRefundInPaymentSystem(
    transportMessage: ITransportMessagePayload<IScheduleRegisterRefundInPaymentSystemCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PROCESS_REFUND_BY_PAYMENT_SYSTEM',
      message: transportMessage,
    });
  }

  async scheduleReservePurchase(transportMessage: ITransportMessagePayload<IScheduleReservePurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_PURCHASE_RESERVATION_CANCELLATION',
      message: transportMessage,
    });
  }

  async scheduleSendPrintedPurchaseToCustomer(
    transportMessage: ITransportMessagePayload<IScheduleSendPrintedPurchaseToCustomerCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_SEND_PRINTED_PURCHASE_TO_CUSTOMER',
      message: transportMessage,
    });
  }

  async scheduleRefundPurchaseDueToServiceCancellation(
    transportMessage: ITransportMessagePayload<IScheduleRefundPurchaseDueToServiceCancellationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REFUND_PURCHASE_DUE_TO_SERVICE_CANCELLATION',
      message: transportMessage,
    });
  }

  async unscheduleAutorunTicketSales(transportMessage: ITransportMessagePayload<IUnscheduleAutorunTicketSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CANCEL_SCHEDULED_TICKET_SALES_AUTORUN',
      message: transportMessage,
    });
  }

  async unscheduleEventProgressTracking(transportMessage: ITransportMessagePayload<IUnscheduleEventProgressTrackingCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REMOVE_EVENT_PROGRESS_TRACKING_TASKS',
      message: transportMessage,
    });
  }

  async scheduleStartEventSales(transportMessage: ITransportMessagePayload<IScheduleStartEventSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SCHEDULE_START_SALES_ON_EVENT',
      message: transportMessage,
    });
  }

  async unscheduleReservePurchase(transportMessage: ITransportMessagePayload<IUnscheduleReservePurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CANCEL_PURCHASE_RESERVATION_CANCELLATION',
      message: transportMessage,
    });
  }

  async unscheduleFinishSeasonTicketSales(transportMessage: ITransportMessagePayload<IUnscheduleFinishSeasonTicketSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CANCEL_SEASON_TICKET_SALES_FINISH_TRACKING',
      message: transportMessage,
    });
  }

  async unscheduleStartEventSales(transportMessage: ITransportMessagePayload<IUnscheduleStartEventSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REMOVE_START_SALES_ON_EVENT_TASK',
      message: transportMessage,
    });
  }

  async unscheduleClosePurchaseDueToUnpay(transportMessage: ITransportMessagePayload<IUnscheduleClosePurchaseDueToUnpayCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CANCEL_PURCHASE_PAYMENT_TRACKING',
      message: transportMessage,
    });
  }

  async unscheduleDeleteUnconfirmedAdminAccount(
    transportMessage: ITransportMessagePayload<IUnscheduleDeleteUnconfirmedAdminAccountCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ADMIN_ACCOUNT_CONFIRMED',
      message: transportMessage,
    });
  }

  async unscheduleClosePurchaseDueToExpiration(
    transportMessage: ITransportMessagePayload<IUnscheduleClosePurchaseDueToExpirationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CANCEL_PURCHASE_EXPIRATION_TRACKING',
      message: transportMessage,
    });
  }

  async unscheduleFinishTicketSales(transportMessage: ITransportMessagePayload<IUnscheduleFinishTicketSalesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CANCEL_TICKET_SALES_SCHEDULE_FINISH_TRACKING',
      message: transportMessage,
    });
  }
}
