import {
  IAttachFilesToMuseumCommand,
  IAttachFilesToPreviewMuseumCommand,
  ICheckAffiliationOfMuseumsToOrganizationQuery,
  ICheckAffiliationOfMuseumsToOrganizationResponse,
  IEnsureMuseumsAvailabilityQuery,
  IEnsureMuseumsAvailabilityResponse,
  IFetchActiveMuseumsIdsQuery,
  IFetchActiveMuseumsIdsResponse,
  IFetchEventSellerQuery,
  IFetchEventSellerResponse,
  IFetchMuseumAttachedFilesQuery,
  IFetchMuseumAttachedFilesResponse,
  IFetchMuseumsDataForReportQuery,
  IFetchMuseumsDataForReportResponse,
  IFetchMuseumWorkingScheduleQuery,
  IFetchMuseumWorkingScheduleResponse,
  IFetchPreviewMuseumAttachedFilesQuery,
  IFetchPreviewMuseumAttachedFilesResponse,
  IFetchVisitedMuseumForPurchaseQuery,
  IFetchVisitedMuseumForPurchaseResponse,
  IGetListMuseumsForWidgetQuery,
  IGetListMuseumsForWidgetResponse,
  IGetVisitedMuseumForNomenclatureQuery,
  IGetVisitedMuseumForNomenclatureResponse,
  IIncrementMuseumViewsCounterCommand,
  IListMuseumsByEventAddressQuery,
  IListMuseumsByEventAddressResponse,
  IPublishMuseumToCatalogCommand,
  IPurgeMuseumsOfOrganizationCommand,
  IRecalcMuseumTresholdPricesCommand,
  IRemoveMuseumFromCatalogCommand,
  IRequireAdditionalInformationForMuseumsOfAssignedOrganizationCommand,
  IUnmarkFreeMuseumCommand,
  IUpdateMuseumInCatalogCommand,
  IUpdateMuseumSalesCounterCommand,
  IViewMuseumModerationQueuePlenumQuery,
  IViewMuseumModerationQueuePlenumResponse,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IMuseumAppModuleFacade } from './i-museum-app-module-facade';

export class MuseumAppModuleFacade implements IMuseumAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async attachFilesToMuseum(transportMessage: ITransportMessagePayload<IAttachFilesToMuseumCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'ADMIN_ATTACH_FILES',
      payload: transportMessage,
    });
  }

  async attachFilesToPreviewMuseum(transportMessage: ITransportMessagePayload<IAttachFilesToPreviewMuseumCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'CONTENT_MANAGER_ATTACH_FILES',
      payload: transportMessage,
    });
  }

  checkAffiliationOfMuseumsToOrganization(
    transportMessage: ITransportMessagePayload<ICheckAffiliationOfMuseumsToOrganizationQuery>
  ): Promise<ICheckAffiliationOfMuseumsToOrganizationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'CHECK_AFFILIATION_OF_MUSEUMS_TO_ORGANIZATION',
      payload: transportMessage,
    });
  }

  ensureMuseumsAvailability(
    transportMessage: ITransportMessagePayload<IEnsureMuseumsAvailabilityQuery>
  ): Promise<IEnsureMuseumsAvailabilityResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'CHECK_MUSEUMS_ASSIGNED_TO_CONTROLLER',
      payload: transportMessage,
    });
  }

  fetchActiveMuseumsIds(transportMessage: ITransportMessagePayload<IFetchActiveMuseumsIdsQuery>): Promise<IFetchActiveMuseumsIdsResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'GET_ACTIVE_MUSEUMS_IDS',
      payload: transportMessage,
    });
  }

  fetchEventSeller(transportMessage: ITransportMessagePayload<IFetchEventSellerQuery>): Promise<IFetchEventSellerResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'GET_SELLER_MUSEUM_FOR_EVENT',
      payload: transportMessage,
    });
  }

  fetchMuseumAttachedFiles(transportMessage: ITransportMessagePayload<IFetchMuseumAttachedFilesQuery>): Promise<IFetchMuseumAttachedFilesResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'ADMIN_VIEW_MUSEUM_BEFORE_ATTACH_FILES',
      payload: transportMessage,
    });
  }

  fetchMuseumsDataForReport(
    transportMessage: ITransportMessagePayload<IFetchMuseumsDataForReportQuery>
  ): Promise<IFetchMuseumsDataForReportResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'FETCH_MUSEUMS_DATA_FOR_REPORT',
      payload: transportMessage,
    });
  }

  fetchMuseumWorkingSchedule(
    transportMessage: ITransportMessagePayload<IFetchMuseumWorkingScheduleQuery>
  ): Promise<IFetchMuseumWorkingScheduleResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'FETCH_MUSEUM_WORKING_SETTINGS',
      payload: transportMessage,
    });
  }

  fetchPreviewMuseumAttachedFiles(
    transportMessage: ITransportMessagePayload<IFetchPreviewMuseumAttachedFilesQuery>
  ): Promise<IFetchPreviewMuseumAttachedFilesResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'CONTENT_MANAGER_VIEW_MUSEUM_BEFORE_ATTACH_FILES',
      payload: transportMessage,
    });
  }

  fetchVisitedMuseumForPurchase(
    transportMessage: ITransportMessagePayload<IFetchVisitedMuseumForPurchaseQuery>
  ): Promise<IFetchVisitedMuseumForPurchaseResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'FETCH_BEFORE_PURCHASE',
      payload: transportMessage,
    });
  }

  getListMuseumsForWidget(transportMessage: ITransportMessagePayload<IGetListMuseumsForWidgetQuery>): Promise<IGetListMuseumsForWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'GET_LIST_MUSEUMS_FOR_WIDGET',
      payload: transportMessage,
    });
  }

  getVisitedMuseumForNomenclature(
    transportMessage: ITransportMessagePayload<IGetVisitedMuseumForNomenclatureQuery>
  ): Promise<IGetVisitedMuseumForNomenclatureResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'CHECK_MUSEUM_BY_TICKET',
      payload: transportMessage,
    });
  }

  async incrementMuseumViewsCounter(transportMessage: ITransportMessagePayload<IIncrementMuseumViewsCounterCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INCREMENT_MUSEUM_VIEWS_COUNTER',
      message: transportMessage,
    });
  }

  listMuseumsByEventAddress(
    transportMessage: ITransportMessagePayload<IListMuseumsByEventAddressQuery>
  ): Promise<IListMuseumsByEventAddressResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'LIST_MUSEUMS_BY_EVENT_ADDRESSES',
      payload: transportMessage,
    });
  }

  async publishMuseumToCatalog(transportMessage: ITransportMessagePayload<IPublishMuseumToCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PUBLISH_MUSEUM_TO_CATALOG',
      message: transportMessage,
    });
  }

  async purgeMuseumsOfOrganization(transportMessage: ITransportMessagePayload<IPurgeMuseumsOfOrganizationCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'DELETE_MUSEUMS_OF_DELETED_ORGANIZATION',
      message: transportMessage,
    });
  }

  async recalcMuseumTresholdPrices(transportMessage: ITransportMessagePayload<IRecalcMuseumTresholdPricesCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'RECALC_MUSEUM_THRESHOLD_PRICES',
      message: transportMessage,
    });
  }

  async removeMuseumFromCatalog(transportMessage: ITransportMessagePayload<IRemoveMuseumFromCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REMOVE_MUSEUM_FROM_CATALOG',
      message: transportMessage,
    });
  }

  async requireAdditionalInformationForMuseumsOfAssignedOrganization(
    transportMessage: ITransportMessagePayload<IRequireAdditionalInformationForMuseumsOfAssignedOrganizationCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'HIDE_MUSEUMS_FROM_CONTENT_MANAGER',
      message: transportMessage,
    });
  }

  async unmarkFreeMuseum(transportMessage: ITransportMessagePayload<IUnmarkFreeMuseumCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UNMARK_FREE_MUSEUM',
      message: transportMessage,
    });
  }

  async updateMuseumInCatalog(transportMessage: ITransportMessagePayload<IUpdateMuseumInCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_MUSEUM_IN_CATALOG',
      message: transportMessage,
    });
  }

  async updateMuseumSalesCounter(transportMessage: ITransportMessagePayload<IUpdateMuseumSalesCounterCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_MUSEUM_SALES_COUNTER',
      message: transportMessage,
    });
  }

  viewMuseumModerationQueuePlenum(
    transportMessage: ITransportMessagePayload<IViewMuseumModerationQueuePlenumQuery>
  ): Promise<IViewMuseumModerationQueuePlenumResponse[]> {
    return this.VmuzeyRpcProvider.call({
      service: 'museum',
      method: 'COUNT_PENDING_BY_ORGANIZATIONS',
      payload: transportMessage,
    });
  }
}
