import {
  IAttachFilesToMuseumCommand,
  IAttachFilesToPreviewMuseumCommand,
  ICheckAffiliationOfMuseumsToOrganizationQuery,
  ICheckAffiliationOfMuseumsToOrganizationResponse,
  IEnsureMuseumsAvailabilityQuery,
  IEnsureMuseumsAvailabilityResponse,
  IFetchActiveMuseumsIdsQuery,
  IFetchActiveMuseumsIdsResponse,
  IFetchEventSellerQuery,
  IFetchEventSellerResponse,
  IFetchMuseumAttachedFilesQuery,
  IFetchMuseumAttachedFilesResponse,
  IFetchMuseumsDataForReportQuery,
  IFetchMuseumsDataForReportResponse,
  IFetchMuseumWorkingScheduleQuery,
  IFetchMuseumWorkingScheduleResponse,
  IFetchPreviewMuseumAttachedFilesQuery,
  IFetchPreviewMuseumAttachedFilesResponse,
  IFetchVisitedMuseumForPurchaseQuery,
  IFetchVisitedMuseumForPurchaseResponse,
  IGetListMuseumsForWidgetQuery,
  IGetListMuseumsForWidgetResponse,
  IGetVisitedMuseumForNomenclatureQuery,
  IGetVisitedMuseumForNomenclatureResponse,
  IIncrementMuseumViewsCounterCommand,
  IListMuseumsByEventAddressQuery,
  IListMuseumsByEventAddressResponse,
  IPublishMuseumToCatalogCommand,
  IPurgeMuseumsOfOrganizationCommand,
  IRecalcMuseumTresholdPricesCommand,
  IRemoveMuseumFromCatalogCommand,
  IRequireAdditionalInformationForMuseumsOfAssignedOrganizationCommand,
  IUnmarkFreeMuseumCommand,
  IUpdateMuseumInCatalogCommand,
  IUpdateMuseumSalesCounterCommand,
  IViewMuseumModerationQueuePlenumQuery,
  IViewMuseumModerationQueuePlenumResponse,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IMuseumAppModuleFacade {
  attachFilesToMuseum(transportMessage: ITransportMessagePayload<IAttachFilesToMuseumCommand>): Promise<void>;
  attachFilesToPreviewMuseum(transportMessage: ITransportMessagePayload<IAttachFilesToPreviewMuseumCommand>): Promise<void>;
  checkAffiliationOfMuseumsToOrganization(
    transportMessage: ITransportMessagePayload<ICheckAffiliationOfMuseumsToOrganizationQuery>
  ): Promise<ICheckAffiliationOfMuseumsToOrganizationResponse>;
  ensureMuseumsAvailability(transportMessage: ITransportMessagePayload<IEnsureMuseumsAvailabilityQuery>): Promise<IEnsureMuseumsAvailabilityResponse>;
  fetchActiveMuseumsIds(transportMessage: ITransportMessagePayload<IFetchActiveMuseumsIdsQuery>): Promise<IFetchActiveMuseumsIdsResponse>;
  fetchEventSeller(transportMessage: ITransportMessagePayload<IFetchEventSellerQuery>): Promise<IFetchEventSellerResponse>;
  fetchMuseumAttachedFiles(transportMessage: ITransportMessagePayload<IFetchMuseumAttachedFilesQuery>): Promise<IFetchMuseumAttachedFilesResponse>;
  fetchMuseumsDataForReport(transportMessage: ITransportMessagePayload<IFetchMuseumsDataForReportQuery>): Promise<IFetchMuseumsDataForReportResponse>;
  fetchMuseumWorkingSchedule(
    transportMessage: ITransportMessagePayload<IFetchMuseumWorkingScheduleQuery>
  ): Promise<IFetchMuseumWorkingScheduleResponse>;
  fetchPreviewMuseumAttachedFiles(
    transportMessage: ITransportMessagePayload<IFetchPreviewMuseumAttachedFilesQuery>
  ): Promise<IFetchPreviewMuseumAttachedFilesResponse>;
  fetchVisitedMuseumForPurchase(
    transportMessage: ITransportMessagePayload<IFetchVisitedMuseumForPurchaseQuery>
  ): Promise<IFetchVisitedMuseumForPurchaseResponse>;
  getListMuseumsForWidget(transportMessage: ITransportMessagePayload<IGetListMuseumsForWidgetQuery>): Promise<IGetListMuseumsForWidgetResponse>;
  getVisitedMuseumForNomenclature(
    transportMessage: ITransportMessagePayload<IGetVisitedMuseumForNomenclatureQuery>
  ): Promise<IGetVisitedMuseumForNomenclatureResponse>;
  incrementMuseumViewsCounter(transportMessage: ITransportMessagePayload<IIncrementMuseumViewsCounterCommand>): Promise<void>;
  listMuseumsByEventAddress(transportMessage: ITransportMessagePayload<IListMuseumsByEventAddressQuery>): Promise<IListMuseumsByEventAddressResponse>;
  publishMuseumToCatalog(transportMessage: ITransportMessagePayload<IPublishMuseumToCatalogCommand>): Promise<void>;
  purgeMuseumsOfOrganization(transportMessage: ITransportMessagePayload<IPurgeMuseumsOfOrganizationCommand>): Promise<void>;
  recalcMuseumTresholdPrices(transportMessage: ITransportMessagePayload<IRecalcMuseumTresholdPricesCommand>): Promise<void>;
  removeMuseumFromCatalog(transportMessage: ITransportMessagePayload<IRemoveMuseumFromCatalogCommand>): Promise<void>;
  requireAdditionalInformationForMuseumsOfAssignedOrganization(
    transportMessage: ITransportMessagePayload<IRequireAdditionalInformationForMuseumsOfAssignedOrganizationCommand>
  ): Promise<void>;
  unmarkFreeMuseum(transportMessage: ITransportMessagePayload<IUnmarkFreeMuseumCommand>): Promise<void>;
  updateMuseumInCatalog(transportMessage: ITransportMessagePayload<IUpdateMuseumInCatalogCommand>): Promise<void>;
  updateMuseumSalesCounter(transportMessage: ITransportMessagePayload<IUpdateMuseumSalesCounterCommand>): Promise<void>;
  viewMuseumModerationQueuePlenum(
    transportMessage: ITransportMessagePayload<IViewMuseumModerationQueuePlenumQuery>
  ): Promise<IViewMuseumModerationQueuePlenumResponse[]>;
}
