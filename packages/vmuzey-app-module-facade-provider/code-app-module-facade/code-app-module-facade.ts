import {
  IGenerateAdminAccountConfirmationCodeCommand,
  IGenerateClientAccountConfirmationCodeCommand,
  IGenerateEmailChangeConfirmCodeCommand,
  IGenerateResetPasswordCodeCommand,
  IRemoveAccountConfirmationCodeQuery,
  IResendAccountConfirmationCodeCommand,
  IValidateAccountConfirmationCodeQuery,
  IValidateAccountConfirmationCodeResponse,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { ICodeAppModuleFacade } from './i-code-app-module-facade';

export class CodeAppModuleFacade implements ICodeAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  validateAccountConfirmationCode(
    messagePayload: ITransportMessagePayload<IValidateAccountConfirmationCodeQuery>
  ): Promise<IValidateAccountConfirmationCodeResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'code',
      method: 'get',
      payload: messagePayload,
    });
  }

  removeAccountConfirmationCode(messagePayload: ITransportMessagePayload<IRemoveAccountConfirmationCodeQuery>): Promise<void> {
    return this.VmuzeyRpcProvider.call({
      service: 'code',
      method: 'delete',
      payload: messagePayload,
    });
  }

  useAccountConfirmationCode(
    messagePayload: ITransportMessagePayload<IValidateAccountConfirmationCodeQuery>
  ): Promise<IValidateAccountConfirmationCodeResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'code',
      method: 'use',
      payload: messagePayload,
    });
  }

  resendAccountConfirmationCode(messagePayload: ITransportMessagePayload<IResendAccountConfirmationCodeCommand>): Promise<void> {
    return this.VmuzeyRpcProvider.call({
      service: 'code',
      method: 'resend',
      payload: messagePayload,
    });
  }

  async generateResetPasswordCode(messagePayload: ITransportMessagePayload<IGenerateResetPasswordCodeCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'RESET_PASSWORD_REQUESTED',
      message: messagePayload,
    });
  }

  async generateClientAccountConfirmationCode(
    messagePayload: ITransportMessagePayload<IGenerateClientAccountConfirmationCodeCommand>
  ): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CLIENT_ACCOUNT_CREATED',
      message: messagePayload,
    });
  }

  async generateAdminAccountConfirmationCode(messagePayload: ITransportMessagePayload<IGenerateAdminAccountConfirmationCodeCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ADMIN_ACCOUNT_CREATED',
      message: messagePayload,
    });
  }

  async generateEmailChangeConfirmCode(messagePayload: ITransportMessagePayload<IGenerateEmailChangeConfirmCodeCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CLIENT_EMAIL_CHANGED',
      message: messagePayload,
    });
  }
}
