import {
  IGenerateAdminAccountConfirmationCodeCommand,
  IGenerateClientAccountConfirmationCodeCommand,
  IGenerateEmailChangeConfirmCodeCommand,
  IGenerateResetPasswordCodeCommand,
  IRemoveAccountConfirmationCodeQuery,
  IResendAccountConfirmationCodeCommand,
  IValidateAccountConfirmationCodeQuery,
  IValidateAccountConfirmationCodeResponse,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface ICodeAppModuleFacade {
  validateAccountConfirmationCode(
    messagePayload: ITransportMessagePayload<IValidateAccountConfirmationCodeQuery>
  ): Promise<IValidateAccountConfirmationCodeResponse>;

  removeAccountConfirmationCode(messagePayload: ITransportMessagePayload<IRemoveAccountConfirmationCodeQuery>): Promise<void>;

  resendAccountConfirmationCode(messagePayload: ITransportMessagePayload<IResendAccountConfirmationCodeCommand>): Promise<void>;

  generateResetPasswordCode(messagePayload: ITransportMessagePayload<IGenerateResetPasswordCodeCommand>): Promise<void>;

  generateClientAccountConfirmationCode(messagePayload: ITransportMessagePayload<IGenerateClientAccountConfirmationCodeCommand>): Promise<void>;

  generateAdminAccountConfirmationCode(messagePayload: ITransportMessagePayload<IGenerateAdminAccountConfirmationCodeCommand>): Promise<void>;

  useAccountConfirmationCode(
    messagePayload: ITransportMessagePayload<IValidateAccountConfirmationCodeQuery>
  ): Promise<IValidateAccountConfirmationCodeResponse>;

  generateEmailChangeConfirmCode(messagePayload: ITransportMessagePayload<IGenerateEmailChangeConfirmCodeCommand>): Promise<void>;
}
