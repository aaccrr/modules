import {
  IAddLatestClosingReportToListingCommand,
  ICalculateMonthlyReportsForOrganizationQuery,
  ICalculateMonthlyReportsForOrganizationResponse,
  IIndexCreatedPurchaseCommand,
  IIndexExpiredEntranceTicketCommand,
  IIndexPaidPurchaseCommand,
  IIndexPassedEntranceTicketCommand,
  IIndexPassedPurchaseCommand,
  IIndexRefundedEntranceTicketCommand,
  IIndexRefundedPurchaseCommand,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IReportingAppModuleFacade {
  addLatestClosingReportToListing(transportMessage: ITransportMessagePayload<IAddLatestClosingReportToListingCommand>): Promise<void>;
  calculateMonthlyReportsForOrganization(
    transportMessage: ITransportMessagePayload<ICalculateMonthlyReportsForOrganizationQuery>
  ): Promise<ICalculateMonthlyReportsForOrganizationResponse>;
  indexCreatedPurchase(transportMessage: ITransportMessagePayload<IIndexCreatedPurchaseCommand>): Promise<void>;
  indexPaidPurchase(transportMessage: ITransportMessagePayload<IIndexPaidPurchaseCommand>): Promise<void>;
  indexRefundedPurchase(transportMessage: ITransportMessagePayload<IIndexRefundedPurchaseCommand>): Promise<void>;
  indexPassedPurchase(transportMessage: ITransportMessagePayload<IIndexPassedPurchaseCommand>): Promise<void>;
  indexExpiredEntranceTicket(transportMessage: ITransportMessagePayload<IIndexExpiredEntranceTicketCommand>): Promise<void>;
  indexPassedEntranceTicket(transportMessage: ITransportMessagePayload<IIndexPassedEntranceTicketCommand>): Promise<void>;
  indexRefundedEntranceTicket(transportMessage: ITransportMessagePayload<IIndexRefundedEntranceTicketCommand>): Promise<void>;
}
