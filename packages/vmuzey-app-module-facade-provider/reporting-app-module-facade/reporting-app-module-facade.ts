import {
  IAddLatestClosingReportToListingCommand,
  ICalculateMonthlyReportsForOrganizationQuery,
  ICalculateMonthlyReportsForOrganizationResponse,
  IIndexCreatedPurchaseCommand,
  IIndexExpiredEntranceTicketCommand,
  IIndexPaidPurchaseCommand,
  IIndexPassedEntranceTicketCommand,
  IIndexPassedPurchaseCommand,
  IIndexRefundedEntranceTicketCommand,
  IIndexRefundedPurchaseCommand,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IReportingAppModuleFacade } from './i-reporting-app-module-facade';

export class ReportingAppModuleFacade implements IReportingAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async addLatestClosingReportToListing(transportMessage: ITransportMessagePayload<IAddLatestClosingReportToListingCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ADD_LATEST_CLOSING_REPORT_TO_LISTING',
      message: transportMessage,
    });
  }

  calculateMonthlyReportsForOrganization(
    transportMessage: ITransportMessagePayload<ICalculateMonthlyReportsForOrganizationQuery>
  ): Promise<ICalculateMonthlyReportsForOrganizationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'reporting',
      method: 'CALCULATE_MONTHLY_REPORTS_FOR_ORGANIZATION',
      payload: transportMessage,
    });
  }

  async indexCreatedPurchase(transportMessage: ITransportMessagePayload<IIndexCreatedPurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_CREATED_PURCHASE',
      message: transportMessage,
    });
  }

  async indexPaidPurchase(transportMessage: ITransportMessagePayload<IIndexPaidPurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_PAID_PURCHASE',
      message: transportMessage,
    });
  }

  async indexRefundedPurchase(transportMessage: ITransportMessagePayload<IIndexRefundedPurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_REFUNDED_PURCHASE',
      message: transportMessage,
    });
  }

  async indexExpiredEntranceTicket(transportMessage: ITransportMessagePayload<IIndexExpiredEntranceTicketCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_EXPIRED_ENTRANCE_TICKET',
      message: transportMessage,
    });
  }

  async indexPassedPurchase(transportMessage: ITransportMessagePayload<IIndexPassedPurchaseCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_PASSED_PURCHASE',
      message: transportMessage,
    });
  }

  async indexPassedEntranceTicket(transportMessage: ITransportMessagePayload<IIndexPassedEntranceTicketCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_PASSED_ENTRANCE_TICKET',
      message: transportMessage,
    });
  }

  async indexRefundedEntranceTicket(transportMessage: ITransportMessagePayload<IIndexRefundedEntranceTicketCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INDEX_REFUNDED_ENTRANCE_TICKET',
      message: transportMessage,
    });
  }
}
