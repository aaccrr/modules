import {
  IAutomaticApproveRefundCommand,
  ICheckPurchaseOwnershipQuery,
  ICheckPurchaseOwnershipResponse,
  ICheckPurchasePaymentPossibilityQuery,
  ICheckPurchasePaymentPossibilityResponse,
  IClosePurchaseDueToExpirationCommand,
  IClosePurchaseDueToRefundCommand,
  IClosePurchaseDueToUnpayCommand,
  IConfirmPurchasePaymentCommand,
  ICreatePurchaseFromWidgetCommand,
  ICreatePurchaseFromWidgetResponse,
  IGetLastPurchaseExpirationQuery,
  IGetLastPurchaseExpirationResponse,
  IRefundPurchaseDueToServiceCancellationCommand,
  IShowPrintedPurchaseToCustomerCommand,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IOrderingAppModuleFacade } from './i-ordering-app-module-facade';

export class OrderingAppModuleFacade implements IOrderingAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async automaticApproveRefund(transportMessage: ITransportMessagePayload<IAutomaticApproveRefundCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'AUTOMATIC_REFUND_APPROVAL',
      message: transportMessage,
    });
  }

  checkPurchaseOwnership(transportMessage: ITransportMessagePayload<ICheckPurchaseOwnershipQuery>): Promise<ICheckPurchaseOwnershipResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'CAN_VIEW_PRINTED_PURCHASE',
      payload: transportMessage,
    });
  }

  checkPurchasePaymentPossibility(
    transportMessage: ITransportMessagePayload<ICheckPurchasePaymentPossibilityQuery>
  ): Promise<ICheckPurchasePaymentPossibilityResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'CHECK_PURCHASE_PAYMENT_POSSIBILITY',
      payload: transportMessage,
    });
  }

  async closePurchaseDueToExpiration(transportMessage: ITransportMessagePayload<IClosePurchaseDueToExpirationCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'CLOSE_PURCHASE_DUE_TO_EXPIRATION',
      payload: transportMessage,
    });
  }

  async closePurchaseDueToRefund(transportMessage: ITransportMessagePayload<IClosePurchaseDueToRefundCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REFUND_PROCESSED_BY_PAYMENT_SYSTEM',
      message: transportMessage,
    });
  }

  async closePurchaseDueToUnpay(transportMessage: ITransportMessagePayload<IClosePurchaseDueToUnpayCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'CLOSE_PURCHASE_DUE_TO_UNPAY',
      payload: transportMessage,
    });
  }

  async confirmPurchasePayment(transportMessage: ITransportMessagePayload<IConfirmPurchasePaymentCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CONFIRM_PURCHASE_PAYMENT',
      message: transportMessage,
    });
  }

  getLastPurchaseExpiration(
    transportMessage: ITransportMessagePayload<IGetLastPurchaseExpirationQuery>
  ): Promise<IGetLastPurchaseExpirationResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'SET_LAST_ORDER_EXPIRATION',
      payload: transportMessage,
    });
  }

  async refundPurchaseDueToServiceCancellation(
    transportMessage: ITransportMessagePayload<IRefundPurchaseDueToServiceCancellationCommand>
  ): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'REFUND_PURCHASE_DUE_TO_SERVICE_CANCELLATION',
      payload: transportMessage,
    });
  }

  async showPrintedPurchaseToCustomer(transportMessage: ITransportMessagePayload<IShowPrintedPurchaseToCustomerCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'SHOW_PRINTED_PURCHASE_TO_CUSTOMER',
      message: transportMessage,
    });
  }

  createPurchaseFromWidget(transportMessage: ITransportMessagePayload<ICreatePurchaseFromWidgetCommand>): Promise<ICreatePurchaseFromWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'CREATE_PURCHASE_FROM_WIDGET',
      payload: transportMessage,
    });
  }
}
