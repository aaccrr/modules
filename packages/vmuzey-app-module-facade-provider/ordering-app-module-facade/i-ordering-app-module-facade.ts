import {
  ICheckPurchasePaymentPossibilityResponse,
  IGetLastPurchaseExpirationQuery,
  IAutomaticApproveRefundCommand,
  ICheckPurchaseOwnershipQuery,
  ICheckPurchaseOwnershipResponse,
  ICheckPurchasePaymentPossibilityQuery,
  IClosePurchaseDueToExpirationCommand,
  IClosePurchaseDueToUnpayCommand,
  IClosePurchaseDueToRefundCommand,
  IConfirmPurchasePaymentCommand,
  IRefundPurchaseDueToServiceCancellationCommand,
  IGetLastPurchaseExpirationResponse,
  IShowPrintedPurchaseToCustomerCommand,
  ICreatePurchaseFromWidgetCommand,
  ICreatePurchaseFromWidgetResponse,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IOrderingAppModuleFacade {
  automaticApproveRefund(transportMessage: ITransportMessagePayload<IAutomaticApproveRefundCommand>): Promise<void>;
  checkPurchaseOwnership(transportMessage: ITransportMessagePayload<ICheckPurchaseOwnershipQuery>): Promise<ICheckPurchaseOwnershipResponse>;
  checkPurchasePaymentPossibility(
    transportMessage: ITransportMessagePayload<ICheckPurchasePaymentPossibilityQuery>
  ): Promise<ICheckPurchasePaymentPossibilityResponse>;
  closePurchaseDueToExpiration(transportMessage: ITransportMessagePayload<IClosePurchaseDueToExpirationCommand>): Promise<void>;
  closePurchaseDueToRefund(transportMessage: ITransportMessagePayload<IClosePurchaseDueToRefundCommand>): Promise<void>;
  closePurchaseDueToUnpay(transportMessage: ITransportMessagePayload<IClosePurchaseDueToUnpayCommand>): Promise<void>;
  confirmPurchasePayment(transportMessage: ITransportMessagePayload<IConfirmPurchasePaymentCommand>): Promise<void>;
  getLastPurchaseExpiration(transportMessage: ITransportMessagePayload<IGetLastPurchaseExpirationQuery>): Promise<IGetLastPurchaseExpirationResponse>;
  refundPurchaseDueToServiceCancellation(transportMessage: ITransportMessagePayload<IRefundPurchaseDueToServiceCancellationCommand>): Promise<void>;
  showPrintedPurchaseToCustomer(transportMessage: ITransportMessagePayload<IShowPrintedPurchaseToCustomerCommand>): Promise<void>;
  createPurchaseFromWidget(transportMessage: ITransportMessagePayload<ICreatePurchaseFromWidgetCommand>): Promise<ICreatePurchaseFromWidgetResponse>;
}
