import {
  IDetectLocationForCatalogSearchQuery,
  IListGeographyObjectsIdsQuery,
  IListGeographyObjectsIdsResponse,
  IFetchLocationByIpQuery,
  IFetchLocationByIpResponse,
  IFetchLocationByGeoQuery,
  IFetchLocationByGeoResponse,
  IListLocationsByLocalitiesIdsQuery,
  IListLocationsByLocalitiesIdsResponse,
  IFetchLocationByLocalityIdQuery,
  IFetchLocationByLocalityIdResponse,
  IFetchLocationBySubjectAndLocalityQuery,
  IFetchLocationBySubjectAndLocalityResponse,
  IDetectLocationForCatalogSearchResponse,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IGeographyAppModuleFacade {
  listGeographyObjectsIds(transportMessage: ITransportMessagePayload<IListGeographyObjectsIdsQuery>): Promise<IListGeographyObjectsIdsResponse>;
  fetchLocationByIp(transportMessage: ITransportMessagePayload<IFetchLocationByIpQuery>): Promise<IFetchLocationByIpResponse>;
  fetchLocationByGeo(transportMessage: ITransportMessagePayload<IFetchLocationByGeoQuery>): Promise<IFetchLocationByGeoResponse>;
  listLocationsByLocalitiesIds(
    transportMessage: ITransportMessagePayload<IListLocationsByLocalitiesIdsQuery>
  ): Promise<IListLocationsByLocalitiesIdsResponse>;
  fetchLocationByLocalityId(transportMessage: ITransportMessagePayload<IFetchLocationByLocalityIdQuery>): Promise<IFetchLocationByLocalityIdResponse>;
  fetchLocationBySubjectAndLocality(
    transportMessage: ITransportMessagePayload<IFetchLocationBySubjectAndLocalityQuery>
  ): Promise<IFetchLocationBySubjectAndLocalityResponse>;
  detectLocationForCatalogSearch(
    transportMessage: ITransportMessagePayload<IDetectLocationForCatalogSearchQuery>
  ): Promise<IDetectLocationForCatalogSearchResponse>;
}
