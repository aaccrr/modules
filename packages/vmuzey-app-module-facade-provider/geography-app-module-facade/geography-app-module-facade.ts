import {
  IDetectLocationForCatalogSearchQuery,
  IDetectLocationForCatalogSearchResponse,
  IFetchLocationByGeoQuery,
  IFetchLocationByGeoResponse,
  IFetchLocationByIpQuery,
  IFetchLocationByIpResponse,
  IFetchLocationByLocalityIdQuery,
  IFetchLocationByLocalityIdResponse,
  IFetchLocationBySubjectAndLocalityQuery,
  IFetchLocationBySubjectAndLocalityResponse,
  IListGeographyObjectsIdsQuery,
  IListGeographyObjectsIdsResponse,
  IListLocationsByLocalitiesIdsQuery,
  IListLocationsByLocalitiesIdsResponse,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IGeographyAppModuleFacade } from './i-geography-app-module-facade';

export class GeographyAppModuleFacade implements IGeographyAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  listGeographyObjectsIds(transportMessage: ITransportMessagePayload<IListGeographyObjectsIdsQuery>): Promise<IListGeographyObjectsIdsResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'GET_GEOGRAPHY_OBJECTS_IDS',
      payload: transportMessage,
    });
  }

  fetchLocationByIp(transportMessage: ITransportMessagePayload<IFetchLocationByIpQuery>): Promise<IFetchLocationByIpResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'GET_LOCATION_BY_IP',
      payload: transportMessage,
    });
  }

  fetchLocationByGeo(transportMessage: ITransportMessagePayload<IFetchLocationByGeoQuery>): Promise<IFetchLocationByGeoResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'locationByGeo',
      payload: transportMessage,
    });
  }

  listLocationsByLocalitiesIds(
    transportMessage: ITransportMessagePayload<IListLocationsByLocalitiesIdsQuery>
  ): Promise<IListLocationsByLocalitiesIdsResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'GET_LIST_LOCATIONS_BY_LOCALITIES_IDS',
      payload: transportMessage,
    });
  }

  fetchLocationByLocalityId(
    transportMessage: ITransportMessagePayload<IFetchLocationByLocalityIdQuery>
  ): Promise<IFetchLocationByLocalityIdResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'GET_LOCATIONS_BY_LOCALITY_ID',
      payload: transportMessage,
    });
  }

  fetchLocationBySubjectAndLocality(
    transportMessage: ITransportMessagePayload<IFetchLocationBySubjectAndLocalityQuery>
  ): Promise<IFetchLocationBySubjectAndLocalityResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'GET_LOCATION_BY_SUBJECT_AND_LOCALITY',
      payload: transportMessage,
    });
  }

  detectLocationForCatalogSearch(
    transportMessage: ITransportMessagePayload<IDetectLocationForCatalogSearchQuery>
  ): Promise<IDetectLocationForCatalogSearchResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'geography',
      method: 'DETECT_LOCATION_FOR_CATALOG_SEARCH',
      payload: transportMessage,
    });
  }
}
