import {
  IAttachImagesToEventCommand,
  ICalculateNomenclatureQuantityForSaleQuery,
  ICalculateNomenclatureQuantityForSaleResponse,
  ICalculateThresholdPricesInMuseumQuery,
  ICalculateThresholdPricesInMuseumResponse,
  ICancelAbonementCommand,
  ICancelEventCommand,
  ICancelTicketCommand,
  ICheckPossibilityToPurchaseTicketInMuseumQuery,
  ICheckPossibilityToPurchaseTicketInMuseumResponse,
  ICheckPresenceTicketToMuseumQuery,
  ICheckPresenceTicketToMuseumResponse,
  ICloseSeasonTicketSalesCommand,
  IFetchAbonementForPurchaseQuery,
  IFetchAbonementForPurchaseResponse,
  IFetchCanceledAbonementQuery,
  IFetchCanceledAbonementResponse,
  IFetchCanceledEventQuery,
  IFetchCanceledEventResponse,
  IFetchCanceledTicketQuery,
  IFetchCanceledTicketResponse,
  IFetchEventForPurchaseQuery,
  IFetchEventForPurchaseResponse,
  IFetchImagesAttachedToEventQuery,
  IFetchImagesAttachedToEventResponse,
  IFetchNomenclatureDataForReportQuery,
  IFetchNomenclatureDataForReportResponse,
  IFetchPurchasedEventForWidgetQuery,
  IFetchPurchasedEventForWidgetResponse,
  IFinishEventCommand,
  IGetListEntranceTicketsForWidgetQuery,
  IGetListEntranceTicketsForWidgetResponse,
  IGetListOrganizedEventsForWidgetQuery,
  IGetListOrganizedEventsForWidgetResponse,
  IGetListSeasonTicketsForWidgetQuery,
  IGetListSeasonTicketsForWidgetResponse,
  IGetTicketTimezoneQuery,
  IGetTicketTimezoneResponse,
  IIncrementEventViewsCounterCommand,
  IPublishSeasonTicketToCatalogCommand,
  IPublishTicketToCatalogCommand,
  IPurgeNomenclatureOfMuseumCommand,
  IRunTicketSalesCommand,
  IStartEventCommand,
  IStartSalesOnEventCommand,
  ISuspendSalesOnScheduleFinishCommand,
  IUpdateNomenclatureSalesCounterCommand,
  IUpdateSeasonTicketInCatalogCommand,
  IUpdateTicketInCatalogCommand,
  IViewNomenclatureModerationQueuePlenumQuery,
  IViewNomenclatureModerationQueuePlenumResponse,
} from '../../globals';
import {
  IFetchTicketForPurchaseQuery,
  IFetchTicketForPurchaseResponse,
} from '../../globals/interfaces/nomenclature/AppModuleFacade/FetchTicketForPurchase';
import { IGetActiveEventsIdsQuery, IGetActiveEventsIdsResponse } from '../../globals/interfaces/nomenclature/AppModuleFacade/GetActiveEventsIds';
import { IPublishEventToCatalogCommand } from '../../globals/interfaces/nomenclature/AppModuleFacade/PublishEventToCatalog';
import { IRemoveEventFromCatalogCommand } from '../../globals/interfaces/nomenclature/AppModuleFacade/RemoveEventFromCatalog';
import { IUpdateEventInCatalogCommand } from '../../globals/interfaces/nomenclature/AppModuleFacade/UpdateEventInCatalog';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { INomenclatureAppModuleFacade } from './i-nomenclature-app-module-facade';

export class NomenclatureAppModuleFacade implements INomenclatureAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async attachImagesToEvent(transportMessage: ITransportMessagePayload<IAttachImagesToEventCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'AttachEventImages',
      payload: transportMessage,
    });
  }

  calculateNomenclatureQuantityForSale(
    transportMessage: ITransportMessagePayload<ICalculateNomenclatureQuantityForSaleQuery>
  ): Promise<ICalculateNomenclatureQuantityForSaleResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CountSellingNomenclatureByMuseum',
      payload: transportMessage,
    });
  }

  calculateThresholdPricesInMuseum(
    transportMessage: ITransportMessagePayload<ICalculateThresholdPricesInMuseumQuery>
  ): Promise<ICalculateThresholdPricesInMuseumResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'GetTicketThresholdPrices',
      payload: transportMessage,
    });
  }

  async cancelAbonement(transportMessage: ITransportMessagePayload<ICancelAbonementCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CANCEL_ABONEMENT',
      payload: transportMessage,
    });
  }

  async cancelEvent(transportMessage: ITransportMessagePayload<ICancelEventCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CANCEL_EVENT',
      payload: transportMessage,
    });
  }

  async cancelTicket(transportMessage: ITransportMessagePayload<ICancelTicketCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CANCEL_TICKET',
      payload: transportMessage,
    });
  }

  checkPossibilityToPurchaseTicketInMuseum(
    transportMessage: ITransportMessagePayload<ICheckPossibilityToPurchaseTicketInMuseumQuery>
  ): Promise<ICheckPossibilityToPurchaseTicketInMuseumResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'VIEW_TICKET_EXISTENCE_IN_MUSEUM',
      payload: transportMessage,
    });
  }

  async closeSeasonTicketSales(transportMessage: ITransportMessagePayload<ICloseSeasonTicketSalesCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CLOSE_SEASON_TICKET_SALES',
      payload: transportMessage,
    });
  }

  fetchAbonementForPurchase(
    transportMessage: ITransportMessagePayload<IFetchAbonementForPurchaseQuery>
  ): Promise<IFetchAbonementForPurchaseResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_ABONEMENT_BEFORE_PURCHASE',
      payload: transportMessage,
    });
  }

  fetchCanceledAbonement(transportMessage: ITransportMessagePayload<IFetchCanceledAbonementQuery>): Promise<IFetchCanceledAbonementResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_ABONEMENT_BEFORE_CANCELLATION',
      payload: transportMessage,
    });
  }

  fetchCanceledEvent(transportMessage: ITransportMessagePayload<IFetchCanceledEventQuery>): Promise<IFetchCanceledEventResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_EVENT_BEFORE_CANCELLATION',
      payload: transportMessage,
    });
  }

  fetchCanceledTicket(transportMessage: ITransportMessagePayload<IFetchCanceledTicketQuery>): Promise<IFetchCanceledTicketResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_TICKET_BEFORE_CANCELLATION',
      payload: transportMessage,
    });
  }

  fetchImagesAttachedToEvent(
    transportMessage: ITransportMessagePayload<IFetchImagesAttachedToEventQuery>
  ): Promise<IFetchImagesAttachedToEventResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FindEventBeforeAttachingFiles',
      payload: transportMessage,
    });
  }

  fetchEventForPurchase(transportMessage: ITransportMessagePayload<IFetchEventForPurchaseQuery>): Promise<IFetchEventForPurchaseResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_EVENT_BEFORE_PURCHASE',
      payload: transportMessage,
    });
  }

  fetchNomenclatureDataForReport(
    transportMessage: ITransportMessagePayload<IFetchNomenclatureDataForReportQuery>
  ): Promise<IFetchNomenclatureDataForReportResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_NOMENCLATURE_DATA_FOR_REPORT',
      payload: transportMessage,
    });
  }

  fetchTicketForPurchase(transportMessage: ITransportMessagePayload<IFetchTicketForPurchaseQuery>): Promise<IFetchTicketForPurchaseResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_TICKET_BEFORE_PURCHASE',
      payload: transportMessage,
    });
  }

  async finishEvent(transportMessage: ITransportMessagePayload<IFinishEventCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FINISH_EVENT',
      payload: transportMessage,
    });
  }

  getActiveEventsIds(transportMessage: ITransportMessagePayload<IGetActiveEventsIdsQuery>): Promise<IGetActiveEventsIdsResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'GET_ACTIVE_EVENTS_IDS',
      payload: transportMessage,
    });
  }

  getTicketTimezone(transportMessage: ITransportMessagePayload<IGetTicketTimezoneQuery>): Promise<IGetTicketTimezoneResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'GET_ENTRANCE_TICKET_TIMEZONE',
      payload: transportMessage,
    });
  }

  getListEntranceTicketsForWidget(
    transportMessage: ITransportMessagePayload<IGetListEntranceTicketsForWidgetQuery>
  ): Promise<IGetListEntranceTicketsForWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'GET_LIST_ENTRANCE_TICKETS_FOR_WIDGET',
      payload: transportMessage,
    });
  }

  getListOrganizedEventsForWidget(
    transportMessage: ITransportMessagePayload<IGetListOrganizedEventsForWidgetQuery>
  ): Promise<IGetListOrganizedEventsForWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'GET_LIST_ORGANIZED_EVENTS_FOR_WIDGET',
      payload: transportMessage,
    });
  }

  getListSeasonTicketsForWidget(
    transportMessage: ITransportMessagePayload<IGetListSeasonTicketsForWidgetQuery>
  ): Promise<IGetListSeasonTicketsForWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'GET_LIST_SEASON_TICKETS_FOR_WIDGET',
      payload: transportMessage,
    });
  }

  async incrementEventViewsCounter(transportMessage: ITransportMessagePayload<IIncrementEventViewsCounterCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'INCREMENT_EVENT_VIEWS_COUNTER',
      message: transportMessage,
    });
  }

  async publishEventToCatalog(transportMessage: ITransportMessagePayload<IPublishEventToCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PUBLISH_EVENT_TO_CATALOG',
      message: transportMessage,
    });
  }

  async publishSeasonTicketToCatalog(transportMessage: ITransportMessagePayload<IPublishSeasonTicketToCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PUBLISH_SEASON_TICKET_TO_CATALOG',
      message: transportMessage,
    });
  }

  async publishTicketToCatalog(transportMessage: ITransportMessagePayload<IPublishTicketToCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PUBLISH_NEW_TICKET_TO_CATALOG',
      message: transportMessage,
    });
  }

  async purgeNomenclatureOfMuseum(transportMessage: ITransportMessagePayload<IPurgeNomenclatureOfMuseumCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'PURGE_MUSEUM_NOMENCLATURE',
      message: transportMessage,
    });
  }

  async removeEventFromCatalog(transportMessage: ITransportMessagePayload<IRemoveEventFromCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REMOVE_EVENT_FROM_CATALOG',
      message: transportMessage,
    });
  }

  async runTicketSales(transportMessage: ITransportMessagePayload<IRunTicketSalesCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'AUTORUN_TICKET_SALES',
      payload: transportMessage,
    });
  }

  async startEvent(transportMessage: ITransportMessagePayload<IStartEventCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'START_EVENT',
      payload: transportMessage,
    });
  }

  async startSalesOnEvent(transportMessage: ITransportMessagePayload<IStartSalesOnEventCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'START_SALES_ON_EVENT',
      payload: transportMessage,
    });
  }

  async suspendSalesOnScheduleFinish(transportMessage: ITransportMessagePayload<ISuspendSalesOnScheduleFinishCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CLOSE_ENTRANCE_TICKET_SALES',
      payload: transportMessage,
    });
  }

  async updateEventInCatalog(transportMessage: ITransportMessagePayload<IUpdateEventInCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_EVENT_IN_CATALOG',
      message: transportMessage,
    });
  }

  async updateNomenclatureSalesCounter(transportMessage: ITransportMessagePayload<IUpdateNomenclatureSalesCounterCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_NOMENCLATURE_SALES_COUNTER',
      message: transportMessage,
    });
  }

  async updateSeasonTicketInCatalog(transportMessage: ITransportMessagePayload<IUpdateSeasonTicketInCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_SEASON_TICKET_IN_CATALOG',
      message: transportMessage,
    });
  }

  async updateTicketInCatalog(transportMessage: ITransportMessagePayload<IUpdateTicketInCatalogCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_EXISTING_TICKET_IN_CATALOG',
      message: transportMessage,
    });
  }

  viewNomenclatureModerationQueuePlenum(
    transportMessage: ITransportMessagePayload<IViewNomenclatureModerationQueuePlenumQuery>
  ): Promise<IViewNomenclatureModerationQueuePlenumResponse[]> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'COUNT_PENDING_BY_ORGANIZATIONS',
      payload: transportMessage,
    });
  }

  checkPresenceTicketToMuseum(
    transportMessage: ITransportMessagePayload<ICheckPresenceTicketToMuseumQuery>
  ): Promise<ICheckPresenceTicketToMuseumResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'CHECK_PRESENCE_TICKET_TO_MUSEUM',
      payload: transportMessage,
    });
  }

  fetchPurchasedEventForWidget(
    transportMessage: ITransportMessagePayload<IFetchPurchasedEventForWidgetQuery>
  ): Promise<IFetchPurchasedEventForWidgetResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'nomenclature',
      method: 'FETCH_PURCHASED_EVENT_FOR_WIDGET',
      payload: transportMessage,
    });
  }
}
