import {
  IAttachImagesToEventCommand,
  ICalculateNomenclatureQuantityForSaleQuery,
  ICalculateNomenclatureQuantityForSaleResponse,
  ICalculateThresholdPricesInMuseumQuery,
  ICalculateThresholdPricesInMuseumResponse,
  ICancelAbonementCommand,
  ICancelEventCommand,
  ICancelTicketCommand,
  ICheckPossibilityToPurchaseTicketInMuseumQuery,
  ICheckPossibilityToPurchaseTicketInMuseumResponse,
  ICheckPresenceTicketToMuseumQuery,
  ICheckPresenceTicketToMuseumResponse,
  ICloseSeasonTicketSalesCommand,
  IFetchAbonementForPurchaseQuery,
  IFetchAbonementForPurchaseResponse,
  IFetchCanceledAbonementQuery,
  IFetchCanceledAbonementResponse,
  IFetchCanceledEventQuery,
  IFetchCanceledEventResponse,
  IFetchCanceledTicketQuery,
  IFetchCanceledTicketResponse,
  IFetchEventForPurchaseQuery,
  IFetchEventForPurchaseResponse,
  IFetchImagesAttachedToEventQuery,
  IFetchImagesAttachedToEventResponse,
  IFetchNomenclatureDataForReportQuery,
  IFetchNomenclatureDataForReportResponse,
  IFetchPurchasedEventForWidgetQuery,
  IFetchPurchasedEventForWidgetResponse,
  IFinishEventCommand,
  IGetListEntranceTicketsForWidgetQuery,
  IGetListEntranceTicketsForWidgetResponse,
  IGetListOrganizedEventsForWidgetQuery,
  IGetListOrganizedEventsForWidgetResponse,
  IGetListSeasonTicketsForWidgetQuery,
  IGetListSeasonTicketsForWidgetResponse,
  IGetTicketTimezoneQuery,
  IGetTicketTimezoneResponse,
  IIncrementEventViewsCounterCommand,
  IPublishSeasonTicketToCatalogCommand,
  IPublishTicketToCatalogCommand,
  IPurgeNomenclatureOfMuseumCommand,
  IRunTicketSalesCommand,
  IStartEventCommand,
  IStartSalesOnEventCommand,
  ISuspendSalesOnScheduleFinishCommand,
  IUpdateNomenclatureSalesCounterCommand,
  IUpdateSeasonTicketInCatalogCommand,
  IUpdateTicketInCatalogCommand,
  IViewNomenclatureModerationQueuePlenumQuery,
  IViewNomenclatureModerationQueuePlenumResponse,
} from '../../globals';
import {
  IFetchTicketForPurchaseQuery,
  IFetchTicketForPurchaseResponse,
} from '../../globals/interfaces/nomenclature/AppModuleFacade/FetchTicketForPurchase';
import { IGetActiveEventsIdsQuery, IGetActiveEventsIdsResponse } from '../../globals/interfaces/nomenclature/AppModuleFacade/GetActiveEventsIds';
import { IPublishEventToCatalogCommand } from '../../globals/interfaces/nomenclature/AppModuleFacade/PublishEventToCatalog';
import { IRemoveEventFromCatalogCommand } from '../../globals/interfaces/nomenclature/AppModuleFacade/RemoveEventFromCatalog';
import { IUpdateEventInCatalogCommand } from '../../globals/interfaces/nomenclature/AppModuleFacade/UpdateEventInCatalog';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface INomenclatureAppModuleFacade {
  attachImagesToEvent(transportMessage: ITransportMessagePayload<IAttachImagesToEventCommand>): Promise<void>;
  calculateNomenclatureQuantityForSale(
    transportMessage: ITransportMessagePayload<ICalculateNomenclatureQuantityForSaleQuery>
  ): Promise<ICalculateNomenclatureQuantityForSaleResponse>;
  calculateThresholdPricesInMuseum(
    transportMessage: ITransportMessagePayload<ICalculateThresholdPricesInMuseumQuery>
  ): Promise<ICalculateThresholdPricesInMuseumResponse>;
  cancelAbonement(transportMessage: ITransportMessagePayload<ICancelAbonementCommand>): Promise<void>;
  cancelEvent(transportMessage: ITransportMessagePayload<ICancelEventCommand>): Promise<void>;
  cancelTicket(transportMessage: ITransportMessagePayload<ICancelTicketCommand>): Promise<void>;
  checkPossibilityToPurchaseTicketInMuseum(
    transportMessage: ITransportMessagePayload<ICheckPossibilityToPurchaseTicketInMuseumQuery>
  ): Promise<ICheckPossibilityToPurchaseTicketInMuseumResponse>;
  closeSeasonTicketSales(transportMessage: ITransportMessagePayload<ICloseSeasonTicketSalesCommand>): Promise<void>;
  fetchAbonementForPurchase(transportMessage: ITransportMessagePayload<IFetchAbonementForPurchaseQuery>): Promise<IFetchAbonementForPurchaseResponse>;
  fetchCanceledAbonement(transportMessage: ITransportMessagePayload<IFetchCanceledAbonementQuery>): Promise<IFetchCanceledAbonementResponse>;
  fetchCanceledEvent(transportMessage: ITransportMessagePayload<IFetchCanceledEventQuery>): Promise<IFetchCanceledEventResponse>;
  fetchCanceledTicket(transportMessage: ITransportMessagePayload<IFetchCanceledTicketQuery>): Promise<IFetchCanceledTicketResponse>;
  fetchImagesAttachedToEvent(
    transportMessage: ITransportMessagePayload<IFetchImagesAttachedToEventQuery>
  ): Promise<IFetchImagesAttachedToEventResponse>;
  fetchEventForPurchase(transportMessage: ITransportMessagePayload<IFetchEventForPurchaseQuery>): Promise<IFetchEventForPurchaseResponse>;
  fetchNomenclatureDataForReport(
    transportMessage: ITransportMessagePayload<IFetchNomenclatureDataForReportQuery>
  ): Promise<IFetchNomenclatureDataForReportResponse>;
  fetchTicketForPurchase(transportMessage: ITransportMessagePayload<IFetchTicketForPurchaseQuery>): Promise<IFetchTicketForPurchaseResponse>;
  finishEvent(transportMessage: ITransportMessagePayload<IFinishEventCommand>): Promise<void>;
  getActiveEventsIds(transportMessage: ITransportMessagePayload<IGetActiveEventsIdsQuery>): Promise<IGetActiveEventsIdsResponse>;
  getTicketTimezone(transportMessage: ITransportMessagePayload<IGetTicketTimezoneQuery>): Promise<IGetTicketTimezoneResponse>;
  getListEntranceTicketsForWidget(
    transportMessage: ITransportMessagePayload<IGetListEntranceTicketsForWidgetQuery>
  ): Promise<IGetListEntranceTicketsForWidgetResponse>;
  getListOrganizedEventsForWidget(
    transportMessage: ITransportMessagePayload<IGetListOrganizedEventsForWidgetQuery>
  ): Promise<IGetListOrganizedEventsForWidgetResponse>;
  getListSeasonTicketsForWidget(
    transportMessage: ITransportMessagePayload<IGetListSeasonTicketsForWidgetQuery>
  ): Promise<IGetListSeasonTicketsForWidgetResponse>;
  incrementEventViewsCounter(transportMessage: ITransportMessagePayload<IIncrementEventViewsCounterCommand>): Promise<void>;
  publishEventToCatalog(transportMessage: ITransportMessagePayload<IPublishEventToCatalogCommand>): Promise<void>;
  publishSeasonTicketToCatalog(transportMessage: ITransportMessagePayload<IPublishSeasonTicketToCatalogCommand>): Promise<void>;
  publishTicketToCatalog(transportMessage: ITransportMessagePayload<IPublishTicketToCatalogCommand>): Promise<void>;
  purgeNomenclatureOfMuseum(transportMessage: ITransportMessagePayload<IPurgeNomenclatureOfMuseumCommand>): Promise<void>;
  removeEventFromCatalog(transportMessage: ITransportMessagePayload<IRemoveEventFromCatalogCommand>): Promise<void>;
  runTicketSales(transportMessage: ITransportMessagePayload<IRunTicketSalesCommand>): Promise<void>;
  startEvent(transportMessage: ITransportMessagePayload<IStartEventCommand>): Promise<void>;
  startSalesOnEvent(transportMessage: ITransportMessagePayload<IStartSalesOnEventCommand>): Promise<void>;
  suspendSalesOnScheduleFinish(transportMessage: ITransportMessagePayload<ISuspendSalesOnScheduleFinishCommand>): Promise<void>;
  updateEventInCatalog(transportMessage: ITransportMessagePayload<IUpdateEventInCatalogCommand>): Promise<void>;
  updateNomenclatureSalesCounter(transportMessage: ITransportMessagePayload<IUpdateNomenclatureSalesCounterCommand>): Promise<void>;
  updateSeasonTicketInCatalog(transportMessage: ITransportMessagePayload<IUpdateSeasonTicketInCatalogCommand>): Promise<void>;
  updateTicketInCatalog(transportMessage: ITransportMessagePayload<IUpdateTicketInCatalogCommand>): Promise<void>;
  viewNomenclatureModerationQueuePlenum(
    transportMessage: ITransportMessagePayload<IViewNomenclatureModerationQueuePlenumQuery>
  ): Promise<IViewNomenclatureModerationQueuePlenumResponse[]>;
  checkPresenceTicketToMuseum(
    transportMessage: ITransportMessagePayload<ICheckPresenceTicketToMuseumQuery>
  ): Promise<ICheckPresenceTicketToMuseumResponse>;
  fetchPurchasedEventForWidget(
    transportMessage: ITransportMessagePayload<IFetchPurchasedEventForWidgetQuery>
  ): Promise<IFetchPurchasedEventForWidgetResponse>;
}
