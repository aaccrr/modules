import {
  IAddEventStockCommand,
  IAddSeasonTicketStockCommand,
  ICalculateRemainingEventStockQuery,
  ICalculateRemainingEventStockResponse,
  ICleanEventStockCommand,
  ICleanSeasonTicketStockCommand,
  ICleanTicketSalesScheduleCommand,
  IRemoveSelectedTicketIntervalsCommand,
  IRescheduleTicketSalesCommand,
  IReservePurchaseCommand,
  IUpdateSeasonTicketStockCommand,
} from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { IStockAppModuleFacade } from './i-stock-app-module-facade';

export class StockAppModuleFacade implements IStockAppModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async addEventStock(transportMessage: ITransportMessagePayload<IAddEventStockCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'AddEventToStock',
      payload: transportMessage,
    });
  }

  async addSeasonTicketStock(transportMessage: ITransportMessagePayload<IAddSeasonTicketStockCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ADD_SEASON_TICKET_TO_STOCK',
      message: transportMessage,
    });
  }

  calculateRemainingEventStock(
    transportMessage: ITransportMessagePayload<ICalculateRemainingEventStockQuery>
  ): Promise<ICalculateRemainingEventStockResponse> {
    return this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'FetchOnSaleEventPositions',
      payload: transportMessage,
    });
  }

  async cleanEventStock(transportMessage: ITransportMessagePayload<ICleanEventStockCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'EVENT_DELETED',
      message: transportMessage,
    });
  }

  async cleanSeasonTicketStock(transportMessage: ITransportMessagePayload<ICleanSeasonTicketStockCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'CLEAR_SEASON_TICKET_STOCK',
      message: transportMessage,
    });
  }

  async cleanTicketSalesSchedule(transportMessage: ITransportMessagePayload<ICleanTicketSalesScheduleCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'CLEAN_TICKET_SALES_SCHEDULE',
      payload: transportMessage,
    });
  }

  async removeSelectedTicketIntervals(transportMessage: ITransportMessagePayload<IRemoveSelectedTicketIntervalsCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'REMOVE_CANCELED_INTERVALS_FROM_STOCK',
      message: transportMessage,
    });
  }

  async rescheduleTicketSales(transportMessage: ITransportMessagePayload<IRescheduleTicketSalesCommand>): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'RESCHEDULE_TICKET_SALES',
      payload: transportMessage,
    });
  }

  reservePurchase(transportMessage: ITransportMessagePayload<IReservePurchaseCommand>): Promise<void> {
    return this.VmuzeyRpcProvider.call({
      service: 'ordering',
      method: 'RESERVE_PURCHASE',
      payload: transportMessage,
    });
  }

  async updateSeasonTicketStock(transportMessage: ITransportMessagePayload<IUpdateSeasonTicketStockCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'UPDATE_SEASON_TICKET_STOCK',
      message: transportMessage,
    });
  }
}
