import {
  IAddEventStockCommand,
  IAddSeasonTicketStockCommand,
  ICalculateRemainingEventStockQuery,
  ICalculateRemainingEventStockResponse,
  ICleanEventStockCommand,
  ICleanSeasonTicketStockCommand,
  ICleanTicketSalesScheduleCommand,
  IRemoveSelectedTicketIntervalsCommand,
  IRescheduleTicketSalesCommand,
  IReservePurchaseCommand,
  IUpdateSeasonTicketStockCommand,
} from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface IStockAppModuleFacade {
  addEventStock(transportMessage: ITransportMessagePayload<IAddEventStockCommand>): Promise<void>;
  addSeasonTicketStock(transportMessage: ITransportMessagePayload<IAddSeasonTicketStockCommand>): Promise<void>;
  calculateRemainingEventStock(
    transportMessage: ITransportMessagePayload<ICalculateRemainingEventStockQuery>
  ): Promise<ICalculateRemainingEventStockResponse>;
  cleanEventStock(transportMessage: ITransportMessagePayload<ICleanEventStockCommand>): Promise<void>;
  cleanSeasonTicketStock(transportMessage: ITransportMessagePayload<ICleanSeasonTicketStockCommand>): Promise<void>;
  cleanTicketSalesSchedule(transportMessage: ITransportMessagePayload<ICleanTicketSalesScheduleCommand>): Promise<void>;
  removeSelectedTicketIntervals(transportMessage: ITransportMessagePayload<IRemoveSelectedTicketIntervalsCommand>): Promise<void>;
  rescheduleTicketSales(transportMessage: ITransportMessagePayload<IRescheduleTicketSalesCommand>): Promise<void>;
  reservePurchase(transportMessage: ITransportMessagePayload<IReservePurchaseCommand>): Promise<void>;
  updateSeasonTicketStock(transportMessage: ITransportMessagePayload<IUpdateSeasonTicketStockCommand>): Promise<void>;
}
