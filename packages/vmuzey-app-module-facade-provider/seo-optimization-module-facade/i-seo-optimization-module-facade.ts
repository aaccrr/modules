import { IAddSitemapLinkCommand, IDeleteSitemapLinkCommand } from '../../globals';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';

export interface ISeoOptimizationModuleFacade {
  addSitemapLink(transportMessage: ITransportMessagePayload<IAddSitemapLinkCommand>): Promise<void>;
  deleteSitemapLink(transportMessage: ITransportMessagePayload<IDeleteSitemapLinkCommand>): Promise<void>;
  generateDynamicSitemapPart(transportMessage: ITransportMessagePayload): Promise<void>;
  indexExistingSitemapLinks(transportMessage: ITransportMessagePayload): Promise<void>;
}
