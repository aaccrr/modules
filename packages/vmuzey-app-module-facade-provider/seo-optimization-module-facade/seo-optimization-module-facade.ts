import { IAddSitemapLinkCommand, IDeleteSitemapLinkCommand } from '../../globals';
import { IVmuzeyBrokerProducerProvider } from '../../vmuzey-broker-producer-provider';
import { IVmuzeyRpcClientProvider } from '../../vmuzey-rpc-client-provider';
import { ITransportMessagePayload } from '../../vmuzey-transport-layer';
import { ISeoOptimizationModuleFacade } from './i-seo-optimization-module-facade';

export class SeoOptimizationModuleFacade implements ISeoOptimizationModuleFacade {
  constructor(private VmuzeyRpcProvider: IVmuzeyRpcClientProvider, private VmuzeyBrokerProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async addSitemapLink(transportMessage: ITransportMessagePayload<IAddSitemapLinkCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'ADD_SITEMAP_LINK',
      message: transportMessage,
    });
  }

  async deleteSitemapLink(transportMessage: ITransportMessagePayload<IDeleteSitemapLinkCommand>): Promise<void> {
    await this.VmuzeyBrokerProducerProvider.send({
      topic: 'DELETE_SITEMAP_LINK',
      message: transportMessage,
    });
  }

  async generateDynamicSitemapPart(transportMessage: ITransportMessagePayload): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'seo_optimization',
      method: 'GENERATE_DYNAMIC_SITEMAP_PART',
      payload: transportMessage,
    });
  }

  async indexExistingSitemapLinks(transportMessage: ITransportMessagePayload): Promise<void> {
    await this.VmuzeyRpcProvider.call({
      service: 'seo_optimization',
      method: 'INDEX_EXISTING_SITEMAP_LINKS',
      payload: transportMessage,
    });
  }
}
