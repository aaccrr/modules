import { IHttpControllerCallPayload } from '../vmuzey-http-controller-provider';

export interface IVmuzeyGuard {
  validate(request: IHttpControllerCallPayload<any>): void;
}
