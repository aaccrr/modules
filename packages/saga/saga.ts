import { ISagaStep } from "./Interfaces";

export class Saga {
  private steps: ISagaStep[] = [];
  private context: any = { state: {} };

  public addStep(step: ISagaStep) {
    step.isProcessed = false;
    this.steps.push(step);
  }

  public async run(payload) {
    try {
      this.context.payload = payload;
      const res = await this.processStep(this.steps);
      return res;
    } catch (e) {
      const processedSteps = this.steps.filter(step => step.isProcessed);
      const lastStep = processedSteps[processedSteps.length - 1];
      console.log(`ERROR IN "${lastStep.name}" STEP `);
      console.log(e);
      await this.rollback(processedSteps);
      const error: any = new Error(e.message);
      error.code = e.code;
      throw error;
    }
  }

  private async processStep(steps: ISagaStep[], prevStepRes: any = {}) {
    if (!steps.length) return prevStepRes;
    const step = steps[0];
    this.context[step.name] = {};
    this.context[step.name].args = prevStepRes;
    step.isProcessed = true;
    console.time(`STEP "${step.name}" RUN CALLED`);
    const res = await step.run(this.context);
    console.timeEnd(`STEP "${step.name}" RUN CALLED`);
    this.context[step.name].response = res;

    steps = steps.filter(step => !step.isProcessed);
    return await this.processStep(steps, res);
  }

  public async rollback(steps: ISagaStep[]) {
    if (!steps.length) return;
    const step = steps[steps.length - 1];
    console.log(`STEP "${step.name}" ROLLBACK CALLED`);
    await step.rollback(this.context);
    step.isProcessed = false;
    steps = steps.filter(step => step.isProcessed);
    return await this.rollback(steps);
  }
}
