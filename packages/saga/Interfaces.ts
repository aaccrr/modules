export interface ISagaStep {
  name: string;
  run: any;
  rollback: any;
  response?: any;
  isProcessed?: boolean;
  prevStepRes?: any;
}
