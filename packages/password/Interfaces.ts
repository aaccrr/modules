export interface IPassword {
  getHash(password: string, saltRounds?: number): Promise<string>;
  compare(password: string, hash: string): Promise<boolean>;
  generateRandomPassword(): string;
}
