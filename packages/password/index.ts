import { Password } from './password';

const Module = new Password();

export { Module as Password };

export { IPassword } from './Interfaces';
