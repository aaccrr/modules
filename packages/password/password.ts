import { hash, compare } from 'bcryptjs';
import * as cryptoRandomString from 'crypto-random-string';
import { IPassword } from './Interfaces';

export class Password implements IPassword {
  async getHash(password: string, saltRounds: number = 10): Promise<string> {
    return await hash(password, saltRounds);
  }

  async compare(password: string, hash: string): Promise<boolean> {
    return await compare(password, hash);
  }

  generateRandomPassword(): string {
    const password = cryptoRandomString({ length: 10 });
    return password;
  }
}
