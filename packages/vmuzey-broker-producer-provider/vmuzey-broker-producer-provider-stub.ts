import { Kafka, Producer } from 'kafkajs';
import { IProducerSendPayload } from '../broker';
import { IVmuzeyBrokerProducerProvider } from './i-vmuzey-broker-producer-provider';

export class VmuzeyBrokerProducerProviderStub implements IVmuzeyBrokerProducerProvider {
  constructor(private config: any) {}

  private broker: Kafka;
  private producer: Producer;

  async init(): Promise<void> {}

  async send(payload: IProducerSendPayload): Promise<void> {}

  async sendBatch(messages: IProducerSendPayload[]): Promise<void> {}
}
