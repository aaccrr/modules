import { IProducerSendPayload } from '../broker';

export interface IVmuzeyBrokerProducerProvider {
  init(): Promise<void>;
  send(payload: IProducerSendPayload): Promise<void>;
  sendBatch(messages: IProducerSendPayload[]): Promise<void>;
}
