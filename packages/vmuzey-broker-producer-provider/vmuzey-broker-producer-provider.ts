import { Kafka, Producer } from 'kafkajs';
import { IProducerSendPayload } from '../broker';
import { IVmuzeyBrokerProducerProvider } from './i-vmuzey-broker-producer-provider';
import { IVmuzeyBrokerProducerProviderConfig } from './interfaces';

export class VmuzeyBrokerProducerProvider implements IVmuzeyBrokerProducerProvider {
  constructor(private config: IVmuzeyBrokerProducerProviderConfig) {}

  private broker: Kafka;
  private producer: Producer;

  async init(): Promise<void> {
    this.broker = new Kafka({
      clientId: this.config.serviceName,
      brokers: [this.config.host],
    });

    this.producer = this.broker.producer();
    await this.producer.connect();
  }

  async send(payload: IProducerSendPayload): Promise<void> {
    await this.producer.send({
      topic: payload.topic,
      messages: this.normalizeMessages(payload),
    });
  }

  async sendBatch(messages: IProducerSendPayload[]): Promise<void> {
    messages.forEach((message) => {
      message.messages = this.normalizeMessages(message);
    });

    await this.producer.sendBatch({ topicMessages: messages as any[] });
  }

  private normalizeMessages(payload: IProducerSendPayload): any[] {
    if (payload.message !== undefined) {
      return [this.createMessage(payload.message)];
    }

    return payload.messages.map((message) => this.createMessage(message));
  }

  private createMessage(rawMessage: any): any {
    return {
      value: JSON.stringify(rawMessage),
    };
  }
}
