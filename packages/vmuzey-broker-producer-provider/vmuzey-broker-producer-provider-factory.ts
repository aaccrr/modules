import { IVmuzeyBrokerProducerProvider } from './i-vmuzey-broker-producer-provider';
import { VmuzeyBrokerProducerProvider } from './vmuzey-broker-producer-provider';
import { VmuzeyBrokerProducerProviderStub } from './vmuzey-broker-producer-provider-stub';
import { IVmuzeyBrokerProducerProviderConfig } from './interfaces';

export class VmuzeyBrokerProducerProviderFactory {
  static create(env: string, config: IVmuzeyBrokerProducerProviderConfig): IVmuzeyBrokerProducerProvider {
    if (env === 'test' || env === 'openapi') {
      return new VmuzeyBrokerProducerProviderStub(config);
    }

    return new VmuzeyBrokerProducerProvider(config);
  }
}
