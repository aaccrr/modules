export interface IVmuzeyBrokerProducerProviderConfig {
  serviceName: string;
  host: string;
}
