export type IpBlockerMethods =
  | 'FAILED_CONTROLLER_LOGIN_ATTEMPT'
  | 'FAILED_CLIENT_LOGIN_ATTEMPT'
  | 'FAILED_REFUND_AVAILABILITY_CHECK'
  | 'FAILED_REFUND_INITIATOR_PROFILE_CHECK'
  | 'FAILED_REFUND_ATTEMPT'
  | 'FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT';

export interface IIpBlocker {
  middleware(method: IpBlockerMethods): any;
  checkIpBlock(method: IpBlockerMethods, ip: string): Promise<void>;
  updateLockCounter(method: IpBlockerMethods, ip: string): Promise<void>;
}
