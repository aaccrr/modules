import { IRedisConnector } from '../connectors';
import { BusinessError } from '../errors';
import { IpBlockerMethods, IIpBlocker } from './IIpBlocker';

export class IpBlocker implements IIpBlocker {
  constructor(private redis: IRedisConnector) {}
  private settings = {
    FAILED_CONTROLLER_LOGIN_ATTEMPT: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'CONTROLLER_LOGIN_ATTEMPTS_EXCEEDED',
    },
    FAILED_CLIENT_LOGIN_ATTEMPT: {
      maxFails: 7,
      timeout: 30,
      errorCode: 'CLIENT_LOGIN_ATTEMPTS_EXCEEDED',
    },
    FAILED_REFUND_AVAILABILITY_CHECK: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'REFUND_AVAILABILITY_CHECK_ATTEMPTS_EXCEEDED',
    },
    FAILED_REFUND_INITIATOR_PROFILE_CHECK: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'REFUND_INITIATOR_PROFILE_CHECK_ATTEMPTS_EXCEEDED',
    },
    FAILED_REFUND_ATTEMPT: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'REFUND_CREATION_ATTEMPTS_EXCEEDED',
    },
    FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT',
    },
  };
  private prefix = 'ip-blocker';

  middleware(method: IpBlockerMethods): any {
    return async (ctx, next) => {
      const ip = ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'];
      await this.checkIpBlock(method, ip);
      await next();
    };
  }

  // --- Методы для проверки блокировки ip-адреса

  async checkIpBlock(method: IpBlockerMethods, ip: string): Promise<void> {
    const lockCounter = await this.getLockCounter(method, ip);

    if (!lockCounter) return;
    if (this.isLockCounterFull(method, lockCounter)) {
      const { errorCode, timeout } = this.settings[method];
      throw new BusinessError('ru', errorCode, { timeout });
    }
  }

  // ---

  async updateLockCounter(method: IpBlockerMethods, ip: string): Promise<void> {
    const lockCounter = await this.getLockCounter(method, ip);

    if (lockCounter) {
      if (this.isLockCounterFull(method, lockCounter)) return;
      await this.incrementLockCounter(method, ip, lockCounter);
    } else {
      await this.addNewLockCounter(method, ip);
    }
  }

  private async getLockCounter(method: IpBlockerMethods, ip: string): Promise<number> {
    const lockCounter = await this.redis.db.get(`${this.prefix}:${method}:${ip}`);
    if (lockCounter === null) return;
    return parseInt(lockCounter);
  }

  private isLockCounterFull(method: IpBlockerMethods, lockCounter: number): boolean {
    const lockCounterMaximum = this.settings[method].maxFails;
    return lockCounter >= lockCounterMaximum;
  }

  private async incrementLockCounter(method: IpBlockerMethods, ip: string, lockCounter: number): Promise<void> {
    const remainingLockTime = await this.redis.db.ttl(`${this.prefix}:${method}:${ip}`);
    await this.redis.methods.set(`${this.prefix}:${method}:${ip}`, lockCounter + 1, remainingLockTime);
  }

  private async addNewLockCounter(method: IpBlockerMethods, ip: string): Promise<void> {
    const lockPeriodInSeconds = this.settings[method].timeout * 60;
    await this.redis.methods.set(`${this.prefix}:${method}:${ip}`, 1, lockPeriodInSeconds);
  }
}
