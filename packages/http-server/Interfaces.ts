import { RouteMiddleware } from '../http-router/Interfaces';
import { IConsumerTopic } from '../broker';

export interface IServiceServer {
  init(config: IServiceServerConfig): Promise<void>;
}

export interface IServiceServerConfig {
  name: string;

  http?: IHttpServerConfig;
  rpc?: IRpcServerConfig;
  broker?: IBrokerConsumerConfig;
}

export interface IHttpServerConfig {
  port: number;
  middleware: Array<() => RouteMiddleware>;
  preMiddleware?: Array<() => RouteMiddleware>;
}

export interface IRpcServerConfig {
  port: number;
}

export interface IBrokerConsumerConfig {
  groupId: string;
  topics: IConsumerTopic[];
}
