import * as Koa from 'koa';
import { Server } from 'jayson';
import { IBroker } from '../broker';
import { IServiceServer, IServiceServerConfig, IHttpServerConfig, IRpcServerConfig, IBrokerConsumerConfig } from './Interfaces';
import { EventEmitter } from 'events';
import { Errors } from '../errors';

interface IServiceDependencies {
  MESSAGE_BROKER: IBroker;
}

export class ServiceServer extends EventEmitter implements IServiceServer {
  constructor(private dependencies: Partial<IServiceDependencies> = {}) {
    super();
    this.handleExeptions();
  }

  private HttpServer: Koa;
  private RpcServer: Server;
  private BrokerConsumer: any;

  public async init(Config: any): Promise<void> {
    if (Config.http) this.createHttpServer(Config.http);
    if (Config.rpc) this.createRpcServer(Config.rpc);
    if (Config.broker) await this.createBrokerConsumer(Config.broker);
  }

  private createHttpServer(config: any): void {
    this.HttpServer = new Koa();

    if (Array.isArray(config.preMiddleware)) {
      config.preMiddleware.forEach((m) => {
        this.HttpServer.use(m());
      });
    }

    this.HttpServer.listen(config.port);

    if (Array.isArray(config.middleware)) {
      config.middleware.map((middleware) => {
        this.HttpServer.use(middleware());
      });
    }

    if (config.router) {
      const Router = new config.router();

      this.HttpServer.use(Router.getRoutes());
    }

    this.HttpServer.on('error', () => console.log('Http server error...'));
  }

  private createRpcServer(config: any): void {
    const Router = new config.router();

    this.RpcServer = new Server(Router.getRoutes());
    this.RpcServer.http().listen(config.port, () => console.log('RPC STARTED'));
    this.RpcServer.http().on('error', () => console.log('Rpc server error...'));
  }

  private async createBrokerConsumer(config: any): Promise<void> {
    if (!this.dependencies['MESSAGE_BROKER']) {
      Errors.InternalError('Нужно создать брокера сообщений.');
    }

    const broker = this.dependencies['MESSAGE_BROKER'];

    this.BrokerConsumer = await broker.createConsumer({ id: config.groupId, topics: config.topics });
    const handlers = new config.handlers().getRoutes();

    this.BrokerConsumer.onMessage(async (topic, message) => {
      this.emit(topic, message);
      const handler = handlers[topic];
      if (handler) await handler(message);
    });
  }

  private handleExeptions(): void {
    ['unhandledRejection', 'uncaughtException'].map((type: any) => {
      process.on(type, (e) => {
        console.log('UNHANDLED_REJECTION_1 ', e);
        process.exit(1);
      });
    });

    ['SIGTERM', 'SIGINT'].map((type: any) => {
      process.once(type, (e) => {
        console.log('UNHANDLED_REJECTION_2 ', e);
        process.kill(process.pid, type);
      });
    });
  }
}
