import { Tokens } from './tokens';

const Module = new Tokens();
export { Module as Tokens };

export * from './Interfaces';
export * from './ErrorCodes';
