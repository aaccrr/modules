import * as jwt from 'jsonwebtoken';
import { ITokens, ICreateTokensOptions, ICreateTokenOptions } from './Interfaces';
import { ITokenPayload, ITokensSet } from '../globals';
import {
  TOKEN_EXPIRED,
  TOKEN_MALFORMED,
  TOKEN_SIGNATURE_INVALID,
  TOKEN_IS_INACTIVE,
  UNEXPECTED_TOKEN_VERIFY_ERROR,
  TOKEN_IS_NOT_EXISTS,
  UNEXPECTED_TOKENS_CREATE_ERROR,
} from './ErrorCodes';
import { DEFAULT_ACCESS_TOKEN_EXPIRES_IN, DEFAULT_REFRESH_TOKEN_EXPIRES_IN } from './Constants';
import { IContract } from '../interfaces';

const DEFAULT_CREATE_TOKENS_OPTIONS: ICreateTokensOptions = {
  accessTokenExpiration: DEFAULT_ACCESS_TOKEN_EXPIRES_IN,
  refreshTokenExpiration: DEFAULT_REFRESH_TOKEN_EXPIRES_IN,
};

export class Tokens implements ITokens {
  async createTokens(
    payload: ITokenPayload,
    accessTokenSecret: string,
    refreshTokenSecret: string,
    options: ICreateTokensOptions = DEFAULT_CREATE_TOKENS_OPTIONS
  ): Promise<IContract<ITokensSet | string>> {
    try {
      const accessToken = await this.createAccessToken(payload, accessTokenSecret, { expiresIn: options.accessTokenExpiration });
      const refreshToken = await this.createRefreshToken(payload, refreshTokenSecret, { expiresIn: options.refreshTokenExpiration });

      return {
        success: true,
        data: {
          accessToken,
          refreshToken,
        },
      };
    } catch (e) {
      return {
        success: false,
        data: UNEXPECTED_TOKENS_CREATE_ERROR,
      };
    }
  }

  private async createAccessToken(payload: ITokenPayload, secret: string, options: ICreateTokenOptions): Promise<string> {
    return new Promise((resolve, reject) => {
      jwt.sign(payload, secret, { expiresIn: options.expiresIn }, (err, token) => {
        if (err) reject(err);
        resolve(token);
      });
    });
  }

  private async createRefreshToken(payload: ITokenPayload, secret: string, options: ICreateTokenOptions): Promise<string> {
    return new Promise((resolve, reject) => {
      jwt.sign(payload, secret, { expiresIn: options.expiresIn }, (err, token) => {
        if (err) reject(err);
        resolve(token);
      });
    });
  }

  async verify(token: string, secret: string): Promise<IContract<ITokenPayload | string>> {
    try {
      const res = await jwt.verify(token, secret);

      return {
        success: true,
        data: res as any,
      };
    } catch (e) {
      let data = '';
      if (e.name === 'TokenExpiredError') data = TOKEN_EXPIRED;

      if (e.name === 'JsonWebTokenError') {
        if (e.message === 'jwt malformed') data = TOKEN_MALFORMED;
        if (e.message === 'invalid signature') data = TOKEN_SIGNATURE_INVALID;
      }

      if (e.name === 'NotBeforeError') data = TOKEN_IS_INACTIVE;

      return {
        success: false,
        data: data || UNEXPECTED_TOKEN_VERIFY_ERROR,
      };
    }
  }

  getTokenFromString(str: string): IContract<string> {
    if (typeof str !== 'string') return { success: false, data: 'ARG_IN_NOT_A_STRING' };

    const token = str.replace('Bearer ', '');

    return {
      success: true,
      data: token,
    };
  }

  getTokenFromRequest(request: any): IContract<string> {
    const header = request.header.authorization;
    if (!header || typeof header !== 'string' || !header.includes('Bearer ')) {
      return {
        success: false,
        data: TOKEN_IS_NOT_EXISTS,
      };
    }

    const token = header.replace('Bearer ', '');

    if (!token) {
      return {
        success: false,
        data: TOKEN_IS_NOT_EXISTS,
      };
    }

    return {
      success: true,
      data: token,
    };
  }
}
