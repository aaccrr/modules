import { IContract } from '../interfaces';
import { ITokenPayload, ITokensSet } from '../globals';

export interface ITokens {
  createTokens(
    payload: ITokenPayload,
    accessTokenSecret: string,
    refreshTokenSecret: string,
    options?: ICreateTokensOptions
  ): Promise<IContract<ITokensSet | string>>;
  verify(token: string, secret: string): Promise<IContract<ITokenPayload | string>>;
  getTokenFromRequest(request: any): IContract<string>;

  getTokenFromString(str: string): IContract<string>;
}

export interface ICreateTokensOptions {
  accessTokenExpiration?: string;
  refreshTokenExpiration?: string;
}

export interface ICreateTokenOptions {
  expiresIn?: string;
}
