import { IEntranceTicketItemModel, IItemModel } from '../../globals';
import { VmuzeyTimezonedDateHelper } from '../../vmuzey-timezoned-date-helper';

export class VmuzeyMapperEntranceTicketPurchaseToPrintTimeBlock {
  constructor(private purchaseItem: IEntranceTicketItemModel) {}

  map() {
    if (this.purchaseItem.props.date && this.purchaseItem.props.time) {
      return this.mapIntervalBlock();
    }

    if (this.purchaseItem.props.date && !this.purchaseItem.props.time) {
      return this.mapDateBlock();
    }

    return this.mapPeriodBlock();
  }

  private mapIntervalBlock(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    const timeFrom = new VmuzeyTimezonedDateHelper(condition.from, dateOptions).addMinutes(15).toFormattedString('HH:mm');
    const timeTo = new VmuzeyTimezonedDateHelper(condition.to, dateOptions).toFormattedString('HH:mm');

    return {
      isDate: true,
      date: {
        day: new VmuzeyTimezonedDateHelper(condition.from, dateOptions).toFormattedString('D'),
        monthName: new VmuzeyTimezonedDateHelper(condition.from, dateOptions).getMonthName({ case: 'genitive' }),
        time: `${timeFrom}-${timeTo}`,
      },
    };
  }

  private mapDateBlock(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    return {
      isDate: true,
      date: {
        day: new VmuzeyTimezonedDateHelper(condition.to, dateOptions).toFormattedString('D'),
        monthName: new VmuzeyTimezonedDateHelper(condition.to, dateOptions).getMonthName({ case: 'genitive' }),
      },
    };
  }

  private mapPeriodBlock(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    const date = new VmuzeyTimezonedDateHelper(condition.to, dateOptions).toFormattedString('D');
    const monthName = new VmuzeyTimezonedDateHelper(condition.to, dateOptions).getMonthName({ case: 'genitive' });

    return {
      isValidUntil: true,
      validUntilDate: `${date} ${monthName}`,
    };
  }
}
