import { IEntranceTicketItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintInformationToVisitData } from '../vmuzey-mapper-purchase-to-print-shared/vmuzey-mapper-purchase-to-print-information-to-visit-data';
import { VmuzeyMapperEntranceTicketPurchaseToPrintImportantInformation } from './vmuzey-mapper-entrance-ticket-purchase-to-print-important-information';

export class VmuzeyMapperEntranceTicketPurchaseToPrintInformationToVisitData {
  constructor(private purchaseItem: IEntranceTicketItemModel) {}

  map(): any {
    const informationToVisitDataMapper = new VmuzeyMapperPurchaseToPrintInformationToVisitData(this.purchaseItem);
    const importantInformationMapper = new VmuzeyMapperEntranceTicketPurchaseToPrintImportantInformation(this.purchaseItem);

    return {
      ...informationToVisitDataMapper.map(),
      importantInformation: importantInformationMapper.map(),
      openingHours: this.purchaseItem.visitedMuseum.scheduleComment,
    };
  }
}
