import { IEntranceTicketItemModel } from '../../globals';
import { VmuzeyTimezonedDateHelper } from '../../vmuzey-timezoned-date-helper';

export class VmuzeyMapperEntranceTicketPurchaseToPrintImportantInformation {
  constructor(private purchaseItem: IEntranceTicketItemModel) {}

  map() {
    if (this.purchaseItem.props.date && this.purchaseItem.props.time) {
      return this.mapIntervalImportantInformation();
    }

    if (this.purchaseItem.props.date && !this.purchaseItem.props.time) {
      return this.mapDateBlock();
    }

    return this.mapPeriodImportantInformation();
  }

  private mapIntervalImportantInformation(): any {
    return {
      timeTicket: true,
      personalTicket: this.purchaseItem.isPersonal,
    };
  }

  private mapDateBlock(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    return {
      isDate: true,
      date: {
        day: new VmuzeyTimezonedDateHelper(condition.to, dateOptions).toFormattedString('D'),
        monthName: new VmuzeyTimezonedDateHelper(condition.to, dateOptions).getMonthName({ case: 'genitive' }),
      },
    };
  }

  private mapPeriodImportantInformation(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    const periodStartDate = new VmuzeyTimezonedDateHelper(condition.from, dateOptions);
    const periodEndDate = new VmuzeyTimezonedDateHelper(condition.to, dateOptions);

    return {
      ticketValidity: {
        isShow: true,
        days: periodStartDate.countDaysTo(periodEndDate),
      },
      personalTicket: this.purchaseItem.isPersonal,
    };
  }
}
