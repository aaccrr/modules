import { IEntranceTicketItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintMainData } from '../vmuzey-mapper-purchase-to-print-shared';
import { VmuzeyMapperEntranceTicketPurchaseToPrintMainInformationData } from './vmuzey-mapper-entrance-ticket-purchase-to-print-main-information-data';
import { VmuzeyMapperEntranceTicketPurchaseToPrintInformationToVisitData } from './vmuzey-mapper-entrance-ticket-purchase-to-print-information-to-visit-data';

export class VmuzeyMapperEntranceTicketPurchaseToPrint {
  constructor(private purchase: IEntranceTicketItemModel) {}

  map(): any {
    const mainDataMapper = new VmuzeyMapperPurchaseToPrintMainData(this.purchase);
    const mainInformationDataMapper = new VmuzeyMapperEntranceTicketPurchaseToPrintMainInformationData(this.purchase);
    const informationToVisitDataMapper = new VmuzeyMapperEntranceTicketPurchaseToPrintInformationToVisitData(this.purchase);

    return {
      TICKET_MAIN_DATA: mainDataMapper.map(),
      TICKET_MAIN_INFORMATION_DATA: mainInformationDataMapper.map(),
      INFORMATION_TO_VISIT_DATA: informationToVisitDataMapper.map(),
    };
  }
}
