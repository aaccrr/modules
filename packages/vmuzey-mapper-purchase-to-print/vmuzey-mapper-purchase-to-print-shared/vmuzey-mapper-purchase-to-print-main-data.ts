// import { IOrderMainDataView } from '../../../Interfaces';
import { IItemModel, NomenclatureTypeEnum } from '../../globals';

export class VmuzeyMapperPurchaseToPrintMainData {
  constructor(private purchaseItem: IItemModel) {}

  map(): any {
    const aliases = {
      [NomenclatureTypeEnum.TICKET]: 'SINGLE_TICKET',
    };

    return {
      ticketType: aliases[this.purchaseItem.nomenclature.type] || this.purchaseItem.nomenclature.type,
      ticketOrderNumber: this.purchaseItem.orderId,
      qrCodeImgUrl: `${process.env.API_URL}/static/qr/${this.purchaseItem.id}.png`,
      qrCodeNumber: this.purchaseItem.id,
      ageCensor: `${this.purchaseItem.ageCensor || 0}+`,
    };
  }
}
