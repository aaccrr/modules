import { IItemModel } from '../../globals';
import { IVmuzeyTimezonedDateOptions, VmuzeyTimezonedDateHelper } from '../../vmuzey-timezoned-date-helper';

export class VmuzeyMapperPurchaseToPrintInformationToVisitData {
  constructor(private purchaseItem: IItemModel) {}

  map() {
    return {
      tableData: [this.mapRefundContactsBlock(), this.mapTicketInfoBlock()],
      organizerInfo: this.mapOrganizerInfoBlock(),
      agentInfo: this.mapAgentInfoBlock(),
    };
  }

  private mapRefundContactsBlock() {
    const { refundContacts } = this.purchaseItem.serviceProvider;

    return {
      isContact: true,
      headerName: 'вопросы возврата',
      itemData: {
        phone: refundContacts.phone,
        email: refundContacts.email,
      },
    };
  }

  private mapTicketInfoBlock() {
    const dateOptions: IVmuzeyTimezonedDateOptions = { timezone: 'Europe/Moscow', subTimezone: false };

    const purchaseDate = new VmuzeyTimezonedDateHelper(this.purchaseItem.createdAt, dateOptions).toFormattedString('YYYY.MM.DD');
    const purchaseTime = new VmuzeyTimezonedDateHelper(this.purchaseItem.createdAt, dateOptions).toFormattedString('H:mm');

    return {
      isTicketInformation: true,
      headerName: 'информация об электронном билете',
      wasPurchased: {
        date: purchaseDate,
        time: purchaseTime,
        purchaseType: this.purchaseItem.payment.pushkinCard ? 'В рамках программы "ПУШКИНСКАЯ КАРТА"' : 'Банковская карта',
      },
    };
  }

  private mapOrganizerInfoBlock() {
    return {
      fullOrganizationName: this.purchaseItem.serviceProvider.name,
      inn: this.purchaseItem.serviceProvider.inn,
      address: this.purchaseItem.serviceProvider.address,
    };
  }

  private mapAgentInfoBlock() {
    return {
      fullOrganizationName: 'ООО "Бинбрейн"',
      inn: '5031076070',
      address: 'Санкт-Петербург, ул. Академика Павлова 6 к 5',
    };
  }
}
