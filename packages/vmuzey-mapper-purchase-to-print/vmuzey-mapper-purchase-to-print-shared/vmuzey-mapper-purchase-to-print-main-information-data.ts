import { IItemModel } from '../../globals';

export class VmuzeyMapperPurchaseToPrintMainInformationData {
  constructor(private purchaseItem: IItemModel) {}

  mapNameBlock() {
    return {
      headerName: 'имя',
      itemData: `${this.purchaseItem.customer.firstName} ${this.purchaseItem.customer.lastName}`,
    };
  }

  mapPriceBlock() {
    return {
      isPrice: true,
      headerName: 'цена электронного билета',
      itemData: this.purchaseItem.price,
    };
  }

  mapCategoryBlock() {
    return {
      headerName: 'категория',
      itemData: this.purchaseItem.customerCategory.name,
    };
  }
}
