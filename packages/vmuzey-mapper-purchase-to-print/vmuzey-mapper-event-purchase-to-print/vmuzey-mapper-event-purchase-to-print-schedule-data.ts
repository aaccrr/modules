import { Dates } from '../../dates';
import { IEventItemModel } from '../../globals';

export class VmuzeyMapperEventPurchaseToPrintScheduleData {
  constructor(private purchaseItem: IEventItemModel) {}

  map(): any {
    if (this.isLongPeriod()) {
      return [];
    }

    return this.purchaseItem.nomenclature.schedule.map((scheduleDay) => {
      return {
        isDayOff: Dates.isDayOff(scheduleDay.startsAt),
        day: Dates.format(scheduleDay.startsAt, scheduleDay.location.timezone, 'D'),
        monthName: Dates.getMonthName(scheduleDay.startsAt),
        eventTime: {
          from: Dates.format(scheduleDay.startsAt, scheduleDay.location.timezone, 'HH:mm'),
          to: Dates.format(scheduleDay.finishAt, scheduleDay.location.timezone, 'HH:mm'),
        },
        dayWeekMame: Dates.getWeekdayName(scheduleDay.startsAt),
        address: `${scheduleDay.location.locality}, ${scheduleDay.location.address}`,
        dayEventProgram: this.mapDayEventProgram(scheduleDay.timeline),
      };
    });
  }

  private isLongPeriod(): boolean {
    const longPeriodCondition = this.purchaseItem.condition.dates[0];
    const longPeriodLength = Dates.getDifferenceInDays(longPeriodCondition.to, longPeriodCondition.from);
    return longPeriodLength > 1;
  }

  private mapDayEventProgram(timeline) {
    return timeline.map((t) => {
      return {
        time: {
          from: t.from,
          to: t.to,
        },
        description: t.description,
      };
    });
  }
}
