import { IEventItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintMainData } from '../vmuzey-mapper-purchase-to-print-shared';
import { VmuzeyMapperEventPurchaseToPrintMainInformationData } from './vmuzey-mapper-event-purchase-to-print-main-information-data';
import { VmuzeyMapperEventPurchaseToPrintInformationToVisitData } from './vmuzey-mapper-event-purchase-to-print-information-to-visit-data';
import { VmuzeyMapperEventPurchaseToPrintScheduleData } from './vmuzey-mapper-event-purchase-to-print-schedule-data';

export class VmuzeyMapperEventPurchaseToPrint {
  constructor(private purchaseItem: IEventItemModel) {}

  map() {
    const mainDataMapper = new VmuzeyMapperPurchaseToPrintMainData(this.purchaseItem);
    const mainInformationDataMapper = new VmuzeyMapperEventPurchaseToPrintMainInformationData(this.purchaseItem);
    const informationToVisitData = new VmuzeyMapperEventPurchaseToPrintInformationToVisitData(this.purchaseItem);
    const eventScheduleMapper = new VmuzeyMapperEventPurchaseToPrintScheduleData(this.purchaseItem);

    const mapped = {
      TICKET_MAIN_DATA: mainDataMapper.map(),
      TICKET_MAIN_INFORMATION_DATA: mainInformationDataMapper.map(),
      INFORMATION_TO_VISIT_DATA: informationToVisitData.map(),
      EVENT_SCHEDULE_DATA: eventScheduleMapper.map(),
    };

    return mapped;
  }
}
