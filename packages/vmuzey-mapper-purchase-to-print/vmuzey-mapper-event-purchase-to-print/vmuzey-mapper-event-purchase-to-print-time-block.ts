import { Dates } from '../../dates';
import { IEventItemModel } from '../../globals';
import { VmuzeyTimezonedDateHelper } from '../../vmuzey-timezoned-date-helper';

export class VmuzeyMapperEventPurchaseToPrintTimeBlock {
  constructor(private purchaseItem: IEventItemModel) {}

  map() {
    if (this.isLongPeriod()) {
      return {
        isLongEvent: true,
        untilTheDate: this.calculateLongPeriodFinish(),
      };
    }

    const sortedDatesList = this.getSortedDatesList();

    if (sortedDatesList.length === 1) {
      return this.getSingleDate(sortedDatesList[0]);
    }

    if (this.isDateSequence(sortedDatesList)) {
      return this.getConditionPeriod(sortedDatesList);
    }

    if (sortedDatesList.length <= 3) {
      return this.getColumnView(sortedDatesList);
    }

    return this.getValidUntilView(sortedDatesList);
  }

  calculateLongPeriodFinish(): string {
    const longPeriod = this.purchaseItem.condition.dates[0];
    const longPeriodCondition = new VmuzeyTimezonedDateHelper(longPeriod.to, { timezone: longPeriod.timezone, subTimezone: false });
    const date = longPeriodCondition.toFormattedString('D');
    const monthName = longPeriodCondition.getMonthName({ case: 'genitive' });
    return `${date} ${monthName}`;
  }

  isLongPeriod(): boolean {
    const longPeriodCondition = this.purchaseItem.condition.dates[0];
    const longPeriodLength = Dates.getDifferenceInDays(longPeriodCondition.to, longPeriodCondition.from);
    return longPeriodLength > 1;
  }

  getSortedDatesList(): any[] {
    return this.purchaseItem.condition.dates
      .map((date) => ({ from: date.from, to: date.to }))
      .sort((a, b) => {
        const firstEventDateStart = new VmuzeyTimezonedDateHelper(a.from);
        const lastEventDateStart = new VmuzeyTimezonedDateHelper(b.from);

        return firstEventDateStart.isAfter(lastEventDateStart) ? 0 : 1;
      });
  }

  isDateSequence(dates: any): boolean {
    dates = dates.map((date) => date.from);
    const isSequence = true;
    for (let i = 0; i < dates.length; i++) {
      const currentDate = new VmuzeyTimezonedDateHelper(dates[i]).addDays(1);
      if (!dates[i + 1]) return isSequence;
      const nextDate = new VmuzeyTimezonedDateHelper(dates[i + 1]).addDays(1);
      if (!currentDate.isSameDay(nextDate)) return false;
    }
    return isSequence;
  }

  getConditionPeriod(dates: any[]): any {
    const firstDate = dates[0];
    const lastDate = dates[dates.length - 1];

    const isSameYear = Dates.isSameYear(firstDate, lastDate);
    const isSameMonth = Dates.isSameMonth(firstDate, lastDate);

    const firstDay = Dates.format(firstDate.from, 'Europe/Moscow', 'D');
    const lastDay = Dates.format(lastDate.from, 'Europe/Moscow', 'D');
    const firstMonth = Dates.getMonthName(firstDate.from);
    const lastMonth = Dates.getMonthName(lastDate.from);

    if (isSameYear && isSameMonth) {
      return {
        isDate: true,
        date: {
          day: `${firstDay}-${lastDay}`,
          monthName: firstMonth,
        },
      };
    }

    if (isSameYear && !isSameMonth) {
      return {
        isDate: true,
        date: {
          from: {
            day: firstDay,
            monthName: firstMonth,
          },
          to: {
            day: lastDay,
            monthName: lastMonth,
          },
        },
      };
    }

    if (!isSameYear) {
      return {
        isDate: true,
        date: {
          from: {
            day: firstDay,
            monthName: firstMonth,
            year: '',
          },
          to: {
            day: lastDay,
            monthName: lastMonth,
            year: Dates.format(lastDate, 'Europe/Moscow', 'YYYY'),
          },
        },
      };
    }
  }

  getSingleDate(date: any): any {
    return {
      isDate: true,
      date: {
        day: Dates.format(date.from, 'Europe/Moscow', 'D'),
        monthName: Dates.getMonthName(date.from),
      },
    };
  }

  getColumnView(dates: any[]): any {
    const dateListing = dates.map((date) => `${Dates.format(date.from, 'Europe/Moscow', 'D')} ${Dates.getMonthName(date.from)}`);

    return {
      isDateListing: true,
      dateListing,
    };
  }

  getValidUntilView(dates: any[]): any {
    const lastDate = dates[dates.length - 1];

    return {
      isValidUntil: true,
      validUntilDate: `${Dates.format(lastDate.from, 'Europe/Moscow', 'D')} ${Dates.getMonthName(lastDate.from)}`,
    };
  }
}
