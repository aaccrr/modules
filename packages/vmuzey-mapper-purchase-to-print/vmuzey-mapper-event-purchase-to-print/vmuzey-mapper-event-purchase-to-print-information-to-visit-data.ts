import { IEventItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintInformationToVisitData } from '../vmuzey-mapper-purchase-to-print-shared/vmuzey-mapper-purchase-to-print-information-to-visit-data';

export class VmuzeyMapperEventPurchaseToPrintInformationToVisitData {
  constructor(private purchaseItem: IEventItemModel) {}

  map(): any {
    const informationToVisitDataMapper = new VmuzeyMapperPurchaseToPrintInformationToVisitData(this.purchaseItem);

    return {
      ...informationToVisitDataMapper.map(),
      importantInformation: this.mapImportantInformation(),
    };
  }

  private mapImportantInformation(): any {
    return {
      eventSeveralDaysToGoOnce: this.purchaseItem.condition.dates.length > 1,
      eventGoOnce: this.purchaseItem.condition.dates.length === 1,
    };
  }
}
