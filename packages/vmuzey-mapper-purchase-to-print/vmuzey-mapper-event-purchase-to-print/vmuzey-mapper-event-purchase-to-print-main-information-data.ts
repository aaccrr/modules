import { IEventItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintMainInformationData } from '../vmuzey-mapper-purchase-to-print-shared';
import { VmuzeyMapperEventPurchaseToPrintTimeBlock } from './vmuzey-mapper-event-purchase-to-print-time-block';

export class VmuzeyMapperEventPurchaseToPrintMainInformationData {
  constructor(private purchaseItem: IEventItemModel) {}

  map(): any {
    const purchaseToPrintMainInformationMapper = new VmuzeyMapperPurchaseToPrintMainInformationData(this.purchaseItem);
    const timeBlockMapper = new VmuzeyMapperEventPurchaseToPrintTimeBlock(this.purchaseItem);

    return {
      mainName: this.purchaseItem.nomenclature.title,
      tableData: [
        purchaseToPrintMainInformationMapper.mapNameBlock(),
        purchaseToPrintMainInformationMapper.mapPriceBlock(),
        this.mapTypeBlock(),
        purchaseToPrintMainInformationMapper.mapCategoryBlock(),
        this.mapAddressBlock(),
      ],
      timeBlock: timeBlockMapper.map(),
    };
  }

  private mapTypeBlock() {
    return {
      headerName: 'тип',
      itemData: this.purchaseItem.nomenclature.category.name,
    };
  }

  private mapAddressBlock() {
    const firstDateInSchedule = this.purchaseItem.nomenclature.schedule[0];

    return {
      isAddress: true,
      headerName: 'адрес',
      itemData: {
        cityName: firstDateInSchedule.location.locality,
        address: firstDateInSchedule.location.address,
      },
    };
  }
}
