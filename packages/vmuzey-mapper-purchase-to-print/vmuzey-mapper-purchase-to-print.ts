import { IEntranceTicketItemModel, IEventItemModel, IItemModel, ISeasonTicketItemModel, NomenclatureTypeEnum } from '../globals';
import { VmuzeyMapperEntranceTicketPurchaseToPrint } from './vmuzey-mapper-entrance-ticket-purchase-to-print';
import { VmuzeyMapperSeasonTicketPurchaseToPrint } from './vmuzey-mapper-season-ticket-purchase-to-print';
import { VmuzeyMapperEventPurchaseToPrint } from './vmuzey-mapper-event-purchase-to-print';

export class VmuzeyMapperPurchaseToPrint {
  constructor(private purchaseItem: IItemModel) {
    if (this.purchaseItem.nomenclature.type === NomenclatureTypeEnum.TICKET) {
      this.mapper = new VmuzeyMapperEntranceTicketPurchaseToPrint(this.purchaseItem as IEntranceTicketItemModel);
    }

    if (this.purchaseItem.nomenclature.type === NomenclatureTypeEnum.ABONEMENT) {
      this.mapper = new VmuzeyMapperSeasonTicketPurchaseToPrint(this.purchaseItem as ISeasonTicketItemModel);
    }

    if (this.purchaseItem.nomenclature.type === NomenclatureTypeEnum.EVENT) {
      this.mapper = new VmuzeyMapperEventPurchaseToPrint(this.purchaseItem as IEventItemModel);
    }
  }

  private mapper: any;

  map(): any {
    return this.mapper.map();
  }
}
