import { ISeasonTicketItemModel, SeasonTicketConditionTypeEnum } from '../../globals';
import { VmuzeyTimezonedDateHelper } from '../../vmuzey-timezoned-date-helper';

export class VmuzeyMapperSeasonTicketPurchaseToPrintTimeBlock {
  constructor(private purchaseItem: ISeasonTicketItemModel) {}

  map() {
    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.PERIOD) {
      return this.mapPeriodTimeBlock();
    }

    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.EVENTS) {
      return this.mapEventsTimeBlock();
    }

    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.UNIVERSAL) {
      return this.mapUniversalTimeBlock();
    }

    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.VISITS) {
      return this.mapVisitsTimeBlock();
    }

    return {};
  }

  private mapEventsTimeBlock(): any {
    return {
      isNumberOfEvents: true,
      eventsCounter: this.purchaseItem.condition.events.length,
    };
  }

  mapPeriodTimeBlock(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    const periodStartDate = new VmuzeyTimezonedDateHelper(condition.from, dateOptions);
    const periodEndDate = new VmuzeyTimezonedDateHelper(condition.to, dateOptions);

    const date = periodEndDate.toFormattedString('D');
    const monthName = periodEndDate.getMonthName({ case: 'genitive' });

    return {
      isNumberOfDays: true,
      numberOfDays: {
        daysCounter: periodStartDate.countDaysTo(periodEndDate),
        endDate: `${date} ${monthName}`,
      },
    };
  }

  private mapUniversalTimeBlock(): any {
    const { condition, visitedMuseum } = this.purchaseItem;
    const dateOptions = { timezone: visitedMuseum.location.timezone, subTimezone: false };

    const periodEndDate = new VmuzeyTimezonedDateHelper(condition.to, dateOptions);
    const date = periodEndDate.toFormattedString('D');
    const monthName = periodEndDate.getMonthName({ case: 'genitive' });

    return {
      isNumberOfDaysAndVisits: true,
      numberOfDaysAndVisits: {
        visitsCounter: condition.total,
        endDate: `${date} ${monthName}`,
      },
    };
  }

  mapVisitsTimeBlock(): any {
    return {
      isNumberOfVisits: true,
      numberOfVisits: this.purchaseItem.condition.total,
    };
  }
}
