import { ISeasonTicketItemModel, SeasonTicketConditionTypeEnum } from '../../globals';

export class VmuzeyMapperSeasonTicketPurchaseToPrintImportantInformation {
  constructor(private purchaseItem: ISeasonTicketItemModel) {}

  map() {
    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.PERIOD) {
      return this.mapPeriodImportantInformation();
    }

    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.EVENTS) {
      return this.mapEventsImportantInformation();
    }

    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.UNIVERSAL) {
      return this.mapUniversalImportantInformation();
    }

    if (this.purchaseItem.condition.type === SeasonTicketConditionTypeEnum.VISITS) {
      return this.mapVisitsImportantInformation();
    }
  }

  private mapVisitsImportantInformation(): any {
    return {
      abonementIsNotRefundable: true,
      allSubscriptionsArePersonal: true,
    };
  }

  private mapUniversalImportantInformation(): any {
    return {
      abonementIsNotRefundable: true,
      allSubscriptionsArePersonal: true,
    };
  }

  private mapPeriodImportantInformation(): any {
    return {
      abonementIsNotRefundable: true,
      allSubscriptionsArePersonal: true,
    };
  }

  private mapEventsImportantInformation(): any {
    return {
      abonementIsNotRefundable: true,
      allSubscriptionsArePersonal: true,
    };
  }
}
