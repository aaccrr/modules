import { ISeasonTicketItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintMainData } from '../vmuzey-mapper-purchase-to-print-shared';
import { VmuzeyMapperSeasonTicketPurchaseToPrintMainInformationData } from './vmuzey-mapper-season-ticket-purchase-to-print-main-information-data';
import { VmuzeyMapperSeasonTicketPurchaseToPrintInformationToVisitData } from './vmuzey-mapper-season-ticket-purchase-to-print-information-to-visit-data';
import { VmuzeyMapperSeasonTicketPurchaseToPrintEvents } from './vmuzey-mapper-season-ticket-purchase-to-print-events';

export class VmuzeyMapperSeasonTicketPurchaseToPrint {
  constructor(private purchaseItem: ISeasonTicketItemModel) {}

  map(): any {
    const mainDataMapper = new VmuzeyMapperPurchaseToPrintMainData(this.purchaseItem);
    const mainInformationDataMapper = new VmuzeyMapperSeasonTicketPurchaseToPrintMainInformationData(this.purchaseItem);
    const informationToVisitDataMapper = new VmuzeyMapperSeasonTicketPurchaseToPrintInformationToVisitData(this.purchaseItem);

    const mapped: any = {
      TICKET_MAIN_DATA: mainDataMapper.map(),
      TICKET_MAIN_INFORMATION_DATA: mainInformationDataMapper.map(),
      INFORMATION_TO_VISIT_DATA: informationToVisitDataMapper.map(),
    };

    if (this.purchaseItem.condition.type === 'EVENTS') {
      const eventsMapper = new VmuzeyMapperSeasonTicketPurchaseToPrintEvents(this.purchaseItem);
      mapped.EVENT_INFORMATION_DATA = eventsMapper.map();
    }

    return mapped;
  }
}
