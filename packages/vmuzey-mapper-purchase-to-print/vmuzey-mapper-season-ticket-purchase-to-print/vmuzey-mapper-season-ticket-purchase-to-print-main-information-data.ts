import { ISeasonTicketItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintMainInformationData } from '../vmuzey-mapper-purchase-to-print-shared';
import { VmuzeyMapperSeasonTicketPurchaseToPrintTimeBlock } from './vmuzey-mapper-season-ticket-purchase-to-print-time-block';

export class VmuzeyMapperSeasonTicketPurchaseToPrintMainInformationData {
  constructor(private purchaseItem: ISeasonTicketItemModel) {}

  map(): any {
    const purchaseToPrintMainInformationMapper = new VmuzeyMapperPurchaseToPrintMainInformationData(this.purchaseItem);
    const timeBlockMapper = new VmuzeyMapperSeasonTicketPurchaseToPrintTimeBlock(this.purchaseItem);

    return {
      mainName: this.purchaseItem.nomenclature.title,
      tableData: [
        this.mapMuseumBlock(),
        purchaseToPrintMainInformationMapper.mapNameBlock(),
        this.mapTypeBlock(),
        purchaseToPrintMainInformationMapper.mapPriceBlock(),
        purchaseToPrintMainInformationMapper.mapCategoryBlock(),
        this.mapAddressBlock(),
      ],
      timeBlock: timeBlockMapper.map(),
    };
  }

  private mapMuseumBlock() {
    return {
      headerName: 'музей',
      itemData: this.purchaseItem.visitedMuseum.name,
    };
  }

  private mapTypeBlock() {
    return {
      headerName: 'тип',
      itemData: 'Абонемент',
    };
  }

  private mapAddressBlock() {
    return {
      isAddress: true,
      headerName: 'адрес',
      itemData: {
        cityName: this.purchaseItem.visitedMuseum.location.locality,
        address: this.purchaseItem.visitedMuseum.location.address,
      },
    };
  }
}
