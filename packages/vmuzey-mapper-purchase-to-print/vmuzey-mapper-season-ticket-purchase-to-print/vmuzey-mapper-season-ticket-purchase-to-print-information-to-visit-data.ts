import { ISeasonTicketItemModel } from '../../globals';
import { VmuzeyMapperPurchaseToPrintInformationToVisitData } from '../vmuzey-mapper-purchase-to-print-shared/vmuzey-mapper-purchase-to-print-information-to-visit-data';
import { VmuzeyMapperSeasonTicketPurchaseToPrintImportantInformation } from './vmuzey-mapper-season-ticket-purchase-to-print-important-information';

export class VmuzeyMapperSeasonTicketPurchaseToPrintInformationToVisitData {
  constructor(private purchaseItem: ISeasonTicketItemModel) {}

  map(): any {
    const informationToVisitDataMapper = new VmuzeyMapperPurchaseToPrintInformationToVisitData(this.purchaseItem);
    const importantInformationMapper = new VmuzeyMapperSeasonTicketPurchaseToPrintImportantInformation(this.purchaseItem);

    return {
      ...informationToVisitDataMapper.map(),
      importantInformation: importantInformationMapper.map(),
      openingHours: this.purchaseItem.visitedMuseum.scheduleComment,
    };
  }
}
