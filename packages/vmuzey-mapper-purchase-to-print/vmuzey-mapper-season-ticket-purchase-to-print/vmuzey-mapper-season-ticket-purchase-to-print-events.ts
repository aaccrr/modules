import { ISeasonTicketItemModel } from '../../globals';
import { VmuzeyTimezonedDateHelper } from '../../vmuzey-timezoned-date-helper';

export class VmuzeyMapperSeasonTicketPurchaseToPrintEvents {
  constructor(private purchaseItem: ISeasonTicketItemModel) {}

  map() {
    const { timezone } = this.purchaseItem.visitedMuseum.location;

    return this.purchaseItem.condition.events.map((event) => {
      const dateOptions = { timezone, subTimezone: false };
      const eventStartDate = new VmuzeyTimezonedDateHelper(event.from, dateOptions);

      return {
        isDayOff: eventStartDate.isWeekend(),
        day: eventStartDate.toFormattedString('D'),
        monthName: eventStartDate.getMonthName({ case: 'genitive' }),
        startEventTime: eventStartDate.toFormattedString('HH:mm'),
        dayWeekMame: eventStartDate.getWeekDayName(),
        address: this.purchaseItem.visitedMuseum.location.address,
        eventDescription: event.title || '',
      };
    });
  }
}
