export interface IWebsiteNormalizer {
  normalize(): string;
}

export interface IWebsiteFilter {
  (website: string): string;
}
