import { URL } from 'url';
import { IWebsiteNormalizer, IWebsiteFilter } from './i-website-normalizer';

export class WebsiteNormalizer implements IWebsiteNormalizer {
  constructor(private website: string = '') {}

  private socialNetworks: string[] = ['vk.com', 'vk.link', 'instagram.com', 'facebook.com', 'twitter.com', 'ok.ru'];
  private filters: IWebsiteFilter[] = [this.stripProtocol, this.stripWww, this.stripPath(this.isSocialNetwork), this.stripQuery, this.stripHash];

  normalize(): string {
    return this.filters.reduce((originWebsite, filter) => filter(originWebsite), this.website);
  }

  private stripPath(isSocialNetwork: boolean): (website: string) => string {
    return (website: string): string => {
      if (isSocialNetwork) return website;

      return website.split('/')[0];
    };
  }

  private stripProtocol(website: string): string {
    const protocols = ['http://', 'https://'];
    return protocols.reduce((originSite, protocol) => {
      if (originSite.includes(protocol)) originSite = originSite.replace(protocol, '');
      return originSite;
    }, website);
  }

  private stripWww(website: string): string {
    if (website.startsWith('www.')) return website.replace('www.', '');
    return website;
  }

  private stripQuery(website: string): string {
    return website.split('?')[0];
  }

  private stripHash(website: string): string {
    return website.split('#')[0];
  }

  private get isSocialNetwork(): boolean {
    if (!this.website) return false;

    const url = new URL(this.glueProtocolToOrigin());

    return this.socialNetworks.includes(url.hostname);
  }

  private glueProtocolToOrigin(): string {
    if (!this.website.startsWith('http') || !this.website.startsWith('https')) {
      return `https://${this.website}`;
    }

    return this.website;
  }
}
