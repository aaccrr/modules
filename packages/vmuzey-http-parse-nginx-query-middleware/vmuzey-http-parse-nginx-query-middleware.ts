import { RequestPayload } from '../globals';
import { RouteMiddleware } from '../http-router';

export class VmuzeyHttpParseNginxQueryMiddleware {
  middleware(): RouteMiddleware {
    return async (ctx: RequestPayload, next) => {
      const isNginxQueryString = ctx.url.includes('%3F');

      if (isNginxQueryString) {
        ctx.url = ctx.url.replace('%3F', '?');
      }

      await next();
    };
  }
}
