import { Translitor } from './translitor';

const Module = new Translitor();
export { Module as Translitor };

export { ITranslitor } from './Interfaces';
