import { slugify } from 'transliteration';
import { ITranslitor } from './Interfaces';

export class Translitor implements ITranslitor {
  transform(str = '', { delim = '-' } = {}): string {
    return slugify(str, { separator: delim, allowedChars: 'a-zA-Z0-9' }).replace('.', '');
  }
}
