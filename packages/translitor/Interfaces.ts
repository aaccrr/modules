export interface ITranslitor {
  transform(str: string, options?: any): string;
}
