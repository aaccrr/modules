import { Readable } from 'stream';

export class VmuzeyHelperStreamFromBuffer {
  constructor(private buffer: Buffer | string) {
    this.toStream();
  }

  toStream(): Readable {
    const buffer = this.buffer;

    return new Readable({
      read() {
        this.push(buffer);
        this.push(null);
      },
    });
  }
}
