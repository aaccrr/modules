export const CONTACT_NAME_LENGTH = 200;
export const MINIMAL_CONTACTS_COUNT = 1;
export const MAXIMAL_CONTACTS_COUNT = 30;
export const LOCALITY_NAME_LENGTH = 300;
export const ADDRESS_LENGTH = 600;
export const CLIENT_CATEGORY_NAME_LENGTH = 1000;
export const CLIENT_GROUP_NAME_LENGTH = 1000;
export const MINIMUM_CLIENT_CATEGORIES = 1;
export const MAXIMUM_CLIENT_CATEGORIES = 50;
export const MINIMUM_CLIENT_GROUPS_IN_CATEGORY = 1;
export const MAXIMUM_CLIENT_GROUPS_IN_CATEGORY = 30;
export const MINIMAL_PRICE = 10;
export const MAXIMAL_PRICE = 1000000;

export const DATE_PATTERN = 'yyyy-MM-dd';
export const DATE_TIME_PATTERN = 'yyyy-MM-dd HH:mm:ss';
export const ONLY_DIGITS_FORMAT_REGEXP = '^\\d+$';
export const EMAIL_REGEXP = '.+@.+\\..+';
export const PHONE_REGEXP = '^\\+\\d{11,15}$';
export const TIME_REGEXP = '^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$';
export const DATE_REGEXP = '([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))';
export const ACCEPTED_GATEWAYS = ['client', 'admin', 'office', 'bank'];
export const REFRESH_TOKEN_EXPIRATION_TIME = 2592000;
export const ACCESS_TOKEN_EXPIRATION_TIME = 1800;
export const ACTIVATION_CODE_EXPIRATION_TIME = 86400;
export const IDEMPOTENCY_KEY_EXPIRATION_TIME = 86400;

export const MIN_PAGINATION_LIMIT = 0;
export const MAX_PAGINATION_LIMIT = 200;
export const MIN_PAGINATION_OFFSET = 0;
export const DEFAUL_PAGINATION_LIMIT = 10;

export const TimezoneOffset = {
  'Europe/Kaliningrad': 2,
  'Europe/Kirov': 3,
  'Europe/Moscow': 3,
  'Europe/Simferopol': 3,
  'Europe/Volgograd': 3,
  'Europe/Astrakhan': 4,
  'Europe/Samara': 4,
  'Europe/Saratov': 4,
  'Europe/Ulyanovsk': 4,
  'Asia/Yekaterinburg': 5,
  'Asia/Omsk': 6,
  'Asia/Barnaul': 7,
  'Asia/Krasnoyarsk': 7,
  'Asia/Novokuznetsk': 7,
  'Asia/Novosibirsk': 7,
  'Asia/Tomsk': 7,
  'Asia/Irkutsk': 8,
  'Asia/Chita': 9,
  'Asia/Khandyga': 9,
  'Asia/Yakutsk': 9,
  'Asia/Ust-Nera': 10,
  'Asia/Vladivostok': 10,
  'Asia/Magadan': 11,
  'Asia/Sakhalin': 11,
  'Asia/Srednekolymsk': 11,
  'Asia/Anadyr': 12,
  'Asia/Kamchatka': 12,
};
