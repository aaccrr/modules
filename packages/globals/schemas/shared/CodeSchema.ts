import { OpenapiStringSchema } from './openapi';

export const CodeSchema = OpenapiStringSchema({ maxLength: 64 });
