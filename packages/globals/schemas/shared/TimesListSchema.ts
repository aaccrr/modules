import { OpenapiArraySchema } from './openapi';
import { TimeSchema } from './TimeSchema';

export const TimesListSchema = OpenapiArraySchema({
  items: TimeSchema,
});
