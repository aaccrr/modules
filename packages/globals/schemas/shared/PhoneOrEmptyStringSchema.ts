import { PhoneSchema } from './PhoneSchema';
import { EmptyStringSchema } from './EmptyStringSchema';

export const PhoneOrEmptyStringSchema = EmptyStringSchema(PhoneSchema);
