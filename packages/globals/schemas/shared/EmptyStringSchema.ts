export const EmptyStringSchema = (stringSchema) => {
  return {
    oneOf: [
      stringSchema,
      {
        enum: [''],
      },
    ],
  };
};
