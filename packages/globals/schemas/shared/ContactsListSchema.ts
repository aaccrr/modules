import { CONTACT_NAME_LENGTH, MINIMAL_CONTACTS_COUNT, MAXIMAL_CONTACTS_COUNT } from '../../constants';
import { PhoneSchema } from './PhoneSchema';
import { OpenapiArraySchema, OpenapiBooleanSchema, OpenapiObjectSchema, OpenapiOneOfSchema, OpenapiStringSchema } from './openapi';
import { OpenapiEmailSchema } from './types';

export const ContactsListSchema = OpenapiArraySchema({
  items: OpenapiObjectSchema({
    properties: {
      name: OpenapiStringSchema({ maxLength: CONTACT_NAME_LENGTH }),
      phone: PhoneSchema,
      email: OpenapiOneOfSchema({
        oneOf: [OpenapiEmailSchema, OpenapiStringSchema({ enum: [''] })],
      }),
      isPublic: OpenapiBooleanSchema(),
    },
    required: ['name', 'phone'],
    additionalProperties: false,
  }),
  minItems: MINIMAL_CONTACTS_COUNT,
  maxItems: MAXIMAL_CONTACTS_COUNT,
});
