import { MAX_PAGINATION_LIMIT, MIN_PAGINATION_LIMIT } from '../../constants';

export const RequestPaginationSchema = {
  type: 'object',
  properties: {
    limit: {
      type: 'number',
      minimum: MIN_PAGINATION_LIMIT,
      maximum: MAX_PAGINATION_LIMIT,
      default: MAX_PAGINATION_LIMIT,
    },
    offset: {
      type: 'number',
      minimum: MIN_PAGINATION_LIMIT,
      default: MIN_PAGINATION_LIMIT,
    },
  },
  additionalProperties: false,
};
