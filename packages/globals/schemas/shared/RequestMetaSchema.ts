export const RequestMetaSchema = {
  type: 'object',
  properties: {
    clientIp: {
      type: 'string',
      format: 'ipv4',
    },
    requestId: {
      type: 'string',
    },
    user: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
        },
        role: {
          type: 'string',
        },
        organizationId: {
          type: 'string',
        },
        group: {
          type: 'string',
        },
        gh: {
          type: 'string',
        },
      },
      required: ['id', 'role', 'group'],
    },
  },
  additionalProperties: false,
  required: ['clientIp', 'requestId', 'user'],
};
