import { OpenAPIV3 } from 'openapi-types';
import { OpenapiStringSchema } from '../openapi';

export const OpenapiInternationalStringDateSchema = (params: OpenAPIV3.BaseSchemaObject = {}) => {
  return OpenapiStringSchema({ ...params, format: 'international_string_date', description: 'YYYY-MM-DD' });
};
