import { OpenAPIV3 } from 'openapi-types';
import { OpenapiStringSchema } from '../openapi';

export const OpenapiPhoneSchema: OpenAPIV3.SchemaObject = OpenapiStringSchema({ format: 'phone' });
