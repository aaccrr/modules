import { OpenAPIV3 } from 'openapi-types';
import { OpenapiStringSchema } from '../openapi';

export const OpenapiEmailSchema: OpenAPIV3.SchemaObject = OpenapiStringSchema({ format: 'email' });
