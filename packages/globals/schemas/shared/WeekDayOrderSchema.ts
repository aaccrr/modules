import { OpenapiNumberSchema } from './openapi';

export const WeekDayOrderSchema = OpenapiNumberSchema({ minimum: 1, maximum: 7 });
