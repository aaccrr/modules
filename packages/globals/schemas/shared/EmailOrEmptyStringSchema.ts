import { EmailSchema } from './EmailSchema';
import { EmptyStringSchema } from './EmptyStringSchema';

export const EmailOrEmptyStringSchema = EmptyStringSchema(EmailSchema);
