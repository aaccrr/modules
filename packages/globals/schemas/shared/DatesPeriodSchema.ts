import { DateSchema } from './DateSchema';

export const DatesPeriodSchema = {
  type: 'object',
  properties: {
    from: {
      type: 'object',
    },
    to: {
      type: 'object',
    },
  },
  required: ['from', 'to'],
  additionalProperties: false,
};
