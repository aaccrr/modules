import { OpenapiNumberSchema, OpenapiObjectSchema } from './openapi';

export const HourSchema = OpenapiNumberSchema({
  maximum: 23,
  minimum: 0,
});

export const MinuteSchema = OpenapiNumberSchema({
  maximum: 59,
  minimum: 0,
});

export const TimeObjectSchema = OpenapiObjectSchema({
  properties: {
    hour: HourSchema,
    minute: MinuteSchema,
  },
  additionalProperties: false,
  required: ['hour', 'minute'],
  nullable: true,
});
