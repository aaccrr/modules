import { OpenapiNumberSchema } from './openapi';

export const AgeCensorSchema = OpenapiNumberSchema({
  enum: [0, 6, 12, 16, 18],
  example: 12,
});
