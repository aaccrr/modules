import { OpenAPIV3 } from 'openapi-types';

export const OpenapiBooleanSchema = (params: Partial<OpenAPIV3.ArraySchemaObject> = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'boolean',
    ...(params as OpenAPIV3.ArraySchemaObject),
  };
};
