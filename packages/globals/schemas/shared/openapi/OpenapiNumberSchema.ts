import { OpenAPIV3 } from 'openapi-types';

export const OpenapiNumberSchema = (params: OpenAPIV3.BaseSchemaObject = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'number',
    ...params,
  };
};
