import { OpenAPIV3 } from 'openapi-types';
import { PaginationLimitSchema } from '../PaginationLimitSchema';
import { PaginationOffsetSchema } from '../PaginationOffsetSchema';

interface OpenapiPaginationSchemaParam {
  maxOffset?: number;
  maxLimit?: number;
}

export const OpenapiPaginationSchema = (param: OpenapiPaginationSchemaParam = {}): OpenAPIV3.ParameterObject[] => {
  return [
    {
      name: 'offset',
      in: 'query',
      schema: PaginationOffsetSchema({ maximum: param.maxOffset }),
    },
    {
      name: 'limit',
      in: 'query',
      schema: PaginationLimitSchema({ maximum: param.maxLimit }),
    },
  ];
};
