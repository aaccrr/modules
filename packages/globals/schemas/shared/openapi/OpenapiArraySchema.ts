import { OpenAPIV3 } from 'openapi-types';

export const OpenapiArraySchema = (params: Partial<OpenAPIV3.ArraySchemaObject>): OpenAPIV3.SchemaObject => {
  return {
    type: 'array',
    ...(params as OpenAPIV3.ArraySchemaObject),
  };
};
