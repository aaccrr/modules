import { OpenAPIV3 } from 'openapi-types';

export const OpenapiObjectSchema = (params: OpenAPIV3.BaseSchemaObject): OpenAPIV3.SchemaObject => {
  if (typeof params.additionalProperties === 'undefined') {
    params.additionalProperties = false;
  }

  return {
    ...params,
    type: 'object',
  };
};
