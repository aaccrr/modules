import { OpenAPIV3 } from 'openapi-types';

const emptyStringSchema: OpenAPIV3.SchemaObject = {
  type: 'string',
  enum: [''],
};

export const OpenapiEmptyStringSchema = (string: OpenAPIV3.SchemaObject) => {
  return {
    oneOf: [string, emptyStringSchema],
  };
};
