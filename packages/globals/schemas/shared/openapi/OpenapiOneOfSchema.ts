import { OpenAPIV3 } from 'openapi-types';

export const OpenapiOneOfSchema = (params: Partial<OpenAPIV3.SchemaObject>): OpenAPIV3.SchemaObject => {
  return {
    ...(params as OpenAPIV3.ArraySchemaObject),
  };
};
