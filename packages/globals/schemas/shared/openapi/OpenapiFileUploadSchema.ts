import { OpenapiStringSchema } from './OpenapiStringSchema';

export const OpenapiFileUploadSchema = OpenapiStringSchema({
  format: 'binary',
});
