import { LOCALITY_NAME_LENGTH, ADDRESS_LENGTH } from '../../constants';
import { OpenapiNumberSchema, OpenapiObjectSchema, OpenapiStringSchema } from './openapi';

export const LocationSchema = OpenapiObjectSchema({
  properties: {
    locality: OpenapiStringSchema({
      maxLength: LOCALITY_NAME_LENGTH,
    }),
    location: OpenapiObjectSchema({
      properties: {
        lon: OpenapiNumberSchema(),
        lat: OpenapiNumberSchema(),
      },
      required: ['lon', 'lat'],
      additionalProperties: false,
    }),
    address: OpenapiStringSchema({
      maxLength: ADDRESS_LENGTH,
    }),
  },
  required: ['location'],
  additionalProperties: false,
});
