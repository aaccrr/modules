import { OpenAPIV3 } from 'openapi-types';
import { MAX_PAGINATION_LIMIT, MIN_PAGINATION_LIMIT } from '../../constants';

export const PaginationLimitSchema = (params: OpenAPIV3.BaseSchemaObject = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'number',
    minimum: params.minimum || MIN_PAGINATION_LIMIT,
    maximum: params.maximum || MAX_PAGINATION_LIMIT,
  };
};
