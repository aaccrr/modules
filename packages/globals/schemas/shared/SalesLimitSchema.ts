import { OpenapiNumberSchema } from './openapi';

export const SalesLimitSchema = OpenapiNumberSchema({ minimum: 0, nullable: true });
