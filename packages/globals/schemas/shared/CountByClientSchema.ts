import { MINIMUM_ITEMS_IN_ORDER, MAXIMUM_ITEMS_IN_ORDER } from '../../constants';
import { OpenapiNumberSchema } from './openapi';

export const CountByClientSchema = OpenapiNumberSchema({
  minimum: MINIMUM_ITEMS_IN_ORDER,
  maximum: MAXIMUM_ITEMS_IN_ORDER,
  nullable: true,
});
