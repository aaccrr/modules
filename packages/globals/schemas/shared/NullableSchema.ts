export const NullableSchema = (schema) => ({ oneOf: [schema, { type: 'null' }] });
