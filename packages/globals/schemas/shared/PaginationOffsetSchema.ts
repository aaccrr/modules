import { OpenAPIV3 } from 'openapi-types';
import { MIN_PAGINATION_OFFSET } from '../../constants';

export const PaginationOffsetSchema = (params: OpenAPIV3.BaseSchemaObject = {}): OpenAPIV3.SchemaObject => {
  return {
    type: 'number',
    minimum: params.minimum || MIN_PAGINATION_OFFSET,
  };
};
