import { OpenAPIV3 } from 'openapi-types';

export const EmailSchema: OpenAPIV3.SchemaObject = {
  type: 'string',
  format: 'email',
};
