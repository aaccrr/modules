import { OpenapiStringSchema } from './openapi';

export const PhoneSchema = OpenapiStringSchema({ format: 'phone' });
