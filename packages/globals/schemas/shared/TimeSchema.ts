import { OpenapiStringSchema } from './openapi';

export const TimeSchema = OpenapiStringSchema({ format: 'time' });
