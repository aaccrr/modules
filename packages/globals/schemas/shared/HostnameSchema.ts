import { OpenAPIV3 } from 'openapi-types';

export const HostnameSchema: OpenAPIV3.SchemaObject = {
  type: 'string',
  format: 'hostname',
};
