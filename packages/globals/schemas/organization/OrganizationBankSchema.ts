import { OpenapiObjectSchema, OpenapiStringSchema } from '../shared/openapi';

export const OrganizationBankSchema = OpenapiObjectSchema({
  properties: {
    bill: OpenapiStringSchema({ format: 'digits', maxLength: 20 }),
    bankName: OpenapiStringSchema({ maxLength: 500 }),
    bankBIK: OpenapiStringSchema({ format: 'digits', maxLength: 9 }),
    bankKS: OpenapiStringSchema({ format: 'digits', maxLength: 20 }),
    kbk: OpenapiStringSchema({ format: 'digits', maxLength: 20 }),
    paymentsRecipient: OpenapiStringSchema({ maxLength: 200 }),
  },
  required: ['bill', 'bankName', 'bankBIK', 'bankKS'],
  additionalProperties: false,
});
