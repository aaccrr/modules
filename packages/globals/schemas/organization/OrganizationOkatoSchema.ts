import { OpenapiStringSchema, OpenapiEmptyStringSchema } from '../shared/openapi';

export const OrganizationOkatoSchema = OpenapiEmptyStringSchema(OpenapiStringSchema({ format: 'digits', maxLength: 11 }));
