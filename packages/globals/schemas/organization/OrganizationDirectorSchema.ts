import { OpenapiObjectSchema, OpenapiStringSchema, OpenapiEmptyStringSchema } from '../shared/openapi';
import { InnSchema } from './InnSchema';

export const OrganizationDirectorSchema = OpenapiObjectSchema({
  properties: {
    fio: OpenapiStringSchema({ maxLength: 300 }),
    inn: OpenapiEmptyStringSchema(InnSchema),
  },
  required: ['fio'],
  additionalProperties: false,
});
