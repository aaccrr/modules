import { OpenapiStringSchema, OpenapiEmptyStringSchema } from '../shared/openapi';

export const OrganizationOkpoSchema = OpenapiEmptyStringSchema(OpenapiStringSchema({ format: 'digits', maxLength: 10 }));
