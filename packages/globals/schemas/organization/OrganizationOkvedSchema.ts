import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationOkvedSchema = OpenapiStringSchema({ maxLength: 8 });
