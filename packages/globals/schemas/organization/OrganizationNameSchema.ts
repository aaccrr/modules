import { ORGANIZATION_NAME_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationNameSchema = OpenapiStringSchema({ maxLength: ORGANIZATION_NAME_LENGTH });
