import { ORGANIZATION_ADDRESS_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationAddressSchema = OpenapiStringSchema({ maxLength: ORGANIZATION_ADDRESS_LENGTH });
