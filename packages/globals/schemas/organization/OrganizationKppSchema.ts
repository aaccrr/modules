import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationKppSchema = OpenapiStringSchema({ format: 'digits', maxLength: 9 });
