import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationStatusSchema = OpenapiStringSchema({ maxLength: 50 });
