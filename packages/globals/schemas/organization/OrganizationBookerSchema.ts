import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationBookerSchema = OpenapiStringSchema({ maxLength: 300 });
