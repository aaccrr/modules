import { OpenapiStringSchema, OpenapiEmptyStringSchema } from '../shared/openapi';

export const OrganizationOkoguSchema = OpenapiEmptyStringSchema(OpenapiStringSchema({ format: 'digits', maxLength: 8, minLength: 7 }));
