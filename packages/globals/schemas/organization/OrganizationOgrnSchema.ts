import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationOgrnSchema = OpenapiStringSchema({ format: 'digits', maxLength: 15 });
