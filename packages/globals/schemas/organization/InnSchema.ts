import { OpenapiStringSchema } from '../shared/openapi';

export const InnSchema = OpenapiStringSchema({ format: 'digits', maxLength: 12 });
