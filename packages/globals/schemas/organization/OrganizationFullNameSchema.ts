import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationFullNameSchema = OpenapiStringSchema({ maxLength: 800 });
