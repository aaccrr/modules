import { ORGANIZATION_ID_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared/openapi';

export const OrganizationIdSchema = OpenapiStringSchema({ maxLength: ORGANIZATION_ID_LENGTH, minLength: ORGANIZATION_ID_LENGTH });
