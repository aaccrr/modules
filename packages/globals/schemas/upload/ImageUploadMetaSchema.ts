import { OpenapiObjectSchema, OpenapiStringSchema } from '../shared';
import { ImageUploadActionSchema } from './ImageUploadActionSchema';
import { ImageUploadCropSchema } from './ImageUploadCropSchema';

export const ImageUploadMetaSchema = OpenapiObjectSchema({
  properties: {
    action: ImageUploadActionSchema,
    file: OpenapiStringSchema(),
    crop: ImageUploadCropSchema,
  },
});
