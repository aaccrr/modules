import { OpenapiStringSchema } from '../shared';

export const DeletableUploadActionSchema = OpenapiStringSchema({
  enum: ['IDLE', 'ADD', 'UPDATE', 'DELETE'],
});
