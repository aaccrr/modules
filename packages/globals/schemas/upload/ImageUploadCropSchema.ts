import { OpenapiNumberSchema, OpenapiObjectSchema } from '../shared';

export const ImageUploadCropSchema = OpenapiObjectSchema({
  properties: {
    width: OpenapiNumberSchema(),
    height: OpenapiNumberSchema(),
    x: OpenapiNumberSchema(),
    y: OpenapiNumberSchema(),
  },
});
