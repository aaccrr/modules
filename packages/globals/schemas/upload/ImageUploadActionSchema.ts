import { OpenapiStringSchema } from '../shared';

export const ImageUploadActionSchema = OpenapiStringSchema({
  enum: ['IDLE', 'ADD', 'UPDATE'],
});
