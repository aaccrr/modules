import { OpenapiNumberSchema, OpenapiObjectSchema, OpenapiStringSchema } from '../shared';
import { DeletableUploadActionSchema } from './DeletableUploadActionSchema';
import { ImageUploadCropSchema } from './ImageUploadCropSchema';

export const OrderedImageUploadSchema = OpenapiObjectSchema({
  properties: {
    action: DeletableUploadActionSchema,
    file: OpenapiStringSchema(),
    crop: ImageUploadCropSchema,
    order: OpenapiNumberSchema(),
  },
});
