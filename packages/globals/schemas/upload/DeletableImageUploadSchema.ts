import { OpenapiObjectSchema, OpenapiStringSchema } from '../shared';
import { DeletableUploadActionSchema } from './DeletableUploadActionSchema';
import { ImageUploadCropSchema } from './ImageUploadCropSchema';

export const DeletableImageUploadSchema = OpenapiObjectSchema({
  properties: {
    action: DeletableUploadActionSchema,
    file: OpenapiStringSchema(),
    crop: ImageUploadCropSchema,
  },
});
