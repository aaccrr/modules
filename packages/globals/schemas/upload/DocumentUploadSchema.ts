import { OpenapiObjectSchema, OpenapiStringSchema } from '../shared';
import { ImageUploadActionSchema } from './ImageUploadActionSchema';

export const DocumentUploadSchema = OpenapiObjectSchema({
  properties: {
    action: ImageUploadActionSchema,
    file: OpenapiStringSchema(),
  },
});
