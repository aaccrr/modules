import { OpenapiStringSchema } from '../shared';

export const AccountPatronymicSchema = OpenapiStringSchema({ minLength: 2, maxLength: 100 });
