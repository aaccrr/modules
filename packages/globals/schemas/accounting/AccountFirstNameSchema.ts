import { OpenapiStringSchema } from '../shared';

export const AccountFirstNameSchema = OpenapiStringSchema({ minLength: 2, maxLength: 100 });
