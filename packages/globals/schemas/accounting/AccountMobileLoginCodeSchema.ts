import { ACCOUNT_MOBILE_CODE_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const AccountMobileLoginCodeSchema = OpenapiStringSchema({ maxLength: ACCOUNT_MOBILE_CODE_LENGTH });
