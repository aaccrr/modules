import { UserRole } from '../../interfaces/shared';
import { OpenapiStringSchema } from '../shared';

export const OfficeRolesSchema = OpenapiStringSchema({ enum: [UserRole.OFFICE_ADMIN, UserRole.CONTENT_MANAGER, UserRole.MODERATOR] });
