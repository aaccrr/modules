import { OpenapiStringSchema } from '../shared';

export const AccountLastNameSchema = OpenapiStringSchema({ minLength: 2, maxLength: 100 });
