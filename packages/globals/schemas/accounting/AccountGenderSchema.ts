import { Gender } from '../../../interfaces';
import { OpenapiStringSchema } from '../shared';

export const AccountGenderSchema = OpenapiStringSchema({ enum: [Gender.FEMALE, Gender.MALE] });
