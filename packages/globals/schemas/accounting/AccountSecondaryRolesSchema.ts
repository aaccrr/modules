import { UserRole } from '../../interfaces/shared';
import { OpenapiStringSchema } from '../shared';

export const AccountSecondaryRolesSchema = OpenapiStringSchema({ enum: [UserRole.BOOKER, UserRole.MANAGER] });
