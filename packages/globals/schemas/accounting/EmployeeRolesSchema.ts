import { UserRole } from '../../interfaces/shared';
import { OpenapiStringSchema } from '../shared';

export const EmployeeRolesSchema = OpenapiStringSchema({ enum: [UserRole.CONTROLLER, UserRole.GUIDE] });
