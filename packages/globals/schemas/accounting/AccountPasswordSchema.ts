import { MAXIMAL_PASSWORD_LENGTH, MINIMAL_PASSWORD_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const AccountPasswordSchema = OpenapiStringSchema({ minLength: MINIMAL_PASSWORD_LENGTH, maxLength: MAXIMAL_PASSWORD_LENGTH });
