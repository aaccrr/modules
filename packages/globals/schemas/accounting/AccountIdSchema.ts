import { ACCOUNT_ID_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const AccountIdSchema = OpenapiStringSchema({ maxLength: ACCOUNT_ID_LENGTH });
