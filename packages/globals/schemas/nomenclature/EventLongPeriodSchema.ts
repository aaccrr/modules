import { OpenapiObjectSchema, OpenapiStringSchema, LocationSchema } from '../shared';

export const EventLongPeriodSchema = OpenapiObjectSchema({
  properties: {
    start: OpenapiStringSchema({
      format: 'international_string_date',
    }),
    finish: OpenapiStringSchema({
      format: 'international_string_date',
    }),
    saleStart: OpenapiStringSchema({
      format: 'international_string_date',
    }),
    address: LocationSchema,
    placeId: OpenapiStringSchema(),
  },
  required: ['start', 'finish', 'saleStart'],
});
