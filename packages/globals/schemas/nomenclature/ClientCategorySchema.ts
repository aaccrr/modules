import { OpenapiObjectSchema } from '../shared';
import { ClientCategoryNameSchema } from './ClientCategoryNameSchema';
import { ClientCategoryPriceSchema } from './ClientCategoryPriceSchema';
import { ClientGroupsListSchema } from './ClientGroupsListSchema';

export const ClientCategorySchema = OpenapiObjectSchema({
  properties: {
    name: ClientCategoryNameSchema,
    price: ClientCategoryPriceSchema(),
    groups: ClientGroupsListSchema,
  },
  additionalProperties: false,
  required: ['name', 'price', 'groups'],
});
