import { OpenapiStringSchema } from '../shared';

export const EventIdSchema = OpenapiStringSchema({ pattern: '^E[0-9]{12}$' });
