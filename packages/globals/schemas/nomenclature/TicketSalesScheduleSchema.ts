import { MAXIMUM_INTERVALS_COUNT, MINIMUM_INTERVALS_COUNT } from '../../constants';
import {
  TimeSchema,
  WeekDayOrderSchema,
  SalesLimitSchema,
  OpenapiObjectSchema,
  OpenapiArraySchema,
  OpenapiInternationalStringDateSchema,
} from '../shared';

export const FreeSalesSchedule = OpenapiObjectSchema({
  properties: {
    date: OpenapiInternationalStringDateSchema(),
    online: SalesLimitSchema,
    offline: SalesLimitSchema,
  },
  additionalProperties: false,
  required: ['date', 'online', 'offline'],
});

export const WeeklySalesSchedule = OpenapiObjectSchema({
  properties: {
    weekDayOrder: WeekDayOrderSchema,
    online: SalesLimitSchema,
    offline: SalesLimitSchema,
  },
  additionalProperties: false,
  required: ['weekDayOrder', 'online', 'offline'],
});

export const ToTimeSalesSchedule = OpenapiObjectSchema({
  properties: {
    date: OpenapiInternationalStringDateSchema(),
    intervals: OpenapiArraySchema({
      items: OpenapiObjectSchema({
        properties: {
          from: TimeSchema,
          to: TimeSchema,
          online: SalesLimitSchema,
          offline: SalesLimitSchema,
        },
        additionalProperties: false,
        required: ['from', 'to', 'online', 'offline'],
      }),
      minItems: MINIMUM_INTERVALS_COUNT,
      maxItems: MAXIMUM_INTERVALS_COUNT,
    }),
  },
  additionalProperties: false,
  required: ['date', 'intervals'],
});

export const TicketSalesScheduleSchema = OpenapiObjectSchema({
  properties: {
    weekly: OpenapiArraySchema({ items: WeeklySalesSchedule }),
    free: OpenapiArraySchema({ items: FreeSalesSchedule }),
    toTime: OpenapiArraySchema({ items: ToTimeSalesSchedule }),
  },
  additionalProperties: false,
});
