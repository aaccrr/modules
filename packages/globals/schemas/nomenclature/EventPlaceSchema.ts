import { LOCALITY_NAME_LENGTH, ADDRESS_LENGTH } from '../../constants';
import { OpenapiNumberSchema, OpenapiObjectSchema, OpenapiStringSchema } from '../shared/openapi';

export const EventPlaceSchema = OpenapiObjectSchema({
  properties: {
    id: OpenapiStringSchema(),
    locality: OpenapiStringSchema({
      maxLength: LOCALITY_NAME_LENGTH,
    }),
    location: OpenapiObjectSchema({
      properties: {
        lon: OpenapiNumberSchema(),
        lat: OpenapiNumberSchema(),
      },
      required: ['lon', 'lat'],
      additionalProperties: false,
    }),
    address: OpenapiStringSchema({
      maxLength: ADDRESS_LENGTH,
    }),
  },
  required: ['location'],
  additionalProperties: false,
});
