import { OpenapiStringSchema } from '../shared';

export const TicketIdSchema = OpenapiStringSchema({ pattern: '^T[0-9]{12}$' });
