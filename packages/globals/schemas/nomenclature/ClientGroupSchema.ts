import { CLIENT_GROUP_NAME_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const ClientGroupSchema = OpenapiStringSchema({ maxLength: CLIENT_GROUP_NAME_LENGTH });
