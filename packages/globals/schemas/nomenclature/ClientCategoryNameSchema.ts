import { CLIENT_CATEGORY_NAME_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const ClientCategoryNameSchema = OpenapiStringSchema({ maxLength: CLIENT_CATEGORY_NAME_LENGTH });
