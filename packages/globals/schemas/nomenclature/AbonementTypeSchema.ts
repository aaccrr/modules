import { SeasonTicketPatternType } from '../../interfaces/nomenclature';
import { OpenapiStringSchema } from '../shared';

export const AbonementTypeSchema = OpenapiStringSchema({
  enum: [SeasonTicketPatternType.VISITS, SeasonTicketPatternType.UNIVERSAL, SeasonTicketPatternType.EVENTS, SeasonTicketPatternType.PERIOD],
});
