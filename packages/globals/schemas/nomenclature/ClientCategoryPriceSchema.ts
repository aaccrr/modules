import { OpenAPIV3 } from 'openapi-types';
import { MINIMAL_PRICE, MAXIMAL_PRICE } from '../../constants';
import { OpenapiNumberSchema, OpenapiOneOfSchema } from '../shared';

const ClientCategoryZeroPriceSchema = OpenapiNumberSchema({ minimum: 0, maximum: 0 });
const ClientCategoryNormalPriceSchema = OpenapiNumberSchema({ minimum: MINIMAL_PRICE, maximum: MAXIMAL_PRICE });

export const ClientCategoryPriceSchema = (params: OpenAPIV3.SchemaObject = {}) =>
  OpenapiOneOfSchema({
    oneOf: [ClientCategoryNormalPriceSchema, ClientCategoryZeroPriceSchema],
    ...params,
  });
