import {
  MINIMUM_VISITS_IN_ABONEMENT,
  MAXIMUM_VISITS_IN_ABONEMENT,
  MINIMUM_EVENTS_IN_ABONEMENT,
  MAXIMUM_EVENTS_IN_ABONEMENT,
  MINIMAL_ABONEMENT_PERIOD,
  MAXIMAL_ABONEMENT_PERIOD,
  ABONEMENT_EVENT_TITLE_LENGTH,
} from '../../constants';
import {
  TimeSchema,
  OpenapiNumberSchema,
  OpenapiObjectSchema,
  OpenapiStringSchema,
  OpenapiInternationalStringDateSchema,
  OpenapiArraySchema,
} from '../shared';

export const AbonementVisitsSchema = OpenapiNumberSchema({
  minimum: MINIMUM_VISITS_IN_ABONEMENT,
  maximum: MAXIMUM_VISITS_IN_ABONEMENT,
  nullable: true,
});

export const AbonementPeriodSchema = OpenapiNumberSchema({
  minimum: MINIMAL_ABONEMENT_PERIOD,
  maximum: MAXIMAL_ABONEMENT_PERIOD,
  nullable: true,
});

export const AbonementEventSchema = OpenapiObjectSchema({
  properties: {
    name: OpenapiStringSchema({ maxLength: ABONEMENT_EVENT_TITLE_LENGTH }),
    date: OpenapiInternationalStringDateSchema(),
    time: TimeSchema,
  },
  additionalProperties: false,
  required: ['name', 'date', 'time'],
});

export const AbonementEventsListSchema = OpenapiArraySchema({
  items: AbonementEventSchema,
  minItems: MINIMUM_EVENTS_IN_ABONEMENT,
  maxItems: MAXIMUM_EVENTS_IN_ABONEMENT,
  nullable: true,
});

export const AbonementPatternsSchema = OpenapiObjectSchema({
  properties: {
    events: AbonementEventsListSchema,
    visits: AbonementVisitsSchema,
    period: AbonementPeriodSchema,
  },
  additionalProperties: false,
});
