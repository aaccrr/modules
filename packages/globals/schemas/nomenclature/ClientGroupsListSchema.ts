import { ClientGroupSchema } from './ClientGroupSchema';
import { MAXIMUM_CLIENT_GROUPS_IN_CATEGORY, MINIMUM_CLIENT_GROUPS_IN_CATEGORY } from '../../constants';
import { OpenapiArraySchema } from '../shared';

export const ClientGroupsListSchema = OpenapiArraySchema({
  items: ClientGroupSchema,
  minItems: MINIMUM_CLIENT_GROUPS_IN_CATEGORY,
  maxItems: MAXIMUM_CLIENT_GROUPS_IN_CATEGORY,
});
