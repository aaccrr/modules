import { ClientCategoryPriceSchema } from './ClientCategoryPriceSchema';

export const EventClientCategoryPriceSchema = ClientCategoryPriceSchema({ nullable: true });
