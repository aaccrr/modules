import { OpenapiStringSchema } from '../shared';

export const AbonementIdSchema = OpenapiStringSchema({ pattern: '^A[0-9]{12}$' });
