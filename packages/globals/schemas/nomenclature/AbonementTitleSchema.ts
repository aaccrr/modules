import { ABONEMENT_TITLE_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const AbonementTitleSchema = OpenapiStringSchema({ maxLength: ABONEMENT_TITLE_LENGTH });
