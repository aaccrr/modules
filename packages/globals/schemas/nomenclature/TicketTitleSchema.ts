import { TICKET_TITLE_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const TicketTitleSchema = OpenapiStringSchema({ maxLength: TICKET_TITLE_LENGTH });
