import { MAXIMAL_EVENT_DESCRIPTION_LENGTH, MINIMAL_EVENT_DESCRIPTION_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const EventDescriptionSchema = OpenapiStringSchema({
  minLength: MINIMAL_EVENT_DESCRIPTION_LENGTH,
  maxLength: MAXIMAL_EVENT_DESCRIPTION_LENGTH,
});
