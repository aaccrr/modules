import { OpenapiArraySchema, OpenapiBooleanSchema, OpenapiInternationalStringDateSchema, OpenapiObjectSchema } from '../shared';
import { EventSalesLimitSchema } from './EventSalesLimitSchema';
import { MINIMUM_DAYS_IN_EVENT_UNION_TICKET, MAXIMUM_DAYS_IN_EVENT_UNION_TICKET } from '../../constants';

export const EventUnionTicketSchema = OpenapiObjectSchema({
  properties: {
    dates: OpenapiArraySchema({
      items: OpenapiInternationalStringDateSchema(),
      minItems: MINIMUM_DAYS_IN_EVENT_UNION_TICKET,
      maxItems: MAXIMUM_DAYS_IN_EVENT_UNION_TICKET,
    }),
    salesStartsAt: OpenapiInternationalStringDateSchema(),
    tickets: EventSalesLimitSchema,
    isFree: OpenapiBooleanSchema(),
  },
  additionalProperties: false,
  required: ['dates', 'salesStartsAt', 'tickets'],
});
