import { ClientCategorySchema } from './ClientCategorySchema';
import { MINIMUM_CLIENT_CATEGORIES, MAXIMUM_CLIENT_CATEGORIES } from '../../constants';
import { OpenapiArraySchema } from '../shared';

export const ClientCategoriesListSchema = OpenapiArraySchema({
  items: ClientCategorySchema,
  minItems: MINIMUM_CLIENT_CATEGORIES,
  maxItems: MAXIMUM_CLIENT_CATEGORIES,
});
