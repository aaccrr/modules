import { EVENT_SLUG_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const EventSlugSchema = OpenapiStringSchema({ maxLength: EVENT_SLUG_LENGTH });
