import { OpenapiStringSchema } from '../shared';

export const TicketScheduleTypesEnumSchema = OpenapiStringSchema({ enum: ['weekly', 'free', 'toTime'] });
