import { OpenapiBooleanSchema } from '../shared';

export const TicketCanBeTransferredSchema = OpenapiBooleanSchema();
