import { MINIMAL_EVENT_CATEGORY_ID, MAXIMAL_EVENT_CATEGORY_ID, EVENT_CATEGORY_TITLE_LENGTH } from '../../constants';
import { OpenapiNumberSchema, OpenapiOneOfSchema, OpenapiStringSchema } from '../shared';

export const DefaultEventCategorySchema = OpenapiNumberSchema({
  minimum: MINIMAL_EVENT_CATEGORY_ID,
  maximum: MAXIMAL_EVENT_CATEGORY_ID,
});

export const CustomEventCategorySchema = OpenapiStringSchema({ maxLength: EVENT_CATEGORY_TITLE_LENGTH });

export const EventCategorySchema = OpenapiOneOfSchema({
  oneOf: [DefaultEventCategorySchema, CustomEventCategorySchema],
});
