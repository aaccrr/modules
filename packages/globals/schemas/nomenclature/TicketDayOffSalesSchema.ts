import { OpenapiBooleanSchema } from '../shared';

export const TicketDayOffSalesSchema = OpenapiBooleanSchema();
