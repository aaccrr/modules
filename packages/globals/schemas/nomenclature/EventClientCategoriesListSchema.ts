import {
  MINIMUM_CLIENT_CATEGORIES,
  MAXIMUM_CLIENT_CATEGORIES,
  MINIMUM_DAYS_IN_EVENT_UNION_TICKET,
  MAXIMUM_DAYS_IN_EVENT_UNION_TICKET,
  MINIMUM_DAYS_IN_EVENT,
  MAXIMUM_DAYS_IN_EVENT,
} from '../../constants';

import { EventClientCategoryPriceSchema } from './EventClientCategoryPriceSchema';
import { ClientCategoryNameSchema } from './ClientCategoryNameSchema';
import { ClientGroupsListSchema } from './ClientGroupsListSchema';
import { OpenapiArraySchema, OpenapiInternationalStringDateSchema, OpenapiObjectSchema, OpenapiOneOfSchema } from '../shared';

export const EventClientCategorySchema = OpenapiObjectSchema({
  properties: {
    name: ClientCategoryNameSchema,
    prices: OpenapiArraySchema({
      items: OpenapiObjectSchema({
        properties: {
          date: OpenapiOneOfSchema({
            oneOf: [
              OpenapiInternationalStringDateSchema(),
              OpenapiArraySchema({
                items: OpenapiInternationalStringDateSchema(),
                minItems: MINIMUM_DAYS_IN_EVENT_UNION_TICKET,
                maxItems: MAXIMUM_DAYS_IN_EVENT_UNION_TICKET,
              }),
            ],
          }),
          price: OpenapiOneOfSchema({
            properties: {
              online: EventClientCategoryPriceSchema,
              offline: EventClientCategoryPriceSchema,
            },
            required: ['online'],
            additionalProperties: false,
          }),
        },
        additionalProperties: false,
        required: ['date', 'price'],
      }),
      minItems: MINIMUM_DAYS_IN_EVENT,
      maxItems: MAXIMUM_DAYS_IN_EVENT,
    }),
    groups: ClientGroupsListSchema,
  },
  additionalProperties: false,
  required: ['name', 'prices', 'groups'],
});

export const EventClientCategoriesListSchema = OpenapiArraySchema({
  items: EventClientCategorySchema,
  minItems: MINIMUM_CLIENT_CATEGORIES,
  maxItems: MAXIMUM_CLIENT_CATEGORIES,
});
