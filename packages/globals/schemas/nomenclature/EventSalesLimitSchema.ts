import { OpenapiObjectSchema, SalesLimitSchema } from '../shared';

export const EventSalesLimitSchema = OpenapiObjectSchema({
  properties: {
    online: SalesLimitSchema,
    offline: SalesLimitSchema,
  },
  additionalProperties: false,
  required: ['online'],
});
