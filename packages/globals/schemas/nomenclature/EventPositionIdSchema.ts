import { MINIMUM_DAYS_IN_EVENT, MAXIMUM_DAYS_IN_EVENT } from '../../constants';
import { OpenapiNumberSchema } from '../shared';

export const EventPositionIdSchema = OpenapiNumberSchema({
  minimum: MINIMUM_DAYS_IN_EVENT,
  maximum: MAXIMUM_DAYS_IN_EVENT,
});
