import { OpenapiInternationalStringDateSchema } from '../shared';

export const AbonementSalesEndDateSchema = OpenapiInternationalStringDateSchema({ nullable: true });
