import { EVENT_TITLE_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const EventTitleSchema = OpenapiStringSchema({ maxLength: EVENT_TITLE_LENGTH });
