import { OpenAPIV3 } from 'openapi-types';
import { SeasonTicketPatternType } from '../../../interfaces';

const SeasonTicketEventSchema: OpenAPIV3.SchemaObject = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
    },
    startsAt: {
      type: 'string',
      format: 'date-time',
    },
  },
};

const SeasonTicketWithVisitsPatternSchema: OpenAPIV3.SchemaObject = {
  type: 'object',
  properties: {
    type: {
      type: 'string',
      example: SeasonTicketPatternType.VISITS,
    },
    visits: {
      type: 'number',
      example: 20,
    },
  },
};

const SeasonTicketWithPeriodPatternSchema: OpenAPIV3.SchemaObject = {
  type: 'object',
  properties: {
    type: {
      type: 'string',
      example: SeasonTicketPatternType.PERIOD,
    },
    period: {
      type: 'number',
      example: 30,
    },
  },
};

const SeasonTicketWithEventsPatternSchema: OpenAPIV3.SchemaObject = {
  type: 'object',
  properties: {
    type: {
      type: 'string',
      example: SeasonTicketPatternType.EVENTS,
    },
    events: {
      type: 'array',
      items: SeasonTicketEventSchema,
    },
  },
};

export const SeasonTicketPatternSchema: OpenAPIV3.SchemaObject = {
  oneOf: [SeasonTicketWithVisitsPatternSchema, SeasonTicketWithPeriodPatternSchema, SeasonTicketWithEventsPatternSchema],
};
