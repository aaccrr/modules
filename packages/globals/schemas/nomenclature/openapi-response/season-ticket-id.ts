import { OpenAPIV3 } from 'openapi-types';

export const SeasonTicketIdSchema: OpenAPIV3.SchemaObject = {
  type: 'string',
  pattern: '^A[0-9]{12}$',
};
