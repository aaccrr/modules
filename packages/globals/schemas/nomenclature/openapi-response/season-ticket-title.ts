import { ABONEMENT_TITLE_LENGTH } from '../../../constants';

export const SeasonTicketTitleSchema = {
  type: 'string',
  maxLength: ABONEMENT_TITLE_LENGTH,
};
