import { OpenapiBooleanSchema } from '../shared';

export const TicketIsPersonalSchema = OpenapiBooleanSchema();
