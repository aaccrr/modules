import { MINIMUM_VALIDITY_PERIOD, MAXIMUM_VALIDITY_PERIOD } from '../../constants';
import { OpenapiNumberSchema } from '../shared';

export const TicketExpiresAtSchema = OpenapiNumberSchema({
  minimum: MINIMUM_VALIDITY_PERIOD,
  maximum: MAXIMUM_VALIDITY_PERIOD,
  nullable: true,
});
