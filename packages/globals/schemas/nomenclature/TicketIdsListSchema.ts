import { OpenapiArraySchema } from '../shared';
import { TicketIdSchema } from './TicketIdSchema';

export const TicketIdsListSchema = OpenapiArraySchema({ items: TicketIdSchema });
