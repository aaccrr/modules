import {
  TIMELINE_SECTION_TITLE_LENGTH,
  MINIMUM_TIMELINE_SECTIONS_IN_DAY,
  MAXIMUM_TIMELINE_SECTIONS_IN_DAY,
  MINIMUM_DAYS_IN_EVENT,
  MAXIMUM_DAYS_IN_EVENT,
} from '../../constants';
import {
  TimeSchema,
  LocationSchema,
  OpenapiObjectSchema,
  OpenapiBooleanSchema,
  OpenapiStringSchema,
  OpenapiArraySchema,
  OpenapiInternationalStringDateSchema,
} from '../shared';
import { EventSalesLimitSchema } from './EventSalesLimitSchema';

export const EventDayDurationSchema = OpenapiObjectSchema({
  properties: {
    from: TimeSchema,
    to: TimeSchema,
    atNight: OpenapiBooleanSchema(),
  },
  additionalProperties: false,
  required: ['from', 'to'],
});

export const DayTimelineSectionSchema = OpenapiObjectSchema({
  properties: {
    from: TimeSchema,
    to: TimeSchema,
    description: OpenapiStringSchema({ maxLength: TIMELINE_SECTION_TITLE_LENGTH }),
  },
  additionalProperties: false,
  required: ['from', 'to', 'description'],
});

export const DayTimelineSchema = OpenapiArraySchema({
  items: DayTimelineSectionSchema,
  minItems: MINIMUM_TIMELINE_SECTIONS_IN_DAY,
  maxItems: MAXIMUM_TIMELINE_SECTIONS_IN_DAY,
});

export const EventDaySchema = OpenapiObjectSchema({
  properties: {
    date: OpenapiInternationalStringDateSchema(),
    duration: EventDayDurationSchema,
    address: LocationSchema,
    salesStartsAt: OpenapiInternationalStringDateSchema(),
    timeline: DayTimelineSchema,
    tickets: EventSalesLimitSchema,
    isFree: OpenapiBooleanSchema(),
    placeId: OpenapiStringSchema(),
  },
  additionalProperties: false,
  required: ['date', 'duration', 'salesStartsAt', 'tickets'],
});

export const EventDaysListSchema = OpenapiArraySchema({
  items: EventDaySchema,
  maxItems: MAXIMUM_DAYS_IN_EVENT,
});
