import { MINIMAL_ABONEMENT_SALES_LIMIT, MAXIMAL_ABONEMENT_SALES_LIMIT } from '../../constants';
import { OpenapiNumberSchema } from '../shared';

export const AbonementSalesLimitSchema = OpenapiNumberSchema({
  minimum: MINIMAL_ABONEMENT_SALES_LIMIT,
  maximum: MAXIMAL_ABONEMENT_SALES_LIMIT,
  nullable: true,
});
