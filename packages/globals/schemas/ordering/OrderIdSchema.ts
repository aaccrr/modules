import { OpenapiStringSchema } from '../shared';

export const OrderIdSchema = OpenapiStringSchema({ pattern: '[0-9]{12}' });
