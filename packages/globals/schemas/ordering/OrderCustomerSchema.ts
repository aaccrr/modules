import { AccountFirstNameSchema, AccountLastNameSchema } from '../accounting';
import { OpenapiObjectSchema, OpenapiPhoneSchema, OpenapiEmailSchema } from '../shared';

export const OrderCustomerSchema = OpenapiObjectSchema({
  properties: {
    firstName: AccountFirstNameSchema,
    lastName: AccountLastNameSchema,
    phone: OpenapiPhoneSchema,
    email: OpenapiEmailSchema,
  },
  additionalProperties: false,
});
