import { OpenapiArraySchema } from '../shared';
import { EnumNomenclatureTypesSchema } from './EnumNomenclatureTypesSchema';

export const ListEnumNomenclatureTypesSchema = OpenapiArraySchema({
  items: EnumNomenclatureTypesSchema,
});
