import { ClientCategoryIdSchema } from './ClientCategoryIdSchema';
import { CountByClientSchema, OpenapiObjectSchema } from '../shared';

export const OrderPositionCategorySchema = OpenapiObjectSchema({
  properties: {
    id: ClientCategoryIdSchema,
    count: CountByClientSchema,
  },
  required: ['id', 'count'],
  additionalProperties: false,
});
