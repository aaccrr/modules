import { NomenclatureTypeEnum } from '../../interfaces';
import { OpenapiStringSchema } from '../shared';

export const EnumNomenclatureTypesSchema = OpenapiStringSchema({
  enum: [NomenclatureTypeEnum.TICKET, NomenclatureTypeEnum.ABONEMENT, NomenclatureTypeEnum.EVENT],
});
