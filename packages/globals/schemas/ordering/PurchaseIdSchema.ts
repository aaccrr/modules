import { OpenapiStringSchema } from '../shared';

export const PurchaseIdSchema = OpenapiStringSchema({ pattern: '^(VM)?[0-9]{12}$' });
