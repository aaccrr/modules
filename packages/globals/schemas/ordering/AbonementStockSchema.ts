import { OpenapiObjectSchema, SalesLimitSchema } from '../shared';

export const AbonementLimitsSchema = OpenapiObjectSchema({
  properties: {
    online: SalesLimitSchema,
    offline: SalesLimitSchema,
  },
  required: ['online', 'offline'],
  additionalProperties: false,
});
