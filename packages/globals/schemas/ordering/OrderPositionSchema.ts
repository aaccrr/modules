import { OrderPositionCategorySchema } from './OrderPositionCategorySchema';
import { NomenclatureIdSchema } from './NomenclatureIdSchema';
import { TimeSchema, OpenapiObjectSchema, OpenapiArraySchema, OpenapiInternationalStringDateSchema } from '../shared';
import { EventPositionIdSchema } from '../nomenclature';

export const OrderPositionSchema = OpenapiObjectSchema({
  properties: {
    id: NomenclatureIdSchema,
    categories: OpenapiArraySchema({
      items: OrderPositionCategorySchema,
      minItems: 1,
    }),
    date: OpenapiInternationalStringDateSchema(),
    time: TimeSchema,
    positionId: EventPositionIdSchema,
  },
  required: ['id', 'categories'],
  additionalProperties: false,
});
