import { OpenapiStringSchema } from '../shared';

export const ItemIdSchema = OpenapiStringSchema({ pattern: 'VM[0-9]{12}' });
