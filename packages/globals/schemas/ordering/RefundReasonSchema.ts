import { OpenapiStringSchema } from '../shared';

export const RefundReasonSchema = OpenapiStringSchema({
  maxLength: 1000,
});
