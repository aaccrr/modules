import { OpenapiStringSchema } from '../shared';

export const RefundIdSchema = OpenapiStringSchema({
  pattern: '[0-9]{12}',
});
