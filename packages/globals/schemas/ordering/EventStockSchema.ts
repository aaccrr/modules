import { OpenapiArraySchema, OpenapiObjectSchema, SalesLimitSchema } from '../shared';
import { EventPositionIdSchema } from '../nomenclature';

export const EventPositionListSchema = OpenapiArraySchema({
  items: OpenapiObjectSchema({
    properties: {
      id: EventPositionIdSchema,
      online: SalesLimitSchema,
      offline: SalesLimitSchema,
    },
    required: ['id', 'online', 'offline'],
    additionalProperties: false,
  }),
});
