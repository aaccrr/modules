import { OpenapiStringSchema } from '../shared';

export const NomenclatureIdSchema = OpenapiStringSchema({ pattern: '^(T|A|E)[0-9]{12}$' });
