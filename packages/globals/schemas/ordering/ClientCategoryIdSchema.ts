import { MINIMUM_CLIENT_CATEGORIES, MAXIMUM_CLIENT_CATEGORIES } from '../../constants';
import { OpenapiNumberSchema } from '../shared';

export const ClientCategoryIdSchema = OpenapiNumberSchema({
  minimum: MINIMUM_CLIENT_CATEGORIES,
  maximum: MAXIMUM_CLIENT_CATEGORIES,
});
