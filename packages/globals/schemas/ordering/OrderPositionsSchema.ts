import { OpenapiArraySchema } from '../shared';
import { OrderPositionSchema } from './OrderPositionSchema';

export const OrderPositionsSchema = OpenapiArraySchema({
  items: OrderPositionSchema,
  maxItems: 1,
});
