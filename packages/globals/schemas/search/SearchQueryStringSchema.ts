export const SearchQueryStringSchema = {
  type: 'string',
  'vm-sanitize-string': true,
  maxLength: 300,
};
