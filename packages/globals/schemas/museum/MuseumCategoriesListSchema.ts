import {
  MINIMUM_CATEGORIES_BY_MUSEUM,
  MAXIMUM_CATEGORIES_BY_MUSEUM,
  MINIMAL_MUSEUM_CATEGORY_ID,
  MAXIMAL_MUSEUM_CATEGORY_ID,
  MUSEUM_CATEGORY_TITLE_LENGTH,
} from '../../constants';
import { OpenapiArraySchema, OpenapiNumberSchema, OpenapiObjectSchema, OpenapiStringSchema } from '../shared';

export const MuseumCategoryIdSchema = OpenapiNumberSchema({
  minimum: MINIMAL_MUSEUM_CATEGORY_ID,
  maximum: MAXIMAL_MUSEUM_CATEGORY_ID,
});

export const MuseumCategoryNameSchema = OpenapiStringSchema({
  maxLength: MUSEUM_CATEGORY_TITLE_LENGTH,
});

export const MuseumCategoriesListSchema = OpenapiArraySchema({
  items: OpenapiObjectSchema({
    properties: {
      id: MuseumCategoryIdSchema,
      name: MuseumCategoryNameSchema,
    },
    additionalProperties: false,
    required: ['id', 'name'],
  }),
  minItems: MINIMUM_CATEGORIES_BY_MUSEUM,
  maxItems: MAXIMUM_CATEGORIES_BY_MUSEUM,
});
