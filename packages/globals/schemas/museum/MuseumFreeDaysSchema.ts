import { FREE_DAYS_ADDITIONAL_INFO_LENGTH } from '../../constants';
import { OpenapiObjectSchema, OpenapiStringSchema } from '../shared';

export const FreeDaysAdditionalInfoSchema = OpenapiStringSchema({
  maxLength: FREE_DAYS_ADDITIONAL_INFO_LENGTH,
});

export const MuseumFreeDaysSchema = OpenapiObjectSchema({
  properties: {
    additionalInfo: FreeDaysAdditionalInfoSchema,
  },
  additionalProperties: false,
});
