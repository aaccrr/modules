import { MuseumTypeEnum } from '../../interfaces/museum/Enums/MuseumTypeEnum';
import { OpenapiStringSchema } from '../shared';

export const MuseumTypeSchema = OpenapiStringSchema({
  enum: [
    MuseumTypeEnum.MUSEUM,
    MuseumTypeEnum.GALLERY,
    MuseumTypeEnum.EXHIBITION_CENTER,
    MuseumTypeEnum.EXHIBITION_HALL,
    MuseumTypeEnum.PARK,
    MuseumTypeEnum.CULTURAL_LEISURE_INSTITUTION,
    MuseumTypeEnum.LIBRARY,
  ],
});
