import { MUSEUM_SCHEDULE_COMMENT_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const MuseumCommentForScheduleSchema = OpenapiStringSchema({
  maxLength: MUSEUM_SCHEDULE_COMMENT_LENGTH,
});
