import {
  MINIMUM_SERVICES_BY_MUSEUM,
  MAXIMUM_SERVICES_BY_MUSEUM,
  MUSEUM_SERVICE_NAME_LENGTH,
  MINIMAL_MUSEUM_SERVICE_ORDER,
  MAXIMAL_MUSEUM_SERVICE_ORDER,
} from '../../constants';
import { OpenapiArraySchema, OpenapiNumberSchema, OpenapiObjectSchema, OpenapiStringSchema } from '../shared';

const MuseumServiceNameSchema = OpenapiStringSchema({
  maxLength: MUSEUM_SERVICE_NAME_LENGTH,
});

const MuseumServiceOrderSchema = OpenapiNumberSchema({
  minimum: MINIMAL_MUSEUM_SERVICE_ORDER,
  maximum: MAXIMAL_MUSEUM_SERVICE_ORDER,
});

export const MuseumServicesListSchema = OpenapiArraySchema({
  items: OpenapiObjectSchema({
    properties: {
      name: MuseumServiceNameSchema,
      order: MuseumServiceOrderSchema,
    },
    additionalProperties: false,
    required: ['name'],
  }),
  minItems: MINIMUM_SERVICES_BY_MUSEUM,
  maxItems: MAXIMUM_SERVICES_BY_MUSEUM,
});
