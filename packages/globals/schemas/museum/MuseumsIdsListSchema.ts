import { OpenapiArraySchema } from '../shared';
import { MuseumIdSchema } from './MuseumIdSchema';

export const MuseumsIdsListSchema = OpenapiArraySchema({
  items: MuseumIdSchema,
});
