import { MAXIMUM_SANITARY_DAYS_BY_MUSEUM } from '../../constants';
import { OpenapiArraySchema, OpenapiInternationalStringDateSchema } from '../shared';

export const MuseumSanitaryDaysListSchema = OpenapiArraySchema({
  items: OpenapiInternationalStringDateSchema(),
  maxItems: MAXIMUM_SANITARY_DAYS_BY_MUSEUM,
});
