import { OpenapiStringSchema } from '../shared';

export const MuseumIdSchema = OpenapiStringSchema({
  format: 'digits',
  maxLength: 15,
  minLength: 15,
});
