import { MUSEUM_SLUG_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const MuseumSlugSchema = OpenapiStringSchema({
  maxLength: MUSEUM_SLUG_LENGTH,
});
