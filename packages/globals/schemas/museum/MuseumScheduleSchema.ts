import { OpenapiArraySchema, OpenapiObjectSchema, TimeObjectSchema, WeekDayOrderSchema } from '../shared';

export const MuseumScheduleSchema = OpenapiArraySchema({
  items: OpenapiObjectSchema({
    properties: {
      weekDayOrders: OpenapiArraySchema({
        items: WeekDayOrderSchema,
        maxItems: 7,
      }),
      startsAt: TimeObjectSchema,
      endAt: TimeObjectSchema,
      breakStartsAt: TimeObjectSchema,
      breakEndAt: TimeObjectSchema,
    },
    additionalProperties: false,
    required: ['weekDayOrders', 'startsAt', 'endAt'],
  }),
  maxItems: 7,
});
