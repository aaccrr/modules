import { MUSEUM_NAME_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const MuseumNameSchema = OpenapiStringSchema({
  maxLength: MUSEUM_NAME_LENGTH,
});
