import { PhoneSchema, OpenapiObjectSchema, OpenapiEmptyStringSchema, OpenapiEmailSchema } from '../shared';

export const MuseumReturnContactsSchema = OpenapiObjectSchema({
  properties: {
    phone: PhoneSchema,
    email: OpenapiEmptyStringSchema(OpenapiEmailSchema),
  },
  additionalProperties: false,
  required: ['phone'],
});
