import { MUSEUM_DESCRIPTION_LENGTH } from '../../constants';
import { OpenapiStringSchema } from '../shared';

export const MuseumDescriptionSchema = OpenapiStringSchema({
  maxLength: MUSEUM_DESCRIPTION_LENGTH,
});
