import { MUSEUM_OFFICIAL_SITE_LENGTH } from '../../constants';
import { OpenapiEmptyStringSchema, OpenapiStringSchema } from '../shared';

export const MuseumOfficialSiteSchema = OpenapiEmptyStringSchema(
  OpenapiStringSchema({
    format: 'hostname',
    maxLength: MUSEUM_OFFICIAL_SITE_LENGTH,
  })
);
