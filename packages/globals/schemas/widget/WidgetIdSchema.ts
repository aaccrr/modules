import { OpenAPIV3 } from 'openapi-types';

export const WidgetIdSchema: OpenAPIV3.SchemaObject = {
  type: 'string',
  minLength: 15,
  maxLength: 15,
  format: 'digits',
};
