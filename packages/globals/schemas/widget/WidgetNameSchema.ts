import { OpenAPIV3 } from 'openapi-types';

export const WidgetNameSchema: OpenAPIV3.SchemaObject = {
  type: 'string',
  minLength: 3,
  maxLength: 200,
};
