export interface IRescheduleReservePurchaseCommand {
  purchaseId: string;
  newReservationPeriodStart: Date;
}
