import { IOrderModel } from '../../../ordering';

export type ISchedulePrintPurchaseCommand = IOrderModel;
