export interface IScheduleStartEventSalesCommand {
  eventId: string;
  salesStartDate: string;
}
