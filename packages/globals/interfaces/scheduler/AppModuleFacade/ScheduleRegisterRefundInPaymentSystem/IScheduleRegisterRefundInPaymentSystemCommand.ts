import { IRefundModel } from '../../../ordering';

export type IScheduleRegisterRefundInPaymentSystemCommand = IRefundModel;
