export interface IScheduleClosePurchaseDueToUnpayCommand {
  purchaseId: string;
  purchaseCreatedAt: Date;
}
