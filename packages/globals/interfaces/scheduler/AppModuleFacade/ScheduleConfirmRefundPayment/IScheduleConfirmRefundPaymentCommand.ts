import { IRefundModel } from '../../..';

export interface IScheduleConfirmRefundPaymentCommand {
  refund: IRefundModel;
  refundOrderForPayment: string;
}
