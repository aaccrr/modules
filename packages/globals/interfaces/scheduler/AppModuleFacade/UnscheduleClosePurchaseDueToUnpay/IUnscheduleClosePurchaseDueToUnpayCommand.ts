export interface IUnscheduleClosePurchaseDueToUnpayCommand {
  purchaseId: string;
}
