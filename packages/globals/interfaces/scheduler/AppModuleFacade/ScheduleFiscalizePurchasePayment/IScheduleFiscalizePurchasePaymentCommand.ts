import { IOrderModel } from '../../../ordering';

export type IScheduleFiscalizePurchasePaymentCommand = IOrderModel;
