import { IDatesPeriodView } from '../../../shared';

export interface IScheduleCreateMonthlyReportsForOrganizationCommand {
  reportingOrganizationId: string;
  reportingPeriod: IDatesPeriodView;
  mailingList: string[];
}
