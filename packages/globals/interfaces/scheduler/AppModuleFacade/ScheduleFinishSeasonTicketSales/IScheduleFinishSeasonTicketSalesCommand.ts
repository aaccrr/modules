export interface IScheduleFinishSeasonTicketSalesCommand {
  id: string;
  salesFinishDate: Date;
}
