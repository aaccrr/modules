import { IPurchasePropsModel } from '../../../ordering';
import { ITimezone } from '../../../shared';

export interface IScheduleReservePurchaseCommand {
  id: string;
  nomenclatureId: string;
  count: number;
  props: IPurchasePropsModel;
  isFree: boolean;
  timezone: ITimezone;
  reservedAt: Date;
}
