export interface IScheduleEventProgressTrackingCommand {
  id: string;
  startDate: string;
  finishDate: string;
}
