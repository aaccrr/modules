export interface IRescheduleTicketSalesFinishCommand {
  ticketId: string;
  salesOpenedUntil: Date;
}
