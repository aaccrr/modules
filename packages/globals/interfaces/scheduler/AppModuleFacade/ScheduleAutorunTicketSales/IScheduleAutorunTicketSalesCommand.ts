export interface IScheduleAutorunTicketSalesCommand {
  ticketId: string;
  autorunDate: Date;
}
