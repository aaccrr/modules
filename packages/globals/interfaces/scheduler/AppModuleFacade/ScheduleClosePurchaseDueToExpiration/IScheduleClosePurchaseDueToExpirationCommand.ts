export interface IScheduleClosePurchaseDueToExpirationCommand {
  itemsIds: string[];
  expiresAt: Date;
}
