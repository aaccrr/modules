import { IOrderModel } from '../../../ordering';

export type IScheduleSendPrintedPurchaseToCustomerCommand = IOrderModel;
