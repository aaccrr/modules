export interface IUnscheduleClosePurchaseDueToExpirationCommand {
  purchaseId: string;
}
