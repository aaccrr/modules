export interface IScheduleConfirmRefundFiscalizationCommand {
  refundReceiptId: string;
}
