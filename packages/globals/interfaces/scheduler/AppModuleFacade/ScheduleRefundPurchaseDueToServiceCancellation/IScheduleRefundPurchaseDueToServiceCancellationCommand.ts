export interface IScheduleRefundPurchaseDueToServiceCancellationCommand {
  purchaseId: string;
  cancellationReason: string;
}
