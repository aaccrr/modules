export interface IRescheduleFinishSeasonTicketSalesCommand {
  id: string;
  salesFinishDate: Date;
}
