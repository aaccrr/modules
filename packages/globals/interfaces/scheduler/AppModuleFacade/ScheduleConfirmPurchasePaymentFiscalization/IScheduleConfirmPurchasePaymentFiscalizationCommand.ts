export interface IScheduleConfirmPurchasePaymentFiscalizationCommand {
  purchaseReceiptId: string;
  serviceChargeReceiptId: string;
}
