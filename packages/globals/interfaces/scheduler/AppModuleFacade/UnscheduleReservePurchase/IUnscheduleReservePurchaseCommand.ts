export interface IUnscheduleReservePurchaseCommand {
  reservedPurchaseId: string;
}
