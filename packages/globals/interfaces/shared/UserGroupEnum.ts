export enum UserGroup {
  ADMIN = 'ADMIN',
  OFFICE = 'OFFICE',
  CLIENT = 'CLIENT',
  GUEST = 'GUEST',
  ENTRY_CONTROL = 'ENTRY_CONTROL',
}
