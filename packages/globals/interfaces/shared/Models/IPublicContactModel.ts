export interface IPublicContactModel {
  name: string;
  phone: string;
  email: string;
}
