export interface ICancellationLogModel {
  canceledAt: Date;
  canceledBy: string;
}
