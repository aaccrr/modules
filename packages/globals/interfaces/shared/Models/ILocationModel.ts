import { ITimezone } from '../ITimezone';
import { IGeoPoint } from '../IGeoPoint';

export interface ILocationModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  address: string;
  location: IGeoPoint;
  timezone: ITimezone;
  timezoneOffset: string;
  isSubjectCenter: boolean | number;
}
