export interface IContactModel {
  name: string;
  phone: string;
  email: string;
  isPublic: boolean;
}
