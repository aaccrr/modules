export interface ITimeObjectModel {
  hour: string;
  minute: string;
}
