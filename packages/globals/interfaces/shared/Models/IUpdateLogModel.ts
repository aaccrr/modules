export interface IUpdateLogModel {
  updatedAt: Date;
  updatedBy: string;
  updatedFields: string[];
  approvedAt: Date | null;
  approvedBy: string | null;
}
