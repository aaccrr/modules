import { AgeCensorEnum } from '../Enums';

export interface IAgeCensorModel {
  name: string;
  value: AgeCensorEnum;
}
