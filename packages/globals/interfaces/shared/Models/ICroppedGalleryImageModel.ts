import { ICropBounds } from '../ICropBounds';

export interface ICroppedGalleryImageModel {
  order: number;
  crop: ICropBounds;
  src: string;
}
