import { ICropBounds } from '../ICropBounds';

export interface ICroppedPreviewImageModel {
  crop: ICropBounds;
  src: string;
}
