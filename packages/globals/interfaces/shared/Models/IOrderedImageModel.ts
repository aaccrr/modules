export interface IOrderedImageModel {
  order: number;
  src: string;
}
