export interface ISalesLimitModel {
  online: number | null;
  offline: number | null;
}
