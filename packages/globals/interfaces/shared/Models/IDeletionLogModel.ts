export interface IDeletionLogModel {
  deletedAt: Date;
  deletedBy: string;
}
