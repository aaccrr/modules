export interface IClientCategoryModel {
  _id?: string;
  id: number;
  name: string;
  price: IClientCategoryPriceModel;
  groups: string[];
}

export interface IClientCategoryPriceModel {
  online: number;
  offline: number;
}
