export enum LastChangeEnum {
  'CREATE' = 'CREATE',
  'UPDATE' = 'UPDATE',
  'APPROVE' = 'APPROVE',
  'DELETE' = 'DELETE',
}
