import { IRequestMeta } from './IRequestMeta';

export interface IControllerCallPayload<T> {
  payload: T;
  meta: IRequestMeta;
}
