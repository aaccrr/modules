import { IRequestUser } from './IRequestUser';

export interface IRequestMeta {
  clientIp: string;
  requestId: string;
  user: IRequestUser;
}
