export interface ICropBounds {
  width: number;
  height: number;
  x: number;
  y: number;
}
