export interface IMapSearchBoundaryView {
  bottomLeft: number[];
  topRight: number[];
}
