export interface IClientCategoryView {
  id: number;
  name: string;
  groups: string[];
  price: number;
}
