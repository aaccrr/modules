export interface IAgeCensorEditableView {
  name: string;
  value: number;
  isActive: boolean;
}
