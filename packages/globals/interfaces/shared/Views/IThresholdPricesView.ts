export interface IThresholdPricesView {
  minPrice: number | null;
  maxPrice: number | null;
}
