export interface IClientGroupEditableView {
  name: string;
  isDefault: boolean;
  isRequired: boolean;
  isActive: boolean;
}

export interface IClientCategoryEditableView {
  name: string;
  isRequired: boolean;
  isDefault: boolean;
  isActive: boolean;
  groups: IClientGroupEditableView[];
}
