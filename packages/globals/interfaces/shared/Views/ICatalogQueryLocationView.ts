import { IGeoPoint } from '../IGeoPoint';

export interface ICatalogQueryGeographyFilterView {
  regionId?: number[];
  localityId?: string;
}

export interface ICatalogQueryGeographySortingView {
  geo: IGeoPoint;
}

export interface ICatalogQueryLocationView {
  title: string;
  location: IGeoPoint;
  filters: ICatalogQueryGeographyFilterView;
  sorting: ICatalogQueryGeographySortingView;
}
