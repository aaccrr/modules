import { IGeoPoint } from '../IGeoPoint';

export interface IDetectedCatalogSearchSubjectView {
  id: number;
  name: string;
  center: string;
  geo: IGeoPoint;
}
