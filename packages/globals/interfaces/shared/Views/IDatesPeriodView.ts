export interface IDatesPeriodView {
  from: Date;
  to: Date;
}
