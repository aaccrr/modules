export interface IRefundContactView {
  phone: string;
  email?: string;
}
