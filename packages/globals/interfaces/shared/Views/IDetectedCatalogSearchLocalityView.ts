import { IGeoPoint } from '../IGeoPoint';

export interface IDetectedCatalogSearchLocalityView {
  region: string;
  regionId: number;
  locality: string;
  localityId: string;
  location: string;
  geo: IGeoPoint;
}
