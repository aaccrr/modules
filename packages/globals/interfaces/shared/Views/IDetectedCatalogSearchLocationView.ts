import { IDetectedCatalogSearchLocalityView } from './IDetectedCatalogSearchLocalityView';
import { IDetectedCatalogSearchSubjectView } from './IDetectedCatalogSearchSubjectView';

export type IDetectedCatalogSearchLocationView = IDetectedCatalogSearchLocalityView | IDetectedCatalogSearchSubjectView;
