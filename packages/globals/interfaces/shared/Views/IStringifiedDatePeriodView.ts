export interface IStringifiedDatePeriodView {
  from: string;
  to: string;
}
