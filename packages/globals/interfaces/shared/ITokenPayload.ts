export interface ITokenPayload {
  id: string;
  role: string;
  sessionID?: string;
  organizationId?: string;
  status?: string;
}
