import { IInvoicePendingDTO } from './IInvoicePendingDTO';

export interface IInvoicePaidDTO {
  invoice: IInvoicePendingDTO;
  payment: {
    id: string;
    amount: string;
    customer: string;
  };
}
