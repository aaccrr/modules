export interface ITresholdPricesDTO {
  minPrice: number;
  maxPrice: number;
}
