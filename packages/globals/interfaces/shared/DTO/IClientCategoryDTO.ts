export interface IClientCategoryDTO {
  name: string;
  price: number;
  groups: string[];
}
