import { IGeoPoint } from '../IGeoPoint';

export interface IDetectableLocationDTO {
  address: string;
  locality: string;
  location: IGeoPoint;
}
