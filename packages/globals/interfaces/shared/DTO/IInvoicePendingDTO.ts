import { IItemModel } from '../../ordering';
import { ICustomerModel } from '../../accounting';

export interface IInvoicePendingDTO {
  id: string;
  customer: ICustomerModel;
  amount: string;
  price: string;
  serviceCharge: string;
  nomenclatureTitle: string;
  items: Partial<IItemModel>[];
}
