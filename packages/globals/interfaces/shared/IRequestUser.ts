import { UserRole } from './UserRoleEnum';
import { SessionGroup } from '../accounting';

export interface IRequestUser {
  v: number;
  id: string;
  role: UserRole;
  group?: SessionGroup;
  gh?: string;
  organizationId?: string;
  m?: string[] | string;
}
