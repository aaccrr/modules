import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';

export interface IReuploadImagesForMuseumRequest {
  museumId: string;
  previewImage: ICroppedPreviewImageModel;
  gallery: ICroppedGalleryImageModel[];
}
