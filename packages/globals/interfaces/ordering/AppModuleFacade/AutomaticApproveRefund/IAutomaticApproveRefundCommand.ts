export interface IAutomaticApproveRefundCommand {
  refundId: string;
  serviceProviderId: string;
}