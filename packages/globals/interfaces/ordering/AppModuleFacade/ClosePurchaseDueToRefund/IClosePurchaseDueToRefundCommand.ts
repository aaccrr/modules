export interface IClosePurchaseDueToRefundCommand {
  refundId: string;
}
