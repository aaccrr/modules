export interface IRefundPurchaseDueToServiceCancellationCommand {
  purchaseId: string;
  cancellationReason: string;
}
