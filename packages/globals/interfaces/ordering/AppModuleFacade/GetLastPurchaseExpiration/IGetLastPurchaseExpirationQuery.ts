import { NomenclatureTypeEnum } from '../../../nomenclature';

export interface IGetLastPurchaseExpirationQuery {
  nomenclatureIds: string[];
  nomenclatureType: NomenclatureTypeEnum;
}
