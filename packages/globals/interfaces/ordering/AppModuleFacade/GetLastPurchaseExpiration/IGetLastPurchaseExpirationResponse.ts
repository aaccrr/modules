import { ILastPurchaseExpirationDateDTO } from '../../Purchases';

export type IGetLastPurchaseExpirationResponse = ILastPurchaseExpirationDateDTO[];
