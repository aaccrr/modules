export interface IConfirmPurchasePaymentCommand {
  purchaseId: string;
  paymentId: string;
  rnn: string;
}
