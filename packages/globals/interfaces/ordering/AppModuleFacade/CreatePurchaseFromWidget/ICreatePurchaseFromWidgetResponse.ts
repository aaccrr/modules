export interface ICreatePurchaseFromWidgetResponse {
  isFreePurchase: boolean;
  purchaseId: string;
}
