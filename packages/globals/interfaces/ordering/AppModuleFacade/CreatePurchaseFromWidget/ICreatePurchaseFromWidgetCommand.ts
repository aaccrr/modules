import { ICustomerModel } from '../../../accounting';
import { IPurchasedPositionDTO } from '../../Purchases';

export interface ICreatePurchaseFromWidgetCommand {
  purchase: {
    position: IPurchasedPositionDTO;
    customer: ICustomerModel;
  };
  widgetId: string;
  ip: string;
}
