export interface ICheckPurchaseOwnershipQuery {
  purchaseId: string;
  customerId: string;
}
