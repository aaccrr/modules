export interface ICheckPurchaseOwnershipResponse {
  isOwner: boolean;
}
