import { Nullable } from '../../..';
import { Terminal } from '../../../../interfaces';

export interface ICheckPurchasePaymentPossibilityResponse {
  paymentState: Nullable<'waiting' | 'completed'>;
  terminal: Terminal;
}
