export interface ICheckPurchasePaymentPossibilityQuery {
  purchaseId: string;
  paidAmount: string;
  customer: string;
}
