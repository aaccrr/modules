export enum PurchaseSaleTypeEnum {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}
