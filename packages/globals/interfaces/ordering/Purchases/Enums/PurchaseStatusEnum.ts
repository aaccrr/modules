export enum PurchaseStatusEnum {
  CREATED = 'CREATED', // заказ создан и клиенты выставлен счет
  UNPAID = 'UNPAID', // заказ не был оплачен в отведенный срок
  ACTIVE = 'ACTIVE', // заказ оплачен
  IN_REFUND_PROCESS = 'IN_REFUND_PROCESS', // на заказ был создан возврат
  CLOSED_BY_REFUND = 'CLOSED_BY_REFUND', // заказ закрыт после одобрения заявки на возврат
  REFUNDED = 'REFUNDED', // деньги за заказ были возвращены клиенту
  PASSED = 'PASSED', // клиент прошел по билету
  EXPIRED = 'EXPIRED', // у заказа закончился срок действия и он не бы использован
}
