export enum SeasonTicketConditionTypeEnum {
  EVENTS = 'EVENTS',
  VISITS = 'VISITS',
  PERIOD = 'PERIOD',
  UNIVERSAL = 'UNIVERSAL',
}
