export interface ICustomerCategoryModel {
  id: number;
  name: string;
  groups: string[];
}
