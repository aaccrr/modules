import { ICustomerModel } from '../../../accounting';
import { ICustomerCategoryModel } from './ICustomerCategoryModel';
import { IServiceProviderModel } from './IServiceProviderModel';
import { IPurchasePropsModel } from './IPurchasePropsModel';
import { IPurchasePaymentModel } from './IPurchasePaymentModel';
import { PurchaseStatusEnum, PurchaseTypeEnum } from '../Enums';
import { IPurchasePassModel } from './IPurchasePassModel';
import { ICustomerLocationModel } from './ICustomerLocationModel';

export interface IBaseItemModel {
  id: string;
  orderId: string;
  type: PurchaseTypeEnum;
  status: PurchaseStatusEnum;
  museums: string[];
  price: string;
  serviceProvider: IServiceProviderModel;
  props: IPurchasePropsModel;
  customer: ICustomerModel;
  customerCategory: ICustomerCategoryModel;
  customerLocation: ICustomerLocationModel;

  isFree: boolean;
  isPersonal: boolean;

  refund: {
    id: null | string;
    refundedAt: null | Date;
  };
  payment: IPurchasePaymentModel;
  pass: IPurchasePassModel;

  createdAt: Date;
  ageCensor: number;

  widgetId?: string;

  nomenclatureId: string;
  visitConditionId: string;
  visitorCategoryId: string;
}
