import { ISeasonTicketItemModel } from './ISeasonTicketPurchaseModel';
import { IEventItemModel } from './IEventPurchaseModel';
import { IEntranceTicketItemModel } from './IEntranceTicketPurchaseModel';

export type IItemModel = IEntranceTicketItemModel | ISeasonTicketItemModel | IEventItemModel;
