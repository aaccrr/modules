import { IPurchasedEventModel } from '../../../../nomenclature';
import { IEventConditionModel } from './IEventConditionModel';
import { IBaseItemModel } from '../IBaseItemModel';

export interface IEventItemModel extends IBaseItemModel {
  nomenclature: IPurchasedEventModel;
  condition: IEventConditionModel;
}
