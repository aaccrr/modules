import { ITimezone } from '../../../../shared';
import { EventConditionDateStatusEnum } from '../../Enums';
import { IPurchasePassModel } from '../IPurchasePassModel';

export interface IEventConditionModel {
  dates: IEventConditionDateModel[];
}

export interface IEventConditionDateModel {
  id: number;
  from: Date;
  to: Date;
  timezone: ITimezone;
  status: EventConditionDateStatusEnum;
  pass: IPurchasePassModel;
}
