import { IEventOrderModel } from './IEventOrderModel';
import { IEventItemModel } from './IEventItemModel';

export type IEventPurchaseModel = IEventOrderModel | IEventItemModel;
