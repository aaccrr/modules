import { IBaseOrderModel } from '../IBaseOrderModel';
import { IEventConditionModel } from './IEventConditionModel';
import { IPurchasedEventModel } from '../../../../nomenclature';
import { IEventItemModel } from './IEventItemModel';

export interface IEventOrderModel extends IBaseOrderModel {
  nomenclature: IPurchasedEventModel;
  condition: IEventConditionModel;
  items?: IEventItemModel[];
}
