import { Terminal } from './Terminal';

export interface IPurchasePaymentModel {
  id: string;
  invoiceId: string;
  paidAt: Date | null;
  pushkinCard: boolean;
  terminal: Terminal;
}
