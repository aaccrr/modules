import { ISeasonTicketOrderModel } from './ISeasonTicketOrderModel';
import { ISeasonTicketItemModel } from './ISeasonTicketItemModel';

export type ISeasonTicketPurchaseModel = ISeasonTicketOrderModel | ISeasonTicketItemModel;
