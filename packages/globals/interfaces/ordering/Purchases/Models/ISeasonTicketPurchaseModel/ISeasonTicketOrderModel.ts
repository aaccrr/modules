import { IVisitedMuseumModel } from '../../../../museum';
import { IPurchasedSeasonTicketModel } from '../../../../nomenclature';
import { IBaseOrderModel } from '../IBaseOrderModel';
import { ISeasonTicketConditionModel } from './ISeasonTicketConditionModel';
import { ISeasonTicketItemModel } from './ISeasonTicketItemModel';

export interface ISeasonTicketOrderModel extends IBaseOrderModel {
  condition: ISeasonTicketConditionModel;
  visitedMuseum: IVisitedMuseumModel;
  nomenclature: IPurchasedSeasonTicketModel;
  items?: ISeasonTicketItemModel[];
}
