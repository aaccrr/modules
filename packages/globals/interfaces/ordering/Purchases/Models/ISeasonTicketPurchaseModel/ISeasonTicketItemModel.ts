import { IVisitedMuseumModel } from '../../../../museum';
import { IPurchasedSeasonTicketModel } from '../../../../nomenclature';
import { IBaseItemModel } from '../IBaseItemModel';
import { ISeasonTicketConditionModel } from './ISeasonTicketConditionModel';

export interface ISeasonTicketItemModel extends IBaseItemModel {
  condition: ISeasonTicketConditionModel;
  visitedMuseum: IVisitedMuseumModel;
  nomenclature: IPurchasedSeasonTicketModel;
}
