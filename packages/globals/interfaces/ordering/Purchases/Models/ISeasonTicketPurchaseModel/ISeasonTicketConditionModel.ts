import { SeasonTicketConditionTypeEnum } from '../../Enums';
import { IPurchasePassModel } from '../IPurchasePassModel';

export interface ISeasonTicketConditionModel {
  type: SeasonTicketConditionTypeEnum;
  events?: ISeasonTicketConditionEventModel[];
  total?: number;
  left?: number;
  from?: Date;
  to?: Date;
}

export interface ISeasonTicketConditionEventModel {
  id: number;
  title: string;
  from: Date;
  to: Date;
  status: string;
  pass: IPurchasePassModel;
}
