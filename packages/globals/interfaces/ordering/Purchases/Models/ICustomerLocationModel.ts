import { ITimezone } from '../../../shared';

export interface ICustomerLocationModel {
  localityId: string;
  timezone: ITimezone;
}