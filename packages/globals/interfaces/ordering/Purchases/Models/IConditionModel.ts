import { IEntranceTicketConditionModel } from './IEntranceTicketPurchaseModel';
import { ISeasonTicketConditionModel } from './ISeasonTicketPurchaseModel';
import { IEventConditionModel } from './IEventPurchaseModel';

export type IConditionModel = IEntranceTicketConditionModel | ISeasonTicketConditionModel | IEventConditionModel;
