export interface IPurchasePropsModel {
  positionId: number;
  date: string;
  time: string;
}
