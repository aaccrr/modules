import { IEntranceTicketOrderModel } from './IEntranceTicketOrderModel';
import { IEntranceTicketItemModel } from './IEntranceTicketItemModel';

export type IEntranceTicketPurchaseModel = IEntranceTicketOrderModel | IEntranceTicketItemModel;
