import { EntranceTicketConditionTypeEnum } from '../../Enums';

export interface IEntranceTicketConditionModel {
  type: EntranceTicketConditionTypeEnum;
  passLimitNumberTotal: number;
  passLimitNumberLeft: number;
  from: Date;
  to: Date;
}
