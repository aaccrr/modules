import { IPurchasedEntranceTicketModel } from '../../../../nomenclature';
import { IVisitedMuseumModel } from '../../../../museum';
import { IBaseItemModel } from '../IBaseItemModel';
import { IEntranceTicketConditionModel } from './IEntranceTicketConditionModel';

export interface IEntranceTicketItemModel extends IBaseItemModel {
  nomenclature: IPurchasedEntranceTicketModel;
  visitedMuseum: IVisitedMuseumModel;
  condition: IEntranceTicketConditionModel;
}
