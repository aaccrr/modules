import { IVisitedMuseumModel } from '../../../../museum';
import { IPurchasedEntranceTicketModel } from '../../../../nomenclature';
import { IBaseOrderModel } from '../IBaseOrderModel';
import { IEntranceTicketConditionModel } from './IEntranceTicketConditionModel';
import { IEntranceTicketItemModel } from './IEntranceTicketItemModel';

export interface IEntranceTicketOrderModel extends IBaseOrderModel {
  condition: IEntranceTicketConditionModel;
  visitedMuseum: IVisitedMuseumModel;
  nomenclature: IPurchasedEntranceTicketModel;
  items?: IEntranceTicketItemModel[];
}
