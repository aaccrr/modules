import { IEntranceTicketPurchaseModel } from './IEntranceTicketPurchaseModel';
import { ISeasonTicketPurchaseModel } from './ISeasonTicketPurchaseModel';
import { IEventPurchaseModel } from './IEventPurchaseModel';

export type IPurchaseModel = IEntranceTicketPurchaseModel | ISeasonTicketPurchaseModel | IEventPurchaseModel;
