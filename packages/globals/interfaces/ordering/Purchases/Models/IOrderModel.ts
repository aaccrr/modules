import { IEntranceTicketOrderModel } from './IEntranceTicketPurchaseModel';
import { ISeasonTicketOrderModel } from './ISeasonTicketPurchaseModel';
import { IEventOrderModel } from './IEventPurchaseModel';

export type IOrderModel = IEntranceTicketOrderModel | ISeasonTicketOrderModel | IEventOrderModel;
