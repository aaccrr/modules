import { IPublicContactModel, IRefundContactView } from '../../../shared';

export interface IServiceProviderModel {
  id: string;
  inn: string;
  name: string;
  shortName: string;
  address: string;
  contacts: IPublicContactModel[];
  refundContacts: IRefundContactView;
  site: string;
}
