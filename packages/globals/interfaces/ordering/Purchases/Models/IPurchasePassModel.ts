export interface IPurchasePassModel {
  passedAt: Date | null;
  allowedBy: string | null;
}
