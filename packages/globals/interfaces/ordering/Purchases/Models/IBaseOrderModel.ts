import { ICustomerModel } from '../../../accounting';
import { PurchaseTypeEnum, PurchaseStatusEnum } from '../Enums';
import { IPurchasePaymentModel } from './IPurchasePaymentModel';
import { IServiceProviderModel } from './IServiceProviderModel';
import { IPurchasePropsModel } from './IPurchasePropsModel';
import { IOrderRefundModel } from '../../Refunds';
import { ICustomerLocationModel } from './ICustomerLocationModel';

export interface IBaseOrderModel {
  id: string;
  type: PurchaseTypeEnum;
  status: PurchaseStatusEnum;

  serviceProvider: IServiceProviderModel;
  props: Partial<IPurchasePropsModel>;
  customer: ICustomerModel;
  customerLocation: ICustomerLocationModel;

  itemsCount: number;
  price: string;
  serviceCharge: string;
  amount: string;

  isPersonal: boolean;
  isFree: boolean;

  refund: IOrderRefundModel;
  payment: IPurchasePaymentModel;
  createdAt: Date;

  ageCensor: number;
  widgetId?: string;

  visitConditionId: string;
}
