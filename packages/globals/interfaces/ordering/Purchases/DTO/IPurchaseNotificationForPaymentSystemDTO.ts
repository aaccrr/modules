export interface IPurchaseNotificationForPaymentSystemDTO {
  id: string;
  amount: string;
  clientId: string;
  clientEmail: string;
  clientPhone: string;
  sign: string;
}
