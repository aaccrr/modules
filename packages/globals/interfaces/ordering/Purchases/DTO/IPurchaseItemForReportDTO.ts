import { ITimezone } from '../../../shared';
import { IPurchasePropsModel } from '../../Purchases';

export interface IPurchaseItemForReportDTO {
  orderId: string;
  itemId: string;
  organizationId: string;
  nomenclatureId: string;
  purchasedAt: Date;
  refundedAt: Date;
  passedAt: Date;
  price: string;
  clientCategory: string;
  timezone: ITimezone;
  props: Partial<IPurchasePropsModel>;
  type?: 'SALE' | 'REFUND';
}
