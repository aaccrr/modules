export interface ITrackPurchaseExpirationDTO {
  purchaseId: string;
  expiresAt: Date;
}
