import { IPurchasedCategoryDTO } from './IPurchasedCategoryDTO';

export interface IPurchasedPositionDTO {
  id: string;
  categories: IPurchasedCategoryDTO[];
  date?: string;
  time?: string;
  positionId?: number;
}
