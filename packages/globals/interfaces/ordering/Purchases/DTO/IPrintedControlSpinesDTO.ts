import { IListControlSpinesView } from '../Views';
import { IDatesPeriodView } from '../../../shared';

export interface IPrintedControlSpinesDTO {
  organizationId: string;
  organizationName: string;
  period: IDatesPeriodView;
  spines: IListControlSpinesView[];
}
