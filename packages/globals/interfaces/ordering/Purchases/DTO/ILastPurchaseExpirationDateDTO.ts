export interface ILastPurchaseExpirationDateDTO {
  id: string;
  expiresAt: Date;
}
