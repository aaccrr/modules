export interface IPurchasedCategoryDTO {
  id: number;
  count: number;
}
