export interface IUpdateSalesCounterDTO {
  nomenclatureId: string;
  salesCount: number;
}
