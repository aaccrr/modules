export interface ITrackPurchasePaymentDTO {
  purchaseCreatedAt: Date;
  purchaseId: string;
}
