import { ICustomerModel } from '../../../../accounting';
import { IPurchasedEntranceTicketModel } from '../../../../nomenclature';
import { IEntranceTicketConditionModel, IServiceProviderModel } from '../../Models';
import { IVisitedMuseumModel } from '../../../../museum';

export interface IEntranceTicketPurchaseCancellationMailDTO {
  to: string;

  customer: Partial<ICustomerModel>;
  cancellationReason: string;
  customerCategory: string;
  price: string;
  serviceCharge: string;
  purchaseCondition: IEntranceTicketConditionModel;
  nomenclature: IPurchasedEntranceTicketModel;
  serviceProvider: IServiceProviderModel;
  visitedMuseum: IVisitedMuseumModel;
}
