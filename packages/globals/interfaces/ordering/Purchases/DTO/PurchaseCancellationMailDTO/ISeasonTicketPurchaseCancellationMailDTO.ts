import { ICustomerModel } from '../../../../accounting';
import { IPurchasedSeasonTicketModel } from '../../../../nomenclature';
import { ISeasonTicketConditionModel, IServiceProviderModel } from '../../Models';
import { IVisitedMuseumModel } from '../../../../museum';

export interface ISeasonTicketPurchaseCancellationMailDTO {
  to: string;

  customer: Partial<ICustomerModel>;
  cancellationReason: string;
  customerCategory: string;
  price: string;
  serviceCharge: string;
  purchaseCondition: ISeasonTicketConditionModel;
  nomenclature: IPurchasedSeasonTicketModel;
  serviceProvider: IServiceProviderModel;
  visitedMuseum: IVisitedMuseumModel;
}
