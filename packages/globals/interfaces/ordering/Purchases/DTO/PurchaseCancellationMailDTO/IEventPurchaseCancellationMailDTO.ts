import { ICustomerModel } from '../../../../accounting';
import { IPurchasedEventModel } from '../../../../nomenclature';
import { IServiceProviderModel, IEventConditionModel } from '../../Models';

export interface IEventPurchaseCancellationMailDTO {
  to: string;

  customer: Partial<ICustomerModel>;
  cancellationReason: string;
  customerCategory: string;
  price: string;
  serviceCharge: string;
  purchaseCondition: IEventConditionModel;
  nomenclature: IPurchasedEventModel;
  serviceProvider: IServiceProviderModel;
}
