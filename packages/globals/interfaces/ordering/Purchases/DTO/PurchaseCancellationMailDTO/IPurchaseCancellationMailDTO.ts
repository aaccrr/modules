import { IEntranceTicketPurchaseCancellationMailDTO } from './IEntranceTicketPurchaseCancellationMailDTO';
import { ISeasonTicketPurchaseCancellationMailDTO } from './ISeasonTicketPurchaseCancellationMailDTO';
import { IEventPurchaseCancellationMailDTO } from './IEventPurchaseCancellationMailDTO';

export type IPurchaseCancellationMailDTO =
  | IEntranceTicketPurchaseCancellationMailDTO
  | ISeasonTicketPurchaseCancellationMailDTO
  | IEventPurchaseCancellationMailDTO;
