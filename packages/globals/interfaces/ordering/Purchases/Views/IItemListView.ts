import { NomenclatureTypeEnum } from '../../../nomenclature';
import { PurchaseStatusEnum } from '../Enums';

export interface IItemListView {
  id: string;
  qrCodeUrl: string;
  printedTicketUrl: string;
  category: {
    name: string;
    groups: string[];
  };
  nomenclatureType: NomenclatureTypeEnum;
  price: string;
  status: PurchaseStatusEnum;
}
