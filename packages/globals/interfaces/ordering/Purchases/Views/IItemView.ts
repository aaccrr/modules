import { ICustomerCategoryModel } from '../Models';
import { NomenclatureTypeEnum } from '../../../nomenclature';
import { PurchaseStatusEnum } from '../Enums';

export interface IItemView {
  id: string;
  category: ICustomerCategoryModel;
  nomenclatureType: NomenclatureTypeEnum;
  price: string;
  status: PurchaseStatusEnum;
}
