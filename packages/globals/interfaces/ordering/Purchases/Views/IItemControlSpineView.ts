import { NomenclatureTypeEnum } from '../../../nomenclature';

export interface IItemControlSpineView {
  id: string;
  orderId: string;
  nomenclatureTitle: string;
  nomenclatureType: NomenclatureTypeEnum;
  price: string;
  isRefunded: boolean;
}
