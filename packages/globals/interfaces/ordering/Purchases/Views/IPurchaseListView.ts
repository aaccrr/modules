import { IEntranceTicketPurchaseListView } from './IEntranceTicketPurchaseView';
import { ISeasonTicketPurchaseListView } from './ISeasonTicketPurchaseView';
import { IEventPurchaseListView } from './IEventPurchaseView';

export type IPurchaseListView = IEntranceTicketPurchaseListView | ISeasonTicketPurchaseListView | IEventPurchaseListView;
