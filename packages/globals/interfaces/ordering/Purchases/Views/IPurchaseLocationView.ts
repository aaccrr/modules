import { IGeoPoint } from '../../../shared';

export interface IPurchaseLocationView {
  geo: IGeoPoint;
  locality: string;
  address: string;
}
