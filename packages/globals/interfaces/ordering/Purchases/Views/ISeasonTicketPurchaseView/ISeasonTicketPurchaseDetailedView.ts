import { IPurchaseTitleView } from '../IPurchaseTitleView';
import { NomenclatureTypeEnum } from '../../../../nomenclature';
import { ICustomerModel } from '../../../../accounting';
import { ISeasonTicketConditionView } from './ISeasonTicketConditionView';
import { IPurchaseLocationView } from '../IPurchaseLocationView';
import { IVisitedMuseumModel } from '../../../../museum';
import { PurchaseStatusEnum } from '../../Enums';
import { IItemListView } from '../IItemListView';

export interface ISeasonTicketPurchaseDetailedView {
  id: string;
  printedPurchaseUrl: string;
  title: IPurchaseTitleView;
  nomenclatureType: NomenclatureTypeEnum;
  nomenclatureTitle: string;
  customer: Partial<ICustomerModel>;
  condition: ISeasonTicketConditionView;
  location: IPurchaseLocationView;
  price: string;
  items: IItemListView[];
  museum: Partial<IVisitedMuseumModel>;
  importantInfo: string[];
  status: PurchaseStatusEnum;
}
