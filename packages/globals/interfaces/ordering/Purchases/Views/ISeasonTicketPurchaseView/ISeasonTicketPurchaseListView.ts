import { IPurchaseTitleView } from '../IPurchaseTitleView';
import { NomenclatureTypeEnum } from '../../../../nomenclature';
import { IPurchaseLocationView } from '../IPurchaseLocationView';
import { ICustomerModel } from '../../../../accounting';
import { PurchaseStatusEnum } from '../../Enums';
import { ISeasonTicketConditionView } from './ISeasonTicketConditionView';

export interface ISeasonTicketPurchaseListView {
  id: string;
  printedPurchaseUrl: string;
  title: IPurchaseTitleView;
  nomenclatureType: NomenclatureTypeEnum;
  nomenclatureTitle: string;
  condition: ISeasonTicketConditionView;
  location: IPurchaseLocationView;
  customer: Partial<ICustomerModel>;
  status: PurchaseStatusEnum;
}
