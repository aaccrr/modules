import { SeasonTicketConditionTypeEnum } from '../../Enums';

export type ISeasonTicketConditionView = ISeasonTicketPeriodConditionView | ISeasonTicketVisitsConditionView | ISeasonTicketEventsConditionView;

export interface ISeasonTicketPeriodConditionView {
  type: SeasonTicketConditionTypeEnum;
  expiresAt: string;
}

export interface ISeasonTicketVisitsConditionView {
  type: SeasonTicketConditionTypeEnum;
  visitsLeft: number;
  expiresAt: string;
}

export interface ISeasonTicketEventsConditionView {
  type: SeasonTicketConditionTypeEnum;
  events: ISeasonTicketEventConditionView[];
}

export interface ISeasonTicketEventConditionView {
  title: string;
  date: string;
  time: string;
}
