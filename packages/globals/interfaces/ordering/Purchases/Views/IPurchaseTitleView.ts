export interface IPurchaseTitleView {
  text: string;
  link: string;
}
