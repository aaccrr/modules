import { IItemControlSpineView } from './IItemControlSpineView';

export interface IListControlSpinesView {
  museumName: string;
  items: IItemControlSpineView[];
}
