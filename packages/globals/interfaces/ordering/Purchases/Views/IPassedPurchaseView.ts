export interface IPassedPurchaseView {
  id: string;
  title: string;
  customer: string;
  type: string;
  category: string;
  condition: string;
}
