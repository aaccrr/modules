import { IEntranceTicketPurchaseDetailedView } from './IEntranceTicketPurchaseView';
import { ISeasonTicketPurchaseDetailedView } from './ISeasonTicketPurchaseView';
import { IEventPurchaseDetailedView } from './IEventPurchaseView';

export type IPurchaseDetailedView = IEntranceTicketPurchaseDetailedView | ISeasonTicketPurchaseDetailedView | IEventPurchaseDetailedView;
