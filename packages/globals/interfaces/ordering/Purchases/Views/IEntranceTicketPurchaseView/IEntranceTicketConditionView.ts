import { EntranceTicketConditionTypeEnum } from '../../Enums';

export type IEntranceTicketConditionView = IEntranceTicketPeriodConditionView | IEntranceTicketTimeConditionView;

export interface IEntranceTicketPeriodConditionView {
  type: EntranceTicketConditionTypeEnum;
  expiresAt: string;
}

export interface IEntranceTicketTimeConditionView {
  type: EntranceTicketConditionTypeEnum;
  date: string;
  from: string;
  to: string;
}
