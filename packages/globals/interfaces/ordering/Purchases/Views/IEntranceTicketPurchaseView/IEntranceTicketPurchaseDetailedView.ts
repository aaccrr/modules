import { IPurchaseTitleView } from '../IPurchaseTitleView';
import { NomenclatureTypeEnum } from '../../../../nomenclature';
import { ICustomerModel } from '../../../../accounting';
import { IEntranceTicketConditionView } from './IEntranceTicketConditionView';
import { IPurchaseLocationView } from '../IPurchaseLocationView';
import { IVisitedMuseumModel } from '../../../../museum';
import { PurchaseStatusEnum } from '../../Enums';
import { IItemListView } from '../IItemListView';

export interface IEntranceTicketPurchaseDetailedView {
  id: string;
  printedPurchaseUrl: string;
  title: IPurchaseTitleView;
  nomenclatureType: NomenclatureTypeEnum;
  nomenclatureTitle: string;
  customer: Partial<ICustomerModel>;
  condition: IEntranceTicketConditionView;
  location: IPurchaseLocationView;
  price: string;
  items: IItemListView[];
  museum: Partial<IVisitedMuseumModel>;
  importantInfo: string[];
  status: PurchaseStatusEnum;
}
