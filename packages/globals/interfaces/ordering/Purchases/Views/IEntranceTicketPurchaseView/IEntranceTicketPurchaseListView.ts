import { IPurchaseTitleView } from '../IPurchaseTitleView';
import { NomenclatureTypeEnum } from '../../../../nomenclature';
import { IEntranceTicketConditionView } from './IEntranceTicketConditionView';
import { IPurchaseLocationView } from '../IPurchaseLocationView';
import { ICustomerModel } from '../../../../accounting';
import { PurchaseStatusEnum } from '../../Enums';

export interface IEntranceTicketPurchaseListView {
  id: string;
  printedPurchaseUrl: string;
  title: IPurchaseTitleView;
  nomenclatureType: NomenclatureTypeEnum;
  nomenclatureTitle: string;
  condition: IEntranceTicketConditionView;
  location: IPurchaseLocationView;
  customer: Partial<ICustomerModel>;
  status: PurchaseStatusEnum;
  includedInPrice: string[];
}
