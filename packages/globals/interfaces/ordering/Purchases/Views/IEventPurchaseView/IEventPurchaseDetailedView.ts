import { IPurchaseTitleView } from '../IPurchaseTitleView';
import { NomenclatureTypeEnum } from '../../../../nomenclature';
import { ICustomerModel } from '../../../../accounting';
import { IEventConditionView } from './IEventConditionView';
import { IEventScheduleView } from './IEventScheduleView';
import { IPublicContactModel } from '../../../../shared';
import { PurchaseStatusEnum } from '../../Enums';
import { IItemListView } from '../IItemListView';

export interface IEventPurchaseDetailedView {
  id: string;
  printedPurchaseUrl: string;
  title: IPurchaseTitleView;
  nomenclatureType: NomenclatureTypeEnum;
  customer: Partial<ICustomerModel>;
  condition: IEventConditionView;
  locations: string[];
  schedule: IEventScheduleView;
  contacts: IPublicContactModel[];
  price: string;
  items: IItemListView[];
  importantInfo: string[];
  status: PurchaseStatusEnum;
}
