import { IPurchaseTitleView } from '../IPurchaseTitleView';
import { NomenclatureTypeEnum } from '../../../../nomenclature';
import { ICustomerModel } from '../../../../accounting';
import { PurchaseStatusEnum } from '../../Enums';
import { IEventConditionView } from './IEventConditionView';

export interface IEventPurchaseListView {
  id: string;
  printedPurchaseUrl: string;
  title: IPurchaseTitleView;
  category: string;
  nomenclatureType: NomenclatureTypeEnum;
  condition: IEventConditionView;
  locations: string[];
  customer: Partial<ICustomerModel>;
  status: PurchaseStatusEnum;
}
