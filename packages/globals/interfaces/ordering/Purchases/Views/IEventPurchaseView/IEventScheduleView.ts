import { IEventTimelinePointModel } from '../../../../nomenclature';

export type IEventScheduleView = IEventSchedulePointView[];

export interface IEventSchedulePointView {
  date: string;
  timeline: IEventTimelinePointModel[];
  address: string;
}
