export interface IEventConditionView {
  dates: IEventConditionDateView[];
}

export interface IEventConditionDateView {
  date: string;
  from: string;
  to: string;
}
