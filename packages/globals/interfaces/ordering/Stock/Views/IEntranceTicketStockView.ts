import { IEntranceTicketStockDayModel, IEntranceTicketStockIntervalModel } from '../Models';

export interface IEntranceTicketStockView {
  dates: IEntranceTicketStockDayModel[];
  intervals: IEntranceTicketStockIntervalModel[];
}
