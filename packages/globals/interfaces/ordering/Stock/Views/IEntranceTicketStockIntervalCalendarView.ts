export interface IEntranceTicketStockIntervalCalendarView {
  date: string;
  from: string;
  to: string;
  tickets: number | null;
}
