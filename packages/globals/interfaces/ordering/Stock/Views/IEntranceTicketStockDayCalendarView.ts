export interface IEntranceTicketStockDayCalendarView {
  date: string;
  tickets: number;
  hasIntervals: boolean;
}
