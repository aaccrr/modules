import { EntranceTicketStockStatusEnum } from '../Enums';
import { ISalesLimitModel } from '../../../shared';

export interface IEntranceTicketStockDayView {
  nomenclatureId: string;
  date: Date;
  status: EntranceTicketStockStatusEnum;
  tickets: ISalesLimitModel;
  totals: ISalesLimitModel;
  hasIntervals: boolean;
  salesFinishAt: Date;
}
