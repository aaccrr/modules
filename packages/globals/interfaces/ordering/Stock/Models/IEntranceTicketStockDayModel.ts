import { EntranceTicketStockStatusEnum } from '../Enums';
import { ISalesLimitModel } from '../../../shared';

export interface IEntranceTicketStockDayModel {
  nomenclatureId: string;
  date: Date;
  status: EntranceTicketStockStatusEnum;
  totals: ISalesLimitModel;
  tickets: ISalesLimitModel;
  hasIntervals: boolean;
}
