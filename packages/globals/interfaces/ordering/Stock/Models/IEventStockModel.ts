export interface IEventStockModel {
  nomenclatureId: string;
  nomenclatureSlug: string;
  positionId: number;
  online: number | null;
  offline: number | null;
}
