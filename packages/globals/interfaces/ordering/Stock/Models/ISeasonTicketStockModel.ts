export interface ISeasonTicketStockModel {
  nomenclatureId: string;
  online: number | null;
  offline: number | null;
}
