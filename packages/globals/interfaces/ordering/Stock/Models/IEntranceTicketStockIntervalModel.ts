import { ISalesLimitModel } from '../../../shared';
import { EntranceTicketStockStatusEnum } from '../Enums';

export interface IEntranceTicketStockIntervalModel {
  nomenclatureId: string;
  date: Date;
  salesOpenedUntil: Date;
  from: string;
  to: string;
  totals: ISalesLimitModel;
  tickets: ISalesLimitModel;
  status: EntranceTicketStockStatusEnum;
}
