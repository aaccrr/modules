export enum EntranceTicketStockStatusEnum {
  ACTIVE = 'ACTIVE',
  DAY_OFF = 'DAY_OFF',
}
