import { IEntranceTicketRefundSummaryView } from './IEntranceTicketRefundSummaryView';
import { ISeasonTicketRefundSummaryView } from './ISeasonTicketRefundSummaryView';
import { IEventRefundSummaryView } from './IEventRefundSummaryView';

export type IRefundSummaryView = IEntranceTicketRefundSummaryView | ISeasonTicketRefundSummaryView | IEventRefundSummaryView;
