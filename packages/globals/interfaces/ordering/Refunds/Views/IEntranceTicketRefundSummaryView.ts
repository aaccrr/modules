import { NomenclatureTypeEnum } from '../../../nomenclature';
import { IRefundSummaryCategoryView } from './IRefundSummaryCategoryView';
import { IEntranceTicketConditionView } from '../../Purchases';
import { ICustomerModel } from '../../../accounting';

export interface IEntranceTicketRefundSummaryView {
  museumName: string;
  nomenclatureType: NomenclatureTypeEnum;
  nomenclatureTitle: string;
  categories: IRefundSummaryCategoryView[];
  customer: Partial<ICustomerModel>;
  condition: IEntranceTicketConditionView;
  serviceCharge: string;
  price: string;
}
