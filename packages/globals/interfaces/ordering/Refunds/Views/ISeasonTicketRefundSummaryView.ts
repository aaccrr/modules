import { NomenclatureTypeEnum } from '../../../nomenclature';
import { IRefundSummaryCategoryView } from './IRefundSummaryCategoryView';
import { ISeasonTicketConditionView } from '../../Purchases';
import { ICustomerModel } from '../../../accounting';

export interface ISeasonTicketRefundSummaryView {
  museumName: string;
  nomenclatureType: NomenclatureTypeEnum;
  categories: IRefundSummaryCategoryView[];
  customer: Partial<ICustomerModel>;
  condition: ISeasonTicketConditionView;
  serviceCharge: string;
  price: string;
}
