import { IRefundContactView } from '../../../shared';

export interface IRefundRejectionNoticeForCustomerView {
  to: string;
  reason: string;
  purchase: {
    id: string;
    type: string;
  };
  contacts: IRefundContactView;
}
