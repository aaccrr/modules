import { PurchaseTypeEnum } from '../../Purchases';

export interface IItemRefundListView {
  id: string;
  serviceId: string;
  type: PurchaseTypeEnum;
  price: string;
  createdAt: string;
  category: string;
}
