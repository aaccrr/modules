import { NomenclatureTypeEnum } from '../../../nomenclature';
import { IRefundSummaryCategoryView } from './IRefundSummaryCategoryView';
import { IEventConditionView } from '../../Purchases';
import { ICustomerModel } from '../../../accounting';

export interface IEventRefundSummaryView {
  nomenclatureTitle: string;
  nomenclatureType: NomenclatureTypeEnum;
  categories: IRefundSummaryCategoryView[];
  customer: Partial<ICustomerModel>;
  condition: IEventConditionView;
  serviceCharge: string;
  price: string;
}
