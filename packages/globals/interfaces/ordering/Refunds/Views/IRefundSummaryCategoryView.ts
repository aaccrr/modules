export interface IRefundSummaryCategoryView {
  name: string;
  count: number;
}
