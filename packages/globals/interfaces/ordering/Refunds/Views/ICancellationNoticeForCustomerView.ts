import { NomenclatureTypeEnum } from '../../../nomenclature';
import { ITimezone, IRefundContactView } from '../../../shared';
import { IConditionModel } from '../../Purchases';
import { IRefundItemModel } from '../Models';
import { ICustomerModel } from '../../../accounting';

export interface ICancellationNoticeForCustomerView {
  to: string;
  purchase: {
    serviceCharge: string;
    condition: IConditionModel;
    timezone: ITimezone;
  };
  nomenclature: {
    type: NomenclatureTypeEnum;
    title: string;
    museum: {
      name: string;
      refundContacts: IRefundContactView;
      site: string;
      locality: string;
      address: string;
      timezone: ITimezone;
    };
    organization: {
      name: string;
    };
  };
  refund: {
    items: IRefundItemModel[];
    price: string;
    customer: ICustomerModel;
    reason: string;
  };
}
