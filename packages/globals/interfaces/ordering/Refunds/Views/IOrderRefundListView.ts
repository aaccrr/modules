import { PurchaseTypeEnum } from '../../Purchases';

export interface IOrderRefundListView {
  id: string;
  serviceId: string;
  type: PurchaseTypeEnum;
  price: string;
  createdAt: string;
  items: IOrderRefundItemView[];
}

export interface IOrderRefundItemView {
  id: string;
  category: string;
  price: string;
}
