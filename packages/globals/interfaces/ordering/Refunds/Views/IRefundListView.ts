import { IOrderRefundListView } from './IOrderRefundListView';
import { IItemRefundListView } from './IItemRefundListView';

export type IRefundListView = IOrderRefundListView | IItemRefundListView;
