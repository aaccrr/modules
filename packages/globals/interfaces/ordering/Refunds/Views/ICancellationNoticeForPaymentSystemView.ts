import { ICustomerModel } from '../../../accounting';
import { IRefundItemModel } from '../Models';

export interface ICancellationNoticeForPaymentSystemView {
  id: string;
  customer: ICustomerModel;
  price: string;
  items: IRefundItemModel[];
  purchaseId: string;
  paymentId: string;
}
