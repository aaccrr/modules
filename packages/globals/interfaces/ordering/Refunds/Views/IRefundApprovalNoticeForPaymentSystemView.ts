import { IRefundItemModel } from '../Models';
import { ICustomerModel } from '../../../accounting';

export interface IRefundApprovalNoticeForPaymentSystemView {
  id: string;
  customer: ICustomerModel;
  price: string;
  items: IRefundItemModel[];
  purchaseId: string;
  paymentId: string;
}
