export interface IRefundApprovalNoticeForCustomerView {
  to: string;
  refund: {
    price: string;
  };
  purchase: {
    id: string;
    type: string;
  };
  organization: {
    name: string;
  };
  contacts: any[];
  refundContacts: any;
  website: string;
}
