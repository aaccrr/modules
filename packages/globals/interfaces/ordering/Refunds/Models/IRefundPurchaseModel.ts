import { PurchaseTypeEnum } from '../../Purchases';

export interface IRefundPurchaseModel {
  id: string;
  type: PurchaseTypeEnum;
  title: string;
}
