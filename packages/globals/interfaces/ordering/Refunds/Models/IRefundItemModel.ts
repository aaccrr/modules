import { ICustomerCategoryModel } from '../../Purchases/Models/ICustomerCategoryModel';

export interface IRefundItemModel {
  id: string;
  customerCategory: ICustomerCategoryModel;
  nomenclature: {
    title: string;
  };
  price: string;
}
