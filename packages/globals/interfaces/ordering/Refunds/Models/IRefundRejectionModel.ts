export interface IRefundRejectionModel {
  rejectedAt: Date | null;
  rejectedBy: string | null;
}
