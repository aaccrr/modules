import { RefundStatusEnum, RefundCreatorEnum } from '../Enums';
import { ICustomerModel } from '../../../accounting';
import { IRefundItemModel } from './IRefundItemModel';
import { IRefundPurchaseModel } from './IRefundPurchaseModel';
import { IRefundApprovalModel } from './IRefundApprovalModel';
import { IRefundRejectionModel } from './IRefundRejectionModel';
import { IServiceProviderModel } from '../../Purchases';
import { Terminal } from '../../Purchases';

export interface IRefundModel {
  id: string;
  serviceProvider: IServiceProviderModel;
  status: RefundStatusEnum;
  purchase: IRefundPurchaseModel;
  customer: ICustomerModel;
  items: IRefundItemModel[];
  amount: string;
  paymentId: string;
  pushkinCardPayment: boolean;
  terminal: Terminal;
  createdAt: Date;
  createdBy: RefundCreatorEnum;
  approval: IRefundApprovalModel;
  rejection: IRefundRejectionModel;
  refundedAt: Date | null;
}
