export interface IRefundApprovalModel {
  approvedAt: Date | null;
  approvedBy: string | null;
}
