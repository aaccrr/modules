import { OrderRefundReasonEnum } from '../Enums';

export interface IOrderRefundModel {
  type: OrderRefundReasonEnum | null;
  id: string | null;
  refundedAt: Date | null;
}
