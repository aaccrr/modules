export interface IRefundedPurchaseDueToServiceCancellationDTO {
  purchaseId: string;
  cancellationReason: string;
}
