import { PurchaseTypeEnum } from '../../Purchases';
import { IPublicContactModel, IRefundContactView } from '../../../shared';

export interface IRefundApprovedNoticeDTO {
  to: string;
  purchaseId: string;
  purchaseType: PurchaseTypeEnum;
  amount: string;
  serviceProviderName: string;
  contacts: IPublicContactModel[];
  refundContacts: IRefundContactView;
  website: string;
}
