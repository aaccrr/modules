import { PurchaseTypeEnum } from '../../Purchases';
import { IRefundContactView, IPublicContactModel } from '../../../shared';

export interface IRefundRejectedNoticeDTO {
  to: string;
  reason: string;
  purchaseId: string;
  purchaseType: PurchaseTypeEnum;
  serviceProviderName: string;
  contacts: IPublicContactModel[];
  refundContacts: IRefundContactView;
  website: string;
}
