import { IRefundItemModel } from '../Models';
import { ICustomerModel } from '../../../accounting';

export interface IRefundProcessedDTO {
  id: string;
  customer: ICustomerModel;
  price: string;
  purchaseId: string;
  paymentId: string;
  items: IRefundItemModel[];
}
