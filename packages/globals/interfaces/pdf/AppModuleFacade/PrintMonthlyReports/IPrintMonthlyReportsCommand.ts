import { ICalculatedControlSpinesReportDTO, ICalculatedMonthlyClosingReportDTO } from '../../../reporting';

export interface IPrintMonthlyReportsCommand {
  calculatedClosingReport: ICalculatedMonthlyClosingReportDTO;
  calculatedControlSpinesReport: ICalculatedControlSpinesReportDTO;
  mailingList: string[];
}
