import { IDatesPeriodView } from '../../..';
import { IListControlSpinesView } from '../../../ordering';
import { IReportingOrganizationDTO } from '../../../organization';

export interface IPrintControlSpinesCommand {
  reportingOrganization: IReportingOrganizationDTO;
  reportingPeriod: IDatesPeriodView;
  controlSpinesReport: IListControlSpinesView[];
  totalAmount: number;
  refundsAmount: number;
}
