export interface IPrintControlSpinesResponse {
  printedControlSpinesUrl: string;
  printedControlSpinesInternalPath: string;
}
