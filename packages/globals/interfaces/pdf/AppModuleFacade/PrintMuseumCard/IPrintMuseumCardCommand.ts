import { IAdministeredMuseumModel } from '../../../museum';
import { IFetchOrganizationCardInfoResponse } from '../../../organization';

export interface IPrintMuseumCardCommand {
  museum: IAdministeredMuseumModel;
  organization: IFetchOrganizationCardInfoResponse;
  consumer: string;
}
