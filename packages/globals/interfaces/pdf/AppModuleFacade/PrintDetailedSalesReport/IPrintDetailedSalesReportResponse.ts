export interface IPrintDetailedSalesReportResponse {
  printedDetailedSalesReportUrl: string;
}
