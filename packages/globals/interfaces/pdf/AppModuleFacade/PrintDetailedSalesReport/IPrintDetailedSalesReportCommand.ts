import { IDetailedReportView } from '../../../reporting';

export interface IPrintDetailedSalesReportCommand {
  organizationId: string;
  report: IDetailedReportView;
}
