import { IDetailedReportView } from '../../../reporting';

export interface IPrintDetailedSalesReportAndSendOnEmailCommand {
  organizationId: string;
  recipients: string[];
  report: IDetailedReportView;
}
