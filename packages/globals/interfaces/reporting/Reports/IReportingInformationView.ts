import { IMuseumForReportDTO } from '../../museum';
import { INomenclatureForReportDTO } from '../../nomenclature';
import { IReportingOrganizationDTO } from '../../organization';
import { IPurchaseItemForReportDTO } from '../../ordering';

export interface IReportingInformationView {
  reportingOrganizations: IReportingOrganizationDTO[];
  reportingMuseums: IMuseumForReportDTO[];
  reportingNomenclature: INomenclatureForReportDTO[];
  reportingSales: IPurchaseItemForReportDTO[];
}
