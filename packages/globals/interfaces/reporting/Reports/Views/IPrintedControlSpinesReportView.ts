export interface IPrintedControlSpinesReportView {
  printedControlSpinesUrl: string;
  printedControlSpinesInternalPath: string;
}
