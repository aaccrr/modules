import { IAggregatedMuseumAccountingReportView } from './IAggregatedMuseumAccountingReportView';
import { IAggregatedNomenclatureAccountingReportView } from './IAggregatedNomenclatureAccountingReportView';

export interface IAggregatedAccountingReportView {
  organizationId: string;
  museums: IAggregatedMuseumAccountingReportView[];
  events: IAggregatedNomenclatureAccountingReportView[];
}
