import { ICategoryDetailedReportView } from './ICategoryDetailedReportView';

export interface INomenclatureDetailedReportView {
  id: string;
  title: string;
  totalSales: number;
  onlineSales: number;
  refunds: number;
  visits: number;
  offlineSales: number;
  amount: number;
  categories: ICategoryDetailedReportView[];
}
