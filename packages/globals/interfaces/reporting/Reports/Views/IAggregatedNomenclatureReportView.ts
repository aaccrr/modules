import { IAggregatedClientCategoryReportView } from './IAggregatedClientCategoryReportView';

export interface IAggregatedNomenclatureReportView {
  id: string;
  title: string;
  totalSales: number;
  onlineSales: number;
  offlineSales: number;
  refunds: number;
  visits: number;
  amount: number;
  categories: IAggregatedClientCategoryReportView[];
}
