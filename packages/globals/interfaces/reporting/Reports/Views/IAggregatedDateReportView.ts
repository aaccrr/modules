import { IPurchaseItemForReportDTO } from '../../../ordering';

export interface IAggregatedDateReportView {
  date: string;
  totalSales: number;
  onlineSales: number;
  offlineSales: number;
  refunds: number;
  amount: number;
  purchases: Partial<IPurchaseItemForReportDTO>[];
}
