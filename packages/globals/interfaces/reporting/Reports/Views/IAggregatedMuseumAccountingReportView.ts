import { IAggregatedNomenclatureAccountingReportView } from './IAggregatedNomenclatureAccountingReportView';

export interface IAggregatedMuseumAccountingReportView {
  museumId: string;
  tickets: IAggregatedNomenclatureAccountingReportView[];
  abonements: IAggregatedNomenclatureAccountingReportView[];
}
