import { IOrganizationBankModel, IOrganizationContractView } from '../../../organization';

export interface IOrganizationAccountingReportView {
  id: string;
  ogrn: string;
  kpp: string;
  inn: string;
  name: string;
  director: string;
  bank: IOrganizationBankModel;
  region: string;
  locality: string;
  contract: IOrganizationContractView;
}
