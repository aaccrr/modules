import { IMuseumDetailedReportView } from './IMuseumDetailedReportView';
import { INomenclatureDetailedReportView } from './INomenclatureDetailedReportView';

export interface IDetailedReportView {
  [museumName: string]: IMuseumDetailedReportView | INomenclatureDetailedReportView[];
}
