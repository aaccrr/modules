import { IAggregatedDateReportView } from './IAggregatedDateReportView';

export interface IAggregatedClientCategoryAccountingReportView {
  name: string;
  price: number;
  totalSales: number;
  onlineSales: number;
  offlineSales: number;
  refunds: number;
  amount: number;
  days: IAggregatedDateReportView[];
}
