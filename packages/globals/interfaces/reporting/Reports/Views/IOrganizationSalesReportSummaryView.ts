import { NomenclatureTypeEnum } from '../../../nomenclature';

export interface IOrganizationSalesReportSummaryView {
  [key: string]: {
    sales: number;
    amount: number;
  };
}
