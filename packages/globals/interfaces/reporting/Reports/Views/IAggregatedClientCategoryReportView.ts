export interface IAggregatedClientCategoryReportView {
  name: string;
  price: number;
  totalSales: number;
  onlineSales: number;
  offlineSales: number;
  refunds: number;
  visits: number;
  amount: number;
}
