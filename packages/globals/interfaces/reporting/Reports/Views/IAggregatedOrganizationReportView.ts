import { IAggregatedEventsListReportView } from './IAggregatedEventsListReportView';
import { IAggregatedMuseumReportView } from './IAggregatedMuseumReportView';

export interface IAggregatedOrganizationReportView {
  museums: IAggregatedMuseumReportView[];
  events: IAggregatedEventsListReportView;
  amount: number;
}
