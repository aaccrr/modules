import { IAggregatedClientCategoryAccountingReportView } from './IAggregatedClientCategoryAccountingReportView';

export interface IAggregatedNomenclatureAccountingReportView {
  id: string;
  positionId?: number;
  totalSales: number;
  refunds: number;
  onlineSales: number;
  offlineSales: number;
  amount: number;
  expiresAt: Date | null;
  deletedAt: Date | null;
  categories: IAggregatedClientCategoryAccountingReportView[];
}
