import { INomenclatureForAccountingReportInformationView } from './INomenclatureForAccountingReportInformationView';
import { IMuseumForAccountingReportInformationView } from './IMuseumForAccountingReportInformationView';
import { IOrganizationAccountingReportView } from './IOrganizationAccountingReportView';

export interface IAccountingReportInformationView {
  organization: IOrganizationAccountingReportView;
  museums: IMuseumForAccountingReportInformationView[];
  events: INomenclatureForAccountingReportInformationView[];
}
