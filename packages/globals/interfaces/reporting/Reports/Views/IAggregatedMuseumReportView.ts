import { IAggregatedNomenclatureReportView } from './IAggregatedNomenclatureReportView';

export interface IAggregatedMuseumReportView {
  id: string;
  name: string;
  tickets: IAggregatedNomenclatureReportView[];
  abonements: IAggregatedNomenclatureReportView[];
  amount: number;
}
