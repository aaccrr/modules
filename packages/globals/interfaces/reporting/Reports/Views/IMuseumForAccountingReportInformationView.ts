import { INomenclatureForAccountingReportInformationView } from './INomenclatureForAccountingReportInformationView';

export interface IMuseumForAccountingReportInformationView {
  id: string;
  name: string;
  tickets: INomenclatureForAccountingReportInformationView[];
  abonements: INomenclatureForAccountingReportInformationView[];
}
