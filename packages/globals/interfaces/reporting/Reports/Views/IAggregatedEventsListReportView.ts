import { IAggregatedNomenclatureReportView } from './IAggregatedNomenclatureReportView';

export interface IAggregatedEventsListReportView {
  items: IAggregatedNomenclatureReportView[];
  amount: number;
}
