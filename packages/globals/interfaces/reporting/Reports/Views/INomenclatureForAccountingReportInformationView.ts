import { IClientCategoryView } from '../../../shared';

export interface INomenclatureForAccountingReportInformationView {
  id: string;
  title: string;
  clientCategories: Partial<IClientCategoryView>[];
  deletedAt: Date | null;
  expiresAt: Date | null;
  positionId?: number;
}
