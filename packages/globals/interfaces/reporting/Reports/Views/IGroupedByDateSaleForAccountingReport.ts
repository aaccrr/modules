import { IPurchaseItemForReportDTO } from '../../../ordering';

export interface IGroupedByDateSaleForAccountingReport {
  date: string;
  sales: IPurchaseItemForReportDTO[];
}
