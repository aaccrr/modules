export interface ICategoryDetailedReportView {
  name: string;
  totalSales: number;
  onlineSales: number;
  refunds: number;
  offlineSales: number;
  amount: number;
}
