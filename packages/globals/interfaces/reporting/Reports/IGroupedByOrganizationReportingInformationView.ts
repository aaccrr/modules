import { IMuseumForReportDTO } from '../../museum';
import { INomenclatureForReportDTO } from '../../nomenclature';
import { IReportingOrganizationDTO } from '../../organization';
import { IPurchaseItemForReportDTO } from '../../ordering';

export interface IGroupedByOrganizationReportingInformationView extends IReportingOrganizationDTO {
  museums: IMuseumForReportDTO[];
  nomenclature: INomenclatureForReportDTO[];
  sales: IPurchaseItemForReportDTO[];
}
