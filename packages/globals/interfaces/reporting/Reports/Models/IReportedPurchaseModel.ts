import { ITimezone, Nullable } from '../../../shared';
import { PurchaseStatusEnum } from '../../../ordering';
import { NomenclatureTypeEnum } from '../../../nomenclature';

export interface IReportedPurchaseLocationModel {
  locality: string;
  localityId: string;
  province: string;
  provinceId: number;
  region: string;
  regionId: number;
  timezone: ITimezone;
}

export interface IPurchasedProductModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
}

export interface IVisitMuseumModel {
  id: string;
  name: string;
}

export interface IReportedPurchaseModel {
  id: string;
  visitLocations: IReportedPurchaseLocationModel[];
  purchaseLocation: IReportedPurchaseLocationModel;
  amount: string;
  price: string;
  serviceCharge: string;
  itemsCount: number;
  serviceProviderId: string;
  serviceProviderName: string;
  visitMuseum: IVisitMuseumModel;
  product: IPurchasedProductModel;
  status: PurchaseStatusEnum;
  createdAt: Date;
  paidAt: Nullable<Date>;
  refundedAt: Nullable<Date>;
}
