import { IPurchasePropsModel, PurchaseStatusEnum } from '../../../ordering';
import { Nullable } from '../../../shared';
import { IReportedPurchaseLocationModel, IPurchasedProductModel, IVisitMuseumModel } from './IReportedPurchaseModel';

export interface IReportedEntranceTicketModel {
  id: string;
  purchaseId: string;
  visitLocations: IReportedPurchaseLocationModel[];
  purchaseLocation: IReportedPurchaseLocationModel;
  customerCategoryName: string;
  status: PurchaseStatusEnum;
  price: string;
  product: IPurchasedProductModel;
  serviceProviderId: string;
  serviceProviderName: string;
  visitMuseum: IVisitMuseumModel;
  props: Partial<IPurchasePropsModel>;
  createdAt: Date;
  paidAt: Nullable<Date>;
  refundedAt: Nullable<Date>;
  passedAt: Nullable<Date>;
}
