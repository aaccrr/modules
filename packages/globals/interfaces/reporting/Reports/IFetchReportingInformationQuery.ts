import { NomenclatureTypeEnum } from '../../nomenclature';
import { IDatesPeriodView } from '../../shared';

export interface IFetchReportingInformationQuery {
  reportingPeriod: IDatesPeriodView;
  reportingNomenclatureTypes?: NomenclatureTypeEnum[];
  reportingOrganizations?: string[];
  reportinhMuseums?: string[];
}
