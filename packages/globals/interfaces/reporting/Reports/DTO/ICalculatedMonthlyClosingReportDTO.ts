import { IReportingOrganizationDTO } from '../../../organization';
import { IDatesPeriodView } from '../../../shared';
import { IAggregatedOrganizationReportView } from '../Views';

export interface ICalculatedMonthlyClosingReportDTO {
  reportingPeriod: IDatesPeriodView;
  reportingOrganization: IReportingOrganizationDTO;
  closingReport: IAggregatedOrganizationReportView;
}
