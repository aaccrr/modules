import { IDetailedReportView } from '../Views';

export interface ISendPrintedReportOnMailDTO {
  organizationId: string;
  recipients: string[];
  report: IDetailedReportView;
}
