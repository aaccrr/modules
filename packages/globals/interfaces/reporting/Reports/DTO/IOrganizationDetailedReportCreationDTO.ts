export interface IOrganizationDetailedReportCreationDTO {
  organizationId: string;
  museums?: string[];
  types?: string[];

  from: string;
  to: string;
}
