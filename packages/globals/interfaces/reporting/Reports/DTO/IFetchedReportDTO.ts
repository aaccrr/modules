import { IMuseumForReportDTO } from '../../../museum';
import { INomenclatureForReportDTO } from '../../../nomenclature';
import { IReportingOrganizationDTO } from '../../../organization';
import { IReportedEntranceTicketModel } from '../Models';

export interface IFetchedReportDTO {
  reportingOrganizationData: IReportingOrganizationDTO;
  reportingMuseumsData: IMuseumForReportDTO[];
  reportingNomenclatureData: INomenclatureForReportDTO[];
  reportingSalesData: IReportedEntranceTicketModel[];
}
