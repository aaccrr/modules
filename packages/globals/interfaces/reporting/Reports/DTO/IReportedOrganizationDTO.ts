export interface IReportedOrganizationDTO {
  id: string;
  name: string;
}
