import { IReportingOrganizationDTO } from '../../../organization';
import { IDatesPeriodView } from '../../../shared';
import { IAggregatedOrganizationReportView } from '../Views';

export interface IPrintClosingActDTO {
  reportingPeriod: IDatesPeriodView;
  reportingOrganization: IReportingOrganizationDTO;
  closingReport: IAggregatedOrganizationReportView;
}
