export * from './IOrganizationDetailedReportCreationDTO';
export * from './ISendOrganizationDetailedReportOnMailDTO';
export * from './ISendPrintedReportOnMailDTO';

export * from './IReportedOrganizationDTO';
export * from './IReportedNomenclatureDTO';
export * from './IPrintClosingActDTO';
export * from './IFetchedReportDTO';
export * from './ICalculatedMonthlyReportsDTO';
export * from './ICalculatedControlSpinesReportDTO';
export * from './ICalculatedMonthlyClosingReportDTO';
export * from './IDetailedSalesReportDTO';
