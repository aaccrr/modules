import { IReportingOrganizationDTO } from '../../../organization';
import { IAggregatedEventsListReportView, IAggregatedMuseumReportView } from '../Views';

export interface IDetailedSalesReportDTO {
  reportingOrganization: IReportingOrganizationDTO;
  museums: IAggregatedMuseumReportView[];
  events: IAggregatedEventsListReportView;
  amount: number;
}
