export interface IReportedNomenclatureDTO {
  organizationId: string;
  nomenclature: IReportedNomenclatureView[];
}

export interface IReportedNomenclatureView {
  id: string;
  title: string;
  clientCategories: IReportedNomenclatureClientCategoryView[];
  latestExpirationDateOfPurchase: Date;
  deletedAt: Date;
}

export interface IReportedNomenclatureClientCategoryView {
  name: string;
  price: number;
}
