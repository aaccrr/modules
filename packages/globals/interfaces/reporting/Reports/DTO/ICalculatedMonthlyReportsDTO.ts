import { ICalculatedControlSpinesReportDTO } from './ICalculatedControlSpinesReportDTO';
import { ICalculatedMonthlyClosingReportDTO } from './ICalculatedMonthlyClosingReportDTO';

export interface ICalculatedMonthlyReportsDTO {
  calculatedClosingReport: ICalculatedMonthlyClosingReportDTO;
  calculatedControlSpinesReport: ICalculatedControlSpinesReportDTO;
  mailingList: string[];
}
