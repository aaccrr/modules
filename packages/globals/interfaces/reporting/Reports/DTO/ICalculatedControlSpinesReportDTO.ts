import { IReportingOrganizationDTO } from '../../../organization';
import { IDatesPeriodView } from '../../../shared';
import { IListControlSpinesView } from '../../../ordering';

export interface ICalculatedControlSpinesReportDTO {
  reportingOrganization: IReportingOrganizationDTO;
  reportingPeriod: IDatesPeriodView;
  controlSpinesReport: IListControlSpinesView[];
  totalAmount: number;
  refundsAmount: number;
}
