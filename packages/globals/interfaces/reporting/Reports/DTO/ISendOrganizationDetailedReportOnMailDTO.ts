export interface ISendOrganizationDetailedReportOnMailDTO {
  organizationId: string;
  museums?: string[];
  types?: string[];

  from: string;
  to: string;

  recipients: string[];
}
