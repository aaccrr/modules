import { IOrderModel } from '../../../ordering';

export type IIndexPassedPurchaseCommand = IOrderModel;
