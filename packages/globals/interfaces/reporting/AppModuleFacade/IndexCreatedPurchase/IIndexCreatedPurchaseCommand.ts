import { IOrderModel } from '../../../ordering';

export type IIndexCreatedPurchaseCommand = IOrderModel;
