import { IOrderModel } from '../../../ordering';

export type IIndexRefundedPurchaseCommand = IOrderModel;
