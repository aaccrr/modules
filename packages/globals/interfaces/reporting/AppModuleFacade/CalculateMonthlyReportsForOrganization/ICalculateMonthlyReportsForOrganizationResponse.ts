import { ICalculatedControlSpinesReportDTO, ICalculatedMonthlyClosingReportDTO } from '../../Reports';

export interface ICalculateMonthlyReportsForOrganizationResponse {
  calculatedClosingReport: ICalculatedMonthlyClosingReportDTO;
  calculatedControlSpinesReport: ICalculatedControlSpinesReportDTO;
  mailingList: string[];
}
