import { IDatesPeriodView } from '../../../shared';

export interface ICalculateMonthlyReportsForOrganizationQuery {
  reportingOrganizationId: string;
  reportingPeriod: IDatesPeriodView;
  mailingList: string[];
}
