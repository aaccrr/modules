import { IDatesPeriodView } from '../../../shared';
import { IPrintedClosingReportView } from '../../ClosingReports';

export interface IAddLatestClosingReportToListingCommand {
  latestClosingReport: IPrintedClosingReportView;
  reportingPeriod: IDatesPeriodView;
}
