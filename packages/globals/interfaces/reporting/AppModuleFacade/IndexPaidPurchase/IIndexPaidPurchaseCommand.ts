import { IOrderModel } from '../../../ordering';

export type IIndexPaidPurchaseCommand = IOrderModel;
