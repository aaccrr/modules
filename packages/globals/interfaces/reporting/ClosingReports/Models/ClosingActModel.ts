export interface IClosingActModel {
  id: string;
  organizationId: string;
  year: number;
  month: string;
  url: string;
  createdAt: Date;
}
