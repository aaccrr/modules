export interface IActPeriodView {
  from: string;
  to: string;
}
