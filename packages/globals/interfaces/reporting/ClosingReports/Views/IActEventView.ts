export interface IActEventView {
  id: string;
  title: string;
  positions: IActEventPositionView[];
}

export interface IActEventPositionView {
  id: number;
  date?: string;
  dates?: string[];
  clientCategories: IActClientCategoryView[];
}

export interface IActClientCategoryView {
  name: string;
  price: {
    online: number;
  };
}
