export interface IActView {
  id: string;
  organizationId: string;
  year: number;
  month: string;
  url: string;
  createdAt: string;
}
