import { IActMuseumNomenclatureView } from './IActMuseumNomenclatureView';

export interface IActMuseumView {
  id: string;
  name: string;
  tickets: IActMuseumNomenclatureView[];
  abonements: IActMuseumNomenclatureView[];
}
