import { IActClientCategoryView } from './IActEventView';

export interface IActMuseumSet {
  [museumId: string]: {
    tickets: IActMuseumNomenclatureView[];
    abonements: IActMuseumNomenclatureView[];
  };
}

export interface IActMuseumNomenclatureView {
  id: string;
  title: string;
  clientCategories: IActClientCategoryView[];
}
