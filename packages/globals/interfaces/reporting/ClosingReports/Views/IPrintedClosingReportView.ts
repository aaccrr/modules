export interface IPrintedClosingReportView {
  id: string;
  printedClosingReportUrl: string;
  printedClosingReportInternalPath: string;
}
