import { IPurchaseItemForReportDTO } from '../../../ordering';

export interface IAggregatedOrganizationClosingActView {
  organizationId: string;
  sales: IPurchaseItemForReportDTO[];
}
