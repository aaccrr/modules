export interface IActOrganizationView {
  id: string;
  inn: string;
  name: string;
  director: string;
  contract: {
    number: string;
    createdAt: string;
  };
}
