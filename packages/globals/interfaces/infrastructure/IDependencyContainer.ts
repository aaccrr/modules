export interface IDependencyContainer {
  init(): Promise<void>;
}
