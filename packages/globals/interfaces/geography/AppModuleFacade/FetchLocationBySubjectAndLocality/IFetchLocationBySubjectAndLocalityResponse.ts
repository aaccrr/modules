import { ILocationModel } from '../../..';

export type IFetchLocationBySubjectAndLocalityResponse = ILocationModel;
