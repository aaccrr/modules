export interface IFetchLocationBySubjectAndLocalityQuery {
  subject: string;
  locality: string;
}
