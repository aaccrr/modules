import { IPagination } from '../../..';

export interface IListGeographyObjectsIdsQuery {
  pagination: IPagination;
}
