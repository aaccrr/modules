import { ILocalityView } from '../../Views';

export type IFetchLocationByLocalityIdResponse = ILocalityView;
