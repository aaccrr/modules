export interface IFetchLocationByLocalityIdQuery {
  localityId: string;
}
