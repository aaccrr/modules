import { IGeoPoint } from '../../..';

export interface IFetchLocationByGeoQuery {
  locality: string;
  address: string;
  location: IGeoPoint;
}
