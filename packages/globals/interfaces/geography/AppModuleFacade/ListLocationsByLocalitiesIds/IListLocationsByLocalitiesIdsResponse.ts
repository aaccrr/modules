import { ILocalityView } from '../../Views';

export type IListLocationsByLocalitiesIdsResponse = ILocalityView[];
