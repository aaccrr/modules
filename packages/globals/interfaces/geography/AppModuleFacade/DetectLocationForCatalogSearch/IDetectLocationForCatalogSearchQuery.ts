import { IGeoPoint } from '../../../shared';

export interface IDetectLocationForCatalogSearchQuery {
  regionId?: string;
  location?: IGeoPoint;
  userGeohash?: string;
}
