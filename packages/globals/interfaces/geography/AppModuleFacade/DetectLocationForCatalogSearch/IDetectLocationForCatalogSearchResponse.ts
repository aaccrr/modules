import { ICatalogQueryLocationView } from '../../..';

export type IDetectLocationForCatalogSearchResponse = ICatalogQueryLocationView;
