import { ITimezone } from '../shared';

export interface ILocationDTO {
  province: string;
  provinceId: number;
  region: string;
  regionId: number;
  district: string;
  locality: string;
  localityId: string;
  timezone: ITimezone;
  location: string;
}
