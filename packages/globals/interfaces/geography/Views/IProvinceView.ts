export interface IProvinceView {
  id: number;
  name: string;
}
