import { IGeoPoint, ITimezone } from '../../shared';

export interface ILocalityView {
  province: string;
  provinceId: number;
  region: string;
  regionId: number;
  district: string;
  locality: string;
  localityId: string;
  geo: IGeoPoint;
  localityTier: number;
  timezone: ITimezone;
  timezoneOffset: string;
  location: string;
  isLocalitySubjectCenter: boolean;
}
