import { IOrderModel } from '../../../ordering';

export type IFiscalizePurchasePaymentCommand = IOrderModel;
