import { IRefundModel } from '../../../ordering';

export type IFiscalizeRefundCommand = IRefundModel;
