export interface IRegisterRefundInPaymentSystemResponse {
  isAcceptedForProcessing: boolean;
}
