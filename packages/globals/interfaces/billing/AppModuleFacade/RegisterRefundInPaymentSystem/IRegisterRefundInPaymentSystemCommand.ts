import { IRefundModel } from '../../../ordering';

export type IRegisterRefundInPaymentSystemCommand = IRefundModel;
