import { IRefundModel } from '../../..';

export interface IConfirmRefundPaymentCommand {
  refund: IRefundModel;
  refundOrderForPayment: string;
}
