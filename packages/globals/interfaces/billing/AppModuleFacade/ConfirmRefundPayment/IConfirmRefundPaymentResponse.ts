export interface IConfirmRefundPaymentResponse {
  isRefundPaid: boolean;
}
