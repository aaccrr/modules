export interface IConfirmFiscalizationResponse {
  isFiscalized: boolean;
}
