export interface IRefundCustomerByPaymentSystemResponse {
  isAcceptedForProcessing: boolean;
}
