export interface IBillCustomerByPaymentSystemResponse {
  id: string;
  link: string;
}
