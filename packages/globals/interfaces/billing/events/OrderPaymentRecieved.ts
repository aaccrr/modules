import { IPurchaseModel } from '../../ordering';
import { ICustomerModel } from '../../accounting';

export interface IOrderPaymentRecievedEvent {
  order: {
    id: string;
    amount: string;
    price: string;
    serviceCharge: string;
    customer: ICustomerModel;
    items: Partial<IPurchaseModel>[];
  };
  payment: {
    id: string;
    amount: string;
    customer: string;
  };
}
