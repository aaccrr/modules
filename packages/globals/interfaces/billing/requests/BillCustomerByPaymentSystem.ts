import { IPurchaseModel } from '../../ordering';
import { ICustomerModel } from '../../accounting';

export interface IBillCustomerByPaymentSystemRequest {
  id: string;
  customer: ICustomerModel;
  amount: string;
  price: string;
  serviceCharge: string;
  nomenclatureTitle: string;
  items: Partial<IPurchaseModel>[];
}
