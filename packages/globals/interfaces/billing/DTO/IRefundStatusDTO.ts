export interface IRefundStatusDTO {
  isProcessed: boolean;
}
