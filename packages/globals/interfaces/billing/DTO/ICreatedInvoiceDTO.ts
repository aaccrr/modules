export class ICreatedInvoiceDTO {
  invoiceUrl: string;
  invoiceId: string;
}
