export interface IAcceptedRefundByPaymentSystemDTO {
  isRefundAccepted: boolean;
}
