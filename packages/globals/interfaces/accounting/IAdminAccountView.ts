import { UserRole } from '../shared';
import { Gender } from '../../../interfaces';
import { AdminAccountStatus } from './IAdminAccountStatus';

export interface IAdminAccountView {
  id?: string;
  organizationId?: string;
  organizationName?: string;
  role?: UserRole;
  firstName?: string;
  lastName?: string;
  patronymic?: string;
  phone?: string;
  email?: string;
  avatar?: string;
  status?: AdminAccountStatus;
  password?: string;
  birthDate?: string;
  gender?: Gender;
  mobileLogin?: string;
  administeredMuseums?: string[];

  createdAt?: Date;
  updatedAt?: Date;
}
