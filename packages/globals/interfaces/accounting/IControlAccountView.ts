import { UserRole } from '../shared';
import { ControlAccountStatus } from './IControlAccountStatus';

export interface IControlAccountView {
  id?: string;
  organizationId?: string;
  organizationName?: string;
  role?: UserRole;
  museums?: string[];
  firstName?: string;
  lastName?: string;
  patronymic?: string;
  phone?: string;
  email?: string;
  avatar?: string;
  mobileLogin?: string;
  status?: ControlAccountStatus;

  createdBy?: string;
  updatedBy?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
