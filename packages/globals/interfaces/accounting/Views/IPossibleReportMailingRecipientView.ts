export interface IPossibleReportMailingRecipientView {
  fio: string;
  email: string;
}
