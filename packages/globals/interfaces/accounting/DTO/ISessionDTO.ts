export interface ISessionDTO {
  accessToken: string;
  refreshToken: string;
}
