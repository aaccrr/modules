import { ICustomerLocationModel } from '../..';
import { ICustomerModel } from '../Models';
import { ISessionDTO } from './ISessionDTO';

export interface INormalizedCustomerInfoDTO {
  customer: ICustomerModel;
  isNewAccountRegistered: boolean;
  newAccountSession: ISessionDTO | null;
  customerLocation: ICustomerLocationModel;
}
