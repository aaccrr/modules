export interface IAutomaticRegisteredCustomerDTO {
  firstName: string;
  email: string;
  generatedPassword: string;
}
