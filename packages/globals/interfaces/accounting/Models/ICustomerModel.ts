export interface ICustomerModel {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}
