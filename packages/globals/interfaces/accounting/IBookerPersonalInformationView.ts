export interface IBookerPersonalInformationView {
  lastName: string;
  firstName: string;
  patronymic: string;
  email: string;
  phone: string;
}
