export interface IPrimaryManagerPersonalnformationView {
  lastName: string;
  firstName: string;
  patronymic: string;
  email: string;
  phone: string;
}
