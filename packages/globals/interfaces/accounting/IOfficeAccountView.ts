import { UserRole } from '../shared';

export interface IOfficeAccountView {
  id?: string;
  role?: UserRole;
  email?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
  confirmedAt?: Date;
  deletedAt?: Date;
  createdAt?: Date;
}
