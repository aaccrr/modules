export interface IGetAdminAccountForAttachAvatarResponse {
  id: string;
  avatar: string;
}