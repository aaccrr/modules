export interface IGetAdminAccountForAttachAvatarQuery {
  id: string;
  organizationId: string;
}
