import { IRequestUser } from '../../..';

export interface IPurgeEmployeesOfMuseumCommand {
  museum: {
    id: string;
  };
  deleter: IRequestUser;
}
