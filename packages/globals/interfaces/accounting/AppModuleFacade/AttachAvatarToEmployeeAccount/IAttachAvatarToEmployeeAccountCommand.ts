export interface IAttachAvatarToEmployeeAccountCommand {
  id: string;
  organizationId: string;
  avatar: string;
}
