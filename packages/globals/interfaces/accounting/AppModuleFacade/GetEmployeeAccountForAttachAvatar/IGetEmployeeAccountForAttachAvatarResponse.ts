export interface IGetEmployeeAccountForAttachAvatarResponse {
  id: string;
  avatar: string;
}
