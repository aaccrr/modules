export interface IGetEmployeeAccountForAttachAvatarQuery {
  id: string;
  organizationId: string;
}