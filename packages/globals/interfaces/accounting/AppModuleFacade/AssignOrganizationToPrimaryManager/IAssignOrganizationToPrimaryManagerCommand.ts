export interface IAssignOrganizationToPrimaryManagerCommand {
  primaryManagerId: string;
  organizationId: string;
  organizationName: string;
}
