import { IBookerPersonalInformationView } from '../..';

export interface ICreateBookerAccountCommand {
  organizationId: string;
  organizationName: string;
  booker: IBookerPersonalInformationView;
}
