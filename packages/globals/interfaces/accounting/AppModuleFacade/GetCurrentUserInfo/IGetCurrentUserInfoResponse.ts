import { IOrganizationSummaryView } from '../../../organization';
import { UserRole } from '../../../shared';
import { AdminAccountStatus } from '../../IAdminAccountStatus';

export interface IGetCurrentUserInfoResponse {
  id: string;
  role: UserRole;
  status: AdminAccountStatus;
  organizationName: string;
  administeredMuseums?: string[];
  organization: IOrganizationSummaryView;
}
