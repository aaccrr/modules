export interface IGetCurrentUserInfoQuery {
  currentUserId: string;
}
