import { ICustomerModel } from '../../..';

export interface ICollectCustomerInformationQuery {
  currentUserId: string;
  passedCustomerInfo: ICustomerModel;
  ip: string;
}
