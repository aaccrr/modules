import { ICustomerLocationModel, ICustomerModel, ISessionDTO } from "../../..";

export interface ICollectCustomerInformationResponse {
  customer: ICustomerModel;
  customerLocation: ICustomerLocationModel;
  isNewAccountRegistered: boolean;
  newAccountSession: ISessionDTO | null;  
}