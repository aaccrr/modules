export interface IViewPrimaryManagerAccountInOrganizationQuery {
  organizationId: string;
}
