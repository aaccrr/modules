export interface IViewPrimaryManagerAccountInOrganizationResponse {
  id: string;
  firstName: string;
  lastName: string;
}