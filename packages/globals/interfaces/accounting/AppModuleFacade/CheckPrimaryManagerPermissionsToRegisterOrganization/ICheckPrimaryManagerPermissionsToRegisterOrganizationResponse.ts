export interface ICheckPrimaryManagerPermissionsToRegisterOrganizationResponse {
  lastName: string;
  firstName: string;
  patronymic: string;
  email: string;
  phone: string;
}
