export interface IAttachAvatarToAdminAccountCommand {
  id: string;
  organizationId: string;
  avatar: string;
}
