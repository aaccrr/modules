import { ITokenPayload } from '../../..';

export interface IValidateSessionResponse {
  success: boolean;
  data: ITokenPayload;
}
