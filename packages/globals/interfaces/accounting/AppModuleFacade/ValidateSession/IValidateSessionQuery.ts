export interface IValidateSessionQuery {
  userId: string;
  userGroup: string;
  accessToken: string;
}
