export enum ClientAccountStatus {
  CREATED = 'CREATED',
  CONFIRMED = 'CONFIRMED',
  BLOCKED = 'BLOCKED',
  DELETED = 'DELETED',
}
