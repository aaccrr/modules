export interface INormalizedCustomerInfo {
  id: string;
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
}
