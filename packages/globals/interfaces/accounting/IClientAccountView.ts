import { UserRole } from '../shared';
import { ClientAccountStatus } from './IClientAccountStatus';

export interface IClientAccountView {
  id?: string;
  role?: UserRole;
  firstName?: string;
  lastName?: string;
  phone?: string;
  email?: string;
  password?: string;
  status?: ClientAccountStatus;
  location?: any;
  createdAt?: Date;
  confirmedAt?: Date;
  deletedAt?: Date;

  criticalInfoUpdatedAt?: Date;
}
