export enum AdminAccountStatus {
  CREATED = 'CREATED',
  BANNED = 'BANNED',
  CONFIRMED = 'CONFIRMED',
  DELETED = 'DELETED',
}
