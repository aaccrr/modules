export interface IResendAccountConfirmationCodeCommand {
  id: string;
  email: string;
  firstName: string;
}
