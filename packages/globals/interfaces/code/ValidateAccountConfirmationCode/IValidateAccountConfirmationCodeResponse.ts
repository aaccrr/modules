export interface IValidateAccountConfirmationCodeResponse {
  id: string;
  action: string;
  email?: string;
}
