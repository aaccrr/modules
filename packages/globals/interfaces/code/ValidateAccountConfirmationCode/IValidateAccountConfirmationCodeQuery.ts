export interface IValidateAccountConfirmationCodeQuery {
  code: string;
}