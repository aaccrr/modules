export interface IGenerateEmailChangeConfirmCodeCommand {
  account: {
    id: string;
    firstName: string;
  };
  newEmail: string;
}
