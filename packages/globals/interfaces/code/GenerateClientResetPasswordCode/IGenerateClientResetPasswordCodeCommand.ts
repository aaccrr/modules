import { UserRole } from '../../shared';

export interface IGenerateClientResetPasswordCodeCommand {
  id: string;
  firstName: string;
  role: UserRole;
  email: string;
}
