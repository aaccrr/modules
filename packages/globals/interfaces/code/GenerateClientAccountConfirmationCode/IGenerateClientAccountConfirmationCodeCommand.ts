export interface IGenerateClientAccountConfirmationCodeCommand {
  id: string;
  location: {
    gh: string;
  };
  email: string;
  firstName: string;
}