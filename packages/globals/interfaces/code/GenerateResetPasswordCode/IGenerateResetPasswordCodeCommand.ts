export interface IGenerateResetPasswordCodeCommand {
  id: string;
  firstName: string;
  resource: 'organization' | 'client';
  email: string;
}
