export interface IRemoveAccountConfirmationCodeQuery {
  code: string;
}
