export interface IGenerateAdminAccountConfirmationCodeCommand {
  id: string;
  email: string;
  firstName: string;
}
