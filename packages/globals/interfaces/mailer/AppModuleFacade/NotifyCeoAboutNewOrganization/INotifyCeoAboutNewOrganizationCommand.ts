import { IPrimaryManagerPersonalnformationView } from '../../../';
import { IOrganizationBankModel } from '../../../organization';

export interface INotifyCeoAboutNewOrganizationCommand {
  primaryManager: IPrimaryManagerPersonalnformationView;
  defaultContractNumber: string;
  contractSignDate: Date;
  createdOrganization: {
    fullName: string;
    shortName: string;
    legalAddress: string;
    directorFio: string;
    inn: string;
    kpp: string;
    okpo: string;
    bank: IOrganizationBankModel;
  };
}
