import { IPublicContactModel, IRefundContactView } from '../../..';
import { PurchaseTypeEnum } from '../../../ordering';

export interface ISendRefundApprovedNotificationToCustomerCommand {
  to: string;
  purchaseId: string;
  purchaseType: PurchaseTypeEnum;
  amount: string;
  serviceProviderName: string;
  contacts: IPublicContactModel[];
  refundContacts: IRefundContactView;
  website: string;
}
