import { IDatesPeriodView } from '../../../shared';

export interface ISendOrganizationMonthlyReportsOnEmailCommand {
  reportingPeriod: IDatesPeriodView;
  printedControlSpinesReportPath: string;
  printedMonthlyClosingReportPath: string;
  mailingList: string[];
}
