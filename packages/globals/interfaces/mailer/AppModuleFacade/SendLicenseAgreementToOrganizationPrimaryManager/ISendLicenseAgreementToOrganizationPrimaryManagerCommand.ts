import { IOrganizationBankModel } from '../../../organization';

export interface ISendLicenseAgreementToOrganizationPrimaryManagerCommand {
  primaryManagerEmail: string;
  defaultContractNumber: string;
  contractSignDate: Date;
  createdOrganization: {
    fullName: string;
    shortName: string;
    legalAddress: string;
    directorFio: string;
    inn: string;
    kpp: string;
    okpo: string;
    bank: IOrganizationBankModel;
  };
}
