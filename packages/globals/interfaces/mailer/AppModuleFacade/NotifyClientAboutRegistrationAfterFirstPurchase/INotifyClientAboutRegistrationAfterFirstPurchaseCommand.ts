export interface INotifyClientAboutRegistrationAfterFirstPurchaseCommand {
  firstName: string;
  email: string;
  generatedPassword: string;
}