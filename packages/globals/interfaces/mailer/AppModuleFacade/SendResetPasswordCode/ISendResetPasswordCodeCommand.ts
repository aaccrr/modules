export interface ISendResetPasswordCodeCommand {
  to: string;
  name: string;
  code: string;
  resource: 'organization' | 'client';
}
