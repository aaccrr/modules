export interface ISendOrganizationAccountRegistrationNotificationCommand {
  firstName: string;
  email: string;
  generatedPassword: string;
}
