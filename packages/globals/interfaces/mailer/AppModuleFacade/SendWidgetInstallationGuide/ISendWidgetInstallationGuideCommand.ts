export interface ISendWidgetInstallationGuideCommand {
  widgetId: string;
  webmasterEmail: string;
  widgetPlacementAddress: string;
  serviceProviderName: string;
}
