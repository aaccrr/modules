export interface ISendSalesReportCommand {
  recipients: string[];
  printedReportFilePath: string;
}
