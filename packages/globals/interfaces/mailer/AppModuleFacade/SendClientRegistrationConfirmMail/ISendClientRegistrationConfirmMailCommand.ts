export interface ISendClientRegistrationConfirmMailCommand {
  to: string;
  name: string;
  code: string;
}
