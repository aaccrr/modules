import { IOrderModel } from '../../../ordering';

export type ISendPrintedPurchaseToCustomerCommand = IOrderModel;
