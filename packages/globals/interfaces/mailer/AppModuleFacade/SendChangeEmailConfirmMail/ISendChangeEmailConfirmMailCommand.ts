export interface ISendChangeEmailConfirmMailCommand {
  to: string;
  name: string;
  code: string;
}
