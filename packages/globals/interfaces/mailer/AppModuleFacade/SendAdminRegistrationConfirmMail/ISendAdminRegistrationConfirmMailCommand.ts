export interface ISendAdminRegistrationConfirmMailCommand {
  to: string;
  name: string;
  code: string;
}
