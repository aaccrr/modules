export interface ISendMuseumCardCommand {
  to: string;
  printedMuseumCardPath: string;
  museumName: string;
}
