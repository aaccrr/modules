import { IPublicContactModel, IRefundContactView } from "../../..";
import { PurchaseTypeEnum } from "../../../ordering";

export interface ISendRefundRejectNotificationCommand {
  to: string;
  reason: string;
  purchaseId: string;
  purchaseType: PurchaseTypeEnum;
  serviceProviderName: string;
  contacts: IPublicContactModel[];
  refundContacts: IRefundContactView;
  website: string;
}