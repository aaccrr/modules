import {
  IEntranceTicketPurchaseCancellationMailDTO,
  IEventPurchaseCancellationMailDTO,
  ISeasonTicketPurchaseCancellationMailDTO,
} from '../../../ordering';

export type ISendPurchaseCancellationNotificationToCustomerCommand =
  | IEntranceTicketPurchaseCancellationMailDTO
  | ISeasonTicketPurchaseCancellationMailDTO
  | IEventPurchaseCancellationMailDTO;
