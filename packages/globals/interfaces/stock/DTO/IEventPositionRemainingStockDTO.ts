export interface IEventPositionRemainingStockDTO {
  id: string;
  onSale: number | null;
}
