import { IEntranceTicketSalesScheduleModel } from '../../../nomenclature';
import { ITimezone } from '../../../shared';

export interface IRescheduleTicketSalesCommand {
  id: string;
  salesSchedule: IEntranceTicketSalesScheduleModel;
  dayOffSales: boolean;
  museumId: string;
  timezone: ITimezone;
}
