import { ISalesLimitModel } from '../../..';

export interface IUpdateSeasonTicketStockCommand {
  id: string;
  limits: ISalesLimitModel;
}
