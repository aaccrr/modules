import { ITimezone } from '../../../shared';
import { IPurchasePropsModel } from '../../../ordering';

export interface ICancelPurchaseReservationCommand {
  id: string;
  nomenclatureId: string;
  count: number;
  props: IPurchasePropsModel;
  isFree: boolean;
  timezone: ITimezone;
  reservedAt: Date;
}
