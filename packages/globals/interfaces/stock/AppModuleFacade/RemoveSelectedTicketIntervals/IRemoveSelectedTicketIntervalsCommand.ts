import { ITimezone } from "../../../shared";

export interface IRemoveSelectedTicketIntervalsCommand {
  id: string;
  date: string;
  times: string[]
  timezone: ITimezone
}