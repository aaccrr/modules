import { ISalesLimitModel } from '../../..';

export interface IAddSeasonTicketStockCommand {
  id: string;
  limits: ISalesLimitModel;
}
