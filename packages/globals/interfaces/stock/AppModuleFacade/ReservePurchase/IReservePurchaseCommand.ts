import { IPurchasePropsModel } from '../../../ordering';
import { ITimezone } from '../../../shared';

export interface IReservePurchaseCommand {
  id: string;
  nomenclatureId: string;
  count: number;
  props: IPurchasePropsModel;
  isFree: boolean;
  timezone: ITimezone;
  reservedAt: Date;
}
