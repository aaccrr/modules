import { IEventPositionSalesLimitDTO } from '../../../nomenclature';

export interface IAddEventStockCommand {
  id: string;
  positions: IEventPositionSalesLimitDTO[];
}
