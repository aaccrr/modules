import { IEventPositionRemainingStockDTO } from '../../DTO';

export interface ICalculateRemainingEventStockResponse {
  positions: IEventPositionRemainingStockDTO[];
}
