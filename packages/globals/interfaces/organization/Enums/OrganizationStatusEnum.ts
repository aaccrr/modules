export enum OrganizationStatusEnum {
  PREVIEW = 'PREVIEW',
  WAITING_FOR_CONFIRMATION = 'WAITING_FOR_CONFIRMATION',
  CONFIRMED = 'CONFIRMED',
}
