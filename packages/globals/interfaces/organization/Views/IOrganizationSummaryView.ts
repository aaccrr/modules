export interface IOrganizationSummaryView {
  isExists: boolean;
  summary: {
    id: string;
    isConfirmed: boolean;
    isOfflineSalesEnabled: boolean;
    hasMuseum: boolean;
    hasController: boolean;
    hasGuide: boolean;
    automaticRefundApprovalEnabled: boolean;
  };
}
