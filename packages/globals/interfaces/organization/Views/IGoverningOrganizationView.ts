export interface IGoverningOrganizationView {
  id: string;
  name: string;
}
