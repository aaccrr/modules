export interface IGoverningOrganizationReportsMailingListView {
  id: string;
  reportsMailingList: string[];
}
