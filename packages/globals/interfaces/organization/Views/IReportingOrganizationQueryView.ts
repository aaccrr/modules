export interface IReportingOrganizationQueryView {
  id: string;
  museums: string[];
}
