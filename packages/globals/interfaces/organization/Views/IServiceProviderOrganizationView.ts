export interface IServiceProviderOrganizationView {
  id: string;
  address: string;
  fullName: string;
  inn: string;
}
