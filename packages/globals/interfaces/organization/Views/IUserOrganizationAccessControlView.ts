import { OrganizationStatusEnum } from "../Enums";

export interface IUserOrganizationAccessControlView {
  id: string;
  name: string;
  status: OrganizationStatusEnum
  hasMuseum: boolean;
  hasController: boolean;
}