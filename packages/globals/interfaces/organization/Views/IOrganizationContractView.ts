export interface IOrganizationContractView {
  signedAt: Date;
  number: string;
}