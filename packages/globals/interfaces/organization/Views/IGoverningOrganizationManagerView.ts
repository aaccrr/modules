export interface IGoverningOrganizationManagerView {
  name: string;
  manager: {
    firstName: string;
    lastName: string;
  };
}
