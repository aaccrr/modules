export interface IOrganizationInModerationQueueView {
  organization: {
    id: string;
    name: string;
  };
  total: number;
  museums: number;
  tickets: number;
  abonements: number;
  events: number;
}
