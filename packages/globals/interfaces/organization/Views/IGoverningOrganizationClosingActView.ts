import { IOrganizationContractModel } from '../Models';

export interface IGoverningOrganizationClosingActView {
  id: string;
  inn: string;
  name: string;
  director: string;
  contract: IOrganizationContractModel;
}
