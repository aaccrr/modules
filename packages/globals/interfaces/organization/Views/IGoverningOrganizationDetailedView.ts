import { IOrganizationDirectorModel, IOrganizationBankModel } from '../Models';
import { IOrganizationDocumentsModel } from '../Models/IOrganizationDocumentsModel';
import { OrganizationStatusEnum } from '../Enums';

export interface IGoverningOrganizationDetailedView {
  id: string;
  name: string;
  address: string;
  ogrn: string;
  inn: string;
  kpp: string;
  director: IOrganizationDirectorModel;
  registrationDate: string;
  organizationStatus: string;
  shortName: string;
  fullName: string;
  legalAddress: string;
  mailingAddress: string;
  contactPhone: string[];
  contactEmail: string;
  bank: IOrganizationBankModel;
  okved: string;
  okpo: string;
  okato: string;
  okogu: string;
  booker: string;
  docs: IOrganizationDocumentsModel;
  status: OrganizationStatusEnum;
  automaticRefundApprovalEnabled: boolean;
  hasMuseum: boolean;
  hasController: boolean;
}
