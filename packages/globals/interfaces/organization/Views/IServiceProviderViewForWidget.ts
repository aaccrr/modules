export interface IServiceProviderViewForWidget {
  shortName: string;
  inn: string;
  ogrn: string;
  legalAddress: string;
}
