import { IOrganizationBankModel, IOrganizationContractModel, IOrganizationLocationModel } from '../Models';

export interface IReportingOrganizationDTO {
  id: string;
  name: string;
  inn: string;
  kpp: string;
  ogrn: string;
  bankDetails: IOrganizationBankModel;
  mailingAddress: string;
  director: string;
  location: IOrganizationLocationModel;
  contract: IOrganizationContractModel;
  contacts: {
    phones: string[];
    email: string;
  };
}
