export interface IOrganizationDeletionDTO {
  deletedOrganizationId: string;
}
