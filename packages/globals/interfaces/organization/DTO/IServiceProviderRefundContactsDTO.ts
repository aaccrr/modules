import { IRefundContactView, IPublicContactModel } from '../../shared';

export interface IServiceProviderRefundContactsDTO {
  name: string;
  refundContacts: IRefundContactView;
  contacts: IPublicContactModel[];
  site: string;
}
