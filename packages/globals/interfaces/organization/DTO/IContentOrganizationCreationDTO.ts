export interface IContentOrganizationCreationDTO {
  name: string;
  inn: string;
}
