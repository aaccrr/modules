export interface IGoverningOrganizationReportMailingDTO {
  id: string;
  reportsMailingList: string[];
  from: string;
  to: string;
}
