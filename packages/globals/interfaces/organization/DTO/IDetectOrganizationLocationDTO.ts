export interface IDetectOrganizationLocationDTO {
  organizationId: string;
  region: string;
  locality: string;
}
