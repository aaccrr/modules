import { IOrganizationBankModel } from '../Models/IOrganizationBankModel';
import { IOrganizationDirectorModel } from '../Models/IOrganizationDirectorModel';

export interface IGoverningOrganizationCreationDTO {
  name: string;
  address: string;
  inn: string;
  ogrn: string;
  kpp: string;
  director: IOrganizationDirectorModel;
  mailingAddress: string;
  contactPhone: string[];
  contactEmail: string;
  bank: IOrganizationBankModel;
  booker: string;
  fullName?: string;
  shortName?: string;
  legalAddress?: string;
  okato?: string;
  okogu?: string;
  okpo?: string;
  okved?: string;
  organizationStatus?: string;
  registrationDate?: string;
  region: string;
  locality: string;
}
