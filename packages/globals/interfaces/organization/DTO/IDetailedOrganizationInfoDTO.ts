import { IOrganizationBankModel, IOrganizationDirectorModel } from '../Models';
import { IOrganizationDocumentsModel } from '../Models/IOrganizationDocumentsModel';

export interface IDetailedOrganizationInfoDTO {
  name: string;
  shortName: string;
  fullName: string;

  address: string;
  legalAddress: string;
  mailingAddress: string;

  ogrn: string;
  inn: string;
  kpp: string;
  okved: string;
  okato: string;
  okogu: string;
  okpo: string;

  bank: IOrganizationBankModel;
  director: IOrganizationDirectorModel;
  registrationDate: string;
  organizationStatus: string;

  contactPhone: string[];
  contactEmail: string;

  docs: IOrganizationDocumentsModel;
}
