export interface IPrintLicenseAgreementDTO {
  organization: {
    id: string;
    createdAt: Date;
    fullName: string;
    shortName: string;
    legalAddress: string;
    directorFio: string;
    inn: string;
    kpp: string;
    okpo: string;
    bank: {
      rs: string;
      name: string;
      bik: string;
    };
  };
  primaryManagerEmail: string;
}
