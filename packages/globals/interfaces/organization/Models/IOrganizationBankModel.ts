export interface IOrganizationBankModel {
  bill: string;
  bankName: string;
  bankBIK: string;
  bankKS: string;
  paymentsRecipient: string;
  kbk: string;
}
