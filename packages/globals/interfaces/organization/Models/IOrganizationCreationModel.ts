import { UserRole } from '../../shared';

export interface IOrganizationCreationModel {
  createdBy: string;
  createdByRole: UserRole;
  createdAt: Date;
}
