import { IOrganizationDocumentModel } from './IOrganizationDocumentModel';

export interface IOrganizationDocumentsModel {
  registration: IOrganizationDocumentModel;
  nalog: IOrganizationDocumentModel;
  manager: IOrganizationDocumentModel;
  creation: IOrganizationDocumentModel;
  egurl: IOrganizationDocumentModel;
}
