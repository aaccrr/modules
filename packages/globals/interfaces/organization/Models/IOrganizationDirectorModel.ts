export interface IOrganizationDirectorModel {
  fio: string;
  inn: string;
}
