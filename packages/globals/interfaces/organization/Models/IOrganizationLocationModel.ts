import { ITimezone } from '../../shared';

export interface IOrganizationLocationModel {
  locality: string;
  localityId: string;
  provinceId: number;
  region: string;
  regionId: number;
  timezone: ITimezone;
}
