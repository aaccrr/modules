export interface IOrganizationSettingsModel {
  isOfflineSalesEnabled: boolean;
  automaticRefundApprovalEnabled: boolean;
  reportsMailingList: string[];
}
