import { IOrganizationCreationModel } from './IOrganizationCreationModel';
import { OrganizationStatusEnum } from '../Enums';
import { IOrganizationSettingsModel } from './IOrganizationSettingsModel';
import { IOrganizationBankModel } from './IOrganizationBankModel';
import { IOrganizationDirectorModel } from './IOrganizationDirectorModel';
import { IOrganizationContractModel } from './IOrganizationContractModel';
import { IOrganizationDocumentsModel } from './IOrganizationDocumentsModel';
import { IOrganizationLocationModel } from './IOrganizationLocationModel';

export interface IOrganizationModel {
  id: string;
  name: string;
  address: string;
  inn: string;
  ogrn: string;
  kpp: string;
  director: IOrganizationDirectorModel;
  mailingAddress: string;
  contactPhone: string[];
  contactEmail: string;
  bank: IOrganizationBankModel;
  booker: string;
  shortName: string;
  fullName: string;
  legalAddress: string;
  okato: string;
  okogu: string;
  okved: string;
  okpo: string;
  registrationDate: string;
  organizationStatus: string;
  location: IOrganizationLocationModel;

  status: OrganizationStatusEnum;

  pushkinCardProgram: boolean;
  hasMuseum: boolean;
  hasController: boolean;
  hasGuide: boolean;

  settings: IOrganizationSettingsModel;
  docs: IOrganizationDocumentsModel;

  contract: IOrganizationContractModel;
  creation: IOrganizationCreationModel;
}
