export interface IOrganizationContractModel {
  number: string;
  signedAt: Date;
}
