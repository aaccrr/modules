export interface IOrganizationDocumentModel {
  src: string;
  uploadedAt: string;
}
