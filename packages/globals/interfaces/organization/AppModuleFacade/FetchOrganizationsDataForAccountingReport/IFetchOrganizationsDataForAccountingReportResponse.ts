import { IReportingOrganizationDTO } from '../../DTO';

export type IFetchOrganizationsDataForAccountingReportResponse = IReportingOrganizationDTO[];
