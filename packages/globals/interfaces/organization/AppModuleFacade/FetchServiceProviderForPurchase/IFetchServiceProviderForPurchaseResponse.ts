export interface IFetchServiceProviderForPurchaseResponse {
  id: string;
  address: string;
  name: string;
  shortName: string;
  inn: string;
}
