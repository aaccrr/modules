export interface IFetchOrganizationNameQuery {
  id: string;
  onlyPreview: boolean;
}
