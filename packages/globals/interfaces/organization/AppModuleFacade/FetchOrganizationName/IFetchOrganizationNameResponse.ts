export interface IFetchOrganizationNameResponse {
  id: string;
  name: string;
  fullName: string;
  shortName: string;
}
