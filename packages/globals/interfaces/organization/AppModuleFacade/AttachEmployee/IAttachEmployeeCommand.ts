import { UserRole } from '../../../shared';

export interface IAttachEmployeeCommand {
  id: string;
  organizationId: string;
  role: UserRole;
}
