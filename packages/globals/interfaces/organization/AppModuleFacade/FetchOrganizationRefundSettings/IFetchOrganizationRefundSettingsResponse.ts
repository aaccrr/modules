export interface IFetchOrganizationRefundSettingsResponse {
  automaticRefundApprovalEnabled: boolean;
}
