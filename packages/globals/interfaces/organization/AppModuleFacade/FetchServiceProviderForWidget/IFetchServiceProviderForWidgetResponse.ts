export interface IFetchServiceProviderForWidgetResponse {
  shortName: string;
  inn: string;
  ogrn: string;
  legalAddress: string;
}
