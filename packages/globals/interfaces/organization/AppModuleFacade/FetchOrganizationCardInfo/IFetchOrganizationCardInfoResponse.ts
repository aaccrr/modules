import { IOrganizationBankModel, IOrganizationDirectorModel } from '../../Models';

export interface IFetchOrganizationCardInfoResponse {
  name: string;
  fullName: string;
  address: string;
  legalAddress: string;
  mailingAddress: string;
  contactPhone: string[];
  ogrn: string;
  inn: string;
  kpp: string;
  director: IOrganizationDirectorModel;
  booker: string;
  registrationDate: string;
  bank: IOrganizationBankModel;
  okved: string;
  okpo: string;
  okato: string;
  okogu: string;
}
