import { IOrganizationDocumentsModel } from '../../Models/IOrganizationDocumentsModel';

export interface IFetchOrganizationDocumentsResponse {
  id: string;
  docs: IOrganizationDocumentsModel;
}
