export interface IFetchOrganizationSummaryResponse {
  isExists: boolean;
  summary: {
    id: string;
    isConfirmed: boolean;
    isOfflineSalesEnabled: boolean;
    hasMuseum: boolean;
    hasController: boolean;
    hasGuide: boolean;
    automaticRefundApprovalEnabled: boolean;
    pushkinCardProgram: boolean;
  };
}
