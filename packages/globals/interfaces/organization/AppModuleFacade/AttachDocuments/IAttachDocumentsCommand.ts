import { IOrganizationDocumentsModel } from '../../Models/IOrganizationDocumentsModel';

export interface IAttachDocumentsCommand {
  id: string;
  docs: IOrganizationDocumentsModel;
}
