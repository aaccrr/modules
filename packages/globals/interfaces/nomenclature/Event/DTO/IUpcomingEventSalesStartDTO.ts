export interface IUpcomingEventSalesStartDTO {
  eventId: string;
  salesStartDate: string;
}
