import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../../shared';

export interface IEventUploadedFilesDTO {
  images: ICroppedGalleryImageModel[];
  previewImage: ICroppedPreviewImageModel;
}
