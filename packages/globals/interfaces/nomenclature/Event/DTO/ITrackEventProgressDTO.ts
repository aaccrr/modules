export interface ITrackEventProgressDTO {
  id: string;
  startDate: string;
  finishDate: string;
}
