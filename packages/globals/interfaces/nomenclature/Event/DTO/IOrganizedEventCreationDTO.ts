import { EventLongPeriodView } from '..';
import { AgeCensorEnum, IContactModel, Nullable } from '../../../shared';
import { IOrganizedEventClientCategoryModel } from '../Models';
import { IOrganizedEventDayCreationDTO } from './IOrganizedEventDayCreationDTO';
import { IOrganizedEventUnionTicketCreationDTO } from './IOrganizedEventUnionTicketCreationDTO';

export interface IOrganizedEventCreationDTO {
  museums: string[];
  title: string;
  description: string;
  category: number;
  countByClient: number;
  ageCensor: AgeCensorEnum;
  days: IOrganizedEventDayCreationDTO[];
  unionTicket: IOrganizedEventUnionTicketCreationDTO;
  clientCategories: IOrganizedEventClientCategoryModel[];
  contacts: IContactModel[];
  proCultureId: Nullable<string>;
  longPeriod: EventLongPeriodView;
}
