export interface IEventPositionSalesLimitDTO {
  id: string;
  online: number | null;
  offline: number | null;
}
