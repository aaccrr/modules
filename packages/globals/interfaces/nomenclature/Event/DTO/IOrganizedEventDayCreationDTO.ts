import { IDetectableLocationDTO } from '../../../shared';
import { IEventDurationModel, IEventSalesLimitModel, IEventTimelinePointModel } from '../Models';

export interface IOrganizedEventDayCreationDTO {
  date: string;
  duration: IEventDurationModel;
  address: IDetectableLocationDTO;
  salesStartsAt: string;
  tickets: IEventSalesLimitModel;
  timeline: IEventTimelinePointModel[];
  isFree: boolean;
}
