import { IPurchasePropsModel } from '../../../ordering';
import { IClientCategoryView } from '../../../shared';
import { NomenclatureTypeEnum } from '../../Enums';
import { IMuseumSellingEventModel } from '../Models';

export interface IEventForReportDTO {
  id: string;
  type: NomenclatureTypeEnum;
  organizationId: string;
  title: string;
  clientCategories: Partial<IClientCategoryView>[];
  latestExpirationDateOfPurchase: Date | null;
  deletedAt: Date | null;
  museums: IMuseumSellingEventModel[];
  props: Partial<IPurchasePropsModel>;
}
