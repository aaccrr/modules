import { IEventSalesLimitModel } from '../Models';

export interface IOrganizedEventUnionTicketCreationDTO {
  dates: string[];
  salesStartsAt: string;
  tickets: IEventSalesLimitModel;
  isFree: boolean;
}
