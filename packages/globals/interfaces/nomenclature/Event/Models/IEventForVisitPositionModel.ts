import { IClientCategoryModel } from '../../../shared';
import { IEventPositionDateModel } from './IEventPositionDateModel';
import { EventPositionTypeEnum } from '../Enums';

export interface IEventForVisitPositionModel {
  id: number;
  nomenclatureId: string;
  visitConditionId: string;
  salesLimit: number | null;
  isFree: boolean;
  clientCategories: IClientCategoryModel[];
  salesStartDate: Date;
  dates: IEventPositionDateModel[];
  finishDate: Date;
  minPrice: number;
  maxPrice: number;
  type: EventPositionTypeEnum;
  heldAtNight: boolean;
  heldAtNightUntil: string | null;
}
