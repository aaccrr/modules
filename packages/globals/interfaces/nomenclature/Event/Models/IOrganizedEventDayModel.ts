import { IEventDurationModel } from './IEventDurationModel';
import { ILocationModel } from '../../../shared';
import { IEventTimelinePointModel } from './IEventTimelinePointModel';
import { IEventSalesLimitModel } from './IEventSalesLimitModel';

export interface IOrganizedEventDayModel {
  date: string;
  duration: IEventDurationModel;
  address: ILocationModel;
  salesStartsAt: string;
  timeline: IEventTimelinePointModel[];
  tickets: IEventSalesLimitModel;
  isFree: boolean;
  placeId: string;
}
