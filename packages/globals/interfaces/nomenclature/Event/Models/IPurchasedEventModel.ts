import { IEventCategoryModel } from './IEventCategoryModel';
import { NomenclatureTypeEnum } from '../../Enums';
import { IEventTimelinePointModel } from './IEventTimelinePointModel';
import { ILocationModel } from '../../../shared';

export interface IPurchasedEventModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
  slug: string;
  category: IEventCategoryModel;
  schedule: IPurchasedEventSchedulePointModel[];
}

export interface IPurchasedEventSchedulePointModel {
  id: number;
  startsAt: Date;
  finishAt: Date;
  timeline: IEventTimelinePointModel[];
  location: ILocationModel;
}
