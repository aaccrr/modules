export interface IMuseumSellingEventModel {
  id: string;
  name: string;
}
