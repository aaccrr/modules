import { IGeoPoint } from '../../../shared';

export interface IMuseumAtSameAddressModel {
  id: string;
  slug: string;
  name: string;
  location: IGeoPoint;
}
