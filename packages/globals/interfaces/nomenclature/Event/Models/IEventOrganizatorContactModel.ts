export interface IEventOrganizatorContactModel {
  name: string;
  phone: string;
  email: string;
  isPublic: boolean;
}
