export interface IEventDurationModel {
  from: string;
  to: string;
  atNight: boolean;
}
