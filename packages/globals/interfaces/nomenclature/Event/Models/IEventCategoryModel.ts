export interface IEventCategoryModel {
  id: number | null;
  name: string;
}
