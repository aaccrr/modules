import { ILocationModel } from '../../../shared';
import { IMuseumAtSameAddressModel } from './IMuseumAtSameAddressModel';
import { IEventTimelinePointModel } from './IEventTimelinePointModel';

export interface IEventPositionDateModel {
  id: number;
  startsAt: Date;
  finishAt: Date;
  museum: IMuseumAtSameAddressModel;
  timeline: IEventTimelinePointModel[];
  location: ILocationModel;
}
