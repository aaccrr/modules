import { IEventSalesLimitModel } from './IEventSalesLimitModel';
import { EventPositionTypeEnum } from '../Enums';
import { IEventPositionDateModel } from './IEventPositionDateModel';
import { IClientCategoryModel } from '../../../shared';

export interface IEventPositionModel {
  id: number;
  nomenclatureId: string;
  visitConditionId: string;
  type: EventPositionTypeEnum;
  dates: IEventPositionDateModel[];
  clientCategories: IClientCategoryModel[];
  salesStartDate: Date;
  finishDate: Date;
  salesLimit: IEventSalesLimitModel;
  minPrice: number;
  maxPrice: number;
  isFree: boolean;
  heldAtNight: boolean;
  heldAtNightUntil: string | null;
}
