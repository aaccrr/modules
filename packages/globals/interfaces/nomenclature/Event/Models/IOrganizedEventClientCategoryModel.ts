export interface IOrganizedEventClientCategoryModel {
  id: number;
  name: string;
  prices: IOrganizedEventClientCategoryPriceModel[];
  groups: string[];
}

export interface IOrganizedEventClientCategoryPriceModel {
  date: string | string[];
  price: {
    online: number;
    offline: number;
  };
}
