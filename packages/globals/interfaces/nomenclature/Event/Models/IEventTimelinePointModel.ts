export interface IEventTimelinePointModel {
  from: string;
  to: string;
  description: string;
}
