import { IEventSalesLimitModel } from './IEventSalesLimitModel';

export interface IOrganizedEventUnionTicketModel {
  dates: string[];
  salesStartsAt: string;
  tickets: IEventSalesLimitModel;
  isFree: boolean;
}
