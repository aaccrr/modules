export interface IEventSalesLimitModel {
  online: number | null;
  offline: number | null;
}
