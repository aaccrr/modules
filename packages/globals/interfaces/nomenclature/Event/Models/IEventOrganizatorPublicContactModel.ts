export interface IEventOrganizatorPublicContactModel {
  name: string;
  phone: string;
  email: string;
}
