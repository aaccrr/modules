import { AgeCensorEnum, IOrderedImageModel, IPublicContactModel } from '../../../shared';
import { IEventCategoryModel } from './IEventCategoryModel';
import { EventForVisitStatusEnum } from '../Enums';
import { IEventForVisitPositionModel } from './IEventForVisitPositionModel';

export interface IEventForVisitModel {
  id: string;
  slug: string;
  title: string;
  description: string;
  serviceProviderId: string;
  status: EventForVisitStatusEnum;
  category: IEventCategoryModel;
  ageCensor: AgeCensorEnum | null;
  contacts: IPublicContactModel[];
  countByClient: number;
  maxPrice: number;
  minPrice: number;
  finishDate: Date;
  museums: string[];
  positions: IEventForVisitPositionModel[];
  images: IOrderedImageModel[];
  previewImage: string;
  isSalesOpened: boolean;
  implementsCovidRestriction: boolean;
  pushkinCardProgram: boolean;

  dates: string[];
  localities: string[];

  salesCount: number;
  views: number;
  likes: number;
}
