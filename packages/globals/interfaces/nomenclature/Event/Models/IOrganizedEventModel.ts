import { IEventPositionModel } from './IEventPositionModel';
import {
  LastChangeEnum,
  ICreationLogModel,
  IUpdateLogModel,
  IDeletionLogModel,
  AgeCensorEnum,
  ICroppedGalleryImageModel,
  ICroppedPreviewImageModel,
  ITimezone,
  Nullable,
} from '../../../shared';
import { IOrganizedEventClientCategoryModel } from './IOrganizedEventClientCategoryModel';
import { IEventOrganizatorContactModel } from './IEventOrganizatorContactModel';
import { OrganizedEventStatusEnum } from '../Enums';
import { IEventCategoryModel } from './IEventCategoryModel';
import { IMuseumSellingEventModel } from './IMuseumSellingEventModel';
import { IOrganizedEventDayModel } from './IOrganizedEventDayModel';
import { IOrganizedEventUnionTicketModel } from './IOrganizedEventUnionTicketModel';
import { EventLongPeriodView } from '..';

export interface IOrganizedEventModel {
  id: string;
  title: string;
  slug: string;
  description: string;
  museums: IMuseumSellingEventModel[];
  category: IEventCategoryModel;
  organizationId: string;
  organizationName: string;

  previewImage: ICroppedPreviewImageModel;
  images: ICroppedGalleryImageModel[];

  ageCensor: AgeCensorEnum | null;

  positions: IEventPositionModel[];

  sellerTimezone: ITimezone;

  clientCategories: IOrganizedEventClientCategoryModel[];
  days: IOrganizedEventDayModel[];
  longPeriod: EventLongPeriodView;
  unionTicket: IOrganizedEventUnionTicketModel;
  contacts: IEventOrganizatorContactModel[];

  countByClient: number;
  isFree: boolean;
  status: OrganizedEventStatusEnum;
  canEdit: boolean;
  latestExpirationDateOfPurchase: Date | null;
  isAddedToCatalog: boolean;
  proCultureId: Nullable<number>;
  places?: any[];

  lastChange: LastChangeEnum;
  creation: ICreationLogModel;
  update: IUpdateLogModel;
  deletion: IDeletionLogModel;
}
