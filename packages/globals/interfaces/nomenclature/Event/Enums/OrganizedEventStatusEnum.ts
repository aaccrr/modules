export enum OrganizedEventStatusEnum {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  DELETED = 'DELETED',
  CANCELED = 'CANCELED',
  FINISHED = 'FINISHED',
}
