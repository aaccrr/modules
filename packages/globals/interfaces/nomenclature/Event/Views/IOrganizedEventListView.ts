import { OrganizedEventStatusEnum } from '../Enums';

export interface IOrganizedEventListView {
  id: string;
  title: string;
  status: OrganizedEventStatusEnum;
  primaryImage: string;
  canEdit: boolean;
}
