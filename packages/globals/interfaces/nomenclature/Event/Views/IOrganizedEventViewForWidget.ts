import { AgeCensorEnum } from '../../..';
import { IOrganizedEventSchedulePeriodViewForWidget } from './IOrganizedEventSchedulePeriodViewForWidget';

export interface IOrganizedEventViewForWidget {
  id: string;
  slug: string;
  title: string;
  category: string;
  ageCensor: AgeCensorEnum;
  minPrice: number;
  imagesGallery: string[];

  description: string;
  schedule: IOrganizedEventSchedulePeriodViewForWidget[];
}
