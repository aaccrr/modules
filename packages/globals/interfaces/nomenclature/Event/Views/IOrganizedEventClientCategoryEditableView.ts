export interface IOrganizedEventClientGroupEditableView {
  name: string;
  isDefault: boolean;
  isRequired: boolean;
  isActive: boolean;
}

export interface IOrganizedEventClientCategoryEditableView {
  name: string;
  isRequired: boolean;
  isDefault: boolean;
  isActive: boolean;
  groups: IOrganizedEventClientGroupEditableView[];
}
