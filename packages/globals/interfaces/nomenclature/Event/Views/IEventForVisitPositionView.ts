import { IEventForVisitDayView } from './IEventForVisitDayView';
import { IEventForVisitUnionTicketView } from './IEventForVisitUnionTicketView';

export type IEventForVisitPositionView = IEventForVisitDayView | IEventForVisitUnionTicketView;
