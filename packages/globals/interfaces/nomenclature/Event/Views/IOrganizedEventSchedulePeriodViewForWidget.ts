import { IEventTimelinePointModel } from '../Models';
import { IOrganizedEventSchedulePeriodLocationViewForWidget } from './IOrganizedEventSchedulePeriodLocationViewForWidget';

export interface IOrganizedEventSchedulePeriodViewForWidget {
  from: Date;
  to: Date;
  location: IOrganizedEventSchedulePeriodLocationViewForWidget;
  timeline: IEventTimelinePointModel[];
}
