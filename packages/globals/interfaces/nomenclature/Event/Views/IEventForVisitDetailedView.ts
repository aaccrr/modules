import { IOrderedImageModel, AgeCensorEnum, IPublicContactModel } from '../../../shared';
import { IEventForVisitPositionView } from './IEventForVisitPositionView';
import { IEventCategoryModel } from '../Models';
import { EventForVisitStatusEnum } from '../Enums';

export interface IEventForVisitDetailedView {
  id: string;
  slug: string;
  title: string;
  countByClient: number;
  images: IOrderedImageModel[];
  description: string;
  positions: IEventForVisitPositionView;
  category: IEventCategoryModel;
  ageCensor: AgeCensorEnum | null;
  contacts: IPublicContactModel[];
  isLiked: boolean;
  isSalesOpened: boolean;
  salesStartDate: string;
  status: EventForVisitStatusEnum;
}
