import { IEventDurationModel, IEventSalesLimitModel, IEventTimelinePointModel } from '../Models';
import { ILocationModel } from '../../../shared';

export interface IOrganizedEventDayView {
  date: string;
  duration: IEventDurationModel;
  address: ILocationModel;
  salesStartsAt: string;
  tickets: IEventSalesLimitModel;
  timeline: IEventTimelinePointModel[];
  isFree: boolean;
}
