import { IGeoPoint } from '../../../shared';

export interface IEventForVisitCatalogView {
  slug: string;
  previewImage: string;
  title: string;
  localities: string[];
  dates: string[]; // YYYY-MM-DD
  minPrice: number | null;
  maxPrice: number | null;
  likes: number;
  isLiked: boolean;
  locations?: IGeoPoint[];
}
