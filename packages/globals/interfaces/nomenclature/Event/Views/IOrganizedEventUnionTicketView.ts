import { IEventSalesLimitModel } from '../Models';

export interface IOrganizedEventUnionTicketView {
  dates: string[];
  salesStartsAt: string;
  tickets: IEventSalesLimitModel;
  isFree: boolean;
}
