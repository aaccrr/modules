export interface IOrganizedEventSchedulePeriodLocationViewForWidget {
  locality: string;
  address: string;
}
