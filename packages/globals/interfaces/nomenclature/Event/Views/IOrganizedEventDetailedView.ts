import { IMuseumSellingEventModel, IEventCategoryModel, IOrganizedEventClientCategoryModel } from '../Models';
import { AgeCensorEnum, IOrderedImageModel, IContactModel } from '../../../shared';
import { IOrganizedEventDayView } from './IOrganizedEventDayView';

export interface IOrganizedEventDetailedView {
  id: string;
  title: string;
  museums: IMuseumSellingEventModel[];
  description: string;
  category: IEventCategoryModel;
  ageCensor: AgeCensorEnum;
  days: IOrganizedEventDayView[];
  clientCategories: IOrganizedEventClientCategoryModel[];
  images: IOrderedImageModel[];
  contacts: IContactModel[];
}
