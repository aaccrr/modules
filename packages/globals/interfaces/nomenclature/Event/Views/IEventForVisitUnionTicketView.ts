import { EventPositionTypeEnum } from '../Enums';
import { IClientCategoryView } from '../../../shared';

export interface IEventForVisitUnionTicketView {
  id: number;
  type: EventPositionTypeEnum;
  dates: string[];
  clientCategories: IClientCategoryView[];
  minPrice: number;
  tickets: number | null;
  salesStartsAt: string; // YYYY-MM-DD
  isSalesOpened: boolean;
  heldAtNight: boolean;
  heldAtNightUntil: string | null;
}
