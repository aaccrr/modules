import { IEventCategoryModel, IEventPositionDateModel } from '../Models';
import { IClientCategoryView, IPublicContactModel } from '../../../shared';

interface VisitorsCategoryId {
  legacyId: number;
  id: string;
}

export interface IPurchasedEventView {
  id: string;
  title: string;
  slug: string;
  category: IEventCategoryModel;
  serviceProviderId: string;
  serviceProviderContacts: IPublicContactModel[];
  countByClient: number;
  pushkinCardProgram: boolean;
  clientCategories: IClientCategoryView[];
  dates: IEventPositionDateModel[];
  isSalesOpened: boolean;
  cateringMuseums: string[];
  implementsCovidRestriction: boolean;
  ageCensor: number;
  nomenclatureId: string;
  visitConditionId: string;
  visitorsCategoriesIds: VisitorsCategoryId[];
  cinema: boolean;
}
