import { EventPositionTypeEnum } from '../Enums';
import { ILocationModel, IClientCategoryView } from '../../../shared';
import { IEventTimelinePointModel } from '../Models';
import { IMuseumAtEventAddressModel } from '../../../museum';

export interface IEventForVisitDayView {
  id: number;
  type: EventPositionTypeEnum;
  date: string;
  clientCategories: IClientCategoryView[];
  location: ILocationModel;
  timeline: IEventTimelinePointModel[];
  minPrice: number;
  museum: IMuseumAtEventAddressModel | null;
  tickets: number | null;
  salesStartsAt: string; // YYYY-MM-DD
  isSalesOpened: boolean;
  heldAtNight: boolean;
  heldAtNightUntil: string | null;
  duration: {
    startsAt: string;
    finishAt: string;
  };
}
