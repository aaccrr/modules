import { IMuseumSellingEventModel } from '../Models';
import { IEventCategoryEditableView } from './IEventCategoryEditableView';
import { IPublicContactModel, IOrderedImageModel, AgeCensorEnum } from '../../../shared';
import { IOrganizedEventDayView } from './IOrganizedEventDayView';
import { IOrganizedEventClientCategoryEditableView } from './IOrganizedEventClientCategoryEditableView';
import { IOrganizedEventUnionTicketView } from './IOrganizedEventUnionTicketView';
import { EventLongPeriodView } from '..';

export interface IOrganizedEventEditableView {
  id: string;
  title: string;
  description: string;
  museums: IMuseumSellingEventModel[];
  category: IEventCategoryEditableView[];
  ageCensor: AgeCensorEnum;
  contacts: IPublicContactModel[];
  days: IOrganizedEventDayView[];
  clientCategories: IOrganizedEventClientCategoryEditableView[];
  images: IOrderedImageModel[];
  previewImage: string;
  unionTicket: IOrganizedEventUnionTicketView;
  countByClient: number;
  proCultureId: number;
  longPeriod: EventLongPeriodView;
}
