export interface IEventCategoryEditableView {
  id: number | null;
  name: string;
  isActive: boolean;
}
