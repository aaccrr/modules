import { ILocationModel } from '../../..';

export interface EventLongPeriodView {
  start: string;
  finish: string;
  saleStart: string;
  address: ILocationModel;
  placeId: string;
}
