import { IEntranceTicketForReportDTO } from '../EntranceTicket';
import { ISeasonTicketForReportDTO } from '../SeasonTicket';
import { IEventForReportDTO } from '../Event';

export type INomenclatureForReportDTO = IEntranceTicketForReportDTO & ISeasonTicketForReportDTO & IEventForReportDTO;
