export interface ICalculateThresholdPricesInMuseumResponse {
  minPrice: number | null;
  maxPrice: number | null;
}
