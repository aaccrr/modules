import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';

export interface IAttachImagesToEventCommand {
  id: string;
  organizationId: string;
  images?: ICroppedGalleryImageModel[];
  previewImage?: ICroppedPreviewImageModel;
}
