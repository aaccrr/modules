import { IClientCategoryView } from '../../..';
import { IEntranceTicketMuseumModel } from '../../../museum';
import { ISeasonTicketPatternModel } from '../../SeasonTicket';

interface VisitorsCategoryId {
  legacyId: number;
  id: string;
}

export interface IFetchAbonementForPurchaseResponse {
  id: string;
  organizationId: string;
  organizationName: string;
  title: string;
  museums: IEntranceTicketMuseumModel[];
  clientCategories: IClientCategoryView[];
  patterns: ISeasonTicketPatternModel;
  countByClient: number;
  ageCensor: number;
  implementsCovidRestriction: boolean;
  nomenclatureId: string;
  visitConditionId: string;
  visitorsCategoriesIds: VisitorsCategoryId[];
}
