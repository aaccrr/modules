export interface ICalculateNomenclatureQuantityForSaleResponse {
  tickets: number;
  abonements: number;
}
