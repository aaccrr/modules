export interface ICancelTicketCommand {
  id: string;
  date?: string;
  times?: string[];
}
