import { IPublicContactModel, Nullable } from '../../..';
import { EventForVisitStatusEnum, IEventForVisitDayView, IEventForVisitUnionTicketView } from '../../Event';

export interface IFetchPurchasedEventForWidgetResponse {
  id: string;
  slug: string;
  title: string;
  seller: string;
  countByClient: number;
  description: string;
  positions: (IEventForVisitDayView | IEventForVisitUnionTicketView)[];
  category: string;
  ageCensor: number;
  contacts: IPublicContactModel[];
  isSalesOpened: boolean;
  salesStartDate: string;
  status: EventForVisitStatusEnum;
  tickets: Nullable<number>;
}
