export interface IFetchTicketForPurchaseQuery {
  id: string;
  date?: string;
  time?: string;
}
