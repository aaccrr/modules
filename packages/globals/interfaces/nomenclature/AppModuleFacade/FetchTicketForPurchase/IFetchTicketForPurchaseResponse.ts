import { IClientCategoryView } from '../../..';
import { IEntranceTicketMuseumModel } from '../../../museum';
import { ITimezone } from '../../../shared';
import { IEntranceTicketSalesScheduleModel } from '../../EntranceTicket';

interface VisitorsCategoryId {
  legacyId: number;
  id: string;
}

export interface IFetchTicketForPurchaseResponse {
  id: string;
  title: string;
  organizationId: string;
  organizationName: string;
  museum: IEntranceTicketMuseumModel;
  isPersonal: boolean;
  clientCategories: IClientCategoryView[];
  salesSchedule: IEntranceTicketSalesScheduleModel;
  countByClient: number;
  expiresAt: number;
  timezone: ITimezone;
  canBeTransferred: boolean;
  includedInPrice: string[];
  passLimitNumber: number;
  ageCensor: number;
  nomenclatureId: string;
  visitConditionId: string;
  visitorsCategoriesIds: VisitorsCategoryId[];
}
