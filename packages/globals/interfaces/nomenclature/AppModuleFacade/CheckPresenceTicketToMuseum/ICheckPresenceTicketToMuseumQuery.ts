export interface ICheckPresenceTicketToMuseumQuery {
  museumId: string;
  ticketId: string;
}
