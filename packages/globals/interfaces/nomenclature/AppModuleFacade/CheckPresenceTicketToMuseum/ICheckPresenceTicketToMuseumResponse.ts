export interface ICheckPresenceTicketToMuseumResponse {
  isPresented: boolean;
}
