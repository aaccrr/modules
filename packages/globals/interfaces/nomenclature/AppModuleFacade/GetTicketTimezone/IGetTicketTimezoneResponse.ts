import { ITimezone } from '../../../shared';

export interface IGetTicketTimezoneResponse {
  timezone: ITimezone;
}
