import { WithPagination } from '../../../../../interfaces';
import { IOrganizedEventViewForWidget } from '../../Event';

export type IGetListOrganizedEventsForWidgetResponse = WithPagination<IOrganizedEventViewForWidget[]>;
