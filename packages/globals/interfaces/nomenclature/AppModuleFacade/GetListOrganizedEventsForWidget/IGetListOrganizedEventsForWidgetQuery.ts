import { IPagination } from '../../../shared';

export interface IGetListOrganizedEventsForWidgetQuery {
  museumId: string;
  pagination: IPagination;
}
