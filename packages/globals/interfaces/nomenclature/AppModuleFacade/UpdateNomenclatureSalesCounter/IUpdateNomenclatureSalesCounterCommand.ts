export interface IUpdateNomenclatureSalesCounterCommand {
  nomenclatureId: string;
  salesCount: number;
}
