export interface IViewNomenclatureModerationQueuePlenumResponse {
  organizationId: string;
  moderationQueuePlenum: {
    tickets: number;
    abonements: number;
    events: number;
  };
}
