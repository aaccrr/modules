export interface IPurgeNomenclatureOfMuseumCommand {
  museumId: string;
}
