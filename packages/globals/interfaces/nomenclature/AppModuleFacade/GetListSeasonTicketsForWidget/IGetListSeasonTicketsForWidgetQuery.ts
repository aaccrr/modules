import { IPagination } from '../../../shared';

export interface IGetListSeasonTicketsForWidgetQuery {
  museumId: string;
  pagination: IPagination;
}
