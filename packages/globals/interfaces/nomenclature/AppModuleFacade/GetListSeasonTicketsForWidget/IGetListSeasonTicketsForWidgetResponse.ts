import { WithPagination } from '../../../../../interfaces';
import { ISeasonTicketViewForWidget } from '../../SeasonTicket';

export type IGetListSeasonTicketsForWidgetResponse = WithPagination<ISeasonTicketViewForWidget[]>;
