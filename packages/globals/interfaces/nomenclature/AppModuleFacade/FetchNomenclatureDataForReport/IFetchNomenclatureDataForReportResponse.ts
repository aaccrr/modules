import { INomenclatureForReportDTO } from '../../DTO';

export type IFetchNomenclatureDataForReportResponse = INomenclatureForReportDTO[];
