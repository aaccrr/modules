import { IDatesPeriodView } from '../../..';
import { NomenclatureTypeEnum } from '../../Enums';

export interface IFetchNomenclatureDataForReportQuery {
  reportingOrganizationId: string;
  reportingMuseums: string[];
  reportingNomenclatureTypes: NomenclatureTypeEnum[];
  reportingPeriod: IDatesPeriodView;
}
