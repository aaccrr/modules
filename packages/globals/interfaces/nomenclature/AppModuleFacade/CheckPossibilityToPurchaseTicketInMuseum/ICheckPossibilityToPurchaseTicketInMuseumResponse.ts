export interface ICheckPossibilityToPurchaseTicketInMuseumResponse {
  hasTickets: boolean;
}
