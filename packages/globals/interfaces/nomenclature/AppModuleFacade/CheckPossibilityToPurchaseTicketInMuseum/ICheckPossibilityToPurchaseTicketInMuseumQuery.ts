export interface ICheckPossibilityToPurchaseTicketInMuseumQuery {
  museumId: string;
}