export interface IFetchEventForPurchaseQuery {
  id: string;
  orderedPositionId: number;
}
