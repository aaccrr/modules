import { IClientCategoryView, IPublicContactModel } from '../../..';
import { IEventCategoryModel, IEventPositionDateModel } from '../../Event';

interface VisitorsCategoryId {
  legacyId: number;
  id: string;
}

export interface IFetchEventForPurchaseResponse {
  id: string;
  title: string;
  slug: string;
  category: IEventCategoryModel;
  serviceProviderId: string;
  serviceProviderContacts: IPublicContactModel[];
  countByClient: number;
  clientCategories: IClientCategoryView[];
  dates: IEventPositionDateModel[];
  isSalesOpened: boolean;
  pushkinCardProgram: boolean;
  cateringMuseums: string[];
  ageCensor: number;
  implementsCovidRestriction: boolean;
  nomenclatureId: string;
  visitConditionId: string;
  visitorsCategoriesIds: VisitorsCategoryId[];
  cinema: boolean;
}
