import { IEntranceTicketModel } from '../../EntranceTicket';

export type IFetchCanceledTicketResponse = IEntranceTicketModel;
