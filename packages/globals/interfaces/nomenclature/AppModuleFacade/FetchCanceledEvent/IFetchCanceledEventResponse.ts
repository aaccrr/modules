import { IOrganizedEventModel } from '../../Event';

export type IFetchCanceledEventResponse = IOrganizedEventModel;
