import { WithPagination } from '../../../../../interfaces';
import { IEntranceTicketViewForWidget } from '../../EntranceTicket';

export type IGetListEntranceTicketsForWidgetResponse = WithPagination<IEntranceTicketViewForWidget[]>;
