import { IPagination } from '../../../shared';

export interface IGetListEntranceTicketsForWidgetQuery {
  museumId: string;
  pagination: IPagination;
}
