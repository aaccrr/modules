import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';

export interface IFetchImagesAttachedToEventResponse {
  id: string;
  images: ICroppedGalleryImageModel[];
  previewImage: ICroppedPreviewImageModel;
}
