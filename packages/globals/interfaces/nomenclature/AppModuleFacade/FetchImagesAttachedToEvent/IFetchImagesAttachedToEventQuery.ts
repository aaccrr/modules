export interface IFetchImagesAttachedToEventQuery {
  id: string;
  organizationId: string;
}
