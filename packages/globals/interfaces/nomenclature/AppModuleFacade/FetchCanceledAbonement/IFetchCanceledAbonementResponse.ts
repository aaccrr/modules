import { ISeasonTicketModel } from '../../SeasonTicket';

export type IFetchCanceledAbonementResponse = ISeasonTicketModel;
