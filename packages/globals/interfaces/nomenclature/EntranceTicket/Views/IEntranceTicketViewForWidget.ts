import { AgeCensorEnum } from '../../..';

export interface IEntranceTicketViewForWidget {
  id: string;
  title: string;
  minPrice: number;
  ageCensor: AgeCensorEnum;
  includedInPrice: string[];
}
