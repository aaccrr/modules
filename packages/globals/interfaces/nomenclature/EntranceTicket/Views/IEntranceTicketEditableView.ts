import { AgeCensorEnum, IClientCategoryEditableView } from '../../../shared';
import { IEntranceTicketMuseumModel } from '../../../museum';
import { IEntranceTicketSalesScheduleModel } from '../Models';

export interface IEntranceTicketEditableView {
  id: string;
  title: string;
  museum: IEntranceTicketMuseumModel;
  isPersonal: boolean;
  canBeTransferred: boolean;
  clientCategories: IClientCategoryEditableView[];
  salesSchedule: IEntranceTicketSalesScheduleModel;
  dayOffSales: boolean;
  countByClient: number;
  expiresAt: number;
  ageCensor: AgeCensorEnum;
}
