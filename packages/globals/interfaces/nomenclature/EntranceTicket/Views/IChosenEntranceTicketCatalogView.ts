import { AgeCensorEnum } from '../../../shared';

export interface IChosenEntranceTicketCatalogView {
  id: string;
  title: string;
  minPrice: number;
  hasSchedule: boolean;
  ageCensor: AgeCensorEnum;
  includedInPrice: string[];
}
