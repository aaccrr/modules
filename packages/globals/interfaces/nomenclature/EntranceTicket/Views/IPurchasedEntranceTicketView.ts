import { IEntranceTicketMuseumModel } from '../../../museum';
import { IClientCategoryView, ITimezone } from '../../../shared';
import { IEntranceTicketSalesScheduleModel } from '../Models';

interface VisitorsCategoryId {
  legacyId: number;
  id: string;
}

export interface IPurchasedEntranceTicketView {
  id: string;
  title: string;
  organizationId: string;
  organizationName: string;
  museum: IEntranceTicketMuseumModel;
  isPersonal: boolean;
  clientCategories: IClientCategoryView[];
  salesSchedule: IEntranceTicketSalesScheduleModel;
  countByClient: number;
  expiresAt: number;
  timezone: ITimezone;
  includedInPrice: string[];
  passLimitNumber: number;
  ageCensor: number;
  nomenclatureId: string;
  visitConditionId: string;
  visitorsCategoriesIds: VisitorsCategoryId[];
}
