import { IEntranceTicketMuseumModel } from '../../../museum';
import { IClientCategoryModel } from '../../../shared';

export interface IChosenEntranceTicketDetailedView {
  id: string;
  title: string;
  museum: IEntranceTicketMuseumModel;
  isPersonal: boolean;
  canBeTransferred: boolean;
  clientCategories: IClientCategoryModel[];
  countByClient: number;
  expiresAt: number;
}
