import { IEntranceTicketMuseumModel } from '../../../museum';
import { EntranceTicketStatusEnum } from '../Enums';

export interface IEntranceTicketListView {
  id: string;
  museum: IEntranceTicketMuseumModel;
  title: string;
  minPrice: number;
  status: EntranceTicketStatusEnum;
  canEdit: boolean;
}
