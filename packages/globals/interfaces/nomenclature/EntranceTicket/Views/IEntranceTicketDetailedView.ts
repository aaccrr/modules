import { IEntranceTicketMuseumModel } from '../../../museum';
import { IClientCategoryModel } from '../../../shared';
import { IEntranceTicketSalesScheduleModel } from '../Models';

export interface IEntranceTicketDetailedView {
  id: string;
  title: string;
  museum: IEntranceTicketMuseumModel;
  isPersonal: boolean;
  canBeTransferred: boolean;
  clientCategories: IClientCategoryModel[];
  salesSchedule: IEntranceTicketSalesScheduleModel;
  dayOffSales: boolean;
  countByClient: number;
  expiresAt: number;
  includedInPrice: string[];
  passLimitNumber: number;
}
