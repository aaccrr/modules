import { IClientCategoryModel, AgeCensorEnum } from '../../../shared';
import { IEntranceTicketMuseumModel } from '../../../museum';

export interface IChosenEntranceTicketModel {
  id: string;
  title: string;
  canBeTransferred: boolean;
  clientCategories: IClientCategoryModel[];
  countByClient: number;
  expiresAt: number;
  isPersonal: boolean;
  maxPrice: number;
  minPrice: number;
  museum: IEntranceTicketMuseumModel;
  salesCount: number;
  hasSchedule: boolean;
  isSalesOpened: boolean;
  ageCensor: AgeCensorEnum;
}
