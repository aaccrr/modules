import { NomenclatureTypeEnum } from '../../Enums';

export interface IPurchasedEntranceTicketModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
  includedInPrice: string[];
}
