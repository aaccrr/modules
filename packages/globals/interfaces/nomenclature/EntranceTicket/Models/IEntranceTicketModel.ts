import {
  IClientCategoryModel,
  LastChangeEnum,
  ICreationLogModel,
  IUpdateLogModel,
  IDeletionLogModel,
  ITimezone,
  AgeCensorEnum,
} from '../../../shared';
import { EntranceTicketStatusEnum } from '../Enums';
import { IEntranceTicketSalesScheduleModel } from './IEntranceTicketSalesScheduleModel';
import { IEntranceTicketMuseumModel } from '../../../museum';

export interface IEntranceTicketModel {
  id: string;
  title: string;
  organizationId: string;
  organizationName: string;
  museum: IEntranceTicketMuseumModel;
  timezone: ITimezone;
  status: EntranceTicketStatusEnum;
  salesSchedule: IEntranceTicketSalesScheduleModel;
  countByClient: number;
  expiresAt: number;
  isPersonal: boolean;
  clientCategories: IClientCategoryModel[];
  minPrice: number;
  maxPrice: number;
  dayOffSales: boolean;
  canBeTransferred: boolean;
  ageCensor: AgeCensorEnum;

  canEdit: boolean;
  lastChange: LastChangeEnum;
  isAddedToCatalog: boolean;
  latestExpirationDateOfPurchase: Date | null;
  includedInPrice: string[];
  passLimitNumber: number;

  creation: ICreationLogModel;
  update: IUpdateLogModel;
  deletion: IDeletionLogModel;
}
