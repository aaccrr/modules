export interface IEntranceTicketSalesScheduleModel {
  weekly: IEntranceTicketSalesWeeklyModel[];
  free: IEntranceTicketSalesFreeModel[];
  toTime: IEntranceTicketSalesToTimeModel[];
}

export interface IEntranceTicketSalesWeeklyModel {
  weekDayOrder: number;
  online: number | null;
  offline: number | null;
}

export interface IEntranceTicketSalesFreeModel {
  date: string;
  online: number | null;
  offline: number | null;
}

export interface IEntranceTicketSalesToTimeModel {
  date: string;
  intervals: IEntranceTicketToTimeIntervalModel[];
}
export interface IEntranceTicketToTimeIntervalModel {
  _id?: string;
  from: string;
  to: string;
  online: number | null;
  offline: number | null;
}
