export enum EntranceTicketStatusEnum {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  SUSPENDED = 'SUSPENDED',
  DELETED = 'DELETED',
}
