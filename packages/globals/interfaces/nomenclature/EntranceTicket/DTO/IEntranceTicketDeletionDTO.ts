import { IEntranceTicketModel } from '../Models';

export interface IEntranceTicketDeletionDTO {
  isLastActiveTicket: boolean;
  ticket: IEntranceTicketModel;
}
