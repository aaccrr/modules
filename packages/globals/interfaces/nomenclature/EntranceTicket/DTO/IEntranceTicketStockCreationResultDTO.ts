export interface IEntranceTicketStockCreationResultDTO {
  hasSchedule: boolean;
  salesOpenedUntil: Date | null;
}
