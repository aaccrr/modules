import { IClientCategoryDTO, AgeCensorEnum } from '../../../shared';
import { IEntranceTicketSalesScheduleModel } from '../Models';

export interface IEntranceTicketCreationDTO {
  museum: string;
  title: string;
  clientCategories: IClientCategoryDTO[];
  ageCensor: AgeCensorEnum;
  includedInPrice: string[];

  expiresAt?: number;
  countByClient?: number;
  dayOffSales?: boolean;
  isPersonal?: boolean;
  canBeTransferred?: boolean;
  salesSchedule?: Partial<IEntranceTicketSalesScheduleModel>;
  passLimitNumber?: number;
}
