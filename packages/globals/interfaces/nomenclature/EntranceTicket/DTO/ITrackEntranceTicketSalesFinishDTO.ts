export interface ITrackEntranceTicketSalesFinishDTO {
  id: string;
  salesOpenedUntil: Date;
}
