import { ITimezone } from '../../../shared';
import { IEntranceTicketSalesScheduleModel } from '../Models';

export interface IEntranceTicketStockDTO {
  id: string;
  salesSchedule: IEntranceTicketSalesScheduleModel;
  dayOffSales: boolean;
  museumId: string;
  timezone: ITimezone;
}
