export interface ICanceledEntranceTicketIntervalsDTO {
  id: string;
  date: string;
  times: string[];
}
