import { IClientCategoryView } from '../../../shared';
import { NomenclatureTypeEnum } from '../../Enums';

export interface IEntranceTicketForReportDTO {
  id: string;
  type: NomenclatureTypeEnum;
  organizationId: string;
  museum: string;
  title: string;
  clientCategories: Partial<IClientCategoryView>[];
  latestExpirationDateOfPurchase: Date | null;
  deletedAt: Date | null;
}
