import { ISeasonTicketPatternDTO } from './ISeasonTicketPatternDTO';
import { IClientCategoryDTO, AgeCensorEnum } from '../../../shared';

export interface ISeasonTicketCreationDTO {
  museums: string[];
  title: string;
  patterns: ISeasonTicketPatternDTO;
  clientCategories: IClientCategoryDTO[];
  ageCensor: AgeCensorEnum;

  maxSales: number | null;
  salesEndDate: string;
  countByClient: number;
}
