import { IClientCategoryView } from '../../../shared';
import { NomenclatureTypeEnum } from '../../Enums';

export interface ISeasonTicketForReportDTO {
  id: string;
  type: NomenclatureTypeEnum;
  organizationId: string;
  museums: string[];
  title: string;
  clientCategories: Partial<IClientCategoryView>[];
  latestExpirationDateOfPurchase: Date | null;
  deletedAt: Date | null;
}
