import { ISalesLimitModel } from '../../../shared';

export interface IUpdateSeasonTicketStockDTO {
  id: string;
  limits: ISalesLimitModel;
}
