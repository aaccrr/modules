export interface ISeasonTicketEventDTO {
  name: string;
  date: string;
  time: string;
}
