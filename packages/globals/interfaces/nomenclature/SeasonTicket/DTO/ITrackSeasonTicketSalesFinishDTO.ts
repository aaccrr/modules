export interface ITrackSeasonTicketSalesFinishDTO {
  id: string;
  salesFinishDate: Date;
}
