import { ISeasonTicketEventDTO } from './ISeasonTicketEventDTO';

export interface ISeasonTicketPatternDTO {
  visits?: number;
  period?: number;
  events?: ISeasonTicketEventDTO[];
}
