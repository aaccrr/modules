export enum SeasonTicketPatternType {
  PERIOD = 'PERIOD',
  VISITS = 'VISITS',
  UNIVERSAL = 'UNIVERSAL',
  EVENTS = 'EVENTS',
}
