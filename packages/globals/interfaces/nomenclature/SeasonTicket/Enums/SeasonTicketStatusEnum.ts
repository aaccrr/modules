export enum SeasonTicketStatusEnum {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  CANCELED = 'CANCELED',
  DELETED = 'DELETED',
}
