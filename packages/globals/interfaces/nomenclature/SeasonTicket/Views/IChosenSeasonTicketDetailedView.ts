import { IClientCategoryModel } from '../../../shared';
import { ISeasonTicketPatternModel } from '../Models';

export interface IChosenSeasonTicketDetailedView {
  id: string;
  title: string;
  countByClient: number;
  clientCategories: IClientCategoryModel[];
  patterns: ISeasonTicketPatternModel;
}
