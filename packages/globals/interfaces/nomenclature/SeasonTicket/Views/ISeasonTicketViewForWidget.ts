import { AgeCensorEnum } from '../../..';
import { ISeasonTicketPatternModel } from '../Models';

export interface ISeasonTicketViewForWidget {
  id: string;
  title: string;
  minPrice: number;
  ageCensor: AgeCensorEnum;
  patterns: ISeasonTicketPatternModel;
}
