import { IEntranceTicketMuseumModel } from '../../../museum';
import { AgeCensorEnum, IClientCategoryEditableView } from '../../../shared';
import { ISeasonTicketPatternDTO } from '../DTO';

export interface ISeasonTicketEditableView {
  id: string;
  title: string;
  museums: IEntranceTicketMuseumModel[];
  clientCategories: IClientCategoryEditableView[];
  patterns: ISeasonTicketPatternDTO;
  maxSales: number | null;
  salesEndDate: string | null;
  ageCensor: AgeCensorEnum;
}
