import { AgeCensorEnum } from '../../../shared';

export interface IChosenSeasonTicketCatalogView {
  id: string;
  minPrice: number;
  title: string;
  ageCensor: AgeCensorEnum;
}
