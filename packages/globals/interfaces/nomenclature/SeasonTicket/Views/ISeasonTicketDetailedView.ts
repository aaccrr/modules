import { IEntranceTicketMuseumModel } from '../../../museum';
import { IClientCategoryModel } from '../../../shared';
import { ISeasonTicketPatternDTO } from '../DTO';

export interface ISeasonTicketDetailedView {
  id: string;
  title: string;
  museums: IEntranceTicketMuseumModel;
  clientCategories: IClientCategoryModel[];
  patterns: ISeasonTicketPatternDTO;
  maxSales: number | null;
  salesEndDate: number | null;
}
