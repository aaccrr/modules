import { IEntranceTicketMuseumModel } from '../../../museum';
import { IClientCategoryView } from '../../../shared';
import { ISeasonTicketPatternModel } from '../Models';

interface VisitorsCategoryId {
  legacyId: number;
  id: string;
}

export interface IPurchasedSeasonTicketView {
  id: string;
  organizationId: string;
  organizationName: string;
  title: string;
  museums: IEntranceTicketMuseumModel[];
  clientCategories: IClientCategoryView[];
  patterns: ISeasonTicketPatternModel;
  countByClient: number;
  ageCensor: number;
  implementsCovidRestriction: boolean;
  nomenclatureId: string;
  visitConditionId: string;
  visitorsCategoriesIds: VisitorsCategoryId[];
}
