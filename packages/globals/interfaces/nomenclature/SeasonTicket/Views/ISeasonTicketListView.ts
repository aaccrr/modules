import { IEntranceTicketMuseumModel } from '../../../museum';
import { SeasonTicketStatusEnum } from '../Enums';

export interface ISeasonTicketListView {
  id: string;
  title: string;
  museums: IEntranceTicketMuseumModel[];
  minPrice: number;
  status: SeasonTicketStatusEnum;
  canEdit: boolean;
}
