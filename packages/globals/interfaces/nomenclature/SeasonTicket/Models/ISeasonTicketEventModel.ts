export interface ISeasonTicketEventModel {
  name: string;
  startsAt: Date;
}
