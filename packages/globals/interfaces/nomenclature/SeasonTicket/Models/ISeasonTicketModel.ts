import {
  IClientCategoryModel,
  LastChangeEnum,
  ICreationLogModel,
  IUpdateLogModel,
  IDeletionLogModel,
  ITimezone,
  ISalesLimitModel,
  ICancellationLogModel,
  AgeCensorEnum,
} from '../../../shared';
import { IEntranceTicketMuseumModel } from '../../../museum';
import { ISeasonTicketPatternModel } from './ISeasonTicketPatternModel';
import { SeasonTicketStatusEnum } from '../Enums';

export interface ISeasonTicketModel {
  id: string;
  title: string;
  organizationId: string;
  organizationName: string;
  timezone: ITimezone;
  museums: IEntranceTicketMuseumModel[];
  clientCategories: IClientCategoryModel[];
  patterns: ISeasonTicketPatternModel;
  salesLimit: ISalesLimitModel;
  salesEndDate: Date;
  minPrice: number;
  maxPrice: number;
  countByClient: number;
  status: SeasonTicketStatusEnum;
  ageCensor: AgeCensorEnum;

  lastChange: LastChangeEnum;
  canEdit: boolean;
  latestExpirationDateOfPurchase: Date | null;
  isAddedToCatalog: boolean;

  creation: ICreationLogModel;
  update: IUpdateLogModel;
  deletion: IDeletionLogModel;
  cancellation: ICancellationLogModel;
}
