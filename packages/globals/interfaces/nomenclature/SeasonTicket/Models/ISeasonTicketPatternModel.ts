import { SeasonTicketPatternType } from '../Enums';
import { ISeasonTicketEventModel } from './ISeasonTicketEventModel';

export interface ISeasonTicketPatternModel {
  type: SeasonTicketPatternType;
  visits?: number;
  period?: number;
  events?: ISeasonTicketEventModel[];
}
