import { NomenclatureTypeEnum } from '../../Enums';

export interface IPurchasedSeasonTicketModel {
  id: string;
  title: string;
  type: NomenclatureTypeEnum;
}
