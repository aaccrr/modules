import { IClientCategoryView, AgeCensorEnum } from '../../../shared';
import { ISeasonTicketPatternModel } from './ISeasonTicketPatternModel';

export interface IChosenSeasonTicketModel {
  id: string;
  title: string;
  serviceProviderId: string;
  clientCategories: IClientCategoryView[];
  countByClient: number;
  maxPrice: number;
  minPrice: number;
  museums: string[];
  patterns: ISeasonTicketPatternModel;
  ageCensor: AgeCensorEnum;
  salesCount: number;
}
