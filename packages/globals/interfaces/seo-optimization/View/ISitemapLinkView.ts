import { SitemapLinkTypeEnum } from '../Enums';

export interface ISitemapLinkView {
  id: string;
  type: SitemapLinkTypeEnum;
}
