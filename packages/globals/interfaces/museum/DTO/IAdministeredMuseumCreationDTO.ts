import { MuseumTypeEnum } from '../Enums';
import { IMuseumCategoryModel, IMuseumRefundContactModel, IMuseumServiceModel, IMuseumFreeDaysModel } from '../Models';
import { IDetectableLocationDTO, IContactModel } from '../../shared';
import { IMuseumScheduleDayView } from '../Views/IMuseumScheduleDayView';

export interface IAdministeredMuseumCreationDTO {
  type: MuseumTypeEnum;
  name: string;
  description: string;
  categories: IMuseumCategoryModel[];
  location: IDetectableLocationDTO;
  officialSite: string;
  contacts: IContactModel[];
  returnContacts: IMuseumRefundContactModel;
  schedule: IMuseumScheduleDayView[];
  commentForSchedule: string;
  services: IMuseumServiceModel[];
  sanitaryDays: string[];
  freeDays: IMuseumFreeDaysModel;
  isFree: boolean;
}
