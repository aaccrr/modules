export interface IUpdateMuseumSalesCounterDTO {
  museumId: string;
  salesCount: number;
}
