export * from './IAdministeredMuseumCreationDTO';
export * from './IMuseumUploadedFilesDTO';
export * from './IContentMuseumCreationDTO';
export * from './IIncrementMuseumViewsCounterDTO';
export * from './IUpdateMuseumSalesCounterDTO';
export * from './IEnableEditForMuseumNomenclatureDTO';
export * from './IDisableEditForMuseumNomenclatureDTO';
export * from './IUpdateMuseumUpdatesToNomenclatureDTO';
export * from './IPublishMuseumToCatalogDTO';
export * from './IUpdateMuseumInCatalogDTO';
export * from './IUpdatedMuseumDTO';
export * from './IContentManagerUpdateMuseumDTO';

export * from './IMuseumForReportDTO';
