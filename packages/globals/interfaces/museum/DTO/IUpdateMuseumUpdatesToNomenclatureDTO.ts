import { IAdministeredMuseumModel } from '../Models';

export type IUpdateMuseumUpdatesToNomenclatureDTO = Partial<IAdministeredMuseumModel>;
