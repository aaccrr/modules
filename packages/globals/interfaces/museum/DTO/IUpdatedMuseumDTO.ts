import { IAdministeredMuseumModel } from '../Models';

export interface IUpdatedMuseumDTO {
  id: string;
  updated: Partial<IAdministeredMuseumModel>;
  updatedFields: string[];
}
