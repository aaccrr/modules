import { MuseumTypeEnum } from '../Enums';
import { IDetectableLocationDTO, IContactModel } from '../../shared';
import { IMuseumScheduleDayView } from '../Views';

export interface IContentManagerUpdateMuseumDTO {
  name: string;
  type: MuseumTypeEnum;
  description: string;
  location: IDetectableLocationDTO;
  officialSite: string;
  contacts: IContactModel[];
  schedule: IMuseumScheduleDayView;
  commentForSchedule: string;
}
