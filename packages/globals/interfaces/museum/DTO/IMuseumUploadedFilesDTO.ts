import { IMuseumRulesModel } from '../Models';
import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../shared';

export interface IMuseumUploadedFilesModel {
  previewImage: ICroppedPreviewImageModel;
  images: ICroppedGalleryImageModel[];
  rules: IMuseumRulesModel;
}
