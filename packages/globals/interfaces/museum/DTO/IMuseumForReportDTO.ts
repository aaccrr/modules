export interface IMuseumForReportDTO {
  id: string;
  name: string;
}
