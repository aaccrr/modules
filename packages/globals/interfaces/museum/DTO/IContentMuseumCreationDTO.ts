import { MuseumTypeEnum } from '../Enums';
import { IDetectableLocationDTO, IContactModel } from '../../shared';
import { IMuseumScheduleDayView } from '../Views';

export interface IContentMuseumCreationDTO {
  organizationId: string;
  type: MuseumTypeEnum;
  name: string;
  description: string;
  location: IDetectableLocationDTO;
  officialSite: string;
  contacts: IContactModel[];
  schedule: IMuseumScheduleDayView;
  commentForSchedule: string;
}
