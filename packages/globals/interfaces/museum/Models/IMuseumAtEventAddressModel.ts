import { IGeoPoint } from '../../shared';

export interface IMuseumAtEventAddressModel {
  id: string;
  slug: string;
  name: string;
  location: IGeoPoint;
}
