import { IMuseumScheduleDayModel } from './IMuseumScheduleDayModel';

export interface IMuseumScheduleModel {
  openingHours: IMuseumScheduleDayModel[];
  additionalInfo: string;
}
