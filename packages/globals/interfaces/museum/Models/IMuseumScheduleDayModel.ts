export interface IMuseumScheduleDayModel {
  weekDay: number;
  from: string | null;
  to: string | null;
  breakFrom: string | null;
  breakTo: string | null;
}
