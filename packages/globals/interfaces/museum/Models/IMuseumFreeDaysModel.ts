export interface IMuseumFreeDaysModel {
  patterns: IMuseumFreeDayPatternModel[];
  dates: IMuseumFreeDayDateModel[];
  additionalInfo: string;
}

export interface IMuseumFreeDayPatternModel {
  inMonthOrder: number;
  weekDay: number;
  toString?: string;
}

export interface IMuseumFreeDayDateModel {
  date: string;
  name: string;
}
