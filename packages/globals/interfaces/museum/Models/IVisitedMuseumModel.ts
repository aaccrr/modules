import { ILocationModel } from '../../shared';
import { IMuseumScheduleDayModel } from './IMuseumScheduleDayModel';

export interface IVisitedMuseumModel {
  id: string;
  name: string;
  slug: string;
  location: ILocationModel;
  schedule: IMuseumScheduleDayModel[];
  scheduleComment: string;
  site: string;
}
