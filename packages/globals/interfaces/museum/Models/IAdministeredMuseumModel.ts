import { MuseumTypeEnum } from '../Enums';
import {
  ICreationLogModel,
  IUpdateLogModel,
  IDeletionLogModel,
  LastChangeEnum,
  IContactModel,
  ILocationModel,
  ICroppedGalleryImageModel,
  ICroppedPreviewImageModel,
} from '../../shared';
import { MuseumStatusEnum } from '../Enums';
import { IMuseumRulesModel } from './IMuseumRulesModel';
import { IMuseumFreeDaysModel } from './IMuseumFreeDaysModel';
import { IMuseumServiceModel } from './IMuseumServiceModel';
import { IMuseumRefundContactModel } from './IMuseumRefundContactModel';
import { IMuseumCategoryModel } from './IMuseumCategoryModel';
import { IMuseumScheduleModel } from './IMuseumScheduleModel';

export interface IAdministeredMuseumModel {
  id: string;
  slug: string;
  organizationId: string;
  organizationName: string;
  name: string;
  description: string;
  location: ILocationModel;
  type: MuseumTypeEnum;
  categories: IMuseumCategoryModel[];
  contacts: IContactModel[];
  refundContacts: IMuseumRefundContactModel;
  officialSite: string;
  schedule: IMuseumScheduleModel;
  services: IMuseumServiceModel[];
  freeDays: IMuseumFreeDaysModel;
  previewImage: ICroppedPreviewImageModel;
  images: ICroppedGalleryImageModel[];
  rules: IMuseumRulesModel;
  status: MuseumStatusEnum;
  sanitaryDays: string[];
  isFree: boolean;

  implementsCovidRestriction: boolean;

  isAddedToCatalog: boolean;
  canContentManagerView: boolean;
  lastChange: LastChangeEnum;

  creation: ICreationLogModel;
  update: IUpdateLogModel;
  delete: IDeletionLogModel;
}
