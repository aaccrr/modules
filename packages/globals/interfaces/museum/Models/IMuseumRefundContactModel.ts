export interface IMuseumRefundContactModel {
  phone: string;
  email: string;
}
