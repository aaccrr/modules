export interface IMuseumCategoryModel {
  id: number;
  name: string;
}
