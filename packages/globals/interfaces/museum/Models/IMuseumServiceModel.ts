export interface IMuseumServiceModel {
  name: string;
  slug: string;
  order: number | null;
}
