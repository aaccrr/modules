export interface IMuseumRulesModel {
  src: string;
  uploadedAt: Date;
}
