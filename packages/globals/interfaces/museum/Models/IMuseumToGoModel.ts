import { IOrderedImageModel, ILocationModel, IPublicContactModel } from '../../shared';
import { IMuseumServiceModel } from './IMuseumServiceModel';
import { IMuseumCategoryModel } from './IMuseumCategoryModel';
import { IMuseumScheduleModel } from './IMuseumScheduleModel';

export interface IMuseumToGoModel {
  id: string;
  organizationId: string;
  name: string;
  slug: string;
  description: string;
  freeVisit: string;
  categories: IMuseumCategoryModel[];
  contacts: IPublicContactModel[];
  images: IOrderedImageModel[];
  previewImage: string;

  location: ILocationModel;

  maxPrice: number | null;
  minPrice: number | null;

  officialSite: string;

  rules: string;
  schedule: IMuseumScheduleModel;

  services: IMuseumServiceModel[];
  sanitaryDays: string[];

  views: number;
  likes: number;
  salesCount: number;
  isFree: number;
}
