import { IContactModel, ILocationModel } from '../../..';
import { IMuseumRefundContactModel, IMuseumScheduleModel } from '../../Models';

export interface IFetchVisitedMuseumForPurchaseResponse {
  id: string;
  slug: string;
  name: string;
  organizationName: string;
  refundContacts: IMuseumRefundContactModel;
  officialSite: string;
  schedule: IMuseumScheduleModel;
  contacts: IContactModel[];
  location: ILocationModel;
}
