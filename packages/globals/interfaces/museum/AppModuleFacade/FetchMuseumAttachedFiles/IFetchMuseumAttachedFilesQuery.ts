export interface IFetchMuseumAttachedFilesQuery {
  id: string;
  organizationId: string;
}
