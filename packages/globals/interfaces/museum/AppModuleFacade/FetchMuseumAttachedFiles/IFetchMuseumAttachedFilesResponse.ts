import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';
import { IMuseumRulesModel } from '../../Models';

export interface IFetchMuseumAttachedFilesResponse {
  id: string;
  images: ICroppedGalleryImageModel[];
  previewImage: ICroppedPreviewImageModel;
  rules: IMuseumRulesModel;
}
