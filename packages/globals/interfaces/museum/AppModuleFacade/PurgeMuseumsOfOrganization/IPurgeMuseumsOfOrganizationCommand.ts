export interface IPurgeMuseumsOfOrganizationCommand {
  organizationId: string;
}
