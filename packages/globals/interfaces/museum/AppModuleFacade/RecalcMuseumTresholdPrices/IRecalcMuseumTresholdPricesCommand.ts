import { ITresholdPricesDTO } from '../../../';

export interface IRecalcMuseumTresholdPricesCommand {
  museumId: string;
  isLastActiveTicket: boolean;
  tresholdPrices: ITresholdPricesDTO;
}
