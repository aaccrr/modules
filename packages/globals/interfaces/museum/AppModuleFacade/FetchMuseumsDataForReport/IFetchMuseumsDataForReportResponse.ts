import { IMuseumForReportDTO } from '../../DTO';

export type IFetchMuseumsDataForReportResponse = IMuseumForReportDTO[];
