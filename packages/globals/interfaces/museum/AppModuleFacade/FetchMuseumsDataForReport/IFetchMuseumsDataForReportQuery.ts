import { IDatesPeriodView } from '../../../shared';

export interface IFetchMuseumsDataForReportQuery {
  reportingOrganizationId: string;
  reportingMuseums: string[];
  reportingPeriod: IDatesPeriodView;
}
