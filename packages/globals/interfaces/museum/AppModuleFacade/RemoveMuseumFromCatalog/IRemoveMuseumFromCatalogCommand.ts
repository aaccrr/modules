import { IAdministeredMuseumModel } from '../../Models';

export type IRemoveMuseumFromCatalogCommand = IAdministeredMuseumModel;
