import { ILocationModel } from '../../..';
import { IMuseumFreeDaysModel, IMuseumScheduleDayModel } from '../../Models';

export interface IGetVisitedMuseumForNomenclatureResponse {
  id: string;
  name: string;
  location: ILocationModel;
  schedule: IMuseumScheduleDayModel[];
  sanitaryDays: string[];
  freeDays: IMuseumFreeDaysModel;
  isFree: boolean;
}
