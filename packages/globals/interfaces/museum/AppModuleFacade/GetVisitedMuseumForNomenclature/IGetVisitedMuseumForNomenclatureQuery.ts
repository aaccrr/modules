export interface IGetVisitedMuseumForNomenclatureQuery {
  museumId: string;
}
