import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';

export interface IFetchPreviewMuseumAttachedFilesResponse {
  id: string;
  images: ICroppedGalleryImageModel[];
  previewImage: ICroppedPreviewImageModel;
}
