import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';
import { IMuseumRulesModel } from '../../Models';

export interface IAttachFilesToMuseumCommand {
  id: string;
  organizationId: string;
  images?: ICroppedGalleryImageModel[];
  previewImage?: ICroppedPreviewImageModel;
  rules?: IMuseumRulesModel;
}
