export interface IUpdateMuseumInCatalogCommand {
  updatedMuseumId: string;
}