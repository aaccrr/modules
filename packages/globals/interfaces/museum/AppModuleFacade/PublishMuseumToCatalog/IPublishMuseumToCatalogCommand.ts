import { IAdministeredMuseumModel } from '../../Models';

export type IPublishMuseumToCatalogCommand = IAdministeredMuseumModel;
