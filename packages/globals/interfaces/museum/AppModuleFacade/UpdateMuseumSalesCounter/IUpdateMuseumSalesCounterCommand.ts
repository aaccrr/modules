export interface IUpdateMuseumSalesCounterCommand {
  museumSlug: string;
  salesCount: number;
}
