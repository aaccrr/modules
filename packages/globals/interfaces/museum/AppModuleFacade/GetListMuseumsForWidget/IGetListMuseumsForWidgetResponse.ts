import { IMuseumViewForWidget } from '../../Views';

export type IGetListMuseumsForWidgetResponse = IMuseumViewForWidget[];
