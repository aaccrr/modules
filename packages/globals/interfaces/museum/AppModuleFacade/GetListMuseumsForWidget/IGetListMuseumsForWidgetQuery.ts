export interface IGetListMuseumsForWidgetQuery {
  museumsInWidget: string[];
}
