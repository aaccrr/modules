export interface IEnsureMuseumsAvailabilityResponse {
  inappropriate: string[];
}
