export interface IEnsureMuseumsAvailabilityQuery {
  museums: string[];
}
