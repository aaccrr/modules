export interface IViewMuseumModerationQueuePlenumResponse {
  organizationId: string;
  moderationQueuePlenum: number;
}
