export interface IRequireAdditionalInformationForMuseumsOfAssignedOrganizationCommand {
  organizationId: string;
}
