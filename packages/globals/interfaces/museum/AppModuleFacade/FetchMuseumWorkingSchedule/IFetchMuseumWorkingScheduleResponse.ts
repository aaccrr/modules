import { IMuseumScheduleDayModel } from '../../Models';

export interface IFetchMuseumWorkingScheduleResponse {
  schedule: IMuseumScheduleDayModel[];
  sanitaryDays: string[];
}
