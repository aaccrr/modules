import { ICroppedGalleryImageModel, ICroppedPreviewImageModel } from '../../..';

export interface IAttachFilesToPreviewMuseumCommand {
  id: string;
  images?: ICroppedGalleryImageModel[];
  previewImage?: ICroppedPreviewImageModel;
}
