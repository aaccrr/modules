import { IGeoPoint } from '../../..';

export interface IListMuseumsByEventAddressQuery {
  addresses: IGeoPoint[];
}
