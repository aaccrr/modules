import { IAdministeredMuseumModel } from '../../Models';

export interface IListMuseumsByEventAddressResponse {
  museumByAddress: Partial<IAdministeredMuseumModel>[];
}
