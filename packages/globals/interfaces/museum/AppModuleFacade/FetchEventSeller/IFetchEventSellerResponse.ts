import { ITimezone } from '../../../shared';

export interface IFetchEventSellerResponse {
  id: string;
  name: string;
  timezone: ITimezone;
}
