export interface ICheckAffiliationOfMuseumsToOrganizationResponse {
  affiliationCheckResult: boolean;
  didNotPassCheck: string[];
}
