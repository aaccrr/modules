export interface ICheckAffiliationOfMuseumsToOrganizationQuery {
  organizationId: string;
  museumsIds: string[];
}
