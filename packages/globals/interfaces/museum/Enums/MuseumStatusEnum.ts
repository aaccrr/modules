export enum MuseumStatusEnum {
  IDLE = 'IDLE',
  PENDING = 'PENDING',
  REQUIRES_ADDITION = 'REQUIRES_ADDITION',
  ACTIVE = 'ACTIVE',
  DELETED = 'DELETED',
}
