import { IMuseumRefundContactModel, IMuseumScheduleModel } from '../Models';
import { IPublicContactModel, ILocationModel } from '../../shared';

export interface IVisitedMuseumView {
  id: string;
  slug: string;
  name: string;
  organizationName: string;
  refundContacts: IMuseumRefundContactModel;
  officialSite: string;
  schedule: IMuseumScheduleModel;
  contacts: IPublicContactModel[];
  location: ILocationModel;
}
