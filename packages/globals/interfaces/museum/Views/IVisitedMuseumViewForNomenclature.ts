import { ILocationModel } from '../..';
import { IMuseumFreeDaysModel, IMuseumScheduleDayModel } from '../Models';

export interface IVisitedMuseumViewForNomenclature {
  id: string;
  name: string;
  location: ILocationModel;
  schedule: IMuseumScheduleDayModel[];
  sanitaryDays: string[];
  freeDays: IMuseumFreeDaysModel;
  isFree: boolean;
}
