export interface IMuseumCategoryView {
  id: number;
  name: string;
  isPrimary: boolean;
  isActive: boolean;
}
