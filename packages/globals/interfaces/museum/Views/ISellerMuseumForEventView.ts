import { ITimezone } from '../../shared';

export interface ISellerMuseumForEventView {
  id: string;
  name: string;
  timezone: ITimezone;
}
