import { MuseumTypeEnum } from '../Enums';
import { ILocationModel, IContactModel, IOrderedImageModel } from '../../shared';
import { IMuseumRefundContactModel, IMuseumFreeDaysModel, IMuseumRulesModel } from '../Models';
import { IMuseumScheduleDayView } from './IMuseumScheduleDayView';
import { IAdministeredMuseumServiceView } from './IAdministeredMuseumServiceView';
import { IMuseumCategoryView } from './IMuseumCategoryView';

export interface IAdministeredMuseumEditableView {
  id: string;
  type: MuseumTypeEnum;
  name: string;
  description: string;
  location: Partial<ILocationModel>;
  officialSite: string;
  returnContacts: IMuseumRefundContactModel;
  contacts: IContactModel[];
  schedule: IMuseumScheduleDayView[];
  commentForSchedule: string;
  sanitaryDays: string[];
  freeDays: IMuseumFreeDaysModel;
  services: IAdministeredMuseumServiceView[];
  categories: IMuseumCategoryView[];
  images: IOrderedImageModel[];
  previewImage: string;
  rules: IMuseumRulesModel;
  isFree: boolean;
}
