import { IMuseumScheduleDayModel, IMuseumFreeDaysModel } from '../Models';
import { ILocationModel } from '../../shared';

export interface ICateringMuseumView {
  id: string;
  name: string;
  schedule: IMuseumScheduleDayModel[];
  sanitaryDays: string[];
  freeDays: IMuseumFreeDaysModel;
  location: ILocationModel;
}
