import { MuseumTypeEnum } from '../Enums';
import { IPublicContactModel, ILocationModel } from '../../shared';
import { IMuseumRefundContactModel, IMuseumServiceModel, IMuseumFreeDaysModel } from '../Models';
import { IMuseumScheduleDayView } from './IMuseumScheduleDayView';

export interface IAdministeredMuseumDetailedView {
  id: string;
  type: MuseumTypeEnum;
  organizationName: string;
  name: string;
  officialSite: string;
  contacts: IPublicContactModel[];
  returnContacts: IMuseumRefundContactModel;
  schedule: IMuseumScheduleDayView[];
  commentForSchedule: string;
  services: IMuseumServiceModel[];
  freeDays: IMuseumFreeDaysModel;
  location: Partial<ILocationModel>;
}
