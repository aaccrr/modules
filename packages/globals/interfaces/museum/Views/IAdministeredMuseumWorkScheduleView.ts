import { ITimezone } from '../../shared';
import { IMuseumScheduleDayModel } from '../Models';

export interface IAdministeredMuseumWorkScheduleView {
  schedule: IMuseumScheduleDayModel[];
  sanitaryDays: string[];
}
