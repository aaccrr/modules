import { ILocationModel, IContactModel, IOrderedImageModel } from '../../shared';
import { MuseumTypeEnum } from '../Enums';
import { IMuseumScheduleDayView } from './IMuseumScheduleDayView';

export interface IContentMuseumDetailedView {
  id: string;
  name: string;
  type: MuseumTypeEnum;
  description: string;
  location: ILocationModel;
  officialSite: string;
  contacts: IContactModel[];
  schedule: IMuseumScheduleDayView[];
  commentForSchedule: string;
  images: IOrderedImageModel[];
  previewImage: string;
}
