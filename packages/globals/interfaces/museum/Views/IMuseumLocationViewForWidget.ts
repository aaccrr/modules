export interface IMuseumLocationViewForWidget {
  locality: string;
  address: string;
}