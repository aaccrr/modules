import { IMuseumScheduleDayModel } from '../Models';
import { IOrderedImageModel, ILocationModel, IPublicContactModel } from '../../shared';
import { IMuseumToGoServiceView } from './IMuseumToGoServiceView';
import { IMuseumToGoSellingTicketsView } from './IMuseumToGoSellingTicketsView';

export interface IMuseumToGoDetailedView {
  id: string;
  slug: string;
  name: string;
  schedule: IMuseumScheduleDayModel[];
  images: IOrderedImageModel[];
  description: string;
  scheduleComment: string;
  freeVisit: string;
  location: Partial<ILocationModel>;
  services: IMuseumToGoServiceView[];
  contacts: IPublicContactModel[];
  officialSite: string;
  isLiked: boolean;
  selling: IMuseumToGoSellingTicketsView;
  rules: string;
  isFree: boolean;
  sanitaryDays: string[];
}
