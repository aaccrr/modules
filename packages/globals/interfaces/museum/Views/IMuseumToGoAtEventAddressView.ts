export interface IMuseumToGoAtEventAddressView {
  slug: string;
  name: string;
  minPrice: number;
}
