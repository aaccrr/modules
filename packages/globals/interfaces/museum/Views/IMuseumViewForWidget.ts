import { IMuseumScheduleDayModel } from '../Models';
import { IMuseumLocationViewForWidget } from './IMuseumLocationViewForWidget';

export interface IMuseumViewForWidget {
  id: string;
  slug: string;
  name: string;
  workingSchedule: IMuseumScheduleDayModel[];
  additionalInfoToWorkingSchedule: string;
  imagesGallery: string[];
  location: IMuseumLocationViewForWidget;
}
