export interface IAdministeredMuseumServiceView {
  order: number | null;
  name: string;
  isActive: boolean;
  isCustom: boolean;
}
