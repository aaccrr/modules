import { IMuseumScheduleDayModel } from '../Models';
import { ILocationModel } from '../../shared';

export interface IMuseumToGoCatalogView {
  slug: string;
  name: string;
  schedule: Partial<IMuseumScheduleDayModel>;
  minPrice: number;
  maxPrice: number;
  location: Partial<ILocationModel>;
  previewImage: string;
  isLiked: boolean;
  hasTickets: boolean;
  likes: number;
  isFree: boolean;
}
