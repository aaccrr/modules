import { MuseumStatusEnum } from '../Enums';

export interface IAdministeredMuseumListView {
  id: string;
  name: string;
  status: MuseumStatusEnum;
  primaryImage: string;
}
