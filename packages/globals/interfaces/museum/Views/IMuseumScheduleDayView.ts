import { ITimeObjectModel } from '../../shared';

export interface IMuseumScheduleDayView {
  weekDayOrders: number[];
  startsAt: ITimeObjectModel | null;
  endAt: ITimeObjectModel | null;
  breakStartsAt: ITimeObjectModel | null;
  breakEndAt: ITimeObjectModel | null;
}
