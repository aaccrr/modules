export interface IMuseumToGoPricesView {
  minPrice: number;
  maxPrice: number;
}
