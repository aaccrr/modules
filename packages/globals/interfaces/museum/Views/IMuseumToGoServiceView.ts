export interface IMuseumToGoServiceView {
  order: number;
  slug: string;
  name: string;
}
