import { IMuseumLocationViewForWidget } from './IMuseumLocationViewForWidget';

export interface IMuseumViewForWidgetsListing {
  id: string;
  name: string;
  imagesGallery: string[];
  location: IMuseumLocationViewForWidget;
}
