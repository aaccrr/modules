export interface IGetWidgetAddressResponse {
  id: string;
  address: string;
}
