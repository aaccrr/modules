import { IMuseumViewForWidgetsListing } from '../../museum';

export interface IExistingWidgetViewForAdminListing {
  id: string;
  name: string;
  museums: IMuseumViewForWidgetsListing[];
}
