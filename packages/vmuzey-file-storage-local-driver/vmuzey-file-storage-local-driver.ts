import * as fs from 'fs';
import * as path from 'path';
import { Readable } from 'stream';
import {
  FileSavingTypes,
  IVmuzeyFileStorageBatchItem,
  IVmuzeyFileStorageDriver,
  IVmuzeyFileStorageDriverSaveParams,
} from '../vmuzey-file-storage-provider';

export class VmuzeyFileStorageLocalDriver implements IVmuzeyFileStorageDriver {
  async init(): Promise<void> {}

  async save(file: FileSavingTypes, params: IVmuzeyFileStorageDriverSaveParams): Promise<string> {
    if ((file as any).file) file = (file as any).file;
    await this.createDirectoryIfNotExists(this.getTargetDirectoryPath(params.path));

    if (typeof file === 'string') {
      await this.saveBuffer(file, params.path);
      return params.path;
    }

    if (Buffer.isBuffer(file)) {
      await this.saveBuffer(file, params.path);
      return params.path;
    }

    await this.saveStream(file, params.path);
    return params.path;
  }

  async saveBatch(batch: IVmuzeyFileStorageBatchItem[]): Promise<any> {}

  async get(fileName: string): Promise<any> {}

  private async createDirectoryIfNotExists(targetDirPath: string): Promise<void> {
    const isTargetDirectoryExists = fs.existsSync(targetDirPath);
    if (!isTargetDirectoryExists) {
      await fs.promises.mkdir(targetDirPath, { recursive: true });
    }
  }

  private getTargetDirectoryPath(targetFilePath: string): string {
    return path.dirname(targetFilePath);
  }

  private async saveBuffer(file: Buffer | string, path: string): Promise<any> {
    await fs.promises.writeFile(path, file);
  }

  private saveStream(readStream: Readable, path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const writeStream = fs.createWriteStream(path);
      readStream.on('error', reject);
      writeStream.on('error', reject);
      writeStream.on('finish', () => resolve(true));
      readStream.pipe(writeStream);
    });
  }
}
