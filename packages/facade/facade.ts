export class Facade {
  protected operations;

  handle(operationName: string) {
    return this.operations.select(operationName);
  }
}
