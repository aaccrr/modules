import { RouteMiddleware } from '../http-router';
import { IpBlockerMethodsEnum, VmuzeyIpBlocker } from '../vmuzey-ip-blocker';
import { IVmuzeyRedisProvider } from '../vmuzey-redis-provider';

export class VmuzeyHttpIpBlockerMiddleware {
  constructor(private redis: IVmuzeyRedisProvider, private blockingMethod: IpBlockerMethodsEnum) {}

  middleware(): RouteMiddleware {
    const ipBlocker = new VmuzeyIpBlocker(this.redis);

    return async (ctx, next) => {
      const ip = ctx.headers['x-real-ip'] || ctx.headers['x-forwarded-for'] || ctx.headers['client-ip'];
      await ipBlocker.checkIpBlock(this.blockingMethod, ip);
      await next();
    };
  }
}
