import { roles } from '../interfaces';
import { Errors } from '../errors';
import { Tokens } from '../tokens';
import { IAuthorizerDeps, IAuthorizerRules } from './Interfaces';

export function OperationAuthorizer(deps: IAuthorizerDeps, rules: IAuthorizerRules) {
  return async (ctx: any) => {
    const token = Tokens.getTokenFromRequest(ctx.request);

    if (!rules.auth) {
      ctx.state.user = {
        id: null,
        status: null,
        role: roles.GUEST,
        organizationId: null,
      };

      return ctx;
    } else {
      if (!token) Errors.AuthenticationError('Не предоставлен токен авторизации');

      const sessionInfo = await deps.Rpc.pick('session').call('get', { selection: { accessToken: token } });

      ctx.state.user = {
        id: sessionInfo.id,
        status: sessionInfo.status,
        role: sessionInfo.role,
        organizationId: sessionInfo.organizationId,
      };

      checkRoles(ctx.state.user.role, rules.roles);

      return ctx;
    }
  };
}

function checkRoles(currentRole: string, acceptedRoles: string[] = []) {
  if (!acceptedRoles) return;
  const isAccepted = acceptedRoles.includes(currentRole);
  if (!isAccepted) Errors.AccessError('Доступ запрещен.');
}
