import { IRpcClient } from '../rpc';

export interface IOperationAuthorizer {
  (deps: IAuthorizerDeps, rules: IAuthorizerRules): Promise<any>;
}

export interface IAuthorizerRules {
  auth: boolean;
  roles: string[];
}

export interface IAuthorizerDeps {
  Rpc: IRpcClient;
}
