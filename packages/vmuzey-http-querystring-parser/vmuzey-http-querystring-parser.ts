import { RouteMiddleware } from '../http-router';
import { IVmuzeyHttpMiddleware } from '../vmuzey-http-server-provider';

const isArrayItemParsing = (propName: string) => propName.endsWith('[]');
const normalizePropName = (propName: string) => propName.replace('[]', '');

export class VmuzeyHttpQuerystringParser implements IVmuzeyHttpMiddleware {
  private numericFields = ['offset', 'limit'];
  private floatFields = ['lat', 'lon', 'bottomLeftMapBound[]', 'topRightMapBound[]'];
  private booleanFields = ['detailed', 'isEdit', 'withSchedule', 'withMap'];
  private arrayFields = ['museums', 'types', 'ignore', 'times', 'bottomLeftMapBound', 'topRightMapBound', 'fields'];

  middleware(): RouteMiddleware {
    return async (ctx, next) => {
      this.arrayFields.forEach((arrayField) => {
        if (typeof ctx.request.query[arrayField] !== 'undefined') {
          ctx.request.query[arrayField] = [/\[/g, /\]/g, /\"/g]
            .reduce((string: string, bracket: RegExp): string => string.replace(bracket, ''), ctx.request.query[arrayField])
            .split(',');
        }
      });

      this.numericFields.forEach((numericField) => {
        const normalizedPropName = normalizePropName(numericField);

        if (typeof ctx.request.query[normalizedPropName] !== 'undefined') {
          if (isArrayItemParsing(numericField) && Array.isArray(ctx.request.query[normalizedPropName])) {
            ctx.request.query[normalizedPropName] = ctx.request.query[normalizedPropName].map((item) => parseInt(item));
          } else {
            ctx.request.query[normalizedPropName] = parseInt(ctx.request.query[normalizedPropName]);
          }
        }
      });

      this.floatFields.forEach((floatField) => {
        const normalizedPropName = normalizePropName(floatField);

        if (typeof ctx.request.query[normalizedPropName] !== 'undefined') {
          if (isArrayItemParsing(floatField) && Array.isArray(ctx.request.query[normalizedPropName])) {
            ctx.request.query[normalizedPropName] = ctx.request.query[normalizedPropName].map((item) => parseFloat(item));
          } else {
            ctx.request.query[normalizedPropName] = parseFloat(ctx.request.query[normalizedPropName]);
          }
        }
      });

      this.booleanFields.forEach((booleanField) => {
        if (typeof ctx.request.query[booleanField] !== 'undefined') {
          ctx.request.query[booleanField] = ctx.request.query[booleanField] === 'true';
        }
      });

      await next();
    };
  }
}
