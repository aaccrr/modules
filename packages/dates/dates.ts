import {
  Interval,
  eachDayOfInterval,
  getISODay,
  differenceInDays,
  min,
  format,
  endOfMonth,
  isEqual,
  getMonth,
  getYear,
  startOfMonth,
  getDate,
  startOfDay,
  parseISO,
  compareAsc,
  compareDesc,
} from 'date-fns';
import * as moment from 'moment-timezone';
import { format as formatWithTimezone, toDate } from 'date-fns-tz';
import { IDates, DateOrStringInterval, IDatesGetMonthNameOptionsView } from './Interfaces';
import { DateOrString } from './Interfaces';
import { ITimezone, TimezoneOffset } from '../globals';

const TIMEZONED_DATE_PATTERN = `YYYY-MM-DDTHH:mm:ss.SSSZ`;

const MONTH_RU = {
  nominative: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  genitive: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
};
const WEEKDAY_RU = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];

export class Dates implements IDates {
  constructor() {
    moment.locale('ru');
  }

  addYears(date: DateOrString, years: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).add(years, 'years').format(TIMEZONED_DATE_PATTERN);
  }

  subYears(date: DateOrString, years: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).subtract(years, 'years').format(TIMEZONED_DATE_PATTERN);
  }

  addMonths(date: DateOrString, months: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).add(months, 'months').format(TIMEZONED_DATE_PATTERN);
  }

  subMonths(date: DateOrString, months: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).subtract(months, 'months').format(TIMEZONED_DATE_PATTERN);
  }

  addDays(date: DateOrString, days: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).add(days, 'days').format(TIMEZONED_DATE_PATTERN);
  }

  subDays(date: DateOrString, days: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).subtract(days, 'days').format(TIMEZONED_DATE_PATTERN);
  }

  addHours(date: DateOrString, hours: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).add(hours, 'h').format(TIMEZONED_DATE_PATTERN);
  }

  subHours(date: DateOrString, hours: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).subtract(hours, 'h').format(TIMEZONED_DATE_PATTERN);
  }

  addMinutes(date: DateOrString, minutes: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).add(minutes, 'm').format(TIMEZONED_DATE_PATTERN);
  }

  subMinutes(date: DateOrString, minutes: number, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).subtract(minutes, 'm').format(TIMEZONED_DATE_PATTERN);
  }

  getDaysInRange(from: DateOrString, to: DateOrString, tz: ITimezone = 'Europe/Moscow'): Date[] {
    const interval: Interval = {
      start: this.optionalToDate(from),
      end: this.optionalToDate(to),
    };

    return eachDayOfInterval(interval).map((dateInInterval) => new Date(this.formatAndSubTimezone(dateInInterval, tz)));
  }

  getWeekDayOrder(date: DateOrString): number {
    return getISODay(this.optionalToDate(date));
  }

  getWeekDayOrderWithTimezone(date: DateOrString, tz: ITimezone = 'Europe/Moscow'): number {
    return moment(date).tz(tz).weekday() + 1;
  }

  getDifferenceInDays(date1: DateOrString, date2: DateOrString): number {
    return differenceInDays(this.optionalToDate(date1), this.optionalToDate(date2));
  }

  getMinDate(dates: DateOrString[]): DateOrString {
    return min(dates.map((date) => this.optionalToDate(date)));
  }

  format(date: DateOrString, tz: ITimezone = 'Europe/Moscow', pattern: string = TIMEZONED_DATE_PATTERN): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).format(pattern);
  }

  formatAndSubTimezone(date: DateOrString, tz?: ITimezone, pattern?: string): string {
    if (!tz) tz = 'Europe/Moscow';
    const offset = TimezoneOffset[tz];
    return this.subHours(moment(date).tz(tz).format(pattern), offset);
  }

  getStartOfMonth(date: DateOrString): Date {
    return startOfMonth(this.optionalToDate(date));
  }

  getEndOfMonth(date: DateOrString): Date {
    return endOfMonth(this.optionalToDate(date));
  }

  getMonthIndex(date: DateOrString): number {
    return getMonth(this.optionalToDate(date)) + 1;
  }

  getYear(date: DateOrString): number {
    return getYear(this.optionalToDate(date));
  }

  getDayOrderInMonth(date: DateOrString): number {
    return getDate(this.optionalToDate(date));
  }

  /*
    @Param { date | string } date Сравниваемая дата
    @Param { date | string } dateToCompare Дата, с которой сравнивать
  */
  isEqual(date: DateOrString, dateToCompare: DateOrString): boolean {
    return isEqual(this.optionalToDate(date), this.optionalToDate(dateToCompare));
  }

  /*
    @Param { date | string } date Сравниваемая дата
    @Param { date | string } dateToCompare Дата, с которой сравнивать
  */
  isAfter(date: DateOrString, dateToCompare: DateOrString, tz: ITimezone = 'Europe/Moscow'): boolean {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).isAfter(moment(dateToCompare).tz(tz).format());
  }

  /*
    @Param { date | string } date Сравниваемая дата
    @Param { date | string } dateToCompare Дата, с которой сравнивать
  */
  isBefore(date: DateOrString, dateToCompare: DateOrString, tz: ITimezone = 'Europe/Moscow'): boolean {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).isBefore(moment(dateToCompare).tz(tz).format());
  }

  /*
    @Param { date | string } date Сравниваемая дата
    @Param { date | string } dateToCompare Дата, с которой сравнивать
  */
  isAfterOrEqual(date: DateOrString, dateToCompare: DateOrString, tz: ITimezone = 'Europe/Moscow'): boolean {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).isSameOrAfter(moment(dateToCompare).tz(tz).format());
  }

  /*
    @Param { date | string } date Сравниваемая дата
    @Param { date | string } dateToCompare Дата, с которой сравнивать
  */
  isBeforeOrEqual(date: DateOrString, dateToCompare: DateOrString, tz: ITimezone = 'Europe/Moscow'): boolean {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).isSameOrBefore(moment(dateToCompare).tz(tz).format());
  }

  isBetween(interval: DateOrStringInterval, date: DateOrString, tz: ITimezone = 'Europe/Moscow'): boolean {
    date = this.optionalToDate(date);
    interval = {
      from: this.optionalToDate(interval.from),
      to: this.optionalToDate(interval.to),
    };

    return this.isAfterOrEqual(date, interval.from) && this.isBeforeOrEqual(date, interval.to);
  }

  createTimestamp(): Date {
    return new Date();
  }

  getStartOfDay(date: DateOrString, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).startOf('day').format(TIMEZONED_DATE_PATTERN);
  }

  getEndOfDay(date: DateOrString, tz: ITimezone = 'Europe/Moscow'): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(date).tz(tz).endOf('day').format(TIMEZONED_DATE_PATTERN);
  }

  stringToDate(dateString: string): Date {
    return parseISO(dateString);
  }

  getNow(tz?: ITimezone): string {
    if (!tz) tz = 'Europe/Moscow';
    return moment(new Date()).tz(tz).format(TIMEZONED_DATE_PATTERN);
  }

  sort(dateArr: DateOrString[], direction: 'asc' | 'desc'): Date[] {
    const sorters = { asc: compareAsc, desc: compareDesc };
    const sorter = sorters[direction];

    return dateArr.map((date) => this.optionalToDate(date)).sort(sorter);
  }

  toDateWithTimezone(date: DateOrString, timeZone: ITimezone): Date {
    return moment(date).tz(timeZone).toDate();
  }

  isBetweenWithTimezone(date: DateOrString, period: DateOrStringInterval): boolean {
    return moment(date).isBetween(period.from, period.to);
  }

  isBeforeWithTimezone(comparableDate: DateOrString, compareWith: DateOrString): boolean {
    return moment(comparableDate).isBefore(compareWith);
  }
  isAfterWithTimezone(comparableDate: DateOrString, compareWith: DateOrString): boolean {
    return moment(comparableDate).isBefore(compareWith);
  }

  getMonthName(date: DateOrString, options?: IDatesGetMonthNameOptionsView): string {
    const names = MONTH_RU[options?.case || 'genitive'];
    return names[this.getMonthIndex(date) - 1];
  }

  diffInMillisecondsWithTimezone(from: DateOrString, to: DateOrString): number {
    return moment(from).diff(to);
  }

  isDayOff(date: DateOrString): boolean {
    const weekDay = this.getWeekDayOrder(date);
    return [6, 7].includes(weekDay);
  }

  getWeekdayName(date: DateOrString): string {
    const weekdayOrder = this.getWeekDayOrder(date);
    return WEEKDAY_RU[weekdayOrder - 1];
  }

  isSameDay(date1: DateOrString, date2: DateOrString): boolean {
    return moment(date1).isSame(date2, 'day');
  }

  isSameMonth(date1: DateOrString, date2: DateOrString): boolean {
    return moment(date1).isSame(date2, 'month');
  }
  isSameYear(date1: DateOrString, date2: DateOrString): boolean {
    return moment(date1).isSame(date2, 'year');
  }

  daysDiff(from: DateOrString, to: DateOrString): number {
    // Math.abs(moment(from).diff(to, 'days')) + 1
    return moment(from).diff(to, 'days');
  }

  diffInHours(from: DateOrString, to: DateOrString): number {
    return moment(from).diff(to, 'hours');
  }

  formatDate(date: DateOrString, timezone: ITimezone = 'Europe/Moscow'): string {
    return formatWithTimezone(this.optionalToDate(date), `yyyy-MM-dd'T'HH:mm:ss.SSSXXX`, { timeZone: timezone });
  }

  getMin(dates: DateOrString[], timezone: ITimezone = 'Europe/Moscow'): string {
    return moment
      .min(dates.map((date) => moment(date)))
      .tz(timezone)
      .format(TIMEZONED_DATE_PATTERN);
  }

  getMax(dates: DateOrString[], timezone: ITimezone = 'Europe/Moscow'): string {
    return moment
      .max(dates.map((date) => moment(date)))
      .tz(timezone)
      .format(TIMEZONED_DATE_PATTERN);
  }

  isValid(datable: DateOrString): boolean {
    return moment(datable).isValid();
  }

  toUTC(date: DateOrString): Date {
    return moment(date).utc().toDate();
  }

  private optionalToDate(date: DateOrString): Date {
    if (typeof date === 'string') return this.stringToDate(date);
    return date;
  }
}
