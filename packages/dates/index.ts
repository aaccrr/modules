import { Dates } from './dates';

const Module = new Dates();
export { Module as Dates };

export * from './Interfaces';
