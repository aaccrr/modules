import { ITimezone } from '../globals';

export interface IDates {
  addYears(date: DateOrString, years: number, tz?: ITimezone): string;
  subYears(date: DateOrString, years: number, tz?: ITimezone): string;
  addMonths(date: DateOrString, months: number, tz?: ITimezone): string;
  subMonths(date: DateOrString, months: number, tz?: ITimezone): string;
  addDays(date: DateOrString, days: number, tz?: ITimezone): string;
  subDays(date: DateOrString, days: number, tz?: ITimezone): string;
  addHours(date: DateOrString, hours: number, tz?: ITimezone): string;
  subHours(date: DateOrString, hours: number, tz?: ITimezone): string;
  addMinutes(date: DateOrString, minutes: number, tz?: ITimezone): string;
  subMinutes(date: DateOrString, minutes: number, tz: ITimezone): string;
  getDaysInRange(from: DateOrString, to: DateOrString, tz?: ITimezone): Date[];
  getWeekDayOrder(date: DateOrString): number;
  getDifferenceInDays(date1: DateOrString, date2: DateOrString): number;
  getMinDate(dates: DateOrString[]): DateOrString;
  format(date: DateOrString, tz?: ITimezone, pattern?: string): string;
  formatAndSubTimezone(date: DateOrString, tz?: ITimezone, pattern?: string): string;
  getStartOfMonth(date: DateOrString): Date;
  getEndOfMonth(date: DateOrString): Date;
  getMonthIndex(date: DateOrString): number;
  getYear(date: DateOrString): number;
  getDayOrderInMonth(date: DateOrString): number;
  isEqual(date1: DateOrString, date2: DateOrString): boolean;
  isAfter(date: DateOrString, dateToCompare: DateOrString, tz?: ITimezone): boolean;
  isBefore(date: DateOrString, dateToCompare: DateOrString, tz?: ITimezone): boolean;
  isAfterOrEqual(date1: DateOrString, date2: DateOrString, tz?: ITimezone): boolean;
  isBeforeOrEqual(date1: DateOrString, date2: DateOrString, tz?: ITimezone): boolean;
  isBetween(interval: DateOrStringInterval, date: DateOrString, tz?: ITimezone): boolean;
  createTimestamp(): Date;
  getStartOfDay(date: DateOrString, tz?: ITimezone): string;
  getEndOfDay(date: DateOrString, tz?: ITimezone): string;
  stringToDate(dateString: string): Date;
  getNow(tz?: ITimezone): string;
  toDateWithTimezone(date: DateOrString, timeZone: ITimezone): Date;
  isBetweenWithTimezone(date: DateOrString, period: DateOrStringInterval): boolean;
  isBeforeWithTimezone(comparableDate: DateOrString, compareWith: DateOrString): boolean;
  isAfterWithTimezone(comparableDate: DateOrString, compareWith: DateOrString): boolean;
  sort(dateArr: DateOrString[], direction: 'asc' | 'desc'): Date[];
  getMonthName(date: DateOrString, options?: IDatesGetMonthNameOptionsView): string;
  diffInMillisecondsWithTimezone(from: DateOrString, to: DateOrString): number;
  isDayOff(date: DateOrString): boolean;
  getWeekdayName(date: DateOrString): string;
  isSameDay(date1: DateOrString, date2: DateOrString): boolean;
  isSameMonth(date1: DateOrString, date2: DateOrString): boolean;
  isSameYear(date1: DateOrString, date2: DateOrString): boolean;
  daysDiff(from: DateOrString, to: DateOrString): number;
  diffInHours(from: DateOrString, to: DateOrString): number;
  formatDate(date: DateOrString, timezone?: ITimezone): string;

  getMin(dates: DateOrString[], timezone?: ITimezone): string;
  getMax(dates: DateOrString[], timezone?: ITimezone): string;

  isValid(datable: DateOrString): boolean;
  toUTC(date: DateOrString): Date;
  getWeekDayOrderWithTimezone(date: DateOrString, tz?: ITimezone): number;
}

export type DateOrString = Date | string;

export interface DateOrStringInterval {
  from: DateOrString;
  to: DateOrString;
}

export interface IDatesGetMonthNameOptionsView {
  case?: 'nominative' | 'genitive';
}
