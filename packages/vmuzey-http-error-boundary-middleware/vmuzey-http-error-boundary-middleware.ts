import { Responder } from '../common';
import { BusinessError } from '../errors';
import { IVmuzeyLoggerProvider } from '../vmuzey-logger-provider';

export class VmuzeyHttpErrorBoundaryMiddleware {
  constructor(private logger: IVmuzeyLoggerProvider) {}

  middleware() {
    return async (ctx: any, next) => {
      try {
        await next();
      } catch (e) {
        if (e.type === 'BUSINESS_ERROR') {
          return Responder.reply(ctx, e.httpCode, e.format());
        }

        if (e.type === 'VALIDATION_ERROR') {
          return Responder.reply(ctx, e.httpCode, e.format());
        }

        const code = this.normalizeErrorCode(e.code);

        if (e.isJson) {
          const message = this.handleJsonError(e);
          return Responder.reply(ctx, code, message);
        }

        if (code === 500) {
          const internallError = new BusinessError('ru', 'INTERNAL_ERROR');
          return Responder.reply(ctx, internallError.httpCode, internallError.format());
        }

        Responder.reply(ctx, code, e.message);
      }
    };
  }

  normalizeErrorCode(code: number | string): number {
    if (!code) return 500;
    if (typeof code === 'string') return 500;
    if ((typeof code === 'number' && code < 100) || code > 526) return 500;
    return code;
  }

  handleInternalError() {
    return {
      message: 'INTERNAL_ERROR',
    };
  }

  handleJsonError(e) {
    try {
      const parsedMessage = JSON.parse(e.message);

      return parsedMessage;
    } catch (err) {
      return e.message;
    }
  }
}
