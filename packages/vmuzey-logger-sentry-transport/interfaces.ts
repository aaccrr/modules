export interface IVmuzeyLoggerSentryTransportConfig {
  host: string;
  environment: 'local' | 'dev' | 'production';
}
