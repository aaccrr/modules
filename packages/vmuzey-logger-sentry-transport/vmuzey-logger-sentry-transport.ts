import * as Sentry from '@sentry/node';
import { IVmuzeyLoggerErrorContext, IVmuzeyLoggerTransport, IVmuzeyLogView } from '../vmuzey-logger-provider';
import { IVmuzeyLoggerSentryTransportConfig } from './interfaces';

export class VmuzeyLoggerSentryTransport implements IVmuzeyLoggerTransport {
  constructor(config: IVmuzeyLoggerSentryTransportConfig) {
    Sentry.init({
      environment: config.environment,
      maxBreadcrumbs: 20,
      normalizeDepth: 5,
      dsn: config.host,
      debug: true,
      beforeSend: (event) => {
        console.log('BEFORE_SEND HOOK CALLED');
        const fingerprint = this.prepareContextForFingerprint(event);
        console.log('ERROR FINGERPRINT ', JSON.stringify(fingerprint));
        event.fingerprint = ['{{ default }}', JSON.stringify(fingerprint)];
        return event;
      },
    });
  }

  async sendInfoLog(ctx: IVmuzeyLogView): Promise<void> {}

  async sendError(error: Error, meta: IVmuzeyLoggerErrorContext): Promise<void> {
    console.log('SENDING ERROR TO SENTRY');
    if (meta.payload) {
      if (meta.payload.headers) meta.payload.headers = JSON.stringify(meta.payload.headers);
      if (meta.payload.query) meta.payload.query = JSON.stringify(meta.payload.query);
      if (meta.payload.params) meta.payload.params = JSON.stringify(meta.payload.params);
      if (meta.payload.body) meta.payload.body = JSON.stringify(meta.payload.body);
    }

    if ((error as any).errors) error.message = (error as any).errors;

    Sentry.setContext('context', meta);
    Sentry.captureException(error);
  }

  private prepareContextForFingerprint(errorEvent: Sentry.Event): any {
    const fingerprint: any = {};

    const hasContext = !!errorEvent.contexts;

    if (hasContext) {
      const { context } = errorEvent.contexts;

      if (context.handlerTransportType) fingerprint.handlerTransportType = context.handlerTransportType;
      if (context.httpMethod) fingerprint.httpMethod = context.httpMethod;
      if (context.httpUrl) fingerprint.httpUrl = context.httpUrl;
      if (context.rpcMethod) fingerprint.rpcMethod = context.rpcMethod;
      if (context.topic) fingerprint.topic = context.topic;

      fingerprint.payload = this.prepareContextPayloadForFingerprint(context);
    }

    return fingerprint;
  }

  private prepareContextPayloadForFingerprint(context: Record<string, any>) {
    return {
      query: context.payload?.query || {},
      params: context.payload?.params || {},
      body: context.payload?.body || {},
    };
  }
}
