export const regionCenterGeo = {
  '101': 'ubfpc4f48',
  '102': 'uc2qyh89f',
  '103': 'ucuxr3xzs',
  '104': 'uc5vguqy8',
  '105': 'ufjhhppuf',
  '106': 'uc9y1kh51',
  '107': 'ufmgc9fzp',
  '108': 'uc1wr7j0b',
  '109': 'uck5kbdgg',
  '110': 'ucfv0jef5',
  '111': 'uc3tkf958',
  '112': 'ucsqb23br',
  '113': 'u9wz9k971',
  '114': 'ucm7yqp35',
  '115': 'uf1e3qwvm',
  '116': 'ucdu240qw',
  '117': 'ufhrg5m20',
  '118': 'ucfv0jdr1',
  '201': 'szutbbce1',
  '202': 'ubprrp02s',
  '203': 'ub58pp8vd',
  '204': 'v04ptqwuv',
  '205': 'ubxthsz73',
  '206': 'ubkhxy5zu',
  '301': 'ufbrmzjeq',
  '302': 'v4unuf3qn',
  '303': 'ugkz5hdfy',
  '304': 'ufs2g7v3f',
  '305': 'u3wx4xp87',
  '306': 'udtscze60',
  '307': 'usr82es0u',
  '308': 'udqjp9c3p',
  '309': 'udk0uwtek',
  '310': 'udtscze60',
  '311': 'vhj8y1kbb',
  '401': 'ygh2bk38q',
  '402': 'z92vdr1dz',
  '403': 'wztv311g5',
  '404': 'z085f0umr',
  '405': 'ybfwjgnkq',
  '406': 'z6e4zsp3y',
  '407': 'z0m7pvn30',
  '408': 'ybwt8dy9w',
  '409': 'zgw0ppuxv',
  '501': 'vcjps6dmu',
  '502': 'y3hwbhhyk',
  '503': 'y15nhzpmx',
  '504': 'y1918gg72',
  '505': 'vc7xd11r5',
  '506': 'y928n8rzb',
  '507': 'y1fn9dezy',
  '508': 'y361kythv',
  '509': 'vcv4yvnf5',
  '510': 'vcfcp5d4y',
  '511': 'v9u0vd905',
  '512': 'vfh3m49rh',
  '601': 'v3y7quy6b',
  '602': 'v6556g3gf',
  '603': 'v6nt4uep9',
  '604': 'v3g9v0p9c',
  '605': 'vdc542bg3',
  '606': 'v7z6edyv9',
  '701': 'v1xz3ecsv',
  '702': 'v4441y992',
  '703': 'v18hhny73',
  '704': 'v1gj0274h',
  '705': 'v4jg75uu5',
  '706': 'v1cx5pcfe',
  '707': 'v47m6554z',
  '708': 'ufp23qw98',
  '709': 'v1pnw3g7d',
  '710': 'v12n8hztf',
  '711': 'v4rfp8n52',
  '712': 'v17wtjz2h',
  '713': 'v10tpt374',
  '714': 'v1dmj0cme',
  '801': 'tp9u2wzy3',
  '802': 'szxvef682',
  '803': 'szxp2hw9z',
  '804': 'szvgsg380',
  '805': 'szxub9egh',
  '806': 'tp8qrk24j',
  '807': 'ubjb5r47y',
  '901': 'szbrb118s',
};
