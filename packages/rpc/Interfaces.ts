export interface IRpcServerUrl {
  name: string;
  url: string;
}

export interface IRpc {
  call(method: string, payload: any, options?: IRpcCallOptions);
}

export interface IRpcClient {
  pick(serverName: string): IRpc;
}

export interface IRpcClientOptions {
  services: string[];
}

export interface IRpcServerOptions {
  methods: any;
  port: number;
  cb?: () => void;
}

export interface IRpcCallOptions {
  throwError?: boolean;
}
