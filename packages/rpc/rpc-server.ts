import { Server } from 'jayson';
import { IRpcServerOptions } from './Interfaces';

export class RpcServer {
  private Server: Server;

  constructor(options: IRpcServerOptions) {
    if (!options.cb) options.cb = () => console.log('RPC RUNNING ON ', options.port);

    this.Server = new Server(options.methods);
    this.Server.http().listen(options.port, options.cb);
  }
}
