import { Client } from 'jayson';
import { IRpcCallOptions, IRpc, IRpcClient, IRpcClientOptions } from './Interfaces';

const DEFAULT_OPTS: IRpcCallOptions = {
  throwError: true,
};

class Rpc implements IRpc {
  private Client: Client;

  constructor(config) {
    this.Client = Client.http(config.host);
  }

  call(method: string, payload: any, options: IRpcCallOptions = DEFAULT_OPTS) {
    return new Promise((resolve, reject) => {
      this.Client.request(method, payload, (err, res) => {
        if (res && res.error && options.throwError) reject(res.error);
        else if (res && res.error && !options.throwError) resolve(res.error);
        else resolve(res && res.result);
      });
    });
  }
}

export class RpcClient implements IRpcClient {
  private Servers: any = {};

  constructor(options: IRpcClientOptions) {
    for (const service of options.services) {
      const host = `http://${service}:50051`;
      this.Servers[service] = new Rpc({ host });
    }
  }

  pick(serverName: string) {
    return this.Servers[serverName];
  }
}
