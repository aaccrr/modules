import { ITimezone } from '../globals';
import { IVmuzeyScheduledJobHandler } from './interfaces';

export abstract class VmuzeySchedulerCronJob<T> {
  timezone: ITimezone = 'Europe/Moscow';

  abstract payload: T;
  abstract name: string;
  abstract executionInterval: string;

  abstract getHandler(...args): IVmuzeyScheduledJobHandler<T>;
}
