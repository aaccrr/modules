import { ITraceModel } from '../globals';
import { IVmuzeyDependencyContainer } from '../vmuzey-dependency-container';
import { IVmuzeySchedulerTaskHandler } from './vmuzey-scheduler-task';

export interface IVmuzeyScheduledJobHandler<T> {
  (jobPayload: T, trace: ITraceModel): Promise<void>;
}

export interface IVmuzeySchedulerTaskHandlersMap {
  [key: string]: (dependencyContainer: IVmuzeyDependencyContainer) => IVmuzeySchedulerTaskHandler;
}
