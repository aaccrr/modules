import * as Agenda from 'agenda';
import { Nullable } from '../globals';
import { IVmuzeyDependencyContainer } from '../vmuzey-dependency-container';
import { IVmuzeyMongodbProvider } from '../vmuzey-mongodb-provider';
import { IVmuzeyObservabilityProvider, ObservabilityContextSourceTypeEnum, VmuzeyObservabilityContext } from '../vmuzey-observability';
import { IVmuzeySchedulerProvider } from './i-vmuzey-scheduler-provider';
import { IVmuzeySchedulerTaskHandlersMap } from './interfaces';
import { IVmuzeySchedulerTask, IVmuzeySchedulerTaskHandler } from './vmuzey-scheduler-task';

export class VmuzeySchedulerProvider implements IVmuzeySchedulerProvider {
  constructor(private MongodbProvider: IVmuzeyMongodbProvider) {}

  private scheduler: Agenda;

  private runningSchedulerJobWrapper =
    (jobName: string, jobHandler: IVmuzeySchedulerTaskHandler, observabilityProvider: IVmuzeyObservabilityProvider) => async (job) => {
      const span = observabilityProvider.tracer.startRootSpan(
        new VmuzeyObservabilityContext({
          sourceType: ObservabilityContextSourceTypeEnum.SCHEDULED_JOB,
          sourceName: jobName,
          payload: job.attrs.data,
        })
      );

      const trace = span.extractContext();

      try {
        const jobResult = await jobHandler(job.attrs.data, trace);
        span.withResponse(jobResult);
        job.disable();
      } catch (e) {
        span.withError(e);
        job.schedule('in 2 minutes');
        throw e;
      }
    };

  async init(): Promise<void> {
    this.scheduler = new Agenda({
      mongo: this.MongodbProvider.connection.db('scheduler'),
    });

    this.scheduler.on('start:PRINT_PURCHASE', (job) => {
      console.log(`job "PRINT_PURCHASE" starting for ${job.attrs.data.id}`);
    });

    this.scheduler.on('error', (err) => {
      console.log('agenda error', err);
    });

    await this.scheduler.start();
  }

  async start(dependencyContainer: IVmuzeyDependencyContainer, handlers: IVmuzeySchedulerTaskHandlersMap): Promise<void> {
    const observabilityProvider: IVmuzeyObservabilityProvider = (dependencyContainer as any).getObservabilityProvider();

    await Promise.all(
      Object.keys(handlers).map(async (handlerName) => {
        const handler = handlers[handlerName];

        await this.scheduler.define(handlerName, this.runningSchedulerJobWrapper(handlerName, handler(dependencyContainer), observabilityProvider));
      })
    );
  }

  async addJob(task: IVmuzeySchedulerTask): Promise<void> {
    await this.scheduler.schedule(task.getSchedule(), task.getName(), task.getPayload());
  }

  async cronJob(task: IVmuzeySchedulerTask): Promise<void> {
    await this.scheduler.every(task.getSchedule() as string, task.getName(), task.getPayload());
  }

  async findJob<T = {}>(task: IVmuzeySchedulerTask<T>): Promise<Nullable<Agenda.Job<T>>> {
    const jobSelector = this.prepareJobSelector(task.getPayload());
    const jobs = await this.scheduler.jobs({ name: task.getName(), ...jobSelector });
    return (jobs[0] as Agenda.Job<T>) || null;
  }

  async cancelJob(task: IVmuzeySchedulerTask): Promise<void> {
    const selector = this.prepareJobSelector(task.getPayload());
    await this.scheduler.cancel({ name: task.getName(), ...selector });
  }

  async runExpiredJobs(): Promise<void> {
    const expiredJobs = await this.scheduler.jobs({
      nextRunAt: {
        $lt: new Date(),
      },
    });

    for (const expiredJob of expiredJobs) {
      await expiredJob.schedule('in 20 seconds').save();
    }
  }

  private prepareJobSelector(jobSelector: any): any {
    return Object.keys(jobSelector).reduce((preparedSelector, selectorProp) => {
      preparedSelector[`data.${selectorProp}`] = jobSelector[selectorProp];
      return preparedSelector;
    }, {});
  }
}
