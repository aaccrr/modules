import { ITraceModel } from '../../globals';

export interface IVmuzeySchedulerTaskHandler<T = {}> {
  (jobPayload: T, trace: ITraceModel): Promise<void>;
}
