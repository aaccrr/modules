import { IDependencyContainer } from '../../globals';
import { IVmuzeySchedulerTaskHandler } from './interfaces';

export interface IVmuzeySchedulerTask<T = {}> {
  getSchedule(): Date | string;
  getPayload(): T;
  getName(): string;
}
