import { IVmuzeySchedulerTask } from './i-vmuzey-scheduler-task';

export abstract class VmuzeySchedulerTask<T> implements IVmuzeySchedulerTask<T> {
  protected name: string;
  protected payload: T;
  protected schedule: Date | string;

  getSchedule(): Date | string {
    return this.schedule;
  }

  getPayload(): T {
    return this.payload;
  }

  getName(): string {
    return this.name;
  }
}
