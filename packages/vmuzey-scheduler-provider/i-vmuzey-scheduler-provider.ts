import { Job } from 'agenda';
import { Nullable } from '../globals';
import { IVmuzeyDependencyContainer } from '../vmuzey-dependency-container';
import { IVmuzeySchedulerTaskHandlersMap } from './interfaces';
import { IVmuzeySchedulerTask } from './vmuzey-scheduler-task';

export interface IVmuzeySchedulerProvider {
  init(): Promise<void>;
  start(dependencyContainer: IVmuzeyDependencyContainer, handlers: IVmuzeySchedulerTaskHandlersMap): Promise<void>;
  addJob(task: IVmuzeySchedulerTask): Promise<void>;
  cronJob(task: IVmuzeySchedulerTask): Promise<void>;
  findJob<T = {}>(task: IVmuzeySchedulerTask<T>): Promise<Nullable<Job<T>>>;
  cancelJob(task: IVmuzeySchedulerTask): Promise<void>;
  runExpiredJobs(): Promise<void>;
}
