export interface IVmuzeySchedulerScheduledJob<T = {}> {
  name: string;
  payload: T;
  scheduledOn: Date;
}
