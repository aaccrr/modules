export interface IVmuzeyHelperMoney {
  minimal: string;
  toPenny(rubles: string): number;
  toRubles(penny: number): string;
  rublesToString(price: number): string;
  add(first: string, second: string): string;
  substract(from: string, amount: string): string;
  percentage(from: string, percent: number): string;
}
