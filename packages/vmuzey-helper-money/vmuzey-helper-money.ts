import { IVmuzeyHelperMoney } from './i-vmuzey-helper-money';

export class VmuzeyHelperMoney implements IVmuzeyHelperMoney {
  get minimal(): string {
    return '0.00';
  }

  // из рублей в копейки
  toPenny(rubles: string): number {
    const [r, k] = rubles.split('.');
    const value = { rubles: parseInt(r), penny: parseInt(k) };
    const penny = value.rubles * 100 + value.penny;
    return penny;
  }

  // из копеек в рубли
  toRubles(penny: number): string {
    return (penny / 100).toFixed(2);
  }

  // перевод исходной цены для категории в строку
  rublesToString(price: number): string {
    return price.toFixed(2);
  }

  add(first: string, second: string): string {
    const sum = this.toPenny(first) + this.toPenny(second);
    return this.toRubles(sum);
  }

  substract(from: string, amount: string): string {
    const diff = this.toPenny(from) - this.toPenny(amount);
    return this.toRubles(diff);
  }

  percentage(from: string, percent: number): string {
    const p = (this.toPenny(from) / 100) * percent;
    return this.toRubles(p);
  }
}
