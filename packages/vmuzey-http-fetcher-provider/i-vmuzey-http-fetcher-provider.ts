import { IFetcherResponse, IVmuzeyHttpFetcherRequestConfig } from './interfaces';

export interface IVmuzeyHttpFetcherProvider {
  request<T>(requestConfig: IVmuzeyHttpFetcherRequestConfig): Promise<IFetcherResponse<T>>;
}
