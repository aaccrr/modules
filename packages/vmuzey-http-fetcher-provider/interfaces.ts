export interface IVmuzeyHttpFetcherRequestConfig {
  method: 'get' | 'post' | 'put' | 'patch' | 'delete';
  url: string;
  auth?: IVmuzeyHttpFetcherAuthConfig;
  headers?: any;
  type?: 'JSON' | 'URL_ENCODED' | 'FORM_DATA';
  body?: any;
}

export interface IVmuzeyHttpFetcherAuthConfig {
  type: 'basic';
  credentials: {
    username: string;
    password: string;
  };
}

export interface IFetcherResponse<T> {
  status: number;
  data: T;
}
