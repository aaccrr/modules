import { URLSearchParams } from 'url';
import { default as fetch, Response } from 'node-fetch';
import { IFetcherResponse, IVmuzeyHttpFetcherAuthConfig, IVmuzeyHttpFetcherRequestConfig } from './interfaces';

export class VmuzeyHttpFetcherProvider implements VmuzeyHttpFetcherProvider {
  async request<T>(requestConfig: IVmuzeyHttpFetcherRequestConfig): Promise<IFetcherResponse<T>> {
    const response = await fetch(requestConfig.url, this.getRequestOptions(requestConfig));
    const responseBody = await this.parseResponse(response);

    if (!response.ok) {
      responseBody.message = response.statusText;
      throw responseBody;
    }

    return responseBody;
  }

  private getRequestOptions(requestConfig: IVmuzeyHttpFetcherRequestConfig): any {
    const requestPayload: any = {
      method: requestConfig.method,
      headers: requestConfig.headers || {},
      ...this.getBody(requestConfig),
    };

    if (requestConfig.auth) {
      requestPayload.headers['Authorization'] = this.getAuthHeader(requestConfig.auth);
    }

    return requestPayload;
  }

  private async parseResponse(response: Response): Promise<any> {
    const responseText = await response.text();

    try {
      const data = JSON.parse(responseText);
      return {
        status: response.status,
        data,
      };
    } catch (e) {
      return {
        status: response.status,
        data: responseText,
      };
    }
  }

  private getBody(requestConfig: IVmuzeyHttpFetcherRequestConfig): any {
    if (requestConfig.type === 'URL_ENCODED') {
      return this.prepareUrlEncoded(requestConfig);
    }

    if (requestConfig.type === 'FORM_DATA') {
      return this.prepareMultipart(requestConfig);
    }

    if (requestConfig.type === 'JSON') {
      return {
        body: requestConfig.body,
      };
    }

    return {
      body: JSON.stringify(requestConfig.body),
    };
  }

  private prepareUrlEncoded(requestConfig: IVmuzeyHttpFetcherRequestConfig): any {
    const body = new URLSearchParams();

    Object.keys(requestConfig.body).forEach((key) => {
      const value = requestConfig.body[key];
      body.append(key, value);
    });

    return {
      body,
    };
  }

  private prepareMultipart(requestConfig: IVmuzeyHttpFetcherRequestConfig): any {
    return {
      form: requestConfig.body,
    };
  }

  private getAuthHeader(authConfig: IVmuzeyHttpFetcherAuthConfig): string {
    if (authConfig.type === 'basic') return this.getBasicAuthHeader(authConfig);
  }

  private getBasicAuthHeader(authConfig: IVmuzeyHttpFetcherAuthConfig): string {
    return `Basic ${Buffer.from(`${authConfig.credentials.username}:${authConfig.credentials.password}`).toString('base64')}`;
  }
}
