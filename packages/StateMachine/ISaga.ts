export interface IFSM<T, S, U, Z> {
  services: U;
  payload: T;
  start(payload: IFSMStartPayload<T, S, U>): Promise<S>;
  transition(nextBlock: string, ...args): Promise<any>;
  fail(code: number, message, isJson?: boolean);
  success(res?: S): void;
  state: Z;
  helpers: any;
}

export interface IFSMOptions<T, S, U, Z> {
  services?: U;
  initialBlock: string;
  blocks: IFSMBlock<T, S, U, Z>;
  rollbacks?: IFSMBlock<T, S, U, Z>;
  helpers?: any;
}

export interface IFSMBlock<T, S, U, Z> {
  [blockName: string]: (ctx: IFSM<T, S, U, Z>, input?: any) => Promise<any>;
}

export interface IFSMService {
  [serviceName: string]: any;
}

export interface IFSMStartPayload<T, S, U> {
  payload: T;
  services: U;
}
