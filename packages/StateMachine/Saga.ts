import { IFSM, IFSMOptions, IFSMService, IFSMStartPayload } from './ISaga';

export class StateMachine<T, S, U, Z> implements IFSM<T, S, U, Z> {
  public services: U;
  public payload: T;
  public state: Z;
  public helpers: any;

  private initialBlock: string;
  private blocks: any;
  private rollbacks: any;
  private resolver: any;
  private rejecter: any;
  private pastBlocks: any = {};

  constructor(options: IFSMOptions<T, S, U, Z>) {
    this.blocks = options.blocks;
    this.rollbacks = options.rollbacks || {};
    this.initialBlock = options.initialBlock;
    this.helpers = options.helpers || {};
    this.setServices(options.services);
    (this.state as any) = {};
  }

  private setServices(services: IFSMService = {}): void {
    (this.services as any) = {};
    Object.keys(services).forEach((service) => {
      this.services[service] = services[service];
    });
  }

  start(p: IFSMStartPayload<T, S, U>): Promise<S> {
    return new Promise(async (resolve, reject) => {
      this.setServices(p.services);
      this.payload = p.payload;
      this.resolver = resolve;
      this.rejecter = reject;
      await this.transition(this.initialBlock);
    });
  }

  success(res: S = {} as any): void {
    this.resolver(res);
  }

  fail(e): void {
    this.rejecter(e);
  }

  async transition(blockName: string, ...args): Promise<any> {
    try {
      const block = this.blocks[blockName];
      if (!block) throw new Error(`next block "${blockName}" not found`);
      this.pastBlocks[blockName] = { args };
      await block(this, ...args);
    } catch (e) {
      // console.log(`error in block "${blockName}"`, e);
      await this.runRollback();
      this.rejecter(e);
    }
  }

  private async runRollback(): Promise<void> {
    const pastBlocks = Object.keys(this.pastBlocks).reverse();
    console.log('PAST_BLOCKS ', pastBlocks);

    for (const block of pastBlocks) {
      const rollback = this.rollbacks[block];
      if (!rollback) continue;
      await rollback(this, this.pastBlocks[block].args);
    }
  }
}
