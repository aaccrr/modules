import { ITraceModel } from '../globals';

export class VmuzeyDataCollector {
  protected trace: ITraceModel = {};

  setTrace(trace: ITraceModel): void {
    this.trace = trace;
  }
}
