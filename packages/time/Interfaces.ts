export interface ITime {
  between(time: string, range: string[]): boolean;
  greater(time: string, comparer: string): boolean;
  less(time: string, comparer: string): boolean;
  equal(time: string, comparer: string): boolean;
  equalOrGreater(time: string, comparer: string): boolean;
  equalOrLess(time: string, comparer: string): boolean;
  objToTime(obj: ITimeObj, dayHalf?: DayHalfEnum): string;
  format(time: string, pattern: string): string;
  getMin(times: string[]): string;
  getMax(times: string[]): string;
  fromSeconds(timeInSeconds: number): string;
  toSeconds(time: string): number;
}

export interface ITimeObj {
  hour: any;
  minute: any;
}

export type DayHalfEnum = 'first' | 'last';
