import { isGT, isLT, isEQ, format, elapsedSeconds } from '@tuxsudo/time';
import { DayHalfEnum, ITime, ITimeObj } from './Interfaces';

export class Time implements ITime {
  between(time: string, range: string[]): boolean {
    const lessBound = this.equalOrGreater(time, range[0]);
    const greatBound = this.equalOrLess(time, range[1]);
    return lessBound && greatBound ? true : false;
  }

  greater(time: string, comparer: string): boolean {
    return isGT(comparer, time);
  }

  less(time: string, comparer: string): boolean {
    return isLT(comparer, time);
  }

  equal(time: string, comparer: string): boolean {
    return isEQ(comparer, time);
  }

  equalOrGreater(time: string, comparer: string): boolean {
    return this.equal(time, comparer) || this.greater(time, comparer);
  }

  equalOrLess(time: string, comparer: string): boolean {
    return this.equal(time, comparer) || this.less(time, comparer);
  }

  objToTime(dayTimeObj: ITimeObj, dayHalf: DayHalfEnum): string {
    return `${this.hourToString(dayTimeObj.hour, dayHalf)}:${this.minuteToString(dayTimeObj.minute)}`;
  }

  private hourToString(hour: number, dayHalf: DayHalfEnum = 'last'): string {
    if (hour === 0 && dayHalf === 'first') return '00';
    if (hour === 0 && dayHalf === 'last') return '00';

    if (hour.toString().length === 1) return '0' + hour.toString();
    return hour.toString();
  }

  private minuteToString(minute: number): string {
    if (minute === 0) return '00';

    if (minute.toString().length === 1) return '0' + minute.toString();
    return minute.toString();
  }

  format(time: any, pattern: string) {
    return format(time, pattern);
  }

  getMax(times: string[]): string {
    const timesInSeconds = times.map((time) => this.toSeconds(time));
    const maxTimeInSeconds = Math.max(...timesInSeconds);
    return this.fromSeconds(maxTimeInSeconds);
  }

  getMin(times: string[]): string {
    const timesInSeconds = times.map((time) => this.toSeconds(time));
    const minTimeInSeconds = Math.min(...timesInSeconds);
    return this.fromSeconds(minTimeInSeconds);
  }

  toSeconds(time: string): number {
    return elapsedSeconds(time);
  }

  fromSeconds(timeInSeconds: number): string {
    const hours = Math.floor(timeInSeconds / 60 / 60);
    const minutes = Math.floor(timeInSeconds / 60) % 60;

    return `${this.normalizeTimeUnit(hours)}:${this.normalizeTimeUnit(minutes)}`;
  }

  private normalizeTimeUnit(timeUnit: number): string {
    if (timeUnit.toString().length === 1) return `0${timeUnit}`;
    return timeUnit.toString();
  }
}
