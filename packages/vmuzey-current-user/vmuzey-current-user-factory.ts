import { IRequestUser } from '../globals';
import { IAccountingAppModuleFacade } from '../vmuzey-app-module-facade-provider';
import { IVmuzeyCurrentUser } from './i-vmuzey-current-user';
import { VmuzeyCurrentUser } from './vmuzey-current-user';

export class VmuzeyCurrentUserFactory {
  constructor(private AccountsModuleFacade: IAccountingAppModuleFacade) {}

  async create(currentUserModel: IRequestUser): Promise<IVmuzeyCurrentUser> {
    if (currentUserModel.v === 2) {
      return new VmuzeyCurrentUser({
        id: currentUserModel.id,
        role: currentUserModel.role,
        status: '' as any,
        administeredMuseums: currentUserModel.m as string[],
        organizationName: '',
        organization: {
          isExists: true,
          summary: {
            id: currentUserModel.organizationId,
            isConfirmed: true,
            isOfflineSalesEnabled: false,
            hasMuseum: true,
            hasController: true,
            hasGuide: false,
            automaticRefundApprovalEnabled: true,
          },
        },
      });
    } else {
      const currentUserInfo = await this.AccountsModuleFacade.getCurrentUserInfo({
        body: {
          currentUserId: currentUserModel.id,
        },
        trace: {},
      });

      return new VmuzeyCurrentUser(currentUserInfo);
    }
  }
}
