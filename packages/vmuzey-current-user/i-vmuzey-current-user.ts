export interface IVmuzeyCurrentUser {
  isPrimaryAdmin(): boolean;
  isAdmin(): boolean;
  isBooker(): boolean;

  isConfirmed(): boolean;

  getId(): string;
  getOrganizationId(): string;
  getOrganizationName(): string;
  getAdministeredMuseumsList(): string[];
  hasAccessToMuseum(museumId: string): boolean;
  hasConfirmedOrganization(): boolean;
  organizationHasMuseum(): boolean;
  organizationHasController(): boolean;
}
