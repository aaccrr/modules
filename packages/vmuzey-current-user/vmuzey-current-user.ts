import { AdminAccountStatus, ICurrentUserInfoView, UserRole } from '../globals';
import { IVmuzeyCurrentUser } from './i-vmuzey-current-user';

export class VmuzeyCurrentUser implements IVmuzeyCurrentUser {
  constructor(private currentUserInfo: ICurrentUserInfoView) {}

  isPrimaryAdmin(): boolean {
    return this.currentUserInfo.role === UserRole.PRIMARY_MANAGER;
  }

  isAdmin(): boolean {
    return this.currentUserInfo.role === UserRole.MANAGER;
  }

  isBooker(): boolean {
    return this.currentUserInfo.role === UserRole.BOOKER;
  }

  isConfirmed(): boolean {
    return this.currentUserInfo.status === AdminAccountStatus.CONFIRMED;
  }

  getId(): string {
    return this.currentUserInfo.id;
  }

  getOrganizationId(): string {
    return this.currentUserInfo.organization.summary.id;
  }

  getOrganizationName(): string {
    return this.currentUserInfo.organizationName;
  }

  getAdministeredMuseumsList(): string[] {
    return this.currentUserInfo.administeredMuseums || [];
  }

  hasConfirmedOrganization(): boolean {
    return this.currentUserInfo.organization.summary.isConfirmed;
  }

  hasAccessToMuseum(museumId: string): boolean {
    if (this.isPrimaryAdmin()) {
      return true;
    }

    if (this.isAdmin()) {
      return this.getAdministeredMuseumsList().includes(museumId);
    }

    return false;
  }

  organizationHasMuseum(): boolean {
    return this.currentUserInfo.organization.summary.hasMuseum;
  }

  organizationHasController(): boolean {
    return this.currentUserInfo.organization.summary.hasController;
  }
}
