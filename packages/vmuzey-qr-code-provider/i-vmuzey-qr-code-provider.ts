export interface IVmuzeyQrCodeProvider {
  generate(codePayload: string): Promise<Buffer>;
}
