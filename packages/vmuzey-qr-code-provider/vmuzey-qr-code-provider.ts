import * as qr from 'qrcode';
import { IVmuzeyQrCodeProvider } from './i-vmuzey-qr-code-provider';

export class VmuzeyQrCodeProvider implements IVmuzeyQrCodeProvider {
  async generate(codePayload: string): Promise<Buffer> {
    const qrBuffer = await qr.toBuffer(codePayload, { margin: 2 });
    return qrBuffer;
  }
}
