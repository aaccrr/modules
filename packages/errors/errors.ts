import { AuthenticationError } from './ErrorTypes/AuthenticationError';
import { BadRequestError } from './ErrorTypes/BadRequest';
import { AccessError } from './ErrorTypes/AccessError';
import { InternalError } from './ErrorTypes/InternalError';
import { NotFoundError } from './ErrorTypes/NotFound';
import { TooManyRequests } from './ErrorTypes/TooManyRequests';
import { Conflict } from './ErrorTypes/Conflict';
import { IErrors } from './Interfaces';

export class Errors implements IErrors {
  create(code: number, message: string | object): void {
    if (code === 401) this.AuthenticationError(message);
    if (code === 403) this.AccessError(message);
    if (code === 400) this.BadRequestError(message);
    if (code === 404) this.NotFoundError(message);
    if (code === 429) this.TooManyRequests(message);
    if (code === 409) this.Conflict(message);

    this.InternalError(message);
  }

  AuthenticationError(message: string | object): void {
    throw new AuthenticationError(message);
  }

  AccessError(message: string | object): void {
    throw new AccessError(message);
  }

  BadRequestError(message: string | object): void {
    throw new BadRequestError(message);
  }

  InternalError(message: string | object): void {
    throw new InternalError(message);
  }

  NotFoundError(message: string | object): void {
    throw new NotFoundError(message);
  }

  TooManyRequests(message: string | object): void {
    throw new TooManyRequests(message);
  }

  Conflict(message: string | object): void {
    throw new Conflict(message);
  }
}
