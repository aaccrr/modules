import { IBusinessErrorView } from './Interfaces';
import { errors } from './ErrorsList';

export const getBusinessErrorByCode = (errorCode: string): IBusinessErrorView => {
  return {
    type: 'BUSINESS_ERROR',
    code: errorCode,
    message: errors[errorCode].message['ru'](),
  };
};
