import { errors } from './ErrorsList';
import { IBusinessErrorView } from './Interfaces';

type LocaleEnum = 'ru';

export class BusinessError extends Error {
  type = 'BUSINESS_ERROR';
  httpCode: number;
  message: string;
  code: string;
  params: any;
  locale: LocaleEnum = 'ru';

  constructor(locale: LocaleEnum, code: string, params?: any) {
    super();

    const errorStruct = errors[code];

    this.message = errorStruct.message[locale](params);
    this.httpCode = errorStruct.httpCode;
    this.code = code;
    this.locale = locale;

    if (params) this.params = params;
  }

  format(): IBusinessErrorView {
    const errorView: IBusinessErrorView = {
      type: this.type,
      message: this.message,
      code: this.code,
    };

    if (this.params) errorView.params = this.params;

    return errorView;
  }

  formatWithLocale(): IBusinessErrorView {
    const errorView: IBusinessErrorView = {
      locale: this.locale,
      type: this.type,
      message: this.message,
      code: this.code,
      stacktrace: this.stack,
    };

    if (this.params) errorView.params = this.params;

    return errorView;
  }
}
