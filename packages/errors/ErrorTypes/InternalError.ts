export class InternalError extends Error {
  code: number;
  isJson: boolean;
  message: string;

  constructor(message: string | object) {
    super();
    this.isJson = typeof message !== 'string';
    this.message = typeof message !== 'string' ? JSON.stringify(message) : message;
    this.code = 500;
  }
}
