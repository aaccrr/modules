class BaseError {
  protected locale;

  setLocale(locale) {
    this.locale = locale;
  }
}

const errorInstance = new BaseError();
export { errorInstance as BaseError };
