import { Errors } from './errors';

const errors = new Errors();

export { errors as Errors };
export { IErrors } from './Interfaces';
export * from './BusinessError';
export * from './ValidationError';
export * from './getBusinessErrorByCode';
