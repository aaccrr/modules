export interface IErrors {
  create(code: number, message: string | object): void;

  AuthenticationError(message: string | object): void;
  AccessError(message: string | object): void;
  BadRequestError(message: string | object): void;
  InternalError(message: string | object): void;
  NotFoundError(message: string | object): void;
  TooManyRequests(message: string | object): void;
  Conflict(message: string | object): void;
}

export interface IBusinessErrorView {
  type: string;
  message: string;
  code: string;
  stacktrace?: string;
  locale?: string;
  params?: any;
}
