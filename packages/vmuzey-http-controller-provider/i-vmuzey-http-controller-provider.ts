import { OpenAPIV3 } from 'openapi-types';
import { RequestPayload } from '../globals';
import { RouteMiddleware } from '../http-router';
import { IHttpControllerCallPayload } from './intefaces';

export interface IVmuzeyHttpControllerProvider<T = {}, R = {}> {
  initializeRequestValidator(): void;
  getInputRequestMiddleware(): RouteMiddleware[];
  handleInputRequest(request: RequestPayload): Promise<IHttpControllerCallPayload<T>>;
  getOpenapiMetadata(): OpenAPIV3.OperationObject;
}
