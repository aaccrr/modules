import { OpenAPIV3 } from 'openapi-types';
import { getRequestMeta } from '../common';
import { RequestPayload } from '../globals';
import { RouteMiddleware } from '../http-router';
import { IVmuzeyGuard } from '../vmuzey-guard';
import { VmuzeyHttpAdapter } from '../vmuzey-http-controller-adapter-container';
import { IVmuzeyHttpMiddleware } from '../vmuzey-http-server-provider';
import { VmuzeyOpenapiSchemaValidator } from '../vmuzey-openapi-schema-validator';
import { IVmuzeyOpenapiSchemasContainer } from '../vmuzey-openapi-schemas-container';
import { IVmuzeyHttpControllerProvider } from './i-vmuzey-http-controller-provider';
import { IHttpControllerCallPayload } from './intefaces';

export abstract class VmuzeyHttpControllerProvider<T = any> implements IVmuzeyHttpControllerProvider<T> {
  protected inputRequestMiddleware: IVmuzeyHttpMiddleware[] = [];
  protected abstract requestValidatorContainer: IVmuzeyOpenapiSchemasContainer;
  protected abstract requestAdapter: VmuzeyHttpAdapter<T>;
  protected requestGuards: IVmuzeyGuard[] = [];

  private validator: VmuzeyOpenapiSchemaValidator;

  initializeRequestValidator(): void {
    this.validator = new VmuzeyOpenapiSchemaValidator(this.requestValidatorContainer.getRequestSchema());
  }

  getInputRequestMiddleware(): RouteMiddleware[] {
    return this.inputRequestMiddleware.map((inputRequestMiddleware) => inputRequestMiddleware.middleware());
  }

  async handleInputRequest(request: RequestPayload): Promise<IHttpControllerCallPayload<T>> {
    this.cleanOpenapiUnsupportedContentType(request);

    this.validator.validate({
      query: request.request.query,
      params: request.params,
      body: request.request.body,
      headers: request.headers,
    });

    const requestPayload: IHttpControllerCallPayload<T> = {
      body: this.requestAdapter(request),
      meta: getRequestMeta(request),
      currentUser: request.state.currentUser,
      trace: request.state.trace,
    };

    await this.runRequestGuards(requestPayload);

    return requestPayload;
  }

  getOpenapiMetadata(): OpenAPIV3.OperationObject {
    return this.requestValidatorContainer.getControllerMetadata();
  }

  private async runRequestGuards(requestPayload: IHttpControllerCallPayload<T>): Promise<void> {
    for (const guard of this.requestGuards) {
      await guard.validate(requestPayload);
    }
  }

  private cleanOpenapiUnsupportedContentType(request: RequestPayload): void {
    const contentType = request.headers['content-type'] as string;

    if (typeof contentType === 'string') {
      if (contentType.includes('x-www-form-urlencoded')) {
        delete request.headers['content-type'];
        return;
      }

      if (contentType.includes('multipart/form-data')) {
        delete request.headers['content-type'];
        return;
      }
    }
  }
}
