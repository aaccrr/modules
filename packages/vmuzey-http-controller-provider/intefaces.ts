import { IRequestMeta, ITraceModel } from '../globals';
import { IVmuzeyCurrentUser } from '../vmuzey-current-user';

export interface IHttpControllerCallPayload<T> {
  body: T;
  currentUser: IVmuzeyCurrentUser;
  meta: IRequestMeta;
  trace: ITraceModel;
  handler?: IHttpHandlerProperties;
}

export interface IHttpHandlerProperties {
  method: string;
  path: string;
}

export interface IHttpControllerOptions {
  responderOptions?: IHttpControllerResponseOptions;
}

export interface IHttpControllerResponseOptions {
  format: 'JSON' | 'XML';
  withSuccess: boolean;
}
