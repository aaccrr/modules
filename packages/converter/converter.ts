import * as xmlToJson from 'xml-js';
import * as showndown from 'showdown';
import { IConverter } from './Interfaces';

export class Converter implements IConverter {
  private markdownConverter: showndown.Converter;

  constructor() {
    this.markdownConverter = new showndown.Converter();
  }

  jsonToXml(json: object) {
    const xml = xmlToJson.json2xml(JSON.stringify(json), { compact: true });
    return xml;
  }

  markdownToHtml(markdown: string): string {
    return this.markdownConverter.makeHtml(markdown);
  }
}
