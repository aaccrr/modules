export interface IConverter {
  jsonToXml(json: object): any;
  markdownToHtml(markdown: string): string;
}
