import { Converter } from './converter';

const Module = new Converter();

export { Module as Converter };
export * from './Interfaces';
