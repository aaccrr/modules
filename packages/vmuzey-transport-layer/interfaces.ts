export interface ITransportMessage<T = any> {
  address: string;
  payload: ITransportMessagePayload<T>;
}

export interface ITransportMessagePayload<T = any> {
  body: T;
  trace: any;
}
