import { IVmuzeyDependencyContainer } from './i-vmuzey-dependency-container';

export abstract class VmuzeyDependencyContainer implements IVmuzeyDependencyContainer {
  abstract requiredDependencies: string[];

  async init(): Promise<void> {
    for (const requiredDependency of this.requiredDependencies) {
      const dependency = this[requiredDependency];

      if (!dependency) {
        throw Error(`DEPENDENCY_MISSED "${requiredDependency}"`);
      }

      const needInitialization = typeof dependency.init === 'function';

      if (needInitialization) {
        await dependency.init();
      }
    }
  }
}
