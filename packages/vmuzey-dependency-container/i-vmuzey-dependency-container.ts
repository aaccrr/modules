export interface IVmuzeyDependencyContainer {
  requiredDependencies: string[];

  init(): Promise<void>;
}
