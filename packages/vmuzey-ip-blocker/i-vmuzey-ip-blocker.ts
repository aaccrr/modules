import { IpBlockerMethodsEnum } from './intefaces';

export interface IVmuzeyIpBlocker {
  checkIpBlock(method: IpBlockerMethodsEnum, ip: string): Promise<void>;
  updateLockCounter(method: IpBlockerMethodsEnum, ip: string): Promise<void>;
}
