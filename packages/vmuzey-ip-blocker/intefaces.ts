export enum IpBlockerMethodsEnum {
  FAILED_CONTROLLER_LOGIN_ATTEMPT = 'FAILED_CONTROLLER_LOGIN_ATTEMPT',
  FAILED_CLIENT_LOGIN_ATTEMPT = 'FAILED_CLIENT_LOGIN_ATTEMPT',
  FAILED_REFUND_AVAILABILITY_CHECK = 'FAILED_REFUND_AVAILABILITY_CHECK',
  FAILED_REFUND_INITIATOR_PROFILE_CHECK = 'FAILED_REFUND_INITIATOR_PROFILE_CHECK',
  FAILED_REFUND_ATTEMPT = 'FAILED_REFUND_ATTEMPT',
  FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT = 'FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT',
}
