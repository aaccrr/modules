import { BusinessError } from '../errors';
import { IVmuzeyRedisProvider } from '../vmuzey-redis-provider';
import { IVmuzeyIpBlocker } from './i-vmuzey-ip-blocker';
import { IpBlockerMethodsEnum } from './intefaces';

export class VmuzeyIpBlocker implements IVmuzeyIpBlocker {
  constructor(private redis: IVmuzeyRedisProvider) {}
  private settings = {
    FAILED_CONTROLLER_LOGIN_ATTEMPT: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'CONTROLLER_LOGIN_ATTEMPTS_EXCEEDED',
    },
    FAILED_CLIENT_LOGIN_ATTEMPT: {
      maxFails: 7,
      timeout: 30,
      errorCode: 'CLIENT_LOGIN_ATTEMPTS_EXCEEDED',
    },
    FAILED_REFUND_AVAILABILITY_CHECK: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'REFUND_AVAILABILITY_CHECK_ATTEMPTS_EXCEEDED',
    },
    FAILED_REFUND_INITIATOR_PROFILE_CHECK: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'REFUND_INITIATOR_PROFILE_CHECK_ATTEMPTS_EXCEEDED',
    },
    FAILED_REFUND_ATTEMPT: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'REFUND_CREATION_ATTEMPTS_EXCEEDED',
    },
    FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT: {
      maxFails: 5,
      timeout: 30,
      errorCode: 'FAILED_CONTROLLER_ORGANIZATION_LOGIN_ATTEMPT',
    },
  };
  private prefix = 'ip-blocker';

  // --- Методы для проверки блокировки ip-адреса

  async checkIpBlock(method: IpBlockerMethodsEnum, ip: string): Promise<void> {
    const lockCounter = await this.getLockCounter(method, ip);

    if (!lockCounter) return;
    if (this.isLockCounterFull(method, lockCounter)) {
      const { errorCode, timeout } = this.settings[method];
      throw new BusinessError('ru', errorCode, { timeout });
    }
  }

  // ---

  async updateLockCounter(method: IpBlockerMethodsEnum, ip: string): Promise<void> {
    const lockCounter = await this.getLockCounter(method, ip);

    if (lockCounter) {
      if (this.isLockCounterFull(method, lockCounter)) return;
      await this.incrementLockCounter(method, ip, lockCounter);
    } else {
      await this.addNewLockCounter(method, ip);
    }
  }

  private async getLockCounter(method: IpBlockerMethodsEnum, ip: string): Promise<number> {
    const lockCounter = await this.redis.db.get(`${this.prefix}:${method}:${ip}`);
    if (lockCounter === null) return;
    return parseInt(lockCounter);
  }

  private isLockCounterFull(method: IpBlockerMethodsEnum, lockCounter: number): boolean {
    const lockCounterMaximum = this.settings[method].maxFails;
    return lockCounter >= lockCounterMaximum;
  }

  private async incrementLockCounter(method: IpBlockerMethodsEnum, ip: string, lockCounter: number): Promise<void> {
    const remainingLockTime = await this.redis.db.ttl(`${this.prefix}:${method}:${ip}`);
    await this.redis.methods.set(`${this.prefix}:${method}:${ip}`, lockCounter + 1, remainingLockTime);
  }

  private async addNewLockCounter(method: IpBlockerMethodsEnum, ip: string): Promise<void> {
    const lockPeriodInSeconds = this.settings[method].timeout * 60;
    await this.redis.methods.set(`${this.prefix}:${method}:${ip}`, 1, lockPeriodInSeconds);
  }
}
