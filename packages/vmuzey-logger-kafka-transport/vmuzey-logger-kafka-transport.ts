import { IVmuzeyBrokerProducerProvider } from '../vmuzey-broker-producer-provider';
import { IVmuzeyLoggerErrorContext, IVmuzeyLoggerTransport, IVmuzeyLogView } from '../vmuzey-logger-provider';

export class VmuzeyLoggerKafkaTransport implements IVmuzeyLoggerTransport {
  constructor(private ProducerProvider: IVmuzeyBrokerProducerProvider) {}

  async sendInfoLog(log: IVmuzeyLogView): Promise<void> {
    await this.ProducerProvider.send({ topic: 'API_MONITORING', message: log });
  }

  async sendError(error: Error, context: IVmuzeyLoggerErrorContext): Promise<void> {}
}
