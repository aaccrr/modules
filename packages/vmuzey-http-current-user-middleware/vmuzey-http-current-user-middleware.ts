import { getRequestMeta } from '../common';
import { RouteMiddleware } from '../http-router';
import { IAccountingAppModuleFacade } from '../vmuzey-app-module-facade-provider';
import { VmuzeyCurrentUserFactory } from '../vmuzey-current-user';
import { IVmuzeyHttpMiddleware } from '../vmuzey-http-server-provider';

export class VmuzeyHttpCurrentUserMiddleware implements IVmuzeyHttpMiddleware {
  constructor(private AccountingModuleFacade: IAccountingAppModuleFacade) {}

  middleware(): RouteMiddleware {
    const currentUserFactory = new VmuzeyCurrentUserFactory(this.AccountingModuleFacade);

    return async (ctx, next) => {
      const requestMeta = getRequestMeta(ctx);

      const currentUser = await currentUserFactory.create(requestMeta.user);

      ctx.state.currentUser = currentUser;

      await next();
    };
  }
}
