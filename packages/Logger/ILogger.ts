export interface ILogger {
  connect(credentials: any): Promise<void>;
  info(info: IInfoLog): Promise<void>;
  error(error: IErrorLog): Promise<void>;
}

export type HandlerType = 'http' | 'rpc' | 'broker';
export type ActionType = 'request' | 'response';

export interface IErrorLog {
  handlerType: HandlerType;
  method: string;
  stacktrace: string;
  error: any;
}

export interface IInfoLog {
  handlerType: HandlerType;
  actionType: ActionType;
  method: string;
  data: any;
}
