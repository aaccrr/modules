import { IElasticsearchConnector, ConnectorFactory } from '../connectors';
import { ILogger, IErrorLog, IInfoLog } from './ILogger';
import { Random } from '../random';

export class Logger implements ILogger {
  private store: IElasticsearchConnector;

  async connect(credentials: any): Promise<void> {
    this.store = await ConnectorFactory.create<IElasticsearchConnector>('elasticsearch', { credentials });
  }

  async info(info: IInfoLog): Promise<void> {
    const log = { type: 'INFO', timestamp: new Date(), ...info };
    // if (process.env.NODE_ENV !== 'production')
    console.log(JSON.stringify(log));

    // await this.sendInfo(log);
  }

  async error(error: IErrorLog): Promise<void> {
    const log = { type: 'ERROR', timestamp: new Date(), ...error };
    // if (process.env.NODE_ENV !== 'production')
    console.log(JSON.stringify(log));

    // await this.sendError(log);
  }

  private async sendError(log): Promise<void> {
    await this.store.db.create({
      id: Random.generate(),
      index: 'error-log',
      body: log,
    });
  }

  private async sendInfo(log): Promise<void> {
    await this.store.db.create({
      id: Random.generate(),
      index: 'info-log',
      body: log,
    });
  }
}
