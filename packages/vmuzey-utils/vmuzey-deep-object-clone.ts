import { default as cloneDeep } from 'lodash.clonedeep';

export const deepObjectClone = (obj: any) => {
  return cloneDeep(obj);
};
