import { IJsonRpcCallOptions } from '../JsonRpcProvider';
import { IVmuzeyLoggerProvider } from '../vmuzey-logger-provider';
import { IVmuzeyRpcClientProvider } from './i-vmuzey-rpc-client-provider';

export class VmuzeyRpcClientProviderStub implements IVmuzeyRpcClientProvider {
  constructor(private LoggerProvider: IVmuzeyLoggerProvider, private requiredServices: string[]) {}

  init(): void {}

  async call(options: IJsonRpcCallOptions): Promise<any> {
    return {};
  }
}
