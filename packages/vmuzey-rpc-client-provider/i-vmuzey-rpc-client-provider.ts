import { IJsonRpcCallOptions } from '../JsonRpcProvider';

export interface IVmuzeyRpcClientProvider {
  init(): void;
  call(options: IJsonRpcCallOptions): Promise<any>;
}
