import { ITimezone } from '../globals';

export interface IGetMonthNameOptions {
  case?: 'nominative' | 'genitive';
}

export interface IVmuzeyTimezonedDateOptions {
  timezone?: ITimezone;
  subTimezone?: boolean;
}
