import * as moment from 'moment';
import * as timezonedMoment from 'moment-timezone';
import { Interval, eachDayOfInterval } from 'date-fns';
import { ITimezone, TimezoneOffset } from '../globals';
import { IGetMonthNameOptions, IVmuzeyTimezonedDateOptions } from './interfaces';

type DateOrString = Date | string | number;

export class VmuzeyTimezonedDateHelper {
  constructor(private date: DateOrString, private options?: IVmuzeyTimezonedDateOptions) {
    if (!this.options) {
      this.options = {};
    }

    if (typeof this.options.subTimezone !== 'boolean') {
      this.options.subTimezone = true;
    }

    this.normalizeOriginDateOnInit();
  }

  private get isOriginTimezonedString(): boolean {
    const TIMEZONED_STRING_REGEXP_PATTERN = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}(\+|\-)\d{2}:\d{2})$/;
    const regexp = new RegExp(TIMEZONED_STRING_REGEXP_PATTERN);
    const stringifiedOrigin = this.date.toString();
    return regexp.test(stringifiedOrigin);
  }

  private get hasTimezoneOption(): boolean {
    return this.options.timezone !== undefined;
  }

  private get substractTimezoneOffsetOnInit(): boolean {
    return this.hasTimezoneOption && this.options.subTimezone;
  }

  static getHourOffsetByTimezone(timezone: ITimezone): number {
    return TimezoneOffset[timezone];
  }

  static getMin(dates: DateOrString[]): VmuzeyTimezonedDateHelper {
    const normalizedDates = dates.map((date) => moment(date));
    const minDate = moment.min(normalizedDates);
    console.log('MIN_DATE ', minDate.format());
    return new VmuzeyTimezonedDateHelper(minDate.format());
  }

  static getMax(dates: DateOrString[]): VmuzeyTimezonedDateHelper {
    const normalizedDates = dates.map((date) => moment(date));
    const maxDate = moment.max(normalizedDates);
    console.log('MAX_DATE ', maxDate.format());
    return new VmuzeyTimezonedDateHelper(maxDate.format());
  }

  getUnixTimestamp(): number {
    return this.wrapDate().unix();
  }

  getDate(): Date {
    return this.date as Date;
  }

  getWeekdayOrder(): number {
    return this.wrapDate().weekday() + 1;
  }

  getYear(): number {
    return this.wrapDate().get('year');
  }

  getMonthName(options: IGetMonthNameOptions): string {
    const MONTH_RU = {
      nominative: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      genitive: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
    };

    const names = MONTH_RU[options?.case || 'genitive'];
    return names[this.wrapDate().get('month')];
  }

  isEqual(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isSame(date.getDate());
  }

  isAfter(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isAfter(date.getDate());
  }

  isAfterOrEqual(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isSameOrAfter(date.getDate());
  }

  isBefore(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isBefore(date.getDate());
  }

  isBeforeOrEqual(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isSameOrBefore(date.getDate());
  }

  toFormattedString(format?: string): string {
    if (!format) return this.wrapDate().format();
    return this.wrapDate().format(format);
  }

  getStartOfMonth(): this {
    this.date = this.wrapDate().startOf('month').toDate();
    return this;
  }

  getStartOfWeek(): this {
    this.date = this.wrapDate().startOf('week').toDate();
    return this;
  }

  getStartOfDay(): this {
    this.date = this.wrapDate().startOf('day').toDate();
    return this;
  }

  getEndOfMonth(): this {
    this.date = this.wrapDate().endOf('month').toDate();
    return this;
  }

  getEndOfWeek(): this {
    this.date = this.wrapDate().endOf('week').toDate();
    return this;
  }

  getEndOfDay(): this {
    this.date = this.wrapDate().endOf('day').toDate();
    return this;
  }

  getRangeTo(to: VmuzeyTimezonedDateHelper): VmuzeyTimezonedDateHelper[] {
    const interval: Interval = { start: this.getDate(), end: to.getDate() };
    return eachDayOfInterval(interval).map((dateInInterval) => new VmuzeyTimezonedDateHelper(dateInInterval, this.options));
  }

  subMonths(monthsCount: number): this {
    this.date = this.wrapDate().subtract(monthsCount, 'month').toDate();
    return this;
  }

  subDays(daysCount: number): this {
    this.date = this.wrapDate().subtract(daysCount, 'day').toDate();
    return this;
  }

  subHours(hours: number): this {
    this.date = this.wrapDate().subtract(hours, 'hour').toDate();
    return this;
  }

  subMinutes(minutes: number): this {
    this.date = this.wrapDate().subtract(minutes, 'minute').toDate();
    return this;
  }

  subSeconds(seconds: number): this {
    this.date = this.wrapDate().subtract(seconds, 'second').toDate();
    return this;
  }

  addMonths(monthsCount: number): this {
    this.date = this.wrapDate().add(monthsCount, 'month').toDate();
    return this;
  }

  addDays(daysCount: number): this {
    this.date = this.wrapDate().add(daysCount, 'day').toDate();
    return this;
  }

  addHours(hours: number): this {
    this.date = this.wrapDate().add(hours, 'hour').toDate();
    return this;
  }

  addMinutes(minutes: number): this {
    this.date = this.wrapDate().add(minutes, 'minute').toDate();
    return this;
  }

  addSeconds(seconds: number): this {
    this.date = this.wrapDate().add(seconds, 'second').toDate();
    return this;
  }

  addTime(time: string): this {
    const [hours, minutes] = time.split(':').map((t) => parseInt(t));
    this.date = this.addHours(hours).addMinutes(minutes).getDate();
    return this;
  }

  subTime(time: string): this {
    const [hours, minutes] = time.split(':').map((t) => parseInt(t));
    this.date = this.subHours(hours).subMinutes(minutes).getDate();
    return this;
  }

  countDaysTo(to: VmuzeyTimezonedDateHelper): number {
    return Math.abs(this.wrapDate().diff(to.getDate(), 'days'));
  }

  isWeekend(): boolean {
    const weekDay = this.getWeekdayOrder();
    return [6, 7].includes(weekDay);
  }

  getWeekDayName(): string {
    const days = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];

    return days[this.getWeekdayOrder() - 1];
  }

  isSameDay(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isSame(date.getDate(), 'day');
  }

  isSameMonth(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isSame(date.getDate(), 'month');
  }

  isSameYear(date: VmuzeyTimezonedDateHelper): boolean {
    return this.wrapDate().isSame(date.getDate(), 'year');
  }

  clone(): VmuzeyTimezonedDateHelper {
    return new VmuzeyTimezonedDateHelper(this.date.toString(), { timezone: this.options.timezone });
  }

  getTimezone(): ITimezone {
    return this.options.timezone;
  }

  private normalizeOriginDateOnInit(): void {
    if (this.substractTimezoneOffsetOnInit) {
      const timezoneOffset = VmuzeyTimezonedDateHelper.getHourOffsetByTimezone(this.options.timezone);
      this.date = this.wrapDate().subtract(timezoneOffset, 'hour').toDate();
    } else {
      this.date = this.wrapDate().toDate();
    }
  }

  private wrapDate(): moment.Moment {
    if (this.hasTimezoneOption) {
      if (typeof this.date === 'number') {
        return timezonedMoment.unix(this.date).tz(this.options.timezone);
      }

      return timezonedMoment(this.date).tz(this.options.timezone);
    }

    if (this.isOriginTimezonedString) {
      if (typeof this.date === 'number') {
        return timezonedMoment.unix(this.date);
      }

      return timezonedMoment(this.date);
    }

    if (typeof this.date === 'number') {
      return moment.unix(this.date);
    }

    return moment(this.date);
  }
}
