import { RequestPayload } from '../globals';
import { IVmuzeyLoggerProvider, VmuzeyHandlerTransportEnum } from '../vmuzey-logger-provider';

export class VmuzeyHttpLoggerMiddleware {
  constructor(private logger: IVmuzeyLoggerProvider) {}

  middleware(): any {
    return async (ctx: RequestPayload, next) => {
      await this.logger.info({
        requestId: ctx.headers['req-id'],
        handlerTransportType: VmuzeyHandlerTransportEnum.HTTP,
        httpMethod: ctx.method,
        httpUrl: ctx.url,
        payload: {
          headers: ctx.headers,
          query: ctx.request.query,
          params: ctx.params,
          body: ctx.request.body,
        },
      });

      await next();
    };
  }
}
