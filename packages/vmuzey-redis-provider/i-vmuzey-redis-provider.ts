import { IHandyRedis } from 'handy-redis';
import { IRedisUpdateOptions } from './interfaces';

export interface IVmuzeyRedisProvider {
  db: IHandyRedis;
  methods: {
    exists(key: string): Promise<boolean | string>;
    set(key: string, value: any, expiration?: number): Promise<void>;
    delete(...string: string[]): Promise<void>;
    get(key: string): Promise<any>;
    getCompositeKey(key: string): Promise<any>;
    deleteCompositeKey(key: string): Promise<void>;
    keys(key: string): Promise<any>;
    update(key: string, field: IRedisUpdateOptions): Promise<void>;
    findAllKeys(key: string): Promise<string[]>;
    findOne(key: string): Promise<any>;
    findAll(key: string): Promise<any[]>;
    findKeyFullByCompositeKey(compositeKey: string): Promise<string>;
  };
}
