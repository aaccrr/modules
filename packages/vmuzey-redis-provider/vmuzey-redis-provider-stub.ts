import { IHandyRedis } from 'handy-redis';
import { IVmuzeyRedisProvider } from './i-vmuzey-redis-provider';
import { IVmuzeyRedisProviderConfig } from './interfaces';
import { IRedisUpdateOptions } from './interfaces';

export class VmuzeyRedisProviderStub implements IVmuzeyRedisProvider {
  public db: IHandyRedis;

  constructor(config: IVmuzeyRedisProviderConfig) {}

  async init(): Promise<void> {}

  public methods = {
    set: async (key: string, value: any, expiration: number = null): Promise<void> => {},
    get: async (key: string): Promise<any> => {},
    keys: async (key): Promise<any> => {},
    findKeyFullByCompositeKey: async (key: string): Promise<string> => {
      return '';
    },
    getCompositeKey: async (key: string): Promise<any> => {},
    deleteCompositeKey: async (key: string): Promise<void> => {},
    delete: async (...keys): Promise<void> => {},
    exists: async (key: string): Promise<boolean> => {
      return true;
    },
    update: async (key: string, field: IRedisUpdateOptions): Promise<void> => {},
    findAllKeys: async (key: string): Promise<string[]> => {
      return [];
    },
    findOne: async (key: string): Promise<any> => {},
    findAll: async (key: string): Promise<any[]> => {
      return [];
    },
  };
}
