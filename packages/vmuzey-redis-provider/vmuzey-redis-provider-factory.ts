import { IVmuzeyRedisProviderConfig } from './interfaces';

import { IVmuzeyRedisProvider } from './i-vmuzey-redis-provider';
import { VmuzeyRedisProvider } from './vmuzey-redis-provider';
import { VmuzeyRedisProviderStub } from './vmuzey-redis-provider-stub';

export class VmuzeyRedisProviderFactory {
  static create(env: string, redisConfig: IVmuzeyRedisProviderConfig): IVmuzeyRedisProvider {
    if (env === 'test' || env === 'openapi') {
      return new VmuzeyRedisProviderStub(redisConfig);
    }

    return new VmuzeyRedisProvider(redisConfig);
  }
}
