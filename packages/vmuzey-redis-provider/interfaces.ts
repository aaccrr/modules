export interface IVmuzeyRedisProviderConfig {
  host: string;
  password: string;
}

export interface IRedisSetOptions {
  expiration?: number;
}

export interface IRedisUpdateOptions {
  name: string;
  action: 'set' | 'add' | 'delete';
  value: number | string;
}
