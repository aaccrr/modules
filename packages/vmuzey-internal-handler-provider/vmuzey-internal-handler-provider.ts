import { IVmuzeyServiceHandlerProvider } from '../vmuzey-service-handler-provider';
import { IVmuzeyInternalHandlerProvider } from './i-vmuzey-internal-handler-provider';
import { IVmuzeyInternalBrokerHandler, IVmuzeyInternalRpcHandler } from './interfaces';

export class VmuzeyInternalHandlerProvider implements IVmuzeyInternalHandlerProvider {
  constructor(private serviceHandler: IVmuzeyServiceHandlerProvider) {}

  getRpcHandler(): IVmuzeyInternalRpcHandler {
    return (req) => this.serviceHandler.execute(req);
  }

  getBrokerHandler(): IVmuzeyInternalBrokerHandler {
    return (message) => this.serviceHandler.execute(message);
  }
}
