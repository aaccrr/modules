export interface IVmuzeyInternalRpcHandler {
  (req: any): Promise<any>;
}

export interface IVmuzeyInternalBrokerHandler {
  (message: any): Promise<void>;
}
