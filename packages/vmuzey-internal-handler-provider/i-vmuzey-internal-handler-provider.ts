import { IVmuzeyInternalBrokerHandler, IVmuzeyInternalRpcHandler } from './interfaces';

export interface IVmuzeyInternalHandlerProvider {
  getBrokerHandler(): IVmuzeyInternalBrokerHandler;
  getRpcHandler(): IVmuzeyInternalRpcHandler;
}
