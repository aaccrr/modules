import { ICreateConsumerOptions, IBrokerConsumer, IBrokerProducer, IBroker } from './Interfaces';

import { Kafka } from 'kafkajs';
import { BrokerConsumer } from './consumer';
import { BrokerProducer } from './producer';

interface IBrokerConfig {
  brokerId: string;
  brokers: string[];
}

export class Broker implements IBroker {
  constructor(brokerConfig: IBrokerConfig) {
    this.broker = new Kafka({
      clientId: brokerConfig.brokerId,
      brokers: brokerConfig.brokers,
    });
  }

  private broker: Kafka;

  async createConsumer(options: ICreateConsumerOptions): Promise<IBrokerConsumer> {
    const consumer = new BrokerConsumer(this.broker);
    await consumer.connect(options);
    return consumer;
  }

  async createProducer(): Promise<IBrokerProducer> {
    const producer = new BrokerProducer(this.broker);
    await producer.connect();
    return producer;
  }
}
