export interface IBroker {
  createConsumer(options: ICreateConsumerOptions): Promise<IBrokerConsumer>;
  createProducer(): Promise<IBrokerProducer>;
}

export interface IBrokerConfig {
  host: string;
  port: number;
}

export interface IBrokerProducer {
  connect(): Promise<void>;
  send(payload: IProducerSendPayload): Promise<any>;
  sendBatch(messages: IProducerSendPayload[]): Promise<void>;
}

export interface IProducerSendPayload {
  topic: string;
  messages?: any[];
  message?: any;
}

export interface IBrokerCreateTopicsPayload {
  topic: string;
  partitions: number;
}

export interface IBrokerConsumer {
  onMessage(cb): void;
}

export interface IConsumerTopic {
  title: string;
  partition?: number;
}

export interface ICreateConsumerOptions {
  id: string;
  topics: IConsumerTopic[];
}

export interface ICreateProducerOptions {
  host: string;
}
