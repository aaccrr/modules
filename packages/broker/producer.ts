import { Kafka, Producer } from 'kafkajs';
import { IBrokerProducer, IProducerSendPayload } from './Interfaces';

export class BrokerProducer implements IBrokerProducer {
  constructor(broker: Kafka) {
    this.producer = broker.producer();
  }

  private producer: Producer;

  async connect(): Promise<void> {
    try {
      await this.producer.connect();
    } catch (e) {
      console.log('PRODUCER_CONNECT_ERROR ', e);
    }
  }

  async send(payload: IProducerSendPayload): Promise<any> {
    try {
      await this.producer.send({
        topic: payload.topic,
        messages: this.normalizeMessages(payload),
      });
    } catch (e) {
      console.log('PRODUCER_MESSAGE_SEND_ERROR ', e);
    }
  }

  async sendBatch(messages: IProducerSendPayload[]): Promise<void> {
    try {
      messages.forEach((message) => {
        message.messages = this.normalizeMessages(message);
      });

      await this.producer.sendBatch({ topicMessages: messages as any[] });
    } catch (e) {
      console.log('PRODUCER_MESSAGE_SEND_ERROR ', e);
    }
  }

  private normalizeMessages(payload: IProducerSendPayload): any[] {
    if (payload.message !== undefined) {
      return [this.createMessage(payload.message)];
    }

    return payload.messages.map((message) => this.createMessage(message));
  }

  private createMessage(rawMessage: any): any {
    return {
      value: JSON.stringify(rawMessage),
    };
  }
}
