import { Kafka, Consumer } from 'kafkajs';
import { IBrokerConsumer, ICreateConsumerOptions } from './Interfaces';

export class BrokerConsumer implements IBrokerConsumer {
  constructor(private broker: Kafka) {}

  private consumer: Consumer;

  async connect(options: ICreateConsumerOptions): Promise<void> {
    try {
      this.consumer = this.broker.consumer({ groupId: options.id });

      await this.consumer.connect();

      for (const topic of options.topics) {
        await this.consumer.subscribe({ topic: topic.title });
      }
    } catch (e) {
      console.log('CONSUMER_CONNECT_ERROR ', e);
      process.exit(0);
    }
  }

  async onMessage(cb): Promise<void> {
    await this.consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        cb(topic, {
          body: JSON.parse(message.value.toString()),
          meta: { topic, offset: message.offset, partition },
        });
      },
    });
  }
}
