import { IProjection } from "../interfaces";

export class PostProcessing {
  public deleteId<T>(obj: T): T {
    if (!obj) return obj;

    const deleter = data => {
      if (data._id) delete data._id;
      return data;
    };

    return !Array.isArray(obj) ? deleter(obj) : obj.map(deleter);
  }

  public projection<T>(data: T, projection: IProjection): T {
    if (!data || !projection) return data;

    return Array.isArray(data)
      ? this.projectionMany<T>(data, projection)
      : this.projectionOne<T>(data, projection);
  }

  private projectionOne<T>(data: T, projection: IProjection): T {
    const projected = Object.keys(projection).reduce((acc, key, index, arr) => {
      if (projection[key] === 1 && data[key]) acc[key] = data[key];
      return acc;
    }, {});

    return projected as any;
  }

  private projectionMany<T>(data: T, projection: IProjection): T {
    return (data as any).map(item => this.projectionOne(item, projection));
  }
}
